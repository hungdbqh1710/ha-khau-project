let mix = require('laravel-mix');

const CleanWebpackPlugin = require('clean-webpack-plugin');

// paths to clean
var pathsToClean = [
  './assets/app/js',
  './assets/app/css',
  './assets/admin/js',
  './assets/admin/css',
  './assets/auth/css'
];

// the clean options to use
var cleanOptions = {};

mix.webpackConfig({
  plugins: [new CleanWebpackPlugin(pathsToClean, cleanOptions)]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Core
 |--------------------------------------------------------------------------
 |
 */

mix
  .scripts(
    [
      'node_modules/jquery/dist/jquery.js',
    ],
    './assets/app/js/app.js'
  )
  .version();

mix
  .styles(
    [
      'node_modules/font-awesome/css/font-awesome.css',
        'resources/assets/css/custom.css'

    ],
    './assets/app/css/app.css'
  )
  .version();

mix.copy(['node_modules/font-awesome/fonts/'], './assets/app/fonts');

/*
 |--------------------------------------------------------------------------
 | Auth
 |--------------------------------------------------------------------------
 |
 */

mix
  .styles(
    'resources/assets/auth/css/login.css',
    './assets/auth/css/login.css'
  )
  .version();
mix
  .styles(
    'resources/assets/auth/css/register.css',
    './assets/auth/css/register.css'
  )
  .version();
mix
  .styles(
    'resources/assets/auth/css/passwords.css',
    './assets/auth/css/passwords.css'
  )
  .version();

mix
  .styles(
    [
      'node_modules/bootstrap/dist/css/bootstrap.css',
      'node_modules/gentelella/vendors/animate.css/animate.css',
      'node_modules/gentelella/build/css/custom.css'
    ],
    './assets/auth/css/auth.css'
  )
  .version();

mix.styles([
    'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css',
    './vendor/bootstrap-datepicker/bootstrap-datepicker.css',
    'node_modules/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
], './assets/admin/css/revenue/revenue.css').version();
/*
 |--------------------------------------------------------------------------
 | Admin
 |--------------------------------------------------------------------------
 |
 */

mix
  .scripts(
    [
      'node_modules/bootstrap/dist/js/bootstrap.js',
      'resources/assets/js/admin/custom.js',
      './vendor/datatables/datatables.min.js',
      './vendor/datatables/padfmake.min.js',
      './vendor/datatables/vfs_fonts.js',
      './vendor/datatables/buttons.server-side.js',
      './vendor/editor/js/dataTables.editor.js',
      './vendor/editor/js/editor.bootstrap.min.js',
      './vendor/editor/js/editor.select2.js'
    ],
    './assets/admin/js/admin.js'
  )
  .version();

mix
  .styles(
    [
      'node_modules/bootstrap/dist/css/bootstrap.css',
      'node_modules/gentelella/vendors/animate.css/animate.css',
      'node_modules/gentelella/build/css/custom.css',
      './vendor/editor/css/dataTables.editor.css',
      './vendor/editor/css/editor.bootstrap.css',
      './vendor/datatables/datatables.min.css',
      'node_modules/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css',
      'resources/assets/css/custom.css'
    ],
    './assets/admin/css/admin.css'
  )
  .version();

mix.copy(
  ['node_modules/gentelella/vendors/bootstrap/dist/fonts'],
  './assets/admin/fonts'
);

mix
  .babel(
    [
      'node_modules/select2/dist/js/select2.full.js',
      'resources/assets/js/admin/users.js',
    ],
    './assets/admin/js/users/users.js'
  )
  .version();

mix
  .styles(
    ['node_modules/select2/dist/css/select2.css'],
    './assets/admin/css/users/users.css'
  )
  .version();

mix
  .babel(
    [
      'node_modules/gentelella/vendors/Flot/jquery.flot.js',
      'node_modules/gentelella/vendors/Flot/jquery.flot.time.js',
      'node_modules/gentelella/vendors/Flot/jquery.flot.pie.js',
      'node_modules/gentelella/vendors/Flot/jquery.flot.stack.js',
      'node_modules/gentelella/vendors/Flot/jquery.flot.resize.js',

      'node_modules/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js',
      'node_modules/gentelella/vendors/DateJS/build/date.js',
      'node_modules/gentelella/vendors/flot.curvedlines/curvedLines.js',
      'node_modules/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js',

      'node_modules/gentelella/production/js/moment/moment.min.js',
      'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js',

      'node_modules/gentelella/vendors/Chart.js/dist/Chart.js',
      'node_modules/jcarousel/dist/jquery.jcarousel.min.js',
      'node_modules/gentelella/vendors/iCheck/icheck.min.js',
    ],
    './assets/admin/js/dashboard.js'
  )
  .version();

mix
  .styles(
    [
      'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css',
      'resources/assets/admin/css/dashboard.css'
    ],
    './assets/admin/css/dashboard.css'
  )
  .version();

mix.babel([
    'resources/assets/js/admin/exchange-rate.js',
    'resources/assets/js/admin/jquery.priceformat.js',
], './assets/admin/js/exchange-rate.js').version();

mix.babel([
  'node_modules/select2/dist/js/select2.full.js',
    'resources/assets/js/admin/jquery.priceformat.js',
  'resources/assets/js/admin/purchase_orders.js'
], './assets/admin/js/purchase_orders.js').version();

mix.styles([
    'node_modules/select2/dist/css/select2.css',
    'node_modules/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css',
    './vendor/datatables/rowGroup.dataTables.min.css'
], './assets/admin/css/orders/purchase_orders.css').version();

mix.babel([
    'node_modules/select2/dist/js/select2.full.js',
    'resources/assets/js/admin/order_details.js'
], './assets/admin/js/order_details.js').version();

mix.babel([
    'resources/assets/js/admin/customer.js',
    'resources/assets/js/admin/jquery.priceformat.js',
], './assets/admin/js/customer.js').version();

mix.babel([
    'resources/assets/js/admin/feedback.js',
], './assets/admin/js/feedback.js').version();

mix.babel([
    'resources/assets/js/admin/slide.js',
], './assets/admin/js/slide.js').version();

mix.babel([
    'resources/assets/js/admin/permissions.js',
], './assets/admin/js/permissions.js').version();

mix.babel([
    './vendor/datatables/handlebars.js',
    'resources/assets/js/admin/roles.js',
], './assets/admin/js/roles.js').version();

mix.babel([
    'resources/assets/js/admin/detail_rows.js',
], './assets/admin/js/detail_rows.js').version();

mix.styles([
    'node_modules/select2/dist/css/select2.css',
], './assets/admin/css/roles/roles.css').version();

mix.babel([
    'resources/assets/js/admin/blog.js',
], './assets/admin/js/blog.js').version();

mix.babel([
    'resources/assets/js/admin/ckeditor.js',
], './assets/admin/js/ckeditor.js').version();


mix.babel([
    'resources/assets/js/admin/website_configs.js',
], './assets/admin/js/website_configs.js').version();

mix.babel([
    'resources/assets/js/admin/cms-page.js',
], './assets/admin/js/cms-page.js').version();

mix.babel([
    'resources/assets/js/admin/service_fees.js',
], './assets/admin/js/service_fees.js').version();

mix.babel([
    'resources/assets/js/admin/transport_fees.js',
], './assets/admin/js/transport_fees.js').version();

mix.babel([
    'resources/assets/js/admin/count_fees.js',
], './assets/admin/js/count_fees.js').version();

mix.babel([
    'resources/assets/js/admin/wood_fees.js',
], './assets/admin/js/wood_fees.js').version();

mix.babel([
    'resources/assets/js/admin/customer_level.js',
], './assets/admin/js/customer_level.js').version();

mix.babel([
    './vendor/datatables/handlebars.js',
    'node_modules/select2/dist/js/select2.full.js',
    'resources/assets/js/admin/jquery.priceformat.js',
    'resources/assets/js/admin/transport_orders.js',
], './assets/admin/js/transport_orders.js').version();

mix.styles([
    'node_modules/select2/dist/css/select2.css',
], './assets/admin/css/transport_orders/transport_orders.css').version();

mix.babel([
    'resources/assets/js/admin/shipping_order_details.js',
], './assets/admin/js/shipping_order_details.js').version();

mix.babel([
    'node_modules/gentelella/vendors/moment/min/moment.min.js',
    'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js',
    './vendor/bootstrap-datepicker/bootstrap-datepicker.js',
    'node_modules/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    './vendor/datatables/dataTables.rowGroup.min.js',
    'resources/assets/js/admin/reports_debt.js',
], './assets/admin/js/reports_debt.js').version();

mix.styles([
    'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css',
    './vendor/bootstrap-datepicker/bootstrap-datepicker.css',
    'node_modules/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
], './assets/admin/css/reports_debt/reports_debt.css').version();

mix.babel([
    'node_modules/gentelella/vendors/moment/min/moment.min.js',
    'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js',
    './vendor/bootstrap-datepicker/bootstrap-datepicker.js',
    'node_modules/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    'resources/assets/js/admin/revenue.js',
], './assets/admin/js/revenue.js').version();
/*
 |--------------------------------------------------------------------------
 | Frontend
 |--------------------------------------------------------------------------
 |
 */

mix
  .js('resources/assets/js/app.js', './js')
  .sass('resources/assets/sass/app.scss', './css');
