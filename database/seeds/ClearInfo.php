<?php

use Illuminate\Database\Seeder;

class ClearInfo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Customer::truncate();
        \App\Models\Order::truncate();
        \App\Models\PurchaseOrderItem::truncate();
        \App\Models\TransportOrderItem::truncate();
        \App\Models\CustomerProfile::truncate();
        \App\Models\OrderAddress::truncate();
        \App\Models\DeliveryAddress::truncate();
    }
}