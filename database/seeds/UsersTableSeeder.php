<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where([
            'name'              => 'Admin',
            'email'             => 'admin@gmail.com',
        ])->first();
        if (empty($user)) {
            $user = User::create([
                'name'              => 'Admin',
                'email'             => 'admin@gmail.com',
                'phone_number'      => '0123456789',
                'password'          => bcrypt('123456')
            ]);
        }
        $adminRole = Role::firstOrCreate([
           'name' => 'admin',
           'display_name' => 'admin'
        ]);
        $adminRole->syncPermissions(Permission::all());
        $user->syncRoles($adminRole);
    }
}
