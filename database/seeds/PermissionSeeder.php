<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement("SET foreign_key_checks=0");
        \DB::table('role_has_permissions')->truncate();
        \DB::table('model_has_permissions')->truncate();
        \DB::table('model_has_roles')->truncate();
        Permission::truncate();
        Role::truncate();
        \DB::statement("SET foreign_key_checks=1");
        $permissions = static::getDefaultPermission();
        foreach($permissions as $permission ) {
            Permission::create($permission);
        }
    }

    public static function getDefaultPermission()
    {
        return [
            ['name' => 'create_users', 'display_name' => 'Thêm mới người dùng'],
            ['name' => 'edit_users', 'display_name' => 'Sửa thông tin người dùng'],
            ['name' => 'delete_users', 'display_name' => 'Xóa người dùng'],
            ['name' => 'create_exchange_rates', 'display_name' => 'Thêm mới tỷ giá'],
            ['name' => 'edit_exchange_rates', 'display_name' => 'Sửa tỷ giá'],
            ['name' => 'delete_exchange_rates', 'display_name' => 'Xóa tỷ giá'],
            ['name' => 'create_slides', 'display_name' => 'Thêm mới slide'],
            ['name' => 'edit_slides', 'display_name' => 'Sửa slide'],
            ['name' => 'delete_slides', 'display_name' => 'Xóa slide'],
            ['name' => 'edit_orders', 'display_name' => 'Sửa đơn hàng'],
            ['name' => 'view_all_purchase_orders', 'display_name' => 'Xem tất cả đơn hàng mua hộ'],
            ['name' => 'view_all_transport_orders', 'display_name' => 'Xem tất cả đơn hàng vận chuyển'],
            ['name' => 'view_customer_info', 'display_name' => 'Xem thông tin khách hàng'],
            ['name' => 'create_customers', 'display_name' => 'Thêm khách hàng'],
            ['name' => 'view_feedback', 'display_name' => 'Xem hòm thư góp ý'],
            ['name' => 'create_blogs', 'display_name' => 'Thêm mới bài viết'],
            ['name' => 'edit_blogs', 'display_name' => 'Sửa bài viết'],
            ['name' => 'delete_blogs', 'display_name' => 'Xóa bài viết'],
            ['name' => 'create_roles', 'display_name' => 'Thêm mới vai trò'],
            ['name' => 'edit_roles', 'display_name' => 'Sửa vai trò'],
            ['name' => 'delete_roles', 'display_name' => 'Xóa vai trò'],
            ['name' => 'edit_website_configs', 'display_name' => 'Sửa cấu hình website'],
            ['name' => 'create_pages', 'display_name' => 'Thêm page'],
            ['name' => 'edit_pages', 'display_name' => 'Sửa page'],
            ['name' => 'delete_pages', 'display_name' => 'Xóa page'],
            ['name' => 'view_reports', 'display_name' => 'Xem báo cáo'],
        ];
    }
}
