<?php

use App\Models\WebsiteConfig;
use Illuminate\Database\Seeder;

class WebsiteConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WebsiteConfig::truncate();
        $configs = WebsiteConfigSeeder::getDefaultConfig();
        foreach ($configs as $config) {
            WebsiteConfig::firstOrCreate($config);
        }
    }

    public static function getDefaultConfig()
    {
        return [
            [
                'name'         => 'hotline',
                'display_name' => 'Đường dây nóng',
                'content'      => '123456789'
            ],
            [
                'name'         => 'facebook_url',
                'display_name' => 'Đường dẫn facebook',
                'content'      => 'https://facebook.com'
            ],
            [
                'name'         => 'email',
                'display_name' => 'Địa chỉ email',
                'content'      => 'Email@example.com'
            ],
            [
                'name'         => 'payment_cash',
                'display_name' => 'Thanh toán tiền mặt',
                'content'      => '190 Nhạc Sơn - Kim Tân - TP Lào Cai'
            ],
            [
                'name'         => 'payment_transfer',
                'display_name' => 'Thanh toán chuyển khoản',
                'content'      => 'Ngân hàng: Vietcombank. Chủ tài khoản: Nguyễn Văn Anh. Thông tin tài khoản: 3215487984 - Chi nhánh Thanh Xuân - Hà Nội'
            ],
            [
                'name'         => 'popup_image',
                'display_name' => 'Popup Quảng cáo',
                'content'      => ''
            ]
        ];
    }
}
