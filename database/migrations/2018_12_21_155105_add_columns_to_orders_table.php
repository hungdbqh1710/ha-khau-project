<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->float('cn_transport_fee', 11)->nullable();
            $table->float('vn_transport_fee', 11)->nullable();
            $table->string('cn_transport_code')->nullable();
            $table->string('vn_transport_code')->nullable();
            $table->text('customer_note')->nullable();
            $table->dateTime('cn_order_date')->nullable();
            $table->text('receiver_info')->nullable();
            $table->text('link_qty')->nullable();
            $table->float('via_border_transport_fee', 11)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('cn_transport_fee', 'vn_transport_fee', 'cn_transport_code', 'vn_transport_code', 'customer_note', 'cn_order_date', 'link_qty', 'via_border_transport_fee');
        });
    }
}
