<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ship_code');
            $table->string('package_code')->nullable();;
            $table->string('buy_code')->nullable();;
            $table->string('status')->default('in-cn');
            $table->string('size')->nullable();
            $table->float('weight', 12)->nullable();
            $table->float('price', 12)->nullable();;
            $table->float('cn_transport_fee', 11)->nullable();
            $table->float('exchange_rates', 11)->unsigned()->nullable();
            $table->float('total_price', 14)->nullable();;
            $table->text('admin_note')->nullable();
            $table->unsignedInteger('customer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping');
    }
}
