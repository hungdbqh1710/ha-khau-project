<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->float('cn_transport_fee', 11)->nullable();
            $table->float('vn_transport_fee', 11)->nullable();
            $table->string('cn_transport_code')->nullable();
            $table->string('vn_transport_code')->nullable();
            $table->text('customer_note')->nullable();
            $table->text('admin_note')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->dateTime('cn_order_date');
            $table->text('receiver_info');
            $table->text('link_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_order_items');
    }
}
