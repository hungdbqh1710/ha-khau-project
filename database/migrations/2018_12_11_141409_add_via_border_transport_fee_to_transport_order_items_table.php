<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViaBorderTransportFeeToTransportOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transport_order_items', function (Blueprint $table) {
            $table->float('via_border_transport_fee', 11)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transport_order_items', function (Blueprint $table) {
            $table->dropColumn('via_border_transport_fee');
        });
    }
}
