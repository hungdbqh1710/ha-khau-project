<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('supporter_id')->nullable();
            $table->tinyInteger('transport_receive_type')->nullable();
            $table->string('purchase_cn_transport_code')->nullable();
            $table->string('purchase_vn_transport_code')->nullable();
            $table->float('purchase_total_items_price', 14)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->dateTime('confirm_date')->nullable();
            $table->float('purchase_service_fee', 11)->nullable();
            $table->float('purchase_cn_transport_fee', 11)->nullable();
            $table->float('purchase_vn_transport_fee', 11)->nullable();
            $table->float('purchase_returns_fee', 11)->nullable();
            $table->float('purchase_count_fee', 11)->nullable();
            $table->float('purchase_wood_fee', 11)->nullable();
            $table->float('final_total_price', 15)->nullable();
            $table->float('min_deposit', 5, 2)->nullable();
            $table->float('deposited', 15)->nullable();
            $table->text('admin_note')->nullable();
            $table->tinyInteger('order_type')->comment('1: Mua hộ, 2: Vận chuyển');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
