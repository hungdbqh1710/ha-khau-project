<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_image');
            $table->string('product_name');
            $table->text('product_link');
            $table->string('product_id')->nullable();
            $table->text('property')->nullable();
            $table->integer('qty');
            $table->float('price', 14);
            $table->text('customer_note')->nullable();
            $table->text('admin_note')->nullable();
            $table->text('price_range')->nullable();
            $table->string('cn_transport_code')->nullable();
            $table->string('cn_order_number')->nullable();
            $table->string('status')->default(1);
            $table->unsignedInteger('order_group_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
