<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_image');
            $table->text('product_link');
            $table->string('product_name');
            $table->text('property')->nullable();
            $table->integer('qty');
            $table->float('price', 12);
            $table->float('total_price', 14);
            $table->text('price_range')->nullable();
            $table->string('item_id')->nullable();
            $table->text('shop_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_temps');
    }
}
