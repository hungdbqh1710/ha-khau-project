<?php

return [
    'button_delete'     => 'Xóa',
    'button_save'       => 'Lưu',
    'button_close'      => 'Đóng',
    'button_add'        => 'Thêm mới',
    'button_edit'       => 'Sửa',
    'button_send_order' => 'Gửi đơn hàng',
    'button_pay'        => 'Thanh toán',
    'button_back'       => 'Quay lại',
    'button_cancel'     => 'Hủy',
    'button_confirm'    => 'Đồng ý',
];