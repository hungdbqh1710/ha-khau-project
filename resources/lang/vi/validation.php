<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted'             => 'Trường :attribute phải được chấp nhận.',
    'active_url'           => 'Trường :attribute không phải là một URL hợp lệ.',
    'after'                => 'Trường :attribute phải là một ngày sau ngày :date.',
    'after_or_equal'       => 'Trường :attribute phải là thời gian bắt đầu sau hoặc đúng bằng :date.',
    'alpha'                => 'Trường :attribute chỉ có thể chứa các chữ cái.',
    'alpha_dash'           => 'Trường :attribute chỉ có thể chứa chữ cái, số và dấu gạch ngang.',
    'alpha_num'            => 'Trường :attribute chỉ có thể chứa chữ cái và số.',
    'array'                => 'Trường :attribute phải là dạng mảng.',
    'before'               => 'Trường :attribute phải là một ngày trước ngày :date.',
    'before_or_equal'      => 'Trường :attribute phải là thời gian bắt đầu trước hoặc đúng bằng :date.',
    'between'              => [
        'numeric' => 'Trường :attribute phải nằm trong khoảng :min - :max.',
        'file'    => 'Dung lượng tập tin trong trường :attribute phải từ :min - :max kB.',
        'string'  => 'Trường :attribute phải từ :min - :max ký tự.',
        'array'   => 'Trường :attribute phải có từ :min - :max phần tử.',
    ],
    'boolean'              => 'Trường :attribute phải là true hoặc false.',
    'confirmed'            => 'Giá trị xác nhận trong trường :attribute không khớp.',
    'date'                 => 'Trường :attribute không phải là định dạng của ngày-tháng.',
    'date_format'          => 'Trường :attribute không giống với định dạng :format.',
    'different'            => 'Trường :attribute và :other phải khác nhau.',
    'digits'               => 'Độ dài của trường :attribute phải gồm :digits chữ số.',
    'digits_between'       => 'Độ dài của trường :attribute phải nằm trong khoảng từ :min đến :max chữ số.',
    'dimensions'           => 'Trường :attribute có kích thước không hợp lệ.',
    'distinct'             => 'Trường :attribute có giá trị trùng lặp.',
    'email'                => 'Trường :attribute phải là một địa chỉ email hợp lệ.',
    'exists'               => 'Giá trị đã chọn trong trường :attribute không hợp lệ.',
    'file'                 => 'Trường :attribute phải là một tệp tin.',
    'filled'               => 'Trường :attribute không được bỏ trống.',
    'gt'                   => [
        'numeric' => 'Giá trị trường :attribute phải lớn hơn :value.',
        'file'    => 'Dung lượng trường :attribute phải lớn hơn :value kilobytes.',
        'string'  => 'Độ dài trường :attribute phải nhiều hơn :value kí tự.',
        'array'   => 'Mảng :attribute phải có nhiều hơn :value phần tử.',
    ],
    'gte'                  => [
        'numeric' => 'Giá trị trường :attribute phải lớn hơn hoặc bằng :value.',
        'file'    => 'Dung lượng trường :attribute phải lớn hơn hoặc bằng :value kilobytes.',
        'string'  => 'Độ dài trường :attribute phải lớn hơn hoặc bằng :value kí tự.',
        'array'   => 'Mảng :attribute phải có ít nhất :value phần tử.',
    ],
    'image'                => 'Trường :attribute phải là định dạng hình ảnh.',
    'in'                   => 'Giá trị đã chọn trong trường :attribute không hợp lệ.',
    'in_array'             => 'Trường :attribute phải thuộc tập cho phép: :other.',
    'integer'              => 'Trường :attribute phải là một số nguyên.',
    'ip'                   => 'Trường :attribute phải là một địa chỉ IP.',
    'ipv4'                 => 'Trường :attribute phải là một địa chỉ IPv4.',
    'ipv6'                 => 'Trường :attribute phải là một địa chỉ IPv6.',
    'json'                 => 'Trường :attribute phải là một chuỗi JSON.',
    'lt'                   => [
        'numeric' => 'Giá trị trường :attribute phải nhỏ hơn :value.',
        'file'    => 'Dung lượng trường :attribute phải nhỏ hơn :value kilobytes.',
        'string'  => 'Độ dài trường :attribute phải nhỏ hơn :value kí tự.',
        'array'   => 'Mảng :attribute phải có ít hơn :value phần tử.',
    ],
    'lte'                  => [
        'numeric' => 'Giá trị trường :attribute phải nhỏ hơn hoặc bằng :value.',
        'file'    => 'Dung lượng trường :attribute phải nhỏ hơn hoặc bằng :value kilobytes.',
        'string'  => 'Độ dài trường :attribute phải nhỏ hơn hoặc bằng :value kí tự.',
        'array'   => 'Mảng :attribute không được có nhiều hơn :value phần tử.',
    ],
    'max'                  => [
        'numeric' => 'Trường :attribute không được lớn hơn :max.',
        'file'    => 'Dung lượng tập tin trong trường :attribute không được lớn hơn :max kB.',
        'string'  => 'Trường :attribute không được lớn hơn :max ký tự.',
        'array'   => 'Trường :attribute không được lớn hơn :max phần tử.',
    ],
    'mimes'                => 'Trường :attribute phải là một tập tin có định dạng: :values.',
    'mimetypes'            => 'Trường :attribute phải là một tập tin có định dạng: :values.',
    'min'                  => [
        'numeric' => 'Trường :attribute phải tối thiểu là :min.',
        'file'    => 'Dung lượng tập tin trong trường :attribute phải tối thiểu :min kB.',
        'string'  => 'Trường :attribute phải có tối thiểu :min ký tự.',
        'array'   => 'Trường :attribute phải có tối thiểu :min phần tử.',
    ],
    'not_in'               => 'Giá trị đã chọn trong trường :attribute không hợp lệ.',
    'not_regex'            => 'Trường :attribute có định dạng không hợp lệ.',
    'numeric'              => 'Trường :attribute phải là một số.',
    'present'              => 'Trường :attribute phải được cung cấp.',
    'regex'                => 'Trường :attribute có định dạng không hợp lệ.',
    'required'             => 'Trường :attribute không được bỏ trống.',
    'required_if'          => 'Trường :attribute không được bỏ trống khi trường :other là :value.',
    'required_unless'      => 'Trường :attribute không được bỏ trống trừ khi :other là :values.',
    'required_with'        => 'Trường :attribute không được bỏ trống khi một trong :values có giá trị.',
    'required_with_all'    => 'Trường :attribute không được bỏ trống khi tất cả :values có giá trị.',
    'required_without'     => 'Trường :attribute không được bỏ trống khi trường :values không có giá trị.',
    'required_without_all' => 'Trường :attribute không được bỏ trống khi tất cả :values không có giá trị.',
    'same'                 => 'Trường :attribute và :other phải giống nhau.',
    'size'                 => [
        'numeric' => 'Trường :attribute phải bằng :size.',
        'file'    => 'Dung lượng tập tin trong trường :attribute phải bằng :size kB.',
        'string'  => 'Trường :attribute phải chứa :size ký tự.',
        'array'   => 'Trường :attribute phải chứa :size phần tử.',
    ],
    'string'               => 'Trường :attribute phải là một chuỗi ký tự.',
    'timezone'             => 'Trường :attribute phải là một múi giờ hợp lệ.',
    'unique'               => 'Trường :attribute đã có trong cơ sở dữ liệu.',
    'uploaded'             => 'Trường :attribute tải lên thất bại.',
    'url'                  => 'Trường :attribute không giống với định dạng một URL.',
    'empty_html'       => 'Trường :attribute chứa ký tự không hợp lệ',
    'password'             => 'Trường :attribute phải có độ dài nhỏ nhất 8 ký tự, bao gồm chữ hoa, chữ thường, ký tự đặc biệt, số và không được có khoảng trống',
    "greater_than_field"   => 'Trường :attribute phải có giá trị lớn hơn trường :field',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name'                       => 'tên',
        'phone_number'               => 'số điện thoại',
        'is_active'                  => 'trạng thái',
        'password'                   => 'mật khẩu',
        'user_id'                    => 'nhân viên chăm sóc',
        'link'                       => 'link sản phẩm',
        'position'                   => 'vị trí',
        'src'                        => 'file ảnh',
        'vnd'                        => 'VNĐ',
        'ndt'                        => 'NDT',
        'display_name'               => 'tên',
        'title'                      => 'tiêu đề',
        'content'                    => 'nội dung',
        'image'                      => 'ảnh',
        'slug'                       => 'đường dẫn tĩnh',
        'note'                       => 'Ghi chú',
        'purchase_cn_transport_fee'  => 'phí ship nội địa TQ',
        'purchase_vn_transport_fee'  => 'phí ship nội địa VN',
        'desposit'                   => 'đặt cọc',
        'bank_account'               => 'số tài khoản ngân hàng',
        'purchase_service_fee'       => 'phí dịch vụ',
        'total_price'                => 'tổng thanh toán',
        'transport_code'             => 'mã vận đơn',
        'from_price'                 => 'từ giá',
        'to_price'                   => 'đến giá',
        'from_weight'                => 'từ cân',
        'to_weight'                  => 'đến cân',
        'fee'                        => 'phí',
        'hn_fee'                     => 'phí về HN',
        'hcm_fee'                    => 'phí về HCM',
        'from_product'               => 'từ sản phẩm',
        'to_product'                 => 'đến sản phẩm',
        'price_greater_10_fee'       => 'phí (SP > 10 tệ)',
        'price_less_10_fee'          => 'phí (SP < 10 tệ)',
        'first_kg_fee'               => 'kg đầu tiên',
        'next_kg_fee'                => 'kg tiếp theo',
        'level'                      => 'cấp độ thành viên',
        'spent_money_from'           => 'tổng tiền từ',
        'spent_money_to'             => 'tổng tiền đến',
        'purchase_discount'          => 'chiết khấu phí mua hàng',
        'transport_discount'         => 'chiết khấu phí vận chuyển',
        'deposit'                    => 'đặt cọc tối thiểu',
        'china_order_code'           => 'mã đặt hàng TQ',
        'supporter_id'               => 'người hỗ trợ',
        'min_deposit'                => 'đặt cọc tối thiểu',
        'cn_transport_fee'           => 'phí vận chuyển ở TQ',
        'admin_note'                 => 'ghi chú của Hà Khẩu',
        'purchase_count_fee'         => 'phí kiểm đếm',
        'purchase_wood_fee'          => 'phí đóng gỗ',
        'deposited'                  => 'đã đặt cọc',
        'cn_order_number'            => 'mã mua hàng TQ',
        'cn_transport_code'          => 'mã vận chuyển TQ',
        'purchase_cn_transport_code' => 'mã vận chuyển TQ',
        'purchase_vn_transport_code' => 'mã vận chuyển VN',
        'purchase_cn_to_vn_fee'      => 'phí chuyển về VN',
        'purchase_returns_fee'       => 'phí trả hàng',
        'via_border_transport_fee'   => 'phí vận chuyển từ TQ về VN',
        'vn_transport_fee'           => 'số tiền thanh toán hộ',
        'amount_topup'               => 'tiền khách chuyển khoản',
    ],
];
