<?php

return [
    'title_header_revenue' => [
        0 => 'Tiền bán hàng',
        1 => 'Số đơn hàng',
    ],
    'title_order_revenue' => [
        0 => 'Mã đơn hàng',
        1 => 'Ngày cập nhật',
        2 => 'Tên khách hàng',
        3 => 'Tổng tiền',
    ],
    'title_month' => 'Tháng :month',
    'title_employee' => 'Nhân viên :employee',
    'title_date_range' => 'Từ ngày :startDate đến ngày :endDate',
    'revenue_file_name' => 'Doanh thu ',
];