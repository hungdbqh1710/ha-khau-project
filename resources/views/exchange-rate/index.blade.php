@extends('layouts.admin')

@section('title', trans('page-name.page_exchange_rate'))

@section('content')
    {!! $dataTable->table(['id' => 'exchange-rate-table']) !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/exchange-rate.js')) }}

    {!!  $dataTable->scripts() !!}
@endsection