<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('frontend/assets/images/fevicon.png') }}" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">

    {{--Common App Styles--}}
    {{ Html::style(mix('assets/app/css/app.css')) }}
    {{ Html::style(mix('assets/admin/css/admin.css')) }}

    <title>Lỗi! | </title>

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">
                    <h1 class="error-number">403</h1>
                    <h2>Không có quyền truy cập</h2>
                    <p>Bạn không có quyền để truy cập trang này !</a>
                    </p>
                    <div class="mid_center">
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Về trang chủ</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

{{ Html::script(mix('assets/app/js/app.js')) }}
{{ Html::script(mix('assets/admin/js/admin.js')) }}
</body>
</html>