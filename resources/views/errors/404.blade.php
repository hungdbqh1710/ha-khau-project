@extends('layouts.frontend')

@section('page')
    <!-- Error 404 Start -->
    <div class="wshipping-content-block" id="maincontent">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10 offset-lg-1">
                    <div class="error-section">
                        <img src="{{ asset('frontend/assets/images/404.png') }}" alt=""/>
                        <h2>Page not found</h2>
                        <a href="{{ route('home-page') }}" class="wshipping-button cta-btn">Về trang chủ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Error 404 end -->
@endsection
