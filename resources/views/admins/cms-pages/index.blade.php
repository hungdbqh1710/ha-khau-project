@extends('layouts.admin')

@section('title', trans('page-name.page_cms_page'))

@section('content')
    {!! $dataTable->table(['id' => 'cms-page-table']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/cms-page.js')) }}
    {!! $dataTable->scripts() !!}
@endsection