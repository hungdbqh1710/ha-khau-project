@extends('layouts.admin')

@section('title', trans('page-name.page_cms_page_update'))

@section('content')
    <div class="row">
        <form action="{{ route('admin.cms-pages.update', $cmsPage->id) }}" method="POST"  enctype="multipart/form-data" class="form form-action">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-group col-lg-3">
                    <label>@lang('form.title_name_page') <span class="text-danger">*</span></label>
                    <input
                    name="name"
                    class="form-control"
                    value="{{ $cmsPage->name }}"
                    placeholder="@lang('form.placeholder_page_name')"
                    required>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-2">
                    <label>@lang('form.title_slug') <span class="text-danger">*</span></label>
                    <input
                    name="slug"
                    class="form-control"
                    value="{{ $cmsPage->slug }}"
                    placeholder="@lang('form.placeholder_slug')"
                    required>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('slug') }}
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-2">
                    <label>@lang('form.title_active') <span class="text-danger">*</span></label>
                    <select name="is_active" class="form-control">
                        @foreach(config('data.is_active') as $key => $value)
                            <option value="{{ $key }}" {{ $key == $cmsPage->is_active ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('is_active') }}
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-5">
                    <label>@lang('form.title') <span class="text-danger">*</span></label>
                    <input
                    name="title"
                    class="form-control"
                    value="{{ $cmsPage->title }}"
                    placeholder="@lang('form.placeholder_title')"
                    required>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('title') }}
                        </span>
                    @endif
                </div>
                <div class="form-group col-lg-12">
                    <label>@lang('form.title_content') <span class="text-danger">*</span></label>
                    <textarea
                            name="content"
                            id="js-content-blog"
                            class="form-control"
                            required>{!! $cmsPage->content !!}</textarea>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('content') }}
                        </span>
                    @endif
                </div>
                <input type="hidden" name="id" value="{{ $cmsPage->id }}">
                <div class="col-lg-12">
                    <a href="{{ route('admin.cms-pages.index') }}" class="btn btn-default pull-right">
                        @lang('buttons.button_back')
                    </a>
                    <button type="submit" class="btn btn-success pull-right">
                        @lang('buttons.button_save')
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    @parent
    {{ Html::script(asset('vendor/ckeditor/ckeditor.js')) }}
    {{ Html::script(asset('vendor/ckeditor/adapters/jquery.js')) }}
    {{ Html::script(mix('assets/admin/js/blog.js')) }}
    <script>
        var routePrefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    </script>
    {{ Html::script(mix('assets/admin/js/ckeditor.js')) }}
@endsection