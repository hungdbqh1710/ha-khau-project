@extends('layouts.admin')

@section('title', trans('page-name.page_customer_level'))

@section('content')
    {!! $dataTable->table(['id' => 'customer-level-table']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/customer_level.js')) }}
    {!! $dataTable->scripts() !!}
@endsection