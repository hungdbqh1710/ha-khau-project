@extends('layouts.admin')

@section('title', trans('page-name.page_revenue'))

@section('content')
    <div class="row">
        <div class="col-lg-9">
            <form id="search-form" class="form-inline" role="form" data-url-report-revenue="{{ route('admin.reports.revenue') }}" data-url-calculate-revenue="{{ route('admin.reports.calculateRevenue') }}">
                <div class="form-group">
                    <select name="filter_type" id="filter-type" class="form-control">
                        <option value="" hidden>@lang('form.title_filter_type')</option>
                        @foreach(config('data.filter_date_type') as $key => $typeName )
                            <option class="form-control" value="{{ $key }}">{{ $typeName }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group hidden" id="date-range-group">
                    <div class="controls">
                        <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="date_range" id="date-range" class="form-control js-date-range" placeholder="@lang('form.placeholder_range_date')"/>
                        </div>
                    </div>
                </div>
                <div class="form-group hidden" id="single-date-group">
                    <div class="controls">
                        <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" name="single_date" id="single-date" class="form-control js-single-date" placeholder="@lang('form.placeholder_month')"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <select name="supporter_id" class="form-control select-supporter-id">
                        @if (isset($users) && $users->count() > 0)
                            <option value="" hidden> @lang('form.placeholder_employees')</option>
                            <option value=""> @lang('form.title_all_employees')</option>
                            @foreach($users as $user)
                                <option class="form-control" value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-filter"><i class="fa fa-filter"></i> @lang('form.title_filter')</button>
            </form>
        </div>
        <div class="col-lg-3">
            <form action="{{ route('admin.reports.exportRevenue') }}">
                <input type="hidden" name="date_range">
                <input type="hidden" name="single_date">
                <input type="hidden" name="supporter_id">
                <button type="button" class="btn btn-success pull-right js-btn-export-revenue"><span class="glyphicon glyphicon-export"></span> @lang('form.title_export_excel')</button>
            </form>
        </div>
    </div>
    <table id="revenue-table" class="table">
        <div class="pull-right overview-calculate-contain">
            @lang('form.title_calculate_order_revenue', ['totalPrice' => $calculateOrderRevenue['total_price'], 'totalOrder' => $calculateOrderRevenue['total_order']])
        </div>
        <thead>
            <tr>
                <th>@lang('form.title_order_number')</th>
                <th>@lang('form.title_customer_name')</th>
                <th>@lang('form.title_total_revenue')</th>
                <th>@lang('form.title_updated_at')</th>
            </tr>
        </thead>
    </table>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/revenue/revenue.css')) }}
@endsection

@section('scripts')
    @parent
    <script>
        var urlLanguage = '{{ asset('vendor/datatables/languages/Vietnamese.json') }}';
    </script>
    {{ Html::script(mix('assets/admin/js/revenue.js')) }}
@endsection