@if (isset($calculateOrderRevenue))
    @lang('form.title_calculate_order_revenue', ['totalPrice' => $calculateOrderRevenue['total_price'], 'totalOrder' => $calculateOrderRevenue['total_order']])
@endif