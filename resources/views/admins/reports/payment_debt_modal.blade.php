<div class="modal fade" id="{{ PAYMENT_DEBT_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_payment')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div>
                    <span>@lang('form.title_payment_debt_of_customer')</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                <button type="button" class="btn btn-primary pull-right js-btn-confirm-payment" data-dismiss="modal">@lang('buttons.button_confirm')</button>
            </div>
        </div>
    </div>
</div>