@extends('layouts.admin')

@section('title', trans('page-name.page_reports_debt'))

@section('content')
    <div class="row">
        <div class="col-md-8">
            <form method="POST" id="search-form" class="form-inline" role="form" data-url-calculate-debt="{{ route('admin.reports.calculateDebt') }}">
                <div class="form-group">
                    <select name="" id="filter-type" class="form-control">
                        <option value="" hidden>Kiểu lọc</option>
                        <option value="1">Theo khoảng thời gian</option>
                        <option value="2">Theo tháng</option>
                    </select>
                </div>
                <div class="form-group hidden" id="date-range-group">
                    <div class="controls">
                        <div class="input-prepend input-group">
                    <span class="add-on input-group-addon"><i
                                class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" style="width: 200px" name="date_range" id="date-range" class="form-control"
                                   value="" placeholder="Chọn khoảng thời gian"/>
                        </div>
                    </div>
                </div>
                <div class="form-group hidden" id="single-date-group">
                    <div class="controls">
                        <div class="input-prepend input-group">
                    <span class="add-on input-group-addon"><i
                                class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" style="width: 200px" name="single_date" id="single-date" class="form-control"
                                   value="" placeholder="Chọn tháng"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <select name="customer_id" class="form-control select-customer-id">
                        @if (isset($customers) && $customers->count() > 0)
                            <option value="" hidden> @lang('form.placeholder_customer')</option>
                            <option value=""> @lang('form.title_all_customer')</option>
                            @foreach($customers as $customer)
                                <option class="form-control" value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-filter">Lọc</button>
            </form>
        </div>
        <div class="col-md-4">
            <ul class="list-inline">
                <li class="list-inline-item pull-right">
                    <form action="{{ route('admin.reports.debt.export') }}" id="export-excel" method="post">
                        {!!  csrf_field() !!}
                        <input type="hidden" name="single_date">
                        <input type="hidden" name="date_range">
                        <input type="hidden" name="customer_id">
                        <button type="button" class="btn btn-success" id="submit-export" style="display: none;"><span class="glyphicon glyphicon-export"></span>
                            Xuất Excel
                        </button>
                    </form>
                </li>
                <li class="list-inline-item pull-right">
                    <a type="button" class="btn btn-info hidden" id="js-btn-payment-debt" data-url-payment-debt="{{ route('admin.reports.paymentDebt') }}"><i class="fa fa-credit-card"></i>
                        @lang('form.title_payment')
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <table class="table" id="reports-debt">
    </table>
    @include('admins.reports.payment_debt_modal')
    <div class="modal fade" id="view-detail-transactions" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thông tin chi tiết chuyển khoản</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" >
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Ngày chuyển tiền</th>
                            <th>Số tiền đã chuyển</th>
                        </tr>
                        </thead>
                        <tbody id="detail-transactions-info"></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="view-detail-orders" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thông tin chi tiết chuyển khoản</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" >
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Mã đơn hàng</th>
                            <th>Loại đơn hàng</th>
                            <th>Tổng tiền</th>
                            <th>Ngày tạo</th>
                        </tr>
                        </thead>
                        <tbody id="detail-orders-info"></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/reports_debt.js')) }}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/reports_debt/reports_debt.css')) }}
@endsection
