@if($transactions->isNotEmpty())
    @foreach($transactions as $transaction)
        <tr>
            <td>{{ $transaction->created_at_format }}</td>
            <td>{{ number_format($transaction->amount_topup) }}</td>
        </tr>
    @endforeach
@endif