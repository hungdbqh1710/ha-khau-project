@if($orders->isNotEmpty())
    @foreach($orders as $order)
        <tr>
            <td>{{ $order->order_number }}</td>
            <td>{{ config('data.order_type')[$order->order_type] }}</td>
            <td>{{ number_format($order->final_total_price) }}</td>
            <td>{{ $order->created_at_format }}</td>
        </tr>
    @endforeach
@endif
