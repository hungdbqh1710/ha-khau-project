@if (isset($calculateOrdersDebt))
    @lang('form.title_calculate_order_debt', ['totalOrdersPrice' => $calculateOrdersDebt['total_order_price'], 'totalDeposited' => $calculateOrdersDebt['total_deposited'], 'totalDebt' => $calculateOrdersDebt['total_debt']])
@endif