@extends('layouts.admin')

@section('title', trans('page-name.page_transaction_histories', ['name' => $customer->name]))

@section('content')
    {!! $dataTable->table(['id' => 'transaction-histories']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/customer.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection