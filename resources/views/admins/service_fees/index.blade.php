@extends('layouts.admin')

@section('title', trans('page-name.page_service_fees'))

@section('content')
    {!! $dataTable->table(['id' => 'service-fees']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/service_fees.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
