@extends('layouts.admin')

@section('title', trans('page-name.page_purchase_orders_index'))

@section('content')
    <div class="row">
        <div class="col-md-9">
            <form method="POST" id="search-form" class="form-inline" role="form">
                <div class="form-group">
                    <select name="status" id="filter-type" class="form-control">
                        <option value="" hidden>-- Lọc theo trạng thái --</option>
                        <option value="">Tất cả</option>
                        @foreach(config('data.purchase_order_status') as $value => $title)
                            <option value="{{ $value }}">{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-filter">Lọc</button>
            </form>
        </div>
        <div class="col-md-3">
            <button class="btn btn-default buttons-reset pull-right" id="refresh"><i class="fa fa-undo"></i> Làm mới</button>
        </div>
    </div>
    <table id="orders" class="table" style="width:100%">
    </table>
    <script id="details-template" type="text/x-handlebars-template">
        <div class="detail-box container row">
            <div class="col-lg-12">
                <h4><strong>@lang('form.title_order_detail') @{{ order_number }}</strong></h4>
            </div>
            <div class="col-lg-12">
                <table class="table" id="details-@{{ id }}">
                </table>
            </div>
        </div>
    </script>
    @include('admins.orders.delivery_address_modal')
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('vendor/datatables/handlebars.js') }}"></script>
    {{ Html::script(mix('assets/admin/js/purchase_orders.js')) }}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/orders/purchase_orders.css')) }}
@endsection