@extends('layouts.admin')

@section('title', trans('page-name.admin_order_index').' / '.trans('page-name.admin_order_detail', ['order_number' => isset($order) ? $order->order_number : '']))

@section('content')
    {!! $dataTable->table(['id' => 'order-details']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/order_details.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/orders/orders.css')) }}
@endsection