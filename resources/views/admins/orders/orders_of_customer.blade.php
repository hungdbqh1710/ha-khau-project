@extends('layouts.admin')

@section('title', trans('page-name.page_customer').' / '.trans('page-name.page_list_order_of_customer', ['name' => isset($customer) ? $customer->name : '']))

@section('content')
    {!! $dataTable->table(['id' => 'order-of-customer-table']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/customer.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection