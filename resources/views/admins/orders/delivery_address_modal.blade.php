<div class="modal fade" id="{{ DELIVERY_ADDRESS_MODAL }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">@lang('form.title_delivery_address_detail') </h4>
            </div>
            <div class="modal-body">
                <h3>@lang('form.title_delivery_address_detail')</h3>
                <blockquote class="js-delivery-address-content"></blockquote>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('buttons.button_close')</button>
            </div>
        </div>
    </div>
</div>