@extends('layouts.admin')

@section('title', trans('page-name.page_feedback'))

@section('content')
    {!! $dataTable->table(['id' => 'feedback-table']) !!}
    @include('admins.feedback.detail_modal')
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/feedback.js')) }}
    {!! $dataTable->scripts() !!}
@endsection