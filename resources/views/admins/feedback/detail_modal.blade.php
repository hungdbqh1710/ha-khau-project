<div class="modal fade" id="{{ DETAIL_FEEDBACK_MODAL }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">@lang('form.title_feedback_detail') </h4>
            </div>
            <div class="modal-body">
                <h3 class="js-feedback-phone-number"></h3>
                <blockquote class="js-feedback-content"></blockquote>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('buttons.button_close')</button>
            </div>
        </div>
    </div>
</div>