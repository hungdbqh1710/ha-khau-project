<p>Thông tin người gửi:</p>
<p>Hà Khẩu Logistics</p>
<p>Địa chỉ: {{ config('data.ha_khau_address') }}</p>
<p>SĐT: {{ $configs['hotline']->value }}</p>
<p>Email: {{ $configs['email']->value }}</p>
<br>

<p>Thông tin người nhận:</p>
<p>Tên người nhận: {{ $data->orderAdress->name }}</p>
<p>Số điện thoại: {{ $data->orderAdress->phone_number }}</p>
<p>Thành phố: {{ $data->orderAdress->city }}</p>
<p>Quận/Huyện:  {{ $data->orderAdress->district }}</p>
<p>Xã phường: {{ $data->orderAdress->commune }}</p>
<p>Số nhà, phố: {{ $data->orderAdress->street }}</p>
<br>
<br>

<table>
    <thead>
    <tr>
        <th>Tên sản phẩm</th>
        <th>Link sản phẩm</th>
        <th>Số lượng</th>
        <th>Đơn giá</th>
        <th>Tỷ giá</th>
        <th>Thành tiền</th>
        <th>Ghi chú khách</th>
        <th>Ghi chú admin</th>
        <th>Mã vận chuyển TQ</th>
        <th>Mã đăt hàng TQ</th>
    </tr>
    </thead>
    <tbody>
    @if (isset($data->purchaseOrderItems))
        @foreach($data->purchaseOrderItems as $value)
            <tr>
                <td>{{ $value->product_name }}</td>
                <td>{{ $value->product_link }}</td>
                <td>{{ $value->qty }}</td>
                <td>{{ $value->price }}</td>
                <td>{{ $value->current_rate }}</td>
                <td>{{ $value->current_rate * $value->price }}</td>
                <td>{{ $value->customer_note }}</td>
                <td>{{ $value->admin_note }}</td>
                <td>{{ $value->cn_transport_code }}</td>
                <td>{{ $value->cn_order_number }}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>