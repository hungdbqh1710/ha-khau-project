<table>
    <thead>
    <tr>
        <th>
            @if ($customerName != null)
                Khách hàng {{ $customerName }}
            @endif
        </th>
        <th>
            @if ($filterDate != null)
                Thời gian {{ $filterDate }}
            @endif
        </th>
    </tr>
        <tr>
            <th>Ngày tháng</th>
            <th>Khách hàng</th>
            <th>Mã đơn hàng</th>
            <th>Loại đơn hàng</th>
            <th>Tổng giá</th>
            <th></th>
            <th>Ngày tháng</th>
            <th>Nạp tiền</th>
            <th>Tổng nợ</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <td>=SUM(E4:E{{ 4 + $orders->count() - 1 }})</td>
            <th></th>
            <th></th>
            <td>=SUM(H4:H{{ 4 + $transactionHistories->count() - 1 }})</td>
            <td>=E3-H3</td>
        </tr>
    </thead>
    <tbody>
    @if($type == 'order')
        @for($i = 0; $i < $count; $i++)
            <tr>
                <td>{{ $orders[$i]->created_at }}</td>
                <td>{{ $orders[$i]->customer->name }}</td>
                <td>{{ $orders[$i]->order_number }}</td>
                <td>{{ $orders[$i]->order_type == TYPE_ORDER_PURCHASE ? "Mua hộ" : "Vận chuyển" }}</td>
                <td>{{ $orders[$i]->final_total_price }}</td>
                <td></td>
                @if($i < $transactionHistories->count())
                    <td>{{ $transactionHistories[$i]->created_at }}</td>
                    <td>{{ $transactionHistories[$i]->amount_topup }}</td>
                @endif
            </tr>
        @endfor
    @else
        @for($i = 0; $i < $count; $i++)
            <tr>
                @if($i < $orders->count())
                    <td>{{ $orders[$i]->created_at }}</td>
                    <td>{{ $orders[$i]->customer->name }}</td>
                    <td>{{ $orders[$i]->order_number }}</td>
                    <td>{{ $orders[$i]->order_type == TYPE_ORDER_PURCHASE ? "Mua hộ" : "Vận chuyển" }}</td>
                    <td>{{ $orders[$i]->final_total_price }}</td>
                @else
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
                <td></td>
                <td>{{ $transactionHistories[$i]->created_at }}</td>
                <td>{{ $transactionHistories[$i]->amount_topup }}</td>
            </tr>
        @endfor
    @endif
    </tbody>
</table>