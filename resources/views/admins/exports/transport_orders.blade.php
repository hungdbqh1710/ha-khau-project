<table>
    <thead>
    <tr>
        <th>Mã đơn hàng</th>
        <th>Phương thức nhận</th>
        <th>Mã vận đơn TQ</th>
        <th>Mã vận đơn VN</th>
        <th>Phí vận chuyển TQ</th>
        <th>số tiền thanh toán hộ</th>
        <th>Phí chuyển về VN</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $data->order_number }}</td>
            <td>{{ config('data.receive_type')[$data->transport_receive_type] }}</td>
            <td>{{ $data->cn_transport_code }}</td>
            <td>{{ $data->vn_transport_code }}</td>
            <td>{{ $data->cn_transport_fee }}</td>
            <td>{{ $data->vn_transport_fee }}</td>
            <td>{{ $data->via_border_transport_fee }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Tổng tiền đơn: </td>
            <td>{{ $data->final_total_price }}</td>
        </tr>
        <tr></tr>
        <tr>
            <td>Thông tin người nhận</td>
            <td>{{ $data->receiver_info }}</td>
        </tr>
    </tbody>
</table>