@extends('layouts.admin')

@section('title', trans('page-name.page_slide_site', ['site' => $group == TYPE_TAO_BAO ? TYPE_TAO_BAO : ($group == TYPE_1688 ? TYPE_1688 : TYPE_TMALL)]))

@section('content')
    {!! $dataTable->table(['id' => 'slide-table']) !!}
    <input type="hidden" class="js-slide-group" value="{{ $group == TYPE_TAO_BAO ? TYPE_TAO_BAO : ($group == TYPE_1688 ? TYPE_1688 : TYPE_TMALL) }}">
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/slide.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection