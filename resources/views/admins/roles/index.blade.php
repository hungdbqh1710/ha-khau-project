@extends('layouts.admin')

@section('title', trans('page-name.page_roles'))

@section('content')
    {!! $dataTable->table(['id' => 'roles']) !!}
    <script id="details-template" type="text/x-handlebars-template">
        <table>
            <tr>
                <td>Quyền:
                    @{{#each permissions as |permission key|}}
                        <span class="label label-info">@{{permission.display_name}}</span>
                    @{{/each}}
                </td>
            </tr>
        </table>
    </script>
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/roles.js')) }}
    {!! $dataTable->scripts() !!}
    {{ Html::script(mix('assets/admin/js/detail_rows.js')) }}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/roles/roles.css')) }}
@endsection
