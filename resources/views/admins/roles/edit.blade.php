@extends('layouts.admin')

@section('title', trans('page-name.page_edit_roles', ['name' => $role->display_name]))

@section('content')
    {{ Form::open(['route' => ['admin.roles.update', $role->id], 'class' => "form-horizontal form-label-left"]) }}
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="display_name">@lang('form.title_role_display_name') <span class="text-danger">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input value="{{ $role->display_name }}" type="text" placeholder="@lang('form.placeholder_display_name_role')" id="display_name" name="display_name" required="required" class="form-control col-md-7 col-xs-12 {{ $errors->has('display_name') ? 'parsley-error' : '' }}">
                @if($errors->has('display_name'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('display_name') }}.</li></ul>
                @endif
                @if($errors->has('permissions'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('permissions') }}</li></ul>
                @endif
            </div>
        </div>
        <div class="ln_solid"></div>
        <h3 class="">@lang('form.title_permission')</h3>
        <div class="checkbox">
            <label>
                <input id="select_all" name="" type="checkbox" value="" {{ $role->permissions->count() == $permissions->count() ? 'checked' : '' }}> Full quyền
            </label>
        </div>
        <div class="ln_solid"></div>
        <div class="row">
        @if(!empty($permissions))
            @foreach($permissions as $permission)
                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input name="permissions[]" {{ $role->permissions->contains($permission) ? 'checked' : '' }} type="checkbox" value="{{ $permission->name }}"> {{ $permission->display_name }}
                        </label>
                    </div>
                </div>
            @endforeach
        @endif
        </div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6 text-right">
                <button type="submit" class="btn btn-success">@lang('buttons.button_save')</button>
                <a href="{{ route('admin.roles.index') }}" class="btn btn-default" type="button">@lang('buttons.button_back')</a>
            </div>
        </div>
    {{ Form::close() }}
@endsection