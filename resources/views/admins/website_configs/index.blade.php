@extends('layouts.admin')

@section('title', trans('page-name.page_website_config'))

@section('content')
    {!! $dataTable->table(['id' => 'website-configs']) !!}
@endsection

@section('scripts')
    @parent
    {!! $dataTable->scripts() !!}
    {{ Html::script(asset('vendor/ckeditor/ckeditor.js')) }}
    {{ Html::script(asset('vendor/ckeditor/adapters/jquery.js')) }}
    <script>
        var routePrefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    </script>
    {{ Html::script(mix('assets/admin/js/website_configs.js')) }}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection