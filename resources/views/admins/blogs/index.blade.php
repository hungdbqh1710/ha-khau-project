@extends('layouts.admin')

@section('title', trans('page-name.page_blog'))

@section('content')
    {!! $dataTable->table(['id' => 'blog-table']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/blog.js')) }}
    {!! $dataTable->scripts() !!}
@endsection