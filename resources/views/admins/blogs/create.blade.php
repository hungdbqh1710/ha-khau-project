@extends('layouts.admin')

@section('title', trans('page-name.page_blog_create'))

@section('content')
    <div class="row">
        <form action="{{ route('admin.blogs.store') }}" method="POST"  enctype="multipart/form-data" class="form form-action">
            {{ csrf_field() }}
            <div class="col-lg-4">
                <div class="form-group">
                    <label>@lang('form.title') <span class="text-danger">*</span></label>
                    <input
                    name="title"
                    class="form-control"
                    value="{{ old('title') }}"
                    placeholder="@lang('form.placeholder_title')"
                    required>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('title') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label>@lang('form.title_src') <span class="text-danger">*</span></label>
                    <input type="file" id="js-image" name="image" required onchange="previewImage();"/>
                    @if ($errors->any())
                        <span class="text-danger">
                            {{ $errors->first('image') }}
                        </span>
                    @endif
                    <div class="preview-image">
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    <label>@lang('form.title_content') <span class="text-danger">*</span></label>
                    <textarea
                    name="content"
                    id="js-content-blog"
                    class="form-control"
                    required>{!! old('content') !!}</textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ route('admin.blogs.index') }}" class="btn btn-default pull-right">
                        @lang('buttons.button_back')
                    </a>
                    <button type="submit" class="btn btn-success pull-right">
                        @lang('buttons.button_save')
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    @parent
    {{ Html::script(asset('vendor/ckeditor/ckeditor.js')) }}
    {{ Html::script(asset('vendor/ckeditor/adapters/jquery.js')) }}
    {{ Html::script(mix('assets/admin/js/blog.js')) }}
    <script>
        var routePrefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    </script>
    {{ Html::script(mix('assets/admin/js/ckeditor.js')) }}
@endsection