@extends('layouts.admin')

@section('title', trans('page-name.page_transport_orders_index'))

@section('content')
    <div class="row">
        <div class="col-md-9">
            <form method="POST" id="search-form" class="form-inline" role="form">
                <div class="form-group">
                    <select name="status" id="filter-type" class="form-control">
                        <option value="" hidden>-- Lọc theo trạng thái --</option>
                        <option value="">Tất cả</option>
                        @foreach(config('data.transport_order_status') as $value => $title)
                            <option value="{{ $value }}">{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-filter">Lọc</button>
            </form>
        </div>
        <div class="col-md-3">
            <button class="btn btn-default buttons-reset pull-right" id="refresh"><i class="fa fa-undo"></i> Làm mới</button>
        </div>
    </div>
    <table id="transport-orders-table" class="table">
        <thead>
            <tr>
                <th>@lang('form.title_order_number')</th>
                <th>@lang('form.title_customer')</th>
                <th>@lang('form.title_caring_staff')</th>
                <th>@lang('table.receive_type')</th>
                <th>@lang('form.title_status')</th>
                <th>@lang('form.title_transport_code_china')</th>
                <th>@lang('form.title_transport_code_vn')</th>
                <th>@lang('form.title_transport_fee_cn')</th>
                <th>@lang('form.title_transport_fee_via_border')</th>
                <th>@lang('form.title_transport_fee_vn')</th>
                <th>@lang('form.total_order_price')</th>
                <th>@lang('form.title_order_shop_cn_date')</th>
                <th class="transport-order-receiver-info">@lang('form.title_receive_info')</th>
                <th>@lang('form.title_note_admin')</th>
                <th>@lang('form.title_customer_note')</th>
                <th>@lang('form.title_created_at')</th>
                <th>@lang('form.title_action')</th>
            </tr>
        </thead>
    </table>
    <script id="details-template" type="text/x-handlebars-template">
        <div class="detail-box container row">
            <div class="col-lg-12">
                <h4><strong>@lang('form.title_order_detail') @{{ order_number }}</strong></h4>
            </div>
            <div class="col-lg-12">
                <table class="table" id="details-@{{ id }}">
                    <thead>
                        <tr>
                            <th>@lang('form.title_transport_code_china')</th>
                            <th>@lang('form.title_transport_code_vn')</th>
                            <th>@lang('form.title_transport_fee_cn')</th>
                            <th>@lang('form.title_transport_fee_via_border')</th>
                            <th>@lang('form.title_transport_fee_vn')</th>
                            <th>@lang('form.title_status')</th>
                            <th>@lang('form.title_order_shop_cn_date')</th>
                            <th class="transport-order-receiver-info">@lang('form.title_receive_info')</th>
                            <th>@lang('form.title_note_admin')</th>
                            <th>@lang('form.title_customer_note')</th>
                            <th>@lang('form.title_action')</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </script>

    @include('frontend.transport-orders.detail_link_qty_modal')
@endsection

@section('scripts')
    @parent
    <script>
        var urlLanguage = '{{ asset('vendor/datatables/languages/Vietnamese.json') }}';
    </script>
    {{ Html::script(mix('assets/admin/js/transport_orders.js')) }}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/transport_orders/transport_orders.css')) }}
@endsection