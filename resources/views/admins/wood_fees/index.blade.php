@extends('layouts.admin')

@section('title', trans('page-name.page_wood_fees'))

@section('content')
    {!! $dataTable->table(['id' => 'wood-fees']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/wood_fees.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
