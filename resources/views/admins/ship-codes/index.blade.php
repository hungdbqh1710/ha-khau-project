@extends('layouts.admin')

@section('title', trans('page-name.page_list_ship_code'))

@section('content')
    <div class="row">
        <form method="GET" class="form-inline col-md-7" action="{{ route('admin.ship-codes.filter') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input id="valSelect2" type="hidden" disabled @if(isset($customer_filter)) value="{{$customer_filter}}" @endif/>
                <select class="js-example-basic-multiple form-control" style="width:200px" name="states[]" id="mySelect2">
                    @if(!isset($customer_filter))
                        <option value="" hidden>-- Khách hàng --</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <select name="order_status" class="form-control">
                    <option value="" hidden>-- Trạng thái đơn hàng --</option>
                    <option value="">Tất cả</option>
                    @foreach(config('data.shipping_type') as $value => $title)
                        <option value="{{ $value }}" @if(isset($order_status) && $order_status==$value && $order_status!= "") selected @endif>{{ $title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="date-ranger" name="daterange" @if(isset($from)) value="{{$from .'-'. $to}}"@endif />
            </div>
            <button type="submit" class="btn btn-primary" style="margin-bottom: 0">Lọc</button>
        </form>
        <div class="col-md-1 pull-right">
            <button class="btn btn-default buttons-reset pull-right" onclick="refresh()"><i class="fa fa-undo"></i> Làm mới</button>
        </div>
        <form method="POST" class=" col-sm-1 pull-right" action="{{ route('admin.ship-codes.get-excel') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="hidden" name="data" value="{{ json_encode($ship_codes) }}">
                <button class="btn btn-success" type="submit" id="excelSubmit">
                    <i class="fa fa-file-excel-o"></i> Excel
                </button>
            </div>
        </form>
    </div>
    <div class="row" style="margin-top:2%">
        <form class="col-md-3" method="GET" id="per_page">
            <div class="form-inline form-group">
                <span>Xem</span>
                    <select name="per_page" class="form-control" value="" id="item_page">
                        <option value="10" @if(isset($per_page) && $per_page == 10) selected @endif>10</option>
                        <option value="20" @if(isset($per_page) && $per_page == 20) selected @endif>20</option>
                        <option value="50" @if(isset($per_page) && $per_page == 50) selected @endif>50</option>
                        <option value="100" @if(isset($per_page) && $per_page == 100) selected @endif>100</option>
                    </select>
                <span>Mục</span>
            </div>
        </form>
        <form class="col-md-3 pull-right">
            <div class="form-inline form-group">
                <label>Tìm kiếm:</label>
                <input type="text" class="form-control" id="search_content" value="">
            </div>
        </form>
    </div>
    <table id="ship-codes" class="table" style="width:100%">
        <thead>
            <tr>
                <th>@lang('form.transport_code')</th>
                <th>@lang('table.package_code')</th>
                <th>@lang('table.buy_code')</th>
                <th>@lang('table.status')</th>
                <th>@lang('table.date_time')</th>
                <th>@lang('table.size')</th>
                <th>@lang('table.weight')</th>
                <th>@lang('form.title_price')</th>
                <th>@lang('form.cn_transport_fee')</th>
                <th>@lang('form.current_exchange_rate')</th>
                <th>@lang('form.total_price')</th>
                <th>@lang('form.title_note')</th>
                <th>@lang('form.title_customer')</th>
                <th>@lang('table.act')</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @if(count($ship_codes))
                @foreach ($ship_codes as $index => $ship)
                    <tr>
                        <td>{{ $ship->ship_code }}</td>
                        <td id="package_code_{{$index}}" class="text_{{$index}}" >{{ $ship->package_code }}</td>
                        <td id="buy_code_{{$index}}" class="text_{{$index}}">{{ $ship->buy_code }}</td>
                        <td>
                            @if($ship->status == 'in-cn' || $ship->status == 'out-cn' )
                                <span class="label label-info">@lang('table.'.$ship->status)</span>
                            @else
                                <span class="label label-warning">@lang('table.'.$ship->status)</span>
                            @endif
                        </td>
                        <td>{{ $ship->updated_at }}</td>
                        <td id="size_{{$index}}"  class="text_{{$index}}" >{{ $ship->size }}</td>

                        <td id="weight_{{$index}}"  class="number_{{$index}}" >{{ $ship->weight }}</td>

                        <td id="price_{{$index}}"  class="number_{{$index}}" >{{ $ship->price }}</td>

                        <td id="cn_transport_fee_{{$index}}" class="number_{{$index}}" >{{ $ship->cn_transport_fee }}</td>

                        <td id="exchange_rates_{{$index}}" class="number_{{$index}}" >{{ $ship->exchange_rates }}</td>

                        <td id="total_price_{{$index}}">{{ number_format($ship->total_price) }}</td>

                        <td id="admin_note_{{$ship->id}}" data-toggle="modal" data-target="#modalNote_{{$ship->id}}" onclick="return clickEdit({{$ship->id}}, 'admin_note_{{$ship->id}}', '{{ $ship->admin_note }}')">
                            <i>@if($ship->admin_note) {{ substr($ship->admin_note,0,5) . '...'}} @endif</i>
                        </td>
                        <td id="btn_customer_{{$index}}" data-toggle="modal" data-target="#myModal_{{$ship->id}}" onclick=" getCustomers({{$index}}, {{ $ship->id }}, {{$ship->customer_id}})" >
                            <input type="hidden" id="td_cus_id_{{$ship->id}}" value="{{$ship->customer_id}}"/>
                            <span id="customer_name_{{$ship->id}}">
                                {{ $ship->customer_id ? Helper::getNameCustomer($ship->customer_id) : '' }}
                            </span>
                        </td>
                        <td>                          
                            <a href="#" class="btn btn-xs btn-info editor_edit " id="btn_edit_{{$index}}" onclick="return editItems({{ $index }}, {{ $ship->id }})">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="#" class="btn btn-xs btn-info editor_edit hidden" id="btn_confirm_{{$index}}" onclick="return createOrUpdate('{{ route('admin.ship-codes.createOrUpdate') }}', '{{$ship->ship_code}}', {{ $index }}, {{$ship->id}})">
                                <i class="fa fa-check"></i>
                            </a>
                            <a href="#" class="hidden" id="btn_close_{{$index}}" onclick="return closeEdit({{ $index }}, {{$ship->id}})">
                                <i class="fa fa-close"></i>
                            </a>
                            @if(!$ship->buy_code && !$ship->package_code)
                                <a href="#" class="btn btn-xs btn-danger editor_edit " id="btn_delete_{{$index}}" onclick="return deleteItems({{ $ship->ship_code }}, {{ $index }}, '{{$ship->id}}')">
                                    <i class="fa fa-trash"></i>
                                </a>
                            @endif
                        </td>
                    </tr>

                    <!-- The Modal -->
                    <div class="modal" id="myModal_{{$ship->id}}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Danh sách khách hàng</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="sel1">Chọn khách hàng:</label>
                                            <input type="hidden" value="" name="customer_name" id="cus_modal_name_{{$ship->id}}">
                                            <select class="js-example-basic-multiple" name="states[]" style="width: 50%" id="customer_{{$ship->id}}">
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="modal_cs_{{$ship->id}}" class="btn btn-info hidden" data-dismiss="modal" onclick="confirmCus({{$ship->id}})">Xác nhận</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal" id="modalNote_{{$ship->id}}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Nội dung ghi chú</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" id="modal_note_{{$ship->id}}" value="">{{ $ship->admin_note }}</textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" id="modal_ft_{{$ship->id}}" class="btn btn-info hidden" data-dismiss="modal" onclick="confirmNote('{{$ship->id}}')">Xác nhận</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <td>@lang('form.empty_data')</td>
            @endif
        </tbody>
    </table>
    @if(isset($ship_codes) && $ship_codes->links())
        {{ $ship_codes->appends(Request::input())->links() }}
    @endif
    <script id="details-template" type="text/x-handlebars-template">
        <div class="detail-box container row">
            <div class="col-lg-12">
                <h4><strong>@lang('form.title_order_detail') @{{ order_number }}</strong></h4>
            </div>
            <div class="col-lg-12">
                <table class="table" id="details-@{{ id }}">
                </table>
            </div>
        </div>
    </script>
    @include('admins.orders.delivery_address_modal')
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('vendor/datatables/handlebars.js') }}"></script>
    {{ Html::script(mix('assets/admin/js/purchase_orders.js')) }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>     
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
            let val = $("#valSelect2").val();
            $.ajax({
                type: "GET",
                url: "{{ route('admin.customers.index') }}",
                dataType: "json",  
                success: function(data) {
                    var customers = data.data;
                    var options ='';
                    for( let i=0; i < customers.length; i++){
                        options += `<option value="${customers[i].id}-${customers[i].name}"> ${customers[i].name} </option>`
                    }
                    $('.js-example-basic-multiple').html(`${options}`);
                    if(!val){
                        $("#mySelect2").append(`<option value="" hidden>-- Khách hàng --</option>`);
                    }
                    $("#mySelect2").val(val);
                },
            });
            $("#date-ranger").daterangepicker({
                "singleDatePicker": false,
                "showDropdowns": true,
                "autoApply": true,
                "locale": {
                    "format": "DD/MM/YYYY",
                },
                "startDate": $("#date-ranger").val() ? $("#date-ranger").val() : moment().subtract(1, 'days') ,
                "maxDate": moment().endOf("day"),
            });
            $('#item_page').change(function(){
                $('#per_page').submit();
                /* $.ajax({
                type: "GET",
                url: "{{ route('admin.ship-codes.index') }}?per_page="+val,
                dataType: "json",  
                success: function(data) {
                    console.log(data);
                },
            }); */
            });
            $('#search_content').on('input', function(){
                var val = $(this).val() ? $(this).val() : '';
                $.ajax({
                    type: "GET",
                    url: "{{ route('admin.ship-codes.search') }}",
                    dataType: "json",  
                    data: { 'search_content': val },
                    success: function(data) {
                        var names = data.names;
                        var ship_codes = data.data.data;
                        $("#tbody tr").html('');
                        for (const key in ship_codes) {
                            if (ship_codes.hasOwnProperty(key)) {
                                var ship = ship_codes[key];
                                var customer_id = ship.customer_id ? ship.customer_id : ''; 
                                $("#tbody").append(`
                                    <tr>
                                        <td>${ship.ship_code}</td> 
                                        <td id="package_code_${key}" class="text_${key}">${ship.package_code ? ship.package_code : ''}</td>
                                        <td id="buy_code_${key}" class="text_${key}">${ship.buy_code ? ship.buy_code : ''}</td>
                                        <td>
                                            ${(ship.status == 'in-cn') ? "<span class='label label-info'>Đã về kho TQ</span>" :
                                            ship.status == 'out-cn' ? "<span class='label label-info'>Đã xuất kho TQ</span>" : ship.status == 'in-vn' ? "<span class='label label-warning'>Đã về kho VN</span>" : "<span class='label label-warning'>Đã xuất kho VN</span>"}
                                        </td>
                                        <td>${ship.updated_at}</td>
                                        <td id="size_${key}" class="text_${key}">${ship.size ? ship.size : ''}</td>
                                        <td id="weight_${key}" class="number_${key}">${ship.weight ? ship.weight : ''}</td>
                                        <td id="price_${key}" class="number_${key}">${ship.price ? ship.price : ''}</td>
                                        <td id="cn_transport_fee_${key}"  class="number_${key}">${ship.cn_transport_fee ? ship.cn_transport_fee : ''}</td>
                                        <td id="exchange_rates_${key}" class="number_${key}">${ship.exchange_rates ? ship.exchange_rates : ''}</td>
                                        <td id="total_price_${key}">${ship.total_price ? formatNumber(ship.total_price) : ''}</td>
                                        <td id="admin_note_${ship.id}" class="text_${key}" data-toggle="modal" data-target="#modalNote_${ship.id}" onclick="clickEdit('${ship.id}','admin_note_${ship.id}', '${ship.admin_note}')"><i>${ship.admin_note ? ship.admin_note.substring(0, 4) : ''} ...</i></td>
                                        <td id="btn_customer_${key}" data-toggle="modal" data-target="#myModal_${ship.id}" onclick="getCustomers('${key}','${ship.id}','${ship.customer_id}' )">
                                            <input type="hidden" id="td_cus_id_${ship.id}" value="${ship.customer_id}"/>
                                            <span id="aj_customer_name_${ship.id}">
                                                ${names[key]}
                                            </span>
                                        </td>
                                        <td>
                                            <a href='#' class='btn btn-xs btn-info editor_edit' id='btn_edit_${key}' onclick=' editItems("${key}","${ship.id}")'><i class='fa fa-edit'></i></a>
                                            <a href='#' class='btn btn-xs btn-info editor_edit hidden' id='btn_confirm_${key}' onclick=createOrUpdate('{{ route('admin.ship-codes.createOrUpdate') }}','${ship.ship_code}','${key}','${ship.id}')><i class='fa fa-check'></i></a>
                                            <a href='#' class='hidden' id="btn_close_${key}" onclick=closeEdit('${key}','${ship.id}')><i class='fa fa-close'></i></a>
                                            ${(!ship.buy_code && !ship.package_code) ? 
                                                "<a href='#' class='btn btn-xs btn-danger editor_edit' id='btn_delete_"+key+"' onclick=deleteItems('"+ship.ship_code+"','"+key+"','"+ ship.id+"')><i class='fa fa-trash'></i></a>" : ""}
                                        </td>"
                                    </tr>                
                                `);
                            }
                        }
                    },
                });
            });
        });   
        function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }

        function getItems(){
            var item = $("#page_item").val();
            //console.log(item);
            /* $.ajax({
                type: "GET",
                url: "{{ route('admin.ship-codes.index') }}",
                dataType: "json",  
                data: { 'page': item },
                success: function(data) {
                    $("#page_item").val(item);
                },
            }); */
        }

        function createOrUpdate(urlApi, value, index, id){
            if(value){
                var size = $("#size_" + index + " :input").val();
                var weight = $("#weight_" + index + " :input").val();
                var price = $("#price_" + index + " :input").val();
                var buy_code = $("#buy_code_" + index + " :input").val();
                var package_code = $("#package_code_" + index + " :input").val();
                var cn_transport_fee = $("#cn_transport_fee_" + index + " :input").val();
                var exchange_rates = $("#exchange_rates_" + index + " :input").val();
                var admin_note = $("#modal_note_" + id).val();
                var $customer_id = $("#td_cus_id_"+id).val();
                var data = {
                    "type": "not-update-status",
                    "ship_code": value,
                    "size": size ? size : ($('#size_' + index).text() ? $('#size_' + index).text() : 0),
                    "weight": weight ? weight : ($('#weight_' + index).text() ? $('#weight_' + index).text() : 0),
                    "price": price ? price : ($('#price_' + index).text() ? $('#price_' + index).text() : 0),
                    "buy_code": buy_code ? buy_code : ($('#buy_code_' + index).text() ? $('#buy_code_' + index).text() : ''),
                    "package_code": package_code ? package_code : ($('#package_code_' + index).text() ? $('#package_code_' + index).text() : ''),
                    "cn_transport_fee": cn_transport_fee ? cn_transport_fee :  ($('#cn_transport_fee_' + index).text() ? $('#cn_transport_fee_' + index).text() : 0),
                    "exchange_rates": exchange_rates ? exchange_rates : $('#exchange_rates_' + index).text(),
                    "admin_note": admin_note,
                    "customer_id": $customer_id != '' ? $customer_id : ''
                }
                $.ajax({
                    type: "POST",
                    url: urlApi,
                    dataType: "json",  
                    data: { data: JSON.stringify( data ) },
                    success: function(data) {   
                        toastr["success"](`Cập nhật mã vận đơn <b>${value}</b> thành công`);
                        setTimeout(function(){
                            refresh();
                        }, 200);
                    },
                });
            }
        }

        function clickEdit($index, $id, $value){
            if(!$('#'+$id + " :input").length){
                $('#btn_confirm_'+$index).attr('class', 'btn btn-xs btn-info editor_edit');
            }
        }

        function editItems(index, id){
            $(".text_"+index).each(function() {
                let value = $(this).text();
                $(this).html(`<input type="text" name="edit-input" style="width: 5em" value= "${value}" autofocus>`);
            });
            $(".number_"+index).each(function(){
                let value = $(this).text();
                $(this).html(`<input id="ip_${index}" name="edit-input" type="number" style="width: 5em" value= "${value}" min="0" autofocus onblur="validInput('ip_${index}')">`);
            });
            $('#btn_edit_'+index).attr('class', 'hidden');
            $('#btn_delete_'+index).attr('class', 'hidden');
            $('#btn_close_'+index).attr('class', 'btn btn-xs btn-danger editor_edit');
            $('#btn_confirm_'+index).attr('class', 'btn btn-xs btn-info editor_edit');
            $('#modal_ft_'+id).attr('class', 'btn btn-info');
            $('#modal_cs_'+id).attr('class', 'btn btn-info');
        }

        function deleteItems(ship_code, index, id){
            if(confirm('Bạn muốn xoá mã vận đơn '+ ship_code)){
            var data = {
                    "delete_id": id,
                }
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.ship-codes.delete') }}",
                    dataType: "json",  
                    data: { data: JSON.stringify( data ) },
                    success: function(data) {   
                        refresh();
                    },
                });
            }
        }

        function closeEdit(index, id){
            $('#btn_edit_'+index).attr('class', 'btn btn-xs btn-info editor_edit');
            $('#btn_close_'+index).attr('class', 'hidden');
            $('#btn_confirm_'+index).attr('class', 'hidden');
            $('#btn_delete_'+index).attr('class', 'btn btn-xs btn-danger editor_edit');
            $('#modal_ft_'+id).attr('class', 'btn btn-info hidden');
            $('#modal_cs_'+id).attr('class', 'btn btn-info hidden');
            $(".text_"+index).each(function() {
                let value = $(this).children('input').val();
                $(this).html(`${value}`);
            });
            $(".number_"+index).each(function() {
                let value = $(this).children('input').val();
                $(this).html(`${value}`);
            });
            
        }

        function validInput(id){
            var val = $("#"+id).val();
            if(val < 0){
                alert('Vui lòng nhập vào giá trị lớn hơn 0');;
            }
        }

        function getCustomers(index, id, customer_id){
            
            var name =  $("#customer_name_"+id).text().trim();
            var name_in_madal = $("#aj_customer_name_"+id).text().trim();
            $("#td_cus_id_"+id).val(customer_id);
            if(name)
                $("#customer_"+id).val(customer_id+'-'+name).trigger('change'); 
            else
                $("#customer_"+id).val(customer_id+'-'+name_in_madal).trigger('change'); 
        }

        function confirmCus(id){
            var val = $("#customer_"+id).val().split("-");
            var name = val[1];
            $("#customer_name_"+id).html(name);
            $("#td_cus_id_"+id).val(val[0]);
        }

        function confirmNote(index){
            var val = $("#modal_note_" + index).val();
            $("#admin_note_"+index).html(`<i> ${val.substring(0,7)} ... </i>`);
        }

        function excelExport(data){
            if(data && data.current_page){
                data = data.data;
            }
            $.ajax({
                type: "POST",
                url: urlApi,
                dataType: "json",  
                data: { data: JSON.stringify( data ) },
                success: function(data) {
                    refresh();    
                },
            });
        }

        function refresh(){
            window.location.reload();
        }
    </script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/orders/purchase_orders.css')) }}
    <style>
        input[type='number'] {
            -moz-appearance:textfield;
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }
    </style>
@endsection