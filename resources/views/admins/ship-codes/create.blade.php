@extends('layouts.admin')

@section('title', trans('page-name.page_create_ship_code'))

@section('content')
    <div class="row">
        {{-- <form class="col-md-6">
            <div class="form-group">
                <label>Nhập mã vận đơn: </label>
                <input type="text" class="form-control" id="ship-code">
            </div>
        </form> --}}
    <h3 align="center">Import Excel File in Laravel</h3>
    <br />
   @if(count($errors) > 0)
    <div class="alert alert-danger">
     Upload Validation Error<br><br>
     <ul>
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif

   @if($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
   <form method="post" enctype="multipart/form-data" action="{{ route('admin.ship-codes.import') }}">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Select File for Upload</label></td>
       <td width="30">
        <input type="file" name="select_file" />
       </td>
       <td width="30%" align="left">
        <input type="submit" name="upload" class="btn btn-primary" value="Upload">
       </td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><span class="text-muted">.xls, .xslx</span></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
</div>
@endsection
<script id="details-template" type="text/x-handlebars-template">
    <div class="detail-box container row">
        <div class="col-lg-12">
            <h4><strong>@lang('form.title_order_detail') @{{ order_number }}</strong></h4>
        </div>
        <div class="col-lg-12">
            <table class="table" id="details-@{{ id }}">
            </table>
        </div>
    </div>
</script>
@section('scripts')
    @parent
    <script src="{{ asset('vendor/datatables/handlebars.js') }}"></script>
    {{ Html::script(mix('assets/admin/js/purchase_orders.js')) }}
    <script>
        $(document).ready(function() {
            $("#ship-code").on('input', function () {
                let getValue = $(this).val();
                if(getValue){
                    var data = { 
                            "ship_code": getValue ,
                            "type": "update-status"
                        };
                    $.ajax({
                        type: "POST",
                        url: "{{ route('admin.ship-codes.createOrUpdate') }}",
                        dataType: "json",  
                        data: { data: JSON.stringify( data ) },
                        success: function(data) {
                            $("#ship-code").val('');
                            $("#ship-code").focus();
                        },
                        error: function(data) {
                            var res = data.responseJSON;
                            $("#ship-code").val('');
                            $("#ship-code").focus();
                            if(res.data == 'out-vn'){
                                toastr["error"](`<p style="font-size: 1.1em">Mã vận đơn <b style="font-size: 1.3em">${getValue}</b> đã xuất kho VN!</p>`);
                            }else{
                                toastr["error"](`<p style="font-size: 1.1em">Mã vận đơn <b style="font-size: 1.3em">${getValue}</b> vừa được nhập. Vui lòng chờ sau 10 phút!</p>`);
                            }
                        }
                    });
                }
            })
        })
        function refresh(){
            window.location.reload();
        }
    </script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/orders/purchase_orders.css')) }}
@endsection