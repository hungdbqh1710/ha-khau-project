@extends('layouts.admin')

@section('title', trans('page-name.page_count_fees'))

@section('content')
    {!! $dataTable->table(['id' => 'count-fees']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/count_fees.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
