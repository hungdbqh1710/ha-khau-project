@extends('layouts.admin')

@section('title', trans('page-name.page_customer'))

@section('content')
    <div class="row">
        <div class="col-md-9">
            <form method="POST" id="search-form" class="form-inline" role="form">
                <div class="form-group">
                    <select name="status" id="filter-type" class="form-control">
                        <option value="" hidden>-- Lọc theo trạng thái --</option>
                        <option value="">Tất cả</option>
                        @foreach(config('data.is_active') as $value => $title)
                            <option value="{{ $value }}">{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-filter">Lọc</button>
            </form>
        </div>
        <div class="col-md-3">
            <div class="dt-buttons btn-group pull-right">
                @can('create_customers')
                    <button class="btn btn-default buttons-create" tabindex="0" aria-controls="users" type="button">
                        <span><i class="fa fa-plus"></i> Thêm mới</span>
                    </button>
                @endcan
                <button class="btn btn-default buttons-reset" tabindex="0" aria-controls="users" type="button" id="refresh">
                    <span><i class="fa fa-undo"></i> Làm mới</span>
                </button>
            </div>
        </div>
    </div>
    {!! $dataTable->table(['id' => 'customer-table']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/customer.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection