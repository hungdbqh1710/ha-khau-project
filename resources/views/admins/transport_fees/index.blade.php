@extends('layouts.admin')

@section('title', trans('page-name.page_transport_fees'))

@section('content')
    {!! $dataTable->table(['id' => 'transport-fees']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/transport_fees.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
