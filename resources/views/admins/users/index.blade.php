@extends('layouts.admin')

@section('title', trans('page-name.page_users'))

@section('content')
    <div class="row">
        <div class="col-md-9">
            <form method="POST" id="search-form" class="form-inline" role="form">
                <div class="form-group">
                    <select name="status" id="filter-type" class="form-control">
                        <option value="" hidden>-- Lọc theo trạng thái --</option>
                        <option value="">Tất cả</option>
                        @foreach(config('data.is_active') as $value => $title)
                            <option value="{{ $value }}">{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-filter">Lọc</button>
            </form>
        </div>
        <div class="col-md-3">
            <div class="dt-buttons btn-group pull-right">
                @can('create_users')
                    <button class="btn btn-default buttons-create" tabindex="0" aria-controls="users" type="button">
                        <span><i class="fa fa-plus"></i> Thêm mới</span>
                    </button>
                @endcan
                <button class="btn btn-default buttons-reset" tabindex="0" aria-controls="users" type="button" id="refresh">
                    <span><i class="fa fa-undo"></i> Làm mới</span>
                </button>
            </div>
        </div>
    </div>
    {!! $dataTable->table(['id' => 'users']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/users/users.js')) }}
    {!! $dataTable->scripts() !!}

@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
    {{ Html::style(mix('assets/admin/css/users/users.css')) }}
@endsection