@extends('layouts.admin')

@section('title', trans('page-name.page_permissions'))

@section('content')
    {!! $dataTable->table(['id' => 'permissions']) !!}
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/permissions.js')) }}
    {!! $dataTable->scripts() !!}
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
