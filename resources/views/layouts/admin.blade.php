@extends('layouts.app')

@section('body_class','nav-sm')

@section('page')
    <div class="container body">
        <div class="main_container">
            @section('header')
                @include('partials._navigation')
                @include('partials._header')
            @show
            @yield('left-sidebar')

            <div class="right_col" role="main">
                <div class="page-title">
                    <div class="title_left">
                        <h1 class="h3 text-uppercase"><strong>@yield('title')</strong></h1>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-dismissible fade in hidden" id="response" role="alert">
                            <button type="button" class="close" id="js-close-alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            <strong id="response-text"></strong>
                        </div>
                        @include('components.notification')
                        @yield('toolbar')
                        <div class="x_panel">
                            <div class="x_content">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('styles')
    {{ Html::style(mix('assets/admin/css/admin.css')) }}
@endsection

@section('scripts')
    <script>
        window.App = @json([
            'permissions' => optional(auth()->user())->getAllPermissions()->pluck('name'),
            'roles' => optional(auth()->user())->roles()->pluck('name'),
            'signedIn' => auth()->check()
        ]);
    </script>
    {{ Html::script(mix('assets/admin/js/admin.js')) }}
@endsection