@extends('layouts.frontend')

@section('page')
    <!-- Blog Details start -->
    <div class="wshipping-content-block">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">

                    @yield('content')

                    <!-- Comment form end -->
                </div>

                <!-- Right sidebar start -->
                <div class="col-12 col-lg-4">
                    <div class="cat-Search">
                        <form action="#">
                            <input type="search" placeholder="Enter kyewords.." class="form-control">
                        </form>
                    </div>
                    <div class="right-block left-menu menu-with-title">
                        <h4 class="title-with-bg">Category</h4>
                        <ul>
                            <li class="active"><a href="" title="Sea Freight">Sea Freight</a></li>
                            <li><a href="" title="Railway Logistics">Railway Logistics</a></li>
                            <li><a href="" title="Cargo Services">Cargo Services</a></li>
                            <li><a href="" title="Latest Shipments">Latest Shipments</a></li>
                        </ul>
                    </div>
                    <div class="left-block blog-tag">
                        <h3 class="heading3-border text-uppercase">Tags</h3>
                        <a href="#" title="Logistics">Logistics</a>
                        <a href="#" title="How To Build">How To Build</a>
                        <a href="#" title="Curgo Services">Curgo Services</a>
                        <a href="#" title="Tips">Tips</a>
                        <a href="#" title="Latest Offers">Latest Offers</a>
                        <a href="#" title="Sea & Trucking">Sea &amp; Trucking</a>
                    </div>
                </div>
                <!-- Right sidebar end -->
            </div>
        </div>
    </div>
    <!-- Blog Details end -->
@endsection