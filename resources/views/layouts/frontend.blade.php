<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title -->
    <title>Hà Khẩu Logistics</title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('frontend/assets/images/fevicon.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap.min.css') }}">

    <!-- Font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/font-awesome.min.css') }}">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/animate.min.css') }}">

    <!-- OwlCarousel CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.carousel.css') }}">

    <!-- Magnific popup CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/magnific-popup.css') }}">

    <!-- chat CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/chat.css') }}">

    <!-- Slicknav CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/slicknav.min.css') }}">

    <!-- Date picker CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap-datepicker.min.css') }}">

    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css') }}">

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/responsive.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/custom.css') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('style')

    <!-- jQuery -->
    <script src="{{ asset('frontend/assets/js/jquery-2.1.3.min.js') }}"></script>
    <!-- Global site tag (gtag.js) - Google Ads: 835156473 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-835156473"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-835156473'); </script>
</head>
<body>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : "{{ config('services.facebook.app_id') }}",
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.2'
            });
        };
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
     attribution=setup_tool
     page_id="1091420757657374"
     logged_in_greeting="Hà Khẩu Logistics xin chào quý khách, chúng tôi giúp gì được cho quý khách ?"
     logged_out_greeting="Cảm ơn quý khách đã ghé thăm. Chúc quý khách 1 ngày tốt lành !">
    </div>

<!-- skiptocontent start ( This section for blind and Google SEO, Also for page speed )-->
  <a id="skiptocontent" href="#maincontent">skip navigation</a>
  <!-- skiptocontent End -->

  <!-- Main Wrapper Start -->
  <div class="main-wrapper">
<header>
    <!-- Header top area start -->
	<div class="header-top-area">
       <div class="container">
          <div class="row">
             <div class="col-12 text-center">
                  <div class="actions text-right inline-block text-xs-center">
                    <a href="https://chrome.google.com/webstore/detail/hà-khẩu-logistics-đặt-hàn/ghomkbcbllfkpicifgjlcgledhglgloj?hl=vi&authuser=1" target="_blank" class="btn prefix-btn chrome">
                        <i class="fa fa-chrome prefix"></i>Cài đặt công cụ cho Chrome</a>
                    <a href="https://chrome.google.com/webstore/detail/hà-khẩu-logistics-đặt-hàn/ghomkbcbllfkpicifgjlcgledhglgloj?hl=vi&authuser=1" target="_blank" class="btn prefix-btn coccoc">
                        <i class="fa fa-chrome prefix"></i>Cài đặt công cụ cho Cốc Cốc</a>
                  </div>
                  <div class="top-menu inline-block">
                  <ul>
                      <li>
                          <a data-toggle="popover" data-container="body" data-placement="bottom" data-html="true" href="#"><i class="fa fa-envelope" title="Gửi phản hồi"></i></a>
                          <div id="js-popover-content-feedback" class="container" hidden>
                              <h4>GÓP Ý</h4>
                              <form action="" class="form-send-feedback">
                                  <span class="js-feedback-message" hidden></span>
                                  <div class="form-group">
                                      @auth(config('auth.guard_customers.model'))
                                          <input type="hidden" name="phone_number" class="form-control js-feedback-phone-number" value="{{ auth('customers')->user()->phone_number }}">
                                      @else
                                          <span>Số điện thoại</span>
                                          <input type="text" name="phone_number" class="form-control js-feedback-phone-number" required autofocus>
                                      @endauth
                                          <span class="text-danger js-phone-number-error" hidden></span>
                                  </div>
                                  <div class="form-group">
                                      <span>Nội dung</span>
                                      <textarea class="form-control js-feedback-content" name="content" rows="5" required autofocus></textarea>
                                      <span class="text-danger js-content-error" hidden>Nội dung không được bỏ trống</span>
                                  </div>
                                  <div class="form-group popover-footer-feedback">
                                      <button type="button" class="pull-right btn btn-sm btn-danger js-btn-popover-submit" data-url-send-feedback="{{ route('feedback.send') }}">Gửi</button>
                                      <button type="button" class="pull-right btn btn-sm btn-default js-btn-popover-close" data-dismiss="modal">Đóng</button>
                                  </div>
                              </form>
                          </div>
                      </li>
                      @auth(config('auth.guard_customers.model'))
                          <li>
                              <a data-toggle="notification" data-container="body" data-placement="bottom" data-html="true" href="#">
                                  <i class="fa fa-bell" title="Xem thông báo"></i>
                                  <span class="badge badge-danger">{{ auth('customers')->user()->unreadNotifications->count() }}</span>
                              </a>
                              <div id="js-popover-content-notification" class="container" hidden>
                                  <div class="notification-header">
                                      <span class="pull-left"><h4>Thông báo</h4></span>
                                      <span class="pull-right"><a href="{{ route('customers.notifications.index') }}">Xem tất cả</a></span>
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="notification-body">
                                      @if(auth('customers')->user()->notifications->count() > 0)
                                          <ul class="list-unstyled float-left">
                                              @foreach(auth('customers')->user()->notifications->take(5) as $notification)
                                                  <li class="notification-item">
                                                      <div class="order-noti">
                                                          <div class="notification-text">
                                                              Đơn hàng <a href="{{ route('customers.notifications.read', ['id' => $notification->id, 'redirect' => $notification->data['link']]) }}">{{ $notification->data['order_number'] }}</a> đã được báo giá
                                                          </div>
                                                      </div>
                                                      <span class="noti-time">{{ $notification->created_at->format('h:i d/m/Y') }}</span>
                                                  </li>
                                              @endforeach
                                          </ul>
                                      @else
                                          Không có thông báo nào.
                                      @endif
                                  </div>
                              </div>
                          </li>
                      @endauth
                      @auth(config('auth.guard_customers.model'))
                          <li>
                              <div class="dropdown">
                                  <a class="dropdown-toggle dropdown-toggle-customer" data-toggle="dropdown">Xin chào {{ auth('customers')->user()->name }}</a>
                                  <ul class="dropdown-menu dropdown-menu-customer">
                                      <li><a href="{{ route('customers.showProfile') }}"><i class="fa fa-user"></i> Thông tin</a></li>
                                      <li>
                                          <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                              <i class="fa fa-sign-out"></i>Đăng xuất
                                          </a>
                                      </li>
                                      <form id="logout-form" action="{{ route('customers.logout') }}" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                      </form>
                                  </ul>
                              </div>
                          </li>
                      @else
                          <li><a href="#" data-toggle="modal" data-target="#{{ LOGIN_MODAL }}"><i class="fa fa-sign-in"></i> Đăng nhập</a></li>
                          <li><a href="#" data-toggle="modal" data-target="#{{ REGISTER_MODAL }}"><i class="fa fa-unlock-alt"></i> Đăng ký</a></li>
                      @endauth
                  </ul>
                </div>
             </div>
             <!-- <div class="col-12 col-lg-6">
                
             </div> -->
          </div>
          <div class="row">
            <div class="col search-bar">
                 <div class="top-contact">
                    <div class="input-group input-group-search">
                        <div class="newsletter">
                            <select class="js-site-search">
                                @foreach(config('data.list_sites') as $key => $site)
                                    <option value="{{ $key }}">{{ $site['site_name'] }}</option>
                                @endforeach
                            </select>
                            <input type="text" placeholder="Nhập từ khóa để tìm kiếm" class="news-input input-search">
                            <button value="" class="news-btn js-btn-search" data-url-search="{{ route('searchProduct') }}"><i class="fa fa-search"></i></button>
                        </div>
                        @auth('customers')
                            <a href="{{ route('orders.purchases.index') }}" class="btn btn-danger btn-list-order"><i class="fa fa-list"></i> <span>Danh sách đơn hàng mua hộ</span></a>
                        @endauth
                        <a href="#" class="btn btn-danger btn-shipping-order" data-toggle="modal" @auth('customers') data-target="#{{ TRANSPORT_ORDER_MODAL }}" @else data-target="#{{ NOTIFICATION_LOGIN_MODAL }}" @endauth><i class="fa fa-truck"></i><span> Đặt đơn vận chuyển</span></a>
                        <a href="{{ route('ship_codes.index') }}" class="btn btn-danger btn-shipping-order" ><i class="fa fa-search" style="color: red"></i><span> Tra cứu mã vận đơn</span></a>
                    </div>
                 </div>
             </div>
          </div>
       </div>
    </div>
    <!-- Header top area End -->

    <!-- Header area start -->
	<div class="header-area">
      <div class="container">
        <!-- Site logo Start -->
        <div class="logo">
          <a href="{{ route('home-page') }}" title="Hà Khẩu Logistics"><img src="{{ asset('frontend/assets/images/logo.png') }}" alt="Hà Khẩu Logistics"/></a>
        </div>
        <!-- Site logo end -->
        <div class="mobile-menu-wrapper"></div>
        <!-- Search Start -->
        <!-- <div class="dropdown header-search-bar">
          <form action="index.html">
            <span class="" data-toggle="dropdown"><i class="fa fa-search" aria-hidden="true"></i></span>
            <input type="search" placeholder="kyewords.." class="dropdown-menu search-box">
          </form>
        </div> -->
        <!-- Search End -->

        <!-- Main menu start -->
        <nav class="mainmenu">
          <ul id="navigation">
             <li class="{{ request()->is('/') ? 'nav-active' : '' }}"><a href="{{ route('home-page') }}">Trang chủ</a></li>
              @render(\App\ViewComponents\CmsPageComponent::class)
              <li class="{{ request()->is('blog*') ? 'nav-active' : '' }}"><a href="{{ route('blog.index') }}">Tin tức</a></li>
          </ul>
        </nav>
        <!-- Main menu end -->
      </div>
    </div>
    <!-- Header area End -->
  </header>
 <!-- Preloader start -->
  <div class="wshipping-site-preloader-wrapper">
     <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
     </div>
  </div>
  <!-- Preloader End -->

  <div id="app">
      @yield('page')
  </div>

  <!-- Footer start -->
  @render(\App\ViewComponents\FooterComponent::class)

  <!-- Footer end -->
  </div>
  <!-- Main Wrapper end -->

  <!-- Start scroll top -->
  <div class="scrollup"><i class="fa fa-angle-up"></i></div>
  <!-- End scroll top -->
  @include('frontend.transport-orders.transport_order_modal')
  @include('frontend.transport-orders.notification_login_modal')
  @include('frontend.auth.login')
  @include('frontend.auth.register')
  @include('components.error_modal')

  <!-- Tether JS -->
  <script src="{{ asset('frontend/assets/js/js.js') }}"></script>
  <script src="{{ asset('frontend/assets/js/custom.js') }}"></script>
  <!-- Facebook Pixel Code -->
  <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window,document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1563877827172834');
      fbq('track', 'PageView');
  </script>
  <noscript>
      <img height="1" width="1" src="https://www.facebook.com/tr?id=1563877827172834&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
  <script>
      let app = new Application();
      let hasError = '{{ $errors->any() }}';
      let modalTarget = '{{ Session::get('modal_target') }}';
      app.showModalValidateFail(hasError, modalTarget);
      app.focusFirstInputModal();
      app.initDatePicker();

      let slide = new Slide();
      slide.initSlide();

      let feedback = new Feedback();
      feedback.showFeedbackPopover();
      feedback.sendFeedback();
      feedback.showNotification();

      let customer = new Customer();
      customer.searchProduct();
      customer.getVerifyCode();
      customer.register();

      let order = new Order();
      order.addShippingOrderItemHtmlForm();
      order.addNameInputWhenCreateTransportOrder();
      order.removeTransportOrderForm();
  </script>

  @yield('script')
</body>
</html>