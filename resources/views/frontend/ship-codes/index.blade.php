@extends('layouts.frontend')
@section('style')
    {{ Html::style(asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css')) }}
@stop
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.page_search_ship_code')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-md-3">
                @include('components.site_bar')
            </div>
            <div class="col-md-9">      
                <form action="{{ route('ship_codes.search') }}" method="GET" class="row pt-4 form-inline justify-content-center">
                    <div class="col-md-9">
                        <div class="input-group">
                            <input name="ship_code" value="{{ request('ship_code') }}" type="text" class="form-control search-order" placeholder="@lang('form.placeholder_transport_code')">
                            <button class="input-group-addon" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                <br>
                </form>
                <div class="row pt-4 purchase-order-show">
                        @if(isset($ship_codes) && count($ship_codes))
                            <div class="col-md-12">
                                <div class="order-list">
                                    <div class="table table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>@lang('form.transport_code')</th>
                                                <th>@lang('table.status')</th>
                                                <th>@lang('table.date_time')</th>
                                                <th>@lang('form.total_price')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($ship_codes as $ship)               
                                                <tr>
                                                    <td>{{ $ship->ship_code }}</td>
                                                    <td>{{ config('data.shipping_type')[$ship->status] }}</td>
                                                    <td>{{ $ship->updated_at->format('d/m/Y H:m:s') }}</td>
                                                    <td>
                                                        {{ number_format($ship->total_price) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="mt-4">
                                {{ $ship_codes->appends(request()->query())->links('vendor.pagination.bootstrap-4') }}
                            </div> --}}
                        @else
                            <div class="col-lg-9 offset-lg-1">
                                <h3 class="text-center">Không tìm thấy đơn hàng nào</h3>
                            </div>
                        @endif
                    </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(function() {
            var app = new Application();
            app.initDatePickerValidateEndDate();
        });
    </script>
@stop