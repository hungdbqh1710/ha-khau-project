@extends('layouts.frontend')

@section('page')
      <!-- Slider Start -->
  <div class="homepage-slides-wrapper">
    <div class="homepage-slides text-center">
       <!-- Slider item1 start-->
       <div class="single-slide-item slide-bg-1">
       </div>
       <!-- Slider item1 end-->

       <!-- Slider item2 start-->
       <div class="single-slide-item slide-bg-2">
       </div>
       <!-- Slider item2 end-->
     </div>
   </div>
   <!-- Slider End -->

  @render(\App\ViewComponents\SlideComponent::class)

   <!-- Why Choose start -->
   <div class="wshipping-content-block">
     <div class="container">
        <div class="row">
            <div class="col-lg-12 wow fadeInUp">
                <div class="box-title">
                    <div class="box-title-name"><span>Về chúng tôi</span></div>
                </div>
            </div>
           <div class="col-12 col-lg-6">
              <div class="why-choose-us-content wow fadeInUp">
                <img class="img img-responsive" src="{{ asset('frontend/assets/images/banner-temp.jpg') }}" alt=""/>
                <p>Ngày nay, việc mua sắm hàng hóa đa quốc gia đã không còn là điều xa lạ với các doanh nghiệp, khái niệm “biên giới” đã bị xóa bỏ nhất là trong thời kì hội nhập hiện nay. Không nằm ngoài xu thế, hiện tại đặt hàng Trung Quốc  và cụ thể là order hàng Taobao, 1688, Tmall đang được các doanh nghiệp đẩy mạnh,  với mục đích tìm kiếm nguồn hàng giá rẻ, độc, lạ mà thị trường trong nước chưa đáp ứng được. </p>
             </div>
           </div>
           <div class="col-12 col-lg-6">
              <div class="why-choose-us wow fadeInRight">
                  <div class="why-choose-us-icon">
                     <i class="fa fa-handshake-o"></i>
                     Đặt hàng, Ship hàng quốc tế
                  </div>
                   <div class="why-choose-us-icon">
                      <i class="fa fa-unlock-alt"></i>
                      Chuyển hàng 2 chiều Trung - Việt
                   </div>
                   <div class="why-choose-us-icon">
                      <i class="fa fa-thumbs-o-up"></i>
                      Chuyển tiền, nạp tiền Alipay, Wechat
                   </div>
                   <div class="why-choose-us-icon">
                      <i class="fa fa-map-marker"></i>
                      Hỗ trợ và tư vấn chuyên nghiệp
                   </div>
                   <div class="why-choose-us-icon">
                      <i class="fa fa-thumbs-o-up"></i>
                      Dễ dàng quản lý đơn hàng
                   </div>
                   <div class="why-choose-us-icon">
                      <i class="fa fa-map-marker"></i>
                      Giao hàng tận nơi
                   </div>
              </div>
           </div>
        </div>
     </div>
   </div>
   <!-- why choose End -->
  <div class="group-section">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="box-title wow fadeInUp">
                      <div class="box-title-name"><span>Dịch vụ cung cấp</span></div>
                  </div>
                  <div class="box-content clearfix">
                      <div class="column-left pull-left wow fadeInLeft">
                          <div class="grid">
                              <div class="img-reason">
                                  <img src="{{ asset('frontend/assets/images/icon-reason-checked.jpg') }}" alt="check">
                              </div>
                              <div class="content-reason">
                                  <div class="content-reason-title"><h3>Phiên dịch</h3></div>
                                  <div class="content-reason-des">Với đội ngũ nhân viên tốt nghiệp tại học viện Châu hồng hà cùng nhiều năm kinh nghiệm trong lĩnh vực Logistics chúng tôi tự tin là điểm tựa vững chắc cho khách hàng khi thương thảo với các đối tác Trung quốc</div>
                              </div>
                          </div>
                          <div class="grid second-left-grid">
                              <div class="img-reason">
                                  <img src="{{ asset('frontend/assets/images/icon-reason-checked.jpg') }}" alt="check">
                              </div>
                              <div class="content-reason">
                                  <div class="content-reason-title"><h3>Tìm nguồn hàng giá rẻ theo yêu cầu</h3></div>
                                  <div class="content-reason-des">Việc mua hàng trên các trang bán hàng của trung quốc nếu không thông thạo về ngôn ngữ cũng như những kỹ năng bạn có thể mua sản phẩm bị đắt hơn và thậm trí chất lượng cũng không được tốt như mong muốn, dịch vụ tìm nguồn hàng giá rẻ theo yêu câu của chúng tôi sẽ giúp bạn giải quyết các khó khăn</div>
                              </div>
                          </div>
                          <div class="grid">
                              <div class="img-reason">
                                  <img src="{{ asset('frontend/assets/images/icon-reason-checked.jpg') }}" alt="check">
                              </div>
                              <div class="content-reason">
                                  <div class="content-reason-title"><h3>Liên hệ nhà máy đặt gia công</h3></div>
                                  <div class="content-reason-des">Với lợi thế về quy mô sản xuất lớn, giá thành sản phẩm khi sản xuất tại trung quốc rất rẻ, vì vậy việc gia công một số mặt hàng hoặc một số chi tiết bộ phận sau đó lắp dáp tại việt nam sẽ đem lại nhiều lợi ích cho nhà sản xuất trong nước, dịch vụ của chúng tôi giúp bạn tìm được các nhà máy có thể đáp ứng được các yêu cầu từ phía khách hàng</div>
                              </div>
                          </div>
                      </div><!-- v2-reason-col -->
                      <div class="column-right pull-right wow fadeInRight">
                          <div class="grid">
                              <div class="img-reason">
                                  <img src="{{ asset('frontend/assets/images/icon-reason-checked.jpg') }}" alt="check">
                              </div>
                              <div class="content-reason">
                                  <div class="content-reason-title"><h3>Order, đặt hàng 1688.com, taobao.com, tmall.com</h3></div>
                                  <div class="content-reason-des">Chúng tôi cung cấp dịch vụ đặt hàng hộ từ các trang thương mại điện tử hàng đầu của trung quốc với mức phí rẻ nhất toàn quốc, cùng với kinh nghiệm và sự nhiệt tình chúng tôi tin chắc sẽ mang lại sự hài lòng cho khách hàng</div>
                              </div>
                          </div>
                          <div class="grid">
                              <div class="img-reason">
                                  <img src="{{ asset('frontend/assets/images/icon-reason-checked.jpg') }}" alt="check">
                              </div>
                              <div class="content-reason">
                                  <div class="content-reason-title"><h3>Vận chuyển hàng từ trung quốc về Việt Nam</h3></div>
                                  <div class="content-reason-des">Chi phí logistics là một chi phí quyết định đến giá thành của sản phẩm, hiểu được điều này chúng tôi xây dựng dịch vụ vận chuyển hàng từ trung quốc về việt nam với phương châm giảm chi phí đến mức tối đa cho khách hàng để các sản phẩm khi về đến việt nam có thể cạnh tranh tốt hơn với các đối thủ</div>
                              </div>
                          </div>
                          <div class="grid">
                              <div class="img-reason">
                                  <img src="{{ asset('frontend/assets/images/icon-reason-checked.jpg') }}" alt="check">
                              </div>
                              <div class="content-reason">
                                  <div class="content-reason-title"><h3>Vận chuyển hàng ký gửi </h3></div>
                                  <div class="content-reason-des">Chúng tôi nhận vận chuyển hàng ký gửi cho các cá nhân, công ty tự đặt hàng, thông tin người nhận hàng và địa chỉ kho:  Người nhận: 杜玉梅_(tên khách) địa chỉ nhận:云南省 红河哈尼族彝族自治州 河口瑶族自治县 河口镇 云南省 红河哈尼族彝族自治州 河口瑶族自治县 河口县河边街15号 (手机: 13529499439)</div>
                              </div>
                          </div>
                      </div><!-- v2-reason-col -->
                      <div class="background-reason hidden-sm hidden-xs">
                          <img src="{{ asset('frontend/assets/images/bg-reason.jpg') }}" alt="check">
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- why choose End -->
  <div class="bg-gray group-section">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 wow fadeInUp">
                  <div class="box-title">
                      <div class="box-title-name"><span>Quy trình</span> vận chuyển hàng hóa</div>
                  </div>
              </div>
          </div>
          <div class="row border-service">
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service_1.png') }}" alt="check">
                      </div>
                      <div class="number-service">1</div>
                      <div class="title-service">Kho hàng của nhà bán hàng Trung Quốc</div>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service_2.png') }}" alt="check">
                      </div>
                      <div class="number-service">2</div>
                      <div class="title-service">Kho hàng của Hà khẩu&nbsp;logistics tại Trung Quốc&nbsp;(Giáp cửa khẩu quốc tế Lào Cai)</div>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service_3.png') }}" alt="check">
                      </div>
                      <div class="number-service">3</div>
                      <div class="title-service">Kho Hà khẩu&nbsp;logistics tại lào cai (Phân loại, kiểm đếm)</div>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service_4.png') }}" alt="check">
                      </div>
                      <div class="number-service">4</div>
                      <div class="title-service">Địa chỉ khách hàng</div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="bg-gray group-section">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 wow fadeInUp">
                  <div class="box-title">
                      <div class="box-title-name"><span>Quy trình đặt hàng</span></div>
                  </div>
              </div>
          </div>
          <div class="row border-service">
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/extension-icon.png') }}" alt="check">
                      </div>
                      <div class="number-service">1</div>
                      <div class="title-service">Cài đặt extension</div>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service3_2.png') }}" alt="check">
                      </div>
                      <div class="number-service">2</div>
                      <div class="title-service">Đặt hàng qua extension</div>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service2_3.png') }}" alt="check">
                      </div>
                      <div class="number-service">3</div>
                      <div class="title-service">Hà khẩu logistics sẽ báo giá</div>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="service-index">
                      <div class="icon-service">
                          <img src="{{ asset('frontend/assets/images/icon_service2_4.png') }}" alt="check">
                      </div>
                      <div class="number-service">4</div>
                      <div class="title-service">Khách hàng thanh toán</div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="section-img1 mt-3">
      <div class="container">
          <div class="row">
              <div class="col-lg-12 wow fadeInUp">
                  <div class="box-title">
                      <div class="box-title-name"><span>Tổng các chi phí </span></div>
                  </div>
                  <img src="{{ asset('frontend/assets/images/select-img.png') }}" alt="check">
              </div>
          </div>
      </div>
  </div>
  <div class="modal fade" id="admodal" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-header">
              <span class="close-popup">
                  <i class="fa fa-times" aria-hidden="true"></i>
              </span>
          </div>
          <div class="modal-content">
              @if($popup->content != null)
                  <a href="{{ $popup->link }}" target="_blank"><img src="/storage/app/public/slide-images/{{ $popup->content }}" alt=""></a>
              @endif
          </div>
      </div>
  </div>
@endsection
@section('script')
    @if($popup->content != null)
        <script>
            $(function() {
                if (!sessionStorage.adModal) {
                    setTimeout(function () {
                        $('#admodal').find('.item').first().addClass("visible");
                        $('#admodal').modal();
                    }, 0);
                    sessionStorage.adModal = 1;
                }
                $(document).on('click', '.close-popup', function () {
                    $('#admodal').modal('hide');
                })
            });
        </script>
    @endif
@stop