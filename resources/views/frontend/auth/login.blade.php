<div class="modal fade" id="{{ LOGIN_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_login')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('customers.login',request()->query()) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>@lang('form.email')</label>
                        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' parsley-error' : ''}}" value="{{ old('email') }}" required>
                        
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_password')</label>
                        <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' parsley-error' : ''}}" value="{{ old('password') }}" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <a href="{{ route('customers.password.request') }}">@lang('form.title_forget_password')</a><br />
                            <a href="#" data-toggle="modal" data-target="#register-modal">@lang('form.title_register')</a>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                            <button type="submit" class="btn btn-primary pull-right">@lang('form.login_button')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>