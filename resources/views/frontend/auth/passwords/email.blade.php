@extends('layouts.auth')

@section('body_class','login')

@section('content')
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ Form::open(['route' => 'customers.password.email']) }}
                        <h1>@lang('form.forgot_password_header')</h1>
                        <div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' parsley-error' : ''}}" name="email" value="{{ old('email') }}"
                                   placeholder="@lang('form.email')" required autofocus>
                            @if($errors->has('email'))
                                <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required">{{ $errors->first('email') }}</li>
                                </ul>
                            @endif
                        </div>
                        <div>
                            <button class="btn btn-primary form-control submit" type="submit">@lang('form.send_link_reset')</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                        </div>
                        <a class="reset_pass" href="{{ route('home-page') }}">
                            @lang('form.login_header')
                        </a>
                    {{ Form::close() }}
                </section>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @parent

    {{ Html::style(mix('assets/auth/css/passwords.css')) }}
@endsection
