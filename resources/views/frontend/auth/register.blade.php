<div class="modal fade" id="{{ REGISTER_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_register')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('customers.register') }}" class="js-from-register">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>@lang('form.title_name') <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control js-name" value="{{ old('name') }}" placeholder="@lang('form.placeholder_name')" required>
                        <span class="text-danger js-name-message"></span>
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.email') <span class="text-danger">*</span></label>
                        <input type="email" name="email" placeholder="@lang('form.placeholder_email')" value="{{ old('email') }}" class="form-control js-email" required>
                        <span class="text-danger js-email-message"></span>
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_phone_number') <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control js-phone-number" name="phone_number" value="{{ old('phone_number') }}" placeholder="@lang('form.placeholder_phone_number')" required>
                            <span class="input-group-addon js-get-verify-code" data-url-get-verify-code="{{ route('api.getVerifyCode') }}" data-total-time-countdown="{{ config('speed-sms.pin-time-to-live') }}">@lang('form.title_get_verify_code')</span>
                        </div>
                        <span class="text-primary" hidden>@lang('form.time_down_for_verify_code')<span id="js-pin-time-to-live"></span></span>
                        <span class="text-danger js-phone-number-message"></span>
                        @if($errors->has('phone_number'))
                            <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_phone_verification') <span class="text-danger">*</span></label>
                        <input type="number" name="phone_verification" class="form-control js-phone-verification" value="{{ old('phone_verification') }}" placeholder="@lang('form.placeholder_phone_verification')" required>
                        <span class="text-danger js-phone-verification-message"></span>
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_password') <span class="text-danger">*</span></label>
                        <input type="password" name="password" class="form-control js-password" placeholder="@lang('form.placeholder_password')" required>
                        <span class="text-danger js-password-message"></span>
                        @if($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_password_confirm') <span class="text-danger">*</span></label>
                        <input type="password"  name="password_confirmation" class="form-control js-password-confirmation" placeholder="@lang('form.placeholder_password_confirm')" required>
                        <span class="text-danger js-password-confirmation-message"></span>
                    </div>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                    <button type="button" class="btn btn-primary pull-right js-btn-register" data-url-verify-code="{{ route('api.verifyCode') }}">@lang('form.title_register')</button>
                </form>
            </div>
        </div>
    </div>
</div>
