@extends('layouts.frontend')

@section('page')
    <!-- Breadcroumbs start -->
    <div class="wshipping-content-block wshipping-breadcroumb inner-bg-1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <h1>{{ $cmsPage->name }}</h1>
                    <a href="{{ route('home-page') }}" title="Trang chủ">Trang chủ</a> / {{ $cmsPage->name }}
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcroumbs end -->
    <div class="wshipping-content-block shipping-block">
        <div class="container">
            <h2 class="heading2-border mt0">{{ $cmsPage->title }}</h2>
            {!! $cmsPage->content !!}
        </div>
    </div>
@endsection