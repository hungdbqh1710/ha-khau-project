@extends('layouts.right_sidebar')

@section('content')
    <div class="row">
        @foreach($blogs as $blog)
            <div class="col-12 col-lg-6">
                <!-- Blog item start -->
                <div class="blog-item wow fadeInUp">
                    <div class="blog-item-bg" style="background-image: url(/storage/app/public/blog-images/{{$blog->image}})"></div>
                    <div class="blog-content">
                        <h3><a href="{{ route('blog.show', $blog->slug) }}">{!! str_limit($blog->title, config('data.limit_title')) !!}</a></h3>
                        <div class="blog-post-by">
                            <ul>
                                <li><i class="fa fa-calendar"></i>{{ $blog->created_at }}</li>
                                <li><i class="fa fa-user"></i>{{ $blog->creator->name }}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="continue-read-blog"><a href="{{ route('blog.show', $blog->slug) }}">@lang('form.title_continue_reading')</a></div>
                </div>
                <!-- Blog item end -->
            </div>
        @endforeach
    </div>
    {{ $blogs->links() }}
@endsection