@extends('layouts.right_sidebar')

@section('content')
    <div class="row">
        <div class="single-blog-details">
            <div>
                <img src="{{ asset('storage/app/public/blog-images/'. $blog->image) }}" class="img-responsive single-blog-img" alt=""/>
            </div>
            <div class="single-blog-content">
                <h2>{{ $blog->title }}</h2>
                <h6>Posted on {{ $blog->created_at }} by {{ isset($blog->creator) ? $blog->creator->name : '' }}</h6>
                <span>{!! $blog->content !!}</span>
            </div>
        </div>
    </div>
@endsection