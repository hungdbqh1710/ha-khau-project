@extends('layouts.frontend')

@section('page')
    <div class="container">
        <h2 class="h3 text-uppercase">@lang('page-name.page_reports_debt')</h2>
        <hr>
        @include('components.notification')
        @if(isset($orders))
            <div class="row">
                <div class="col-lg-3">
                    @include('components.site_bar')
                </div>
                <div class="col-lg-9">
                    @php
                        $finalTotalPrice = $orders->pluck('final_total_price')->sum();
                        $totalTopup = auth('customers')->user()->customerProfile->remaining_amount;
                        $debt = $finalTotalPrice - $totalTopup;
                    @endphp
                    <div class="text-left">
                        <div class="col-lg-4 offset-lg-8">
                            <div class="text-right">
                                <span class="float-left"><strong>- Tổng tiền các đơn: </strong></span>
                                <span><strong>{{ number_format($finalTotalPrice) }} </strong></span>
                            </div>
                            <div class="text-right">
                                <span class="float-left"><strong>- Đã thanh toán: </strong></span>
                                <span><strong>{{ number_format($totalTopup) }} </strong></span>
                            </div>
                            <div class="text-right">
                                <span class="float-left"><strong>- Còn nợ: </strong></span>
                                <span><strong>{{ number_format($debt) }} </strong></span>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-6">
                            <v-table
                                    url="{{ route('api.orders.byCustomer') }}"
                                    :columns="{{ json_encode([
                                    ['title' => 'Mã', 'name' => 'order_number'],
                                    ['title' => 'Loại', 'name' => 'order_type_text'],
                                    ['title' => 'Tổng', 'name' => 'final_total_price_format'],
                                    ['title' => trans('form.title_created_at'), 'name' => 'created_at_format']
                                ]) }}"
                            />
                        </div>
                        <div class="col-lg-6">
                            <v-table
                                    url="{{ route('api.transactions.byCustomer') }}"
                                    :columns="{{ json_encode([
                                    ['title' => trans('form.title_topup_date'), 'name' => 'created_at_format'],
                                    ['title' => trans('form.title_amount_topup'), 'name' => 'amount_topup_format']
                                ]) }}"
                            />
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection