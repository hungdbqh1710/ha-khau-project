<div class="modal fade" id="{{ CREATE_DELIVERY_ADDRESS_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_create_delivery_address')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('customers.delivery-address.create') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>@lang('form.title_name') <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" placeholder="@lang('form.placeholder_name')" value="{{ old('name') }}" required autofocus>
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_phone_number') <span class="text-danger">*</span></label>
                        <input type="text" name="phone_number" class="form-control" placeholder="@lang('form.placeholder_phone_number')" value="{{ old('phone_number') }}" required>
                        @if($errors->has('phone_number'))
                            <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_city') <span class="text-danger">*</span></label>
                        <input type="text" name="city" class="form-control" placeholder="@lang('form.placeholder_city')" value="{{ old('city') }}" required>
                        @if($errors->has('city'))
                            <span class="text-danger">{{ $errors->first('city') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_district') <span class="text-danger">*</span></label>
                        <input type="text" name="district" class="form-control" placeholder="@lang('form.placeholder_district')" value="{{ old('district') }}" required>
                        @if($errors->has('district'))
                            <span class="text-danger">{{ $errors->first('district') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_commune') <span class="text-danger">*</span></label>
                        <input type="text" name="commune" class="form-control" placeholder="@lang('form.placeholder_commune')" value="{{ old('commune') }}" required>
                        @if($errors->has('commune'))
                            <span class="text-danger">{{ $errors->first('commune') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_street') <span class="text-danger">*</span></label>
                        <input type="text" name="street" class="form-control" placeholder="@lang('form.placeholder_street')" value="{{ old('street') }}" required>
                        @if($errors->has('street'))
                            <span class="text-danger">{{ $errors->first('street') }}</span>
                        @endif
                    </div>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                    <button type="submit" class="btn btn-primary pull-right">@lang('buttons.button_save')</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="{{ UPDATE_DELIVERY_ADDRESS_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_update_delivery_address')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('customers.delivery-address.update') }}" method="POST" class="form-update">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>@lang('form.title_name') <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="@lang('form.placeholder_name')" value="{{ old('name') }}" required autofocus>
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_phone_number') <span class="text-danger">*</span></label>
                        <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="@lang('form.placeholder_phone_number')" value="{{ old('phone_number') }}" required>
                        @if($errors->has('phone_number'))
                            <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_city') <span class="text-danger">*</span></label>
                        <input type="text" name="city" id="city" class="form-control" placeholder="@lang('form.placeholder_city')" value="{{ old('city') }}" required>
                        @if($errors->has('city'))
                            <span class="text-danger">{{ $errors->first('city') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_district') <span class="text-danger">*</span></label>
                        <input type="text" name="district" id="district" class="form-control" placeholder="@lang('form.placeholder_district')" value="{{ old('district') }}" required>
                        @if($errors->has('district'))
                            <span class="text-danger">{{ $errors->first('district') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_commune') <span class="text-danger">*</span></label>
                        <input type="text" name="commune" id="commune" class="form-control" placeholder="@lang('form.placeholder_commune')" value="{{ old('commune') }}" required>
                        @if($errors->has('commune'))
                            <span class="text-danger">{{ $errors->first('commune') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_street') <span class="text-danger">*</span></label>
                        <input type="text" name="street" id="street" class="form-control" placeholder="@lang('form.placeholder_street')" value="{{ old('street') }}" required>
                        @if($errors->has('street'))
                            <span class="text-danger">{{ $errors->first('street') }}</span>
                        @endif
                    </div>
                    <input type="hidden" id="id" name="id" value="{{ old('id') }}">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
                    <button type="submit" class="btn btn-primary pull-right">@lang('buttons.button_save')</button>
                </form>
            </div>
        </div>
    </div>
</div>