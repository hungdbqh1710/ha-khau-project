<a
    href="{{ route('admin.orders.showByCustomerId', $query->id) }}"
    class="btn btn-xs btn-info js-btn-list-order"
    title="Danh sách đơn hàng">
    <i class="fa fa-list-ul"></i>
</a>