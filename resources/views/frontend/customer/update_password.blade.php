@extends('layouts.frontend')

@section('style')
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection

@section('page')
    <div class="container">
        <h2 class="h3 text-uppercase">@lang('page-name.page_update_password')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-lg-3">
                @include('components.site_bar')
            </div>
            <div class="col-lg-9">
                <form action="{{ route('customers.updatePassword') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>@lang('form.title_current_password') <span class="text-danger">*</span></label>
                        <input type="password" name="current_password" class="form-control" placeholder="@lang('form.placeholder_current_password')" required autofocus>
                        @if($errors->has('current_password'))
                            <span class="text-danger">{{ $errors->first('current_password') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_new_password') <span class="text-danger">*</span></label>
                        <input type="password" name="password" class="form-control" placeholder="@lang('form.placeholder_new_password')" required>
                        @if($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@lang('form.title_password_confirm') <span class="text-danger">*</span></label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('form.placeholder_password_confirm')" required>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">@lang('buttons.button_save')</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/custom.js') }}"></script>
@endsection