@extends('layouts.frontend')

@section('style')
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css') }}">
@endsection

@section('page')
    <div class="container">
        <h2 class="h3 text-uppercase">@lang('page-name.page_customer_profile')</h2>
        <hr>
        @include('components.notification')
        @if(isset($customer) && isset($urlUpdate))
            <div class="row">
                <div class="col-lg-3">
                    @include('components.site_bar')
                </div>
                <div class="col-lg-9">
                    <div>
                        <form action="{{ $urlUpdate }}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_name')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="name" class="form-control" value="{{ $customer->name }}" autofocus>
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_phone_number')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="phone_number" class="form-control" value="{{ $customer->phone_number }}">
                                    @if($errors->has('phone_number'))
                                        <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.email')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="email" class="form-control" value="{{ $customer->email }}">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_gender')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="radio" name="gender" @if(isset($customerProfile) && $customerProfile->gender == GENDER_FEMALE) checked @endif value="0"> @lang('form.title_female')
                                    <input type="radio" name="gender" @if(isset($customerProfile) && $customerProfile->gender == GENDER_MALE) checked @endif value="1"> @lang('form.title_male')
                                    <input type="radio" name="gender" @if(isset($customerProfile) && $customerProfile->gender == GENDER_OTHER) checked @endif value="2"> @lang('form.title_other')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_birthday')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="birthday" class="form-control js-date-picker" value="{{ isset($customerProfile) && $customerProfile->birthday != null ? $customerProfile->birthday : old('birthday') }}">
                                    @if($errors->has('birthday'))
                                        <span class="text-danger">{{ $errors->first('birthday') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_bank_account')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="bank_account" class="form-control" value="{{ isset($customerProfile) ? $customerProfile->bank_account : '' }}">
                                    @if($errors->has('bank_account'))
                                        <span class="text-danger">{{ $errors->first('bank_account') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_bank_name')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="bank_name" class="form-control" value="{{ isset($customerProfile) ? $customerProfile->bank_name : '' }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_bank_account_owner')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="bank_account_owner" class="form-control" value="{{ isset($customerProfile) ? $customerProfile->bank_account_owner : '' }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>@lang('form.title_business_sector')</label>
                                </div>
                                <div class="col-lg-8 form-group">
                                    <input type="text" name="business_sector" class="form-control" value="{{ isset($customerProfile) ? $customerProfile->business_sector : '' }}">
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary pull-right">@lang('buttons.button_save')</button>
                            </div>
                        </form>
                    </div>
                    <div class="container-delivery-address">
                        <div class="well well-lg">
                            <h4>@lang('form.title_delivery_address')</h4>
                        </div>
                        @if(!empty($deliveryAddresses))
                            @foreach($deliveryAddresses as $deliveryAddress)
                                <div class="panel-delivery-address">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <input type="radio" class="js-is-default-delivery-address" data-url-update="{{ route('customers.delivery-address.updateDefault',$deliveryAddress->id ) }}" name="is_default" @if ($deliveryAddress->is_default == VALUE_TYPE_TRUE) checked @endif> @lang('form.title_default')
                                        </div>
                                        <div class="col-lg-10">
                                            <div><i class="fa fa-home"></i> {{ $deliveryAddress->street }} - {{ $deliveryAddress->commune }} - {{ $deliveryAddress->district }} - {{ $deliveryAddress->city }}</div>
                                            <div><i class="fa fa-phone"></i> {{ $deliveryAddress->phone_number }}</div>
                                            <div><i class="fa fa-user"></i> {{ $deliveryAddress->name }}</div>
                                            <div><i class="fa fa-edit"></i> <a href="#" data-url-update="{{ route('customers.delivery-address.update') }}" data-url-find="{{ route('customers.delivery-address.ajaxSearch', $deliveryAddress->id) }}" data-modal="#{{ UPDATE_DELIVERY_ADDRESS_MODAL }}" class="js-update-button">@lang('buttons.button_edit')</a></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            @endforeach
                        @endif
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#{{ CREATE_DELIVERY_ADDRESS_MODAL }}">@lang('buttons.button_add')</button>
                    </div>
                    <div class="container-delivery-address">
                        {{ $deliveryAddresses->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        @endif
    </div>
    @include('frontend.customer.create_update_delivery_address_modal')
    @include('components.error_modal');
    @include('components.success_modal');
@endsection

@section('script')
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(function () {
            let app = new Application();
            app.initDatePicker();
            app.buildDataUpdateModal();

            let customer = new Customer();
            customer.updateDeliveryAddress();
        })
    </script>
@endsection