@extends('layouts.frontend')
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_create_order')</h2>
        <hr>
        @include('components.notification')
        <div class="col-md-3">
            @include('components.site_bar')
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul id="progressbar">
                    <li class="active">@lang('form.title_unsent')</li>
                    <li>@lang('form.title_processing')</li>
                    <li>@lang('form.title_price_quotation')</li>
                    <li>@lang('form.title_ordered')</li>
                    <li>@lang('form.title_in_warehouse')</li>
                    <li>@lang('form.title_success')</li>
                </ul>
            </div>
            {{ Form::open(['route' => 'orders.purchases.store', 'class' => 'col-md-12']) }}
                {{--@foreach($orderTemps as $orderTemp)--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[product_image]', $orderTemp['product_image']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[product_name]', $orderTemp['product_name']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[product_link]', $orderTemp['product_link']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[property]', $orderTemp['property']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[qty]', $orderTemp['qty']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[price]', $orderTemp['price']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[price_range]', $orderTemp['price_range']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[product_id]', $orderTemp['item_id']) }}--}}
                    {{--{{ Form::hidden('product' . $orderTemp->id .'[shop_name]', $orderTemp['shop_name']) }}--}}
                {{--@endforeach--}}
                {{ Form::hidden('temp_ids', $tempIds) }}
                {{ Form::hidden('order_type', TYPE_ORDER_PURCHASE) }}
                @foreach($orderTemps as $orderTemp)
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ $orderTemp['product_image'] }}" alt="" srcset="">
                        </div>
                        <div class="col-md-8">
                            <h3>{{  $orderTemp['product_name'] }}</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <h4>@lang('form.title_price'): {{ number_format($orderTemp['price'], 2) }} NDT</h4>
                                        <h4>@lang('form.title_qty'): {{ $orderTemp['qty'] }}</h4>
                                        <h4>@lang('form.current_exchange_rate'): {{ number_format($currentRate) }}</h4>
                                        <h4>@lang('form.title_total_price'): {{ number_format($orderTemp['total_price'] * $currentRate) }} VNĐ</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 {{ $errors->has('customer_note') ? 'has-error' : '' }}">
                                    <textarea name="product{{$orderTemp->id}}[customer_note]" rows="2" class="form-control" placeholder="Ghi chú">{{ old('customer_note') }}</textarea>
                                    @if($errors->has('customer_note'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('customer_note') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                @endforeach
                @guest('customers')
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#{{ LOGIN_MODAL }}">@lang('form.title_add_to_order')</button>
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary pull-right js-add-to-order" type="button">
                                @lang('form.title_add_to_order')
                            </button>
                        </div>
                    </div>
                @endguest
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('script')
    <script>
        @parent
        $(function() {
            var cus = new Customer();
            cus.preventDoubleSubmit();
        });
    </script>
@endsection