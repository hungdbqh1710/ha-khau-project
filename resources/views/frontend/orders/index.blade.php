@extends('layouts.frontend')
@section('style')
    {{ Html::style(asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css')) }}
@stop
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_order_index')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-md-3">
                @include('components.site_bar')
            </div>
            <div class="col-md-9">
                <ul class="list-inline">
                    <li class="list-inline-item {{ request()->status ? '': 'filter-active' }}"><a href="{{ route('orders.purchases.index') }}">@lang('form.title_all')( {{ $allOrderCount }} )</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_UNSENT ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_UNSENT }}">@lang('form.title_unsent')( {{ $unsentOrderCount }} )</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_PROCESSING ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_PROCESSING }}">@lang('form.title_processing')( {{ $processingOrderCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_PRICE_QUOTATION ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_PRICE_QUOTATION }}">@lang('form.title_price_quotation')( {{ $priceQuotationOrderCount }} )</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_ORDERED ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_ORDERED }}">@lang('form.title_ordered')( {{ $orderedOrderCount }} )</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_IN_WAREHOUSE ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_IN_WAREHOUSE }}">@lang('form.title_in_warehouse')( {{ $inWareHouseOrderCount }} )</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_SUCCESS ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_SUCCESS }}">@lang('form.title_success')( {{ $orderSuccessCount }} )</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_CANCEL ? 'filter-active' : ''}}"><a href="{{ route('orders.purchases.index') }}?status={{ STATUS_CANCEL }}">@lang('form.title_cancel')( {{ $orderCancelCount }} )</a></li>
                </ul>
                <form action="{{ route('orders.purchases.index') }}" method="GET" class="pt-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="order_number" value="{{ request('order_number') }}" type="text" class="form-control search-order" placeholder="@lang('form.placeholder_order_code')" autofocus>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="transport_code" value="{{ request('transport_code') }}" type="text" class="form-control search-order" placeholder="@lang('form.placeholder_transport_code')">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="start_date" value="{{ request('start_date') }}" type="text" class="form-control js-start-date search-order" placeholder="@lang('form.placeholder_start_date')">
                                <span class="input-group-addon"><i class="fa fa-calendar order-icon-search"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="end_date" value="{{ request('end_date') }}" type="text" class="form-control js-end-date search-order" placeholder="@lang('form.placeholder_end_date')">
                                <span class="input-group-addon"><i class="fa fa-calendar order-icon-search"></i></span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="product_link" value="{{ request('product_link') }}" type="text" class="form-control search-order" placeholder="@lang('form.placeholder_product_link')">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="product_name" value="{{ request('product_name') }}" type="text" class="form-control search-order" placeholder="@lang('form.placeholder_product_name')">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-sm btn-danger pull-right ml-2"><i class="fa fa-filter"></i> @lang('form.filter')</button>
                            <a href="{{ route('orders.export', request()->query()) }}" class="btn btn-sm btn-info pull-right"><i class="fa fa-download"></i> @lang('form.export_excel')</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        <div class="row pt-4 purchase-order-show">
                @if($orders->count() > 0)
                    <div class="col-md-12">
                        <div class="order-list">
                            <div class="table table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>@lang('table.order_number')</th>
                                        <th>@lang('table.product_image')</th>
                                        <th>@lang('form.final_total_price')</th>
                                        <th>@lang('table.order_created_at')</th>
                                        <th>@lang('table.status')</th>
                                        <th>@lang('table.admin_note')</th>
                                        <th>@lang('table.action')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        @php $item = $order->purchaseOrderItems->first(); @endphp
                                        <tr>
                                            <td>{{ config('data.prefix_order_number') . $order->order_number }}</td>
                                            <td class="td-image">
                                                <img src="{{ optional($item)->product_image }}" alt="" srcset="" width="70px" height="70px" class="pull-left">
                                                <span class="product-name">
                                                    {{ optional($item)->product_name }}
                                                </span>
                                                <br>
                                                <a class="product-link" href="{{ optional($item)->product_link }}">{{ optional($item)->product_link }}</a>
                                            </td>
                                            <td>
                                                {{ number_format($order->final_total_price) }}
                                            </td>
                                            <td>{{ $order->created_at->format('d/m/Y') }}</td>
                                            <td>{{ config('data.order_status')[$order->status] }}</td>
                                            <td>{{ $order->admin_note }}</td>
                                            <td>
                                                <a href="{{ route('orders.purchases.show', $order->id) }}" class="btn btn-primary btn-sm detail-button"> @lang('form.view_order_details')</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        {{ $orders->appends(request()->query())->links('vendor.pagination.bootstrap-4') }}
                    </div>
                @else
                    <div class="col-lg-9 offset-lg-3">
                        <h3 class="text-center">Không tìm thấy đơn hàng nào</h3>
                    </div>
                @endif
            </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(function() {
            var app = new Application();
            app.initDatePickerValidateEndDate();
        });
    </script>
@stop