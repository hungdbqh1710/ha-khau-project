@extends('layouts.frontend')
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_deposite')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-uppercase">@lang('form.final_total_price') : {{ $order->final_total_price }}</h3>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
                <p>Thanh toán tiền mặt</p>
            </div>
            <div class="col-md-8">
                <ul>
                    <li>Công ty BSE Tầng 8, P.805, Tòa nhà Toyota Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội.</li>
                    <li>Công ty BSE Tầng 8, P.805, Tòa nhà Toyota Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội.</li>
                    <li>Công ty BSE Tầng 8, P.805, Tòa nhà Toyota Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội.</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <p>Thanh toán chuyển khoản</p>
            </div>
            <div class="col-md-8">
                <ul>
                    <li>Công ty BSE Tầng 8, P.805, Tòa nhà Toyota Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội.</li>
                    <li>Công ty BSE Tầng 8, P.805, Tòa nhà Toyota Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội.</li>
                    <li>Công ty BSE Tầng 8, P.805, Tòa nhà Toyota Thanh Xuân, 315 Trường Chinh, Thanh Xuân, Hà Nội.</li>
                </ul>
            </div>
        </div>
    </div>
@stop
