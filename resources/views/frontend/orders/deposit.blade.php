@extends('layouts.frontend')
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_deposite')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-uppercase">@lang('form.final_total_price') : {{ number_format($order->final_total_price) }}  @lang('form.title_vnd')</h3>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <p><strong>@lang('form.title_payment_cash_in_address')</strong></p>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-10 offset-lg-1">
                            {!! $paymentCashInfo->content !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 payment-transfer-detail">
                <div class="card">
                    <div class="card-header">
                        <p><strong>@lang('form.title_payment_transfer')</strong></p>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-10 offset-lg-1">
                            {!! $paymentTransferInfo->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop