@extends('layouts.frontend')
@section('style')
    {{ Html::style(asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css')) }}
@stop
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_order_show') {{ $order->order_number }}</h2>
        <hr>
        @include('components.notification')
        @if($order->status != STATUS_CANCEL)
            <div class="wshipping-content-block shipping-block row">
                <div class="steps shipping-form-block">
                    <ul id="progressbar">
                        <li class="{{ $order->status >= STATUS_UNSENT ? 'active' : '' }}">@lang('form.title_unsent')</li>
                        <li class="{{ $order->status >= STATUS_PROCESSING ? 'active' : '' }}">@lang('form.title_processing')</li>
                        <li class="{{ $order->status >= STATUS_PRICE_QUOTATION ? 'active' : '' }}">@lang('form.title_price_quotation')</li>
                        <li class="{{ $order->status >= STATUS_ORDERED ? 'active' : '' }}">@lang('form.title_ordered')</li>
                        <li class="{{ $order->status >= STATUS_IN_WAREHOUSE ? 'active' : '' }}">@lang('form.title_in_warehouse')</li>
                        <li class="{{ $order->status == STATUS_SUCCESS ? 'active' : '' }}">@lang('form.title_success')</li>
                    </ul>
                </div>
            </div>
        @endif
        <div class="row pb-3">
            <div class="col-md-12">
                @isset($defaultDeliveryAddress)
                    <span><strong>@lang('form.delivery_address'):</strong></span> {{ $defaultDeliveryAddress->name }}, @lang('form.delivery_phone_number'): {{ $defaultDeliveryAddress->phone_number }},
                    {{ $defaultDeliveryAddress->street }} - {{ $defaultDeliveryAddress->commune }} - {{ $defaultDeliveryAddress->district }} - {{ $defaultDeliveryAddress->city }}
                @endisset
            </div>
        </div>

        @if($order->purchaseOrderItems->count() > 0)
                @php $totalItemsPrice = $order->purchaseOrderItems->reduce(function ($total, $item) {
                                        return $total + $item->totalPrice();
                                    }, 0);
                $totalItems = $order->purchaseOrderItems->reduce(function ($total, $item) {
                                        return $total + $item->qty;
                                    }, 0);
                @endphp
                @if($order->purchaseOrderItems->count() > 0)
                    <div class="row mb-5 order-details">
                        <div class="col-md-12 order-top-right pt-4">
                            <div class="row">
                                <div class="col-md-3">
                                    <span><strong>Shop: </strong>{{ $order->shop_name }}</span>
                                </div>
                                <div class="col-md-3">
                                    <span><strong>@lang('form.status'): </strong>{{ config('data.order_status')[$order->status]}}</span>
                                </div>
                                <div class="col-md-3">
                                    <span><strong>@lang('form.china_order_code'): </strong>{{ $order->purchaseOrderItems->first()->cn_order_number }}</span>
                                </div>
                                <div class="col-md-3">
                                    <span><strong>@lang('form.title_transport_code_vn'): </strong>{{ $order->purchase_vn_transport_code }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="order-list" id="order-list">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>@lang('table.product_image')</th>
                                            <th>@lang('table.property')</th>
                                            <th>@lang('table.qty')</th>
                                            <th>@lang('table.price')</th>
                                            <th>@lang('form.title_exchange_rate')</th>
                                            <th>@lang('table.total_price')</th>
                                            <th>@lang('form.title_customer_note')</th>
                                            <th>@lang('table.admin_note')</th>
                                            @if($order->status < STATUS_PRICE_QUOTATION)
                                                <th>@lang('form.title_action')</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody class="item-body">
                                        @php $i = 0; @endphp
                                        @foreach($order->purchaseOrderItems as $item)
                                            <tr id="tr-{{ $item->id }}">
                                                <td>{{ ++$i }}</td>
                                                <td class="td-image">
                                                    <img src="{{ $item->product_image }}" alt="" srcset="" width="70px" height="70px" class="pull-left">
                                                    <span class="product-name">
                                                    {{ str_limit($item->product_name, config('data.limit_content')) }}
                                                </span>
                                                    <br>
                                                    <a class="product-link" href="{{ $item->product_link }}" target="_blank">{{ $item->product_link }}</a>
                                                </td>
                                                <td>
                                                    @if(!empty($item->property))
                                                        @foreach(json_decode($item->property) as $property)
                                                            - {{ $property }} <br>
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>{{ number_format($item->qty) }}</td>
                                                <td>{{ number_format($item->price, 2) }}</td>
                                                <td>{{ number_format($item->current_rate) }}</td>
                                                <td>{{ number_format($item->totalPrice()) }} </td>
                                                <td>{{ $item->customer_note }}</td>
                                                <td>{{ $item->admin_note }}</td>
                                                @if($order->status < STATUS_PRICE_QUOTATION)
                                                    <td>
                                                        <a title="@lang('form.button_delete')" href="#" title="" class="delete-button" data-url-delete="{{ route('purchase-items.delete', $item->id) }}" data-id="{{ $item->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="text-left" colspan="3"><h4>Tổng</h4></td>
                                            <td>{{ number_format($totalItems) }}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ number_format($totalItemsPrice) }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left" colspan="6"><h4>Giảm giá</h4></td>
                                            <td>{{ number_format($totalItemsPrice - $order->purchase_total_items_price) }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left" colspan="6"><h4>Thành tiền sản phẩm</h4></td>
                                            <td>{{ number_format($order->purchase_total_items_price) }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                 @endif
            @if($order->status == STATUS_CANCEL)
                <h3 class="text-danger text-center">@lang('message.order_is_cancel')</h3>
            @else
                <div class="row">
                    <div class="col-md-6">
                        <div class="order-left-info">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="order-right-info">
                            @if($order->status >= STATUS_PRICE_QUOTATION)
                                <div class="detail-total-order">
                                    <span ><strong>@lang('form.purchase_service_fee'): </strong></span>
                                    <span >{{ number_format($order->purchase_service_fee) }} @lang('form.title_vnd')</span>
                                </div>
                                <div class="detail-order-fee">
                                    <span ><strong>@lang('form.title_cn_transport_fee'): </strong></span>
                                    <span >{{ number_format($order->purchase_cn_transport_fee) }} @lang('form.title_vnd')</span>
                                </div>
                                <div class="detail-order-fee">
                                    <span ><strong>@lang('form.vn_transport_fee'): </strong></span>
                                    <span >{{ number_format($order->purchase_cn_to_vn_fee) }} @lang('form.title_vnd')</span>
                                </div>
                                <div class="detail-order-fee">
                                    <span ><strong>@lang('form.title_purchase_cn_to_vn_fee'): </strong></span>
                                    <span >{{ number_format($order->purchase_vn_transport_fee) }} @lang('form.title_vnd')</span>
                                </div>
                            @elseif($order->status == STATUS_UNSENT)
                                <span><strong>@lang('form.total_product_price'): </strong></span>
                                <span>{{ number_format($order->purchase_total_items_price) }} @lang('form.title_vnd')</span>
                                @isset($defaultDeliveryAddress)
                                    {{ Form::open(['route' => ['orders.send-order', $order->id], 'class' => 'inline-form mt-3']) }}
                                        {{ Form::hidden('name', $defaultDeliveryAddress->name) }}
                                        {{ Form::hidden('phone_number', $defaultDeliveryAddress->phone_number) }}
                                        {{ Form::hidden('city', $defaultDeliveryAddress->city) }}
                                        {{ Form::hidden('district', $defaultDeliveryAddress->district) }}
                                        {{ Form::hidden('commune', $defaultDeliveryAddress->commune) }}
                                        {{ Form::hidden('street', $defaultDeliveryAddress->street) }}
                                        <div class="form-group mb-2 receive-type">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="transport_receive_type" id="inlineRadio1" value="{{ VALUE_TYPE_FALSE }}">
                                                <label class="form-check-label" for="inlineRadio1"><small>Nhận hàng tại Lào Cai</small></label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="transport_receive_type" id="inlineRadio2" value="{{ VALUE_TYPE_TRUE }}" checked>
                                                <label class="form-check-label" for="inlineRadio2"><small>Nhận hàng tại nhà</small></label>
                                            </div>
                                        </div>
                                        <span class="send-order-button pt-4">
                                            <button type="submit" class="btn btn-primary" > @lang('buttons.button_send_order')</button>
                                        </span>
                                    {{ Form::close() }}
                                @endisset
                            @elseif($order->status == STATUS_PROCESSING)
                                <span><strong>@lang('form.total_product_price'): </strong></span>
                                <span>{{ number_format($order->purchase_total_items_price) }} @lang('form.title_vnd')</span>
                            @endif
                        </div>
                            @if(! isset($defaultDeliveryAddress))
                                <span class="send-order-button pt-4 pull-right">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#{{ CREATE_DELIVERY_ADDRESS_MODAL }}">@lang('form.click_to_add_delivery_address')</button>
                                </span>
                            @endif
                    </div>
                </div>
                @if($order->status >= STATUS_PRICE_QUOTATION)
                    <hr>
                    <div class="row">
                        <div class="col-md-6 offset-md-6">
                            <div class="order-right-info">
                                <div class="detail-order-fee">
                                    <span class="final-total-price"><strong>@lang('form.final_total_price'): </strong></span>
                                    <span class="final-total-price">{{ number_format($order->final_total_price) }} @lang('form.title_vnd')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($order->status == STATUS_PRICE_QUOTATION)
                        <div class="col-lg-12">
                            <span class="send-order-button pt-4 pull-right">
                                <a href="{{ route('orders.show-deposit-view', $order->id) }}" class="btn btn-primary" > @lang('buttons.button_pay')</a>
                                {{ Form::open(['route' => ['orders.cancel-order', $order->id], 'class' => 'inline-form']) }}
                                    <span class="send-order-button pt-4">
                                        <button type="submit" class="btn btn-danger" > @lang('buttons.button_cancel')</button>
                                    </span>
                                {{ Form::close() }}
                            </span>
                        </div>
                    @endif
                @endif
            @endif
        @else
            <h3 class="text-danger text-center">@lang('message.order_is_empty')</h3>
        @endif
    </div>
    @include('components.delete_modal')
    @include('frontend.customer.create_update_delivery_address_modal')
@stop
@section('script')
    <script>
        $(function() {
            var order = new Order();
            order.deleteOrderItem();
        });
    </script>
@stop