@extends('layouts.frontend')
@section('style')
    {{ Html::style(asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css')) }}
@stop
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_order_index')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-md-3">
                @include('components.site_bar')
            </div>
            <div class="col-md-9">
                <form action="{{ route('customers.notifications.index') }}" method="GET" class="pt-4">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <select name="status" class="form-control">
                                    <option {{ request()->status === '-1' ? 'selected' : '' }} value="-1">@lang('form.title_all')</option>
                                    <option {{ request()->status === '0' ? 'selected' : '' }} value="0">@lang('form.title_unread')</option>
                                    <option {{ request()->status === '1' ? 'selected' : '' }} value="1">@lang('form.title_read')</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="start_date" value="{{ request('start_date') }}" type="text" class="form-control js-start-date search-order" placeholder="@lang('form.placeholder_start_date')">
                                <span class="input-group-addon"><i class="fa fa-calendar order-icon-search"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="end_date" value="{{ request('end_date') }}" type="text" class="form-control js-end-date search-order" placeholder="@lang('form.placeholder_end_date')">
                                <span class="input-group-addon"><i class="fa fa-calendar order-icon-search"></i></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-sm btn-danger pull-right ml-2"><i class="fa fa-filter"></i> @lang('form.filter')</button>
                        </div>
                    </div>
                    <br>
                </form>
                <div class="row pt-4">
                    <div class="col-md-12">
                        <div class="order-list">
                            <div class="table">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>@lang('table.created_at')</th>
                                        <th>@lang('table.message')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($notifications->count() > 0)
                                        @foreach($notifications as $notification)
                                            <tr class="{{ $notification->read_at == null ? 'font-weight-bold' : '' }}">
                                                <td>{{ $notification->created_at->format('d/m/Y h:i') }}</td>
                                                <td>
                                                    Đơn hàng <a href="{{ route('customers.notifications.read', ['id' => $notification->id, 'redirect' => $notification->data['link']]) }}">{{ $notification->data['order_number'] }}</a> đã được báo giá
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                {{ $notifications->appends(request()->query())->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    @parent
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(function() {
            var app = new Application();
            app.initDatePickerValidateEndDate();
        });
    </script>
@stop