<div class="container">
    @if(isset($slidesTaobao) && count($slidesTaobao) !== VALUE_TYPE_FALSE)
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title wow fadeInUp origin-header">
                            <h2 class="origin-title">Đặt hàng từ <span class="text-danger">Taobao.com</span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-carousel-slide">
            @foreach($slidesTaobao as $key => $slide)
                <div>
                    <a href="{{ $slide->link }}" target="_blank"><img class="img-fluid mx-auto d-block" src="{{ asset('storage/app/public/slide-images/'.$slide->src) }}"></a>
                    <div class="service-content">
                        <a href="{{ $slide->link }}" target="_blank"><h4>{{ str_limit($slide->name, config('data.slide_limit_title')) }}</h4></a>
                        <a href="{{ $slide->link }}" class="readmore-btn" target="_blank">Xem thêm <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

<div class="container">
    @if(isset($slides1688) && count($slides1688) !== VALUE_TYPE_FALSE)
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title wow fadeInUp origin-header">
                            <h2 class="origin-title">Đặt hàng từ <span class="text-danger">1688.com</span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-carousel-slide">
            @foreach($slides1688 as $key => $slide)
                <div>
                    <a href="{{ $slide->link }}" target="_blank"><img class="img-fluid mx-auto d-block" src="{{ asset('storage/app/public/slide-images/'.$slide->src) }}"></a>
                    <div class="service-content">
                        <a href="{{ $slide->link }}" target="_blank"><h4>{{ str_limit($slide->name, config('data.slide_limit_title')) }}</h4></a>
                        <a href="{{ $slide->link }}" class="readmore-btn" target="_blank">Xem thêm <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>

<div class="container">
    @if(isset($slidesTmall) && count($slidesTmall) !== VALUE_TYPE_FALSE)
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title wow fadeInUp origin-header">
                            <h2 class="origin-title">Đặt hàng từ <span class="text-danger">Tmall.com</span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-carousel-slide">
            @foreach($slidesTmall as $key => $slide)
                <div>
                    <a href="{{ $slide->link }}" target="_blank"><img class="img-fluid mx-auto d-block" src="{{ asset('storage/app/public/slide-images/'.$slide->src) }}"></a>
                    <div class="service-content">
                        <a href="{{ $slide->link }}" target="_blank"><h4>{{ str_limit($slide->name, config('data.slide_limit_title')) }}</h4></a>
                        <a href="{{ $slide->link }}" class="readmore-btn" target="_blank">Xem thêm <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif
</div>