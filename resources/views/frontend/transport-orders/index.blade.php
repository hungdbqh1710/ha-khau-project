@extends('layouts.frontend')
@section('style')
    {{ Html::style(asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css')) }}
@stop
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.page_shipping_order')</h2>
        <hr>
        @include('components.notification')
        <div class="row">
            <div class="col-md-3">
                @include('components.site_bar')
            </div>
            <div class="col-md-9">
                <ul class="list-inline">
                    <li class="list-inline-item {{ request()->status ? '': 'filter-active' }}"><a href="{{ route('orders.indexTransportOrder') }}">@lang('form.title_all')({{ $transportOrderCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_TRANSPORT_CONFIRMED ? 'filter-active' : ''}}"><a href="{{ route('orders.indexTransportOrder') }}?status={{ STATUS_TRANSPORT_CONFIRMED }}">@lang('form.title_confirmed_customer')({{ $processingOrderCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_TRANSPORT_TO_TQ ? 'filter-active' : ''}}"><a href="{{ route('orders.indexTransportOrder') }}?status={{ STATUS_TRANSPORT_TO_TQ }}">@lang('form.title_transport_to_cn')({{ $confirmedOrderCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_TRANSPORT_TO_LC ? 'filter-active' : ''}}"><a href="{{ route('orders.indexTransportOrder') }}?status={{ STATUS_TRANSPORT_TO_LC }}">@lang('form.title_transport_to_lc')({{ $priceQuotationOrderCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_TRANSPORT_TO_CUSTOMER ? 'filter-active' : ''}}"><a href="{{ route('orders.indexTransportOrder') }}?status={{ STATUS_TRANSPORT_TO_CUSTOMER}}">@lang('form.title_transport_to_customer')({{ $priceTransportingCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_SUCCESS ? 'filter-active' : ''}}"><a href="{{ route('orders.indexTransportOrder') }}?status={{ STATUS_SUCCESS }}">@lang('form.title_success')({{ $orderSuccessCount }})</a></li>
                    <li class="list-inline-item {{ request()->status == STATUS_CANCEL ? 'filter-active' : ''}}"><a href="{{ route('orders.indexTransportOrder') }}?status={{ STATUS_CANCEL }}">@lang('form.title_cancel')({{ $orderCancelCount }})</a></li>
                </ul>
                <form action="{{ route('orders.indexTransportOrder') }}" method="GET" class="pt-4">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="order_number" value="{{ request('order_number') }}" type="text" class="form-control search-order" placeholder="@lang('form.placeholder_order_code')" autofocus>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="cn_transport_code" value="{{ request('cn_transport_code') }}" type="text" class="form-control" placeholder="@lang('form.placeholder_find_china_order_code')">
                                <span class="input-group-addon"><i class="fa fa-search order-icon-search"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="start_date" value="{{ request('start_date') }}" type="text" class="form-control js-start-date search-order" placeholder="@lang('form.placeholder_start_date')">
                                <span class="input-group-addon"><i class="fa fa-calendar order-icon-search"></i></span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="input-group input-group-sm">
                                <input name="end_date" value="{{ request('end_date') }}" type="text" class="form-control js-end-date search-order" placeholder="@lang('form.placeholder_end_date')">
                                <span class="input-group-addon"><i class="fa fa-calendar order-icon-search"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <button class="btn btn-sm btn-danger pull-right ml-2"><i class="fa fa-filter"></i> @lang('form.filter')</button>
                            <a href="{{ route('orders.exportTransportOrder', request()->query()) }}" class="btn btn-sm btn-info pull-right"><i class="fa fa-download"></i> @lang('form.export_excel')</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if ($orders->count() > VALUE_TYPE_FALSE)
            <div class="row pt-4 transport-order-show">
                <div class="col-md-12">
                    <div class="order-list">
                        <div class="table table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>@lang('table.order_number')</th>
                                    <th>@lang('form.title_china_order_code')</th>
                                    <th>@lang('form.title_order_date')</th>
                                    <th>@lang('form.title_status')</th>
                                    <th>@lang('form.title_action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $key => $order)
                                        <tr>
                                            <td>#{{ $order->order_number }}</td>
                                            <td>{{ $order->cn_transport_code }}</td>
                                            <td>{{ $order->cn_order_date }}</td>
                                            <td>{{ config('data.transport_order_status')[$order->status] }}</td>
                                            <td style="text-align: left; padding-left: 36px;">
                                                <a href="{{ route('orders.showTransportOrderDetail', $order->id) }}" title="Xem chi tiết"><i class="fa fa-eye fa-2x"></i></a>
                                                @if ($order->status == STATUS_PROCESSING)
                                                    <a data-url-delete="{{ route('orders.delete', $order->id) }}" class="delete-button text-danger" title="Xóa"><i class="fa fa-trash fa-2x"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{ $orders->appends(request()->query())->links('vendor.pagination.bootstrap-4') }}
        @endif
    </div>
    @include('components.delete_modal')
    @include('frontend.transport-orders.update_transport_modal')
    @include('frontend.transport-orders.detail_link_qty_modal')
@stop

@section('script')
    @parent
    <script src="{{ asset('vendor/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(function() {
            let app = new Application();
            app.initDatePickerValidateEndDate();
            app.buildDataUpdateModal();

            let order = new Order();
            order.deleteOrderItem();
        });
    </script>
@stop