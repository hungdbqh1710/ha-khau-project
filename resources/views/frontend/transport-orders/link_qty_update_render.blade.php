@if(isset($linkQtys))
    @foreach($linkQtys as $linkQty)
        <div class="form-group col-lg-6 col-xs-12">
            <input type="url" id="" name="link[]" class="form-control " value="{{$linkQty['link']}}" required placeholder="@lang('form.placeholder_link')">
        </div>
        <div class="form-group col-lg-6 col-xs-12">
            <input type="number" id="" name="qty[]" min="1" class="form-control " value="{{$linkQty['qty']}}" required placeholder="@lang('form.placeholder_qty')">
        </div>
    @endforeach
@endif