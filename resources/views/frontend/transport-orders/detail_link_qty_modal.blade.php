<div class="modal fade" id="{{ DETAIL_LINK_QTY_MODAL }}" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_link_qty_detail')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="col-lg-12">
                <table class="table table-compact table-striped">
                    <thead>
                        <tr>
                            <th>@lang('form.title_link')</th>
                            <th>@lang('form.title_order_items')</th>
                        </tr>
                    </thead>
                    <tbody id="js-detail-link-qty-tbody">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('buttons.button_close')</button>
            </div>
        </div>
    </div>
</div>