@if(!empty($linkQties))
    @foreach ($linkQties as $linkQty)
        <tr>
            <td><a href="{{ $linkQty['link'] }}" target="_blank" title="{{ $linkQty['link'] }}">{{ str_limit($linkQty['link'], config('data.string_limit_link_qty')) }}</a></td>
            <td>{{ $linkQty['qty'] }}</td>
        </tr>
    @endforeach
@endif