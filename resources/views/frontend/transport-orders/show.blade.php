@extends('layouts.frontend')
@section('style')
    {{ Html::style(asset('vendor/bootstrap-datepicker/bootstrap-datepicker.css')) }}
@stop
@section('page')
    <div class="container">
        <h2 class="text-uppercase">@lang('page-name.title_order_show') {{ config('data.prefix_order_number').$order->order_number }}</h2>
        <hr>
        @include('components.notification')
        <div class="col-md-12">
            <h3>
                @lang('form.title_order_status') {{ config('data.transport_order_status')[$order->status] }}
            </h3>
        </div>
        <div class="col-md-12">
            <div class="order-list" id="order-list">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('form.title_transport_code_china')</th>
                            <th>@lang('form.title_transport_code_vn')</th>
                            <th>@lang('form.title_transport_fee_cn')</th>
                            <th>@lang('form.title_transport_fee_via_border')</th>
                            <th>@lang('form.title_money_pay_help')</th>
                            <th>@lang('form.title_status')</th>
                            <th>@lang('form.title_sender_info')</th>
                            <th>@lang('form.title_receive_info')</th>
                            <th>@lang('form.title_customer_note')</th>
                            <th>@lang('form.title_note_admin')</th>
                            <th>@lang('form.title_action')</th>
                        </tr>
                        </thead>
                        <tbody class="item-body">
                            <tr>
                                <td>{{ $order->cn_transport_code }}</td>
                                <td>{{ $order->vn_transport_code }}</td>
                                <td>{{ number_format($order->cn_transport_fee) }}</td>
                                <td>{{ number_format($order->via_border_transport_fee) }}</td>
                                <td>{{ number_format($order->vn_transport_fee) }}</td>
                                <td>{{ config('data.transport_order_status')[$order->status] }}</td>
                                <td>{{ config('data.ha_khau_address') }}</td>
                                <td>{{ $order->receiver_info }}</td>
                                <td>{{ $order->customer_note }}</td>
                                <td>{{ $order->admin_note }}</td>
                                <td>
                                    <a href="#" data-url="{{ route('orders.renderLinkQty', $order->id) }}" class="js-detail-link-qty" title="Xem chi tiết"><i class="fa fa-eye fa-2x"></i></a>
                                    @if ($order->status < STATUS_TRANSPORT_SUCCESS)
                                        <a data-url-update="{{ route('orders.updateTransport', $order->id) }}" data-url-find="{{ route('orders.ajaxFindTransport', $order->id) }}" data-modal="#{{ UPDATE_TRANSPORT_MODAL }}" class="js-update-button text-primary" title="Sửa"><i class="fa fa-edit fa-2x"></i></a>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h3 class="text-right">
                @lang('form.title_total_transport_fee') {{ number_format($order->final_total_price) }} @lang('form.title_vnd')
                @if ($order->status == STATUS_TRANSPORT_CONFIRMED)
                    <a href="{{ route('orders.show-deposit-view', $order->id) }}" class="btn btn-primary" > @lang('buttons.button_pay')</a>
                @endif
            </h3>
        </div>
    </div>
    @include('components.delete_modal')
    @include('frontend.transport-orders.update_transport_modal')
    @include('frontend.transport-orders.detail_link_qty_modal')
@stop
@section('script')
    <script>
        $(function() {
            let app = new Application();
            app.buildDataUpdateModal();

            let order = new Order();
            order.deleteOrderItem();
            order.detailLinkQtyTransportOrderItem();
        });
    </script>
@stop