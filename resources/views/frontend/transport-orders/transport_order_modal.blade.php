<div class="modal fade" id="{{ TRANSPORT_ORDER_MODAL }}" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_shipping_order')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="{{ route('orders.storeTransportOrder') }}" method="POST"  enctype="multipart/form-data" class="form">
                {{ csrf_field() }}
                <div class="receive-content">
                    <span class="bolder">@lang('form.title_type_receive')</span>
                    <input type="radio" id="receive-in-LC" name="transport_receive_type" value="0" required>
                    <label for="receive-in-LC">@lang('form.title_receive_in_LC')</label>
                    <input type="radio" id="ship-to-home" name="transport_receive_type" value="1">
                    <label for="ship-to-home">@lang('form.title_ship_to_home')</label>
                    <input type="hidden" name="order_type" value="2">
                </div>
                <div class="modal-body" id="modal-body-content">
                    <div>
                        <div class="modal-info-order mt-3" data-numerical-order="0">
                            <button type="button" class="close js-remove-order-info" hidden>&times;</button>
                            <div class="row">
                                <div class="form-group col-lg-6 col-xs-12">
                                    <input type="text" name="order[0][cn_transport_code]" class="form-control js-cn-transport-code" required placeholder="@lang('form.placeholder_transport_code_china')">
                                </div>
                                <div class="form-group col-lg-6 col-xs-12">
                                    <input type="text" name="order[0][cn_order_date]" class="form-control js-date-picker js-cn-order-date" required placeholder="@lang('form.placeholder_order_in_china_date')">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4 col-xs-12">
                                    <textarea class="form-control js-customer-note" name="order[0][customer_note]" rows="2" required placeholder="@lang('form.placeholder_content')"></textarea>
                                </div>
                                <div class="form-group col-lg-4 col-xs-12">
                                    <textarea class="form-control js-sender-info" name="order[0][sender_info]" rows="2" required placeholder="@lang('form.placeholder_sender_info')" readonly>@lang('form.title_sender'){{ config('data.ha_khau_address') }}</textarea>
                                </div>
                                <div class="form-group col-lg-4 col-xs-12">
                                    <textarea class="form-control js-receiver-info" name="order[0][receiver_info]" rows="2" required placeholder="@lang('form.placeholder_receive_info')"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-6 col-xs-12">
                                    <input type="url" name="order[0][link][]" class="form-control js-link-order" required placeholder="@lang('form.placeholder_link')">
                                </div>
                                <div class="form-group col-lg-4 col-xs-12">
                                    <input type="number" name="order[0][qty][]" min="1" class="form-control js-qty-order" required placeholder="@lang('form.placeholder_qty')">
                                </div>
                                <div class="form-group col-lg-2">
                                    <button type="button" class="btn btn-info js-btn-add-link-qty" title="Thêm"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-danger js-btn-delete" title="Xóa"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn btn-info mr-auto js-btn-add-order">@lang('form.title_add_order')</button>
                    <button type="submit" class="btn btn-primary">@lang('buttons.button_save')</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('buttons.button_close')</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="js-link-qty-html" hidden>
    <div class="row">
        <div class="form-group col-lg-6 col-xs-12">
            <input type="url" name="" class="form-control js-link-order" required placeholder="@lang('form.placeholder_link')">
        </div>
        <div class="form-group col-lg-4 col-xs-12">
            <input type="number" name="" min="1" class="form-control js-qty-order" required placeholder="@lang('form.placeholder_qty')">
        </div>
        <div class="form-group col-lg-2">
            <button type="button" class="btn btn-info js-btn-add-link-qty" title="Thêm"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-danger js-btn-delete" title="Xóa"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</div>