<div class="modal fade" id="{{ UPDATE_TRANSPORT_MODAL }}" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_shipping_order_update')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="" method="POST"  enctype="multipart/form-data" class="form form-update">
                {{ csrf_field() }}
                <div class="receive-content">
                    <span class="bolder">@lang('form.title_type_receive')</span>
                    <input type="radio" id="transport_receive_type" name="transport_receive_type" value="0" required>
                    <label for="transport_receive_type">@lang('form.title_receive_in_LC')</label>
                    <input type="radio" id="transport_receive_type_other" name="transport_receive_type" value="1">
                    <label for="transport_receive_type_other">@lang('form.title_ship_to_home')</label>
                    <input type="hidden" name="order_type" value="2">
                </div>
                <div class="modal-body" id="modal-body-content">
                    <div>
                        <div class="modal-info-order mt-3" >
                            <button type="button" class="close js-remove-order-info" hidden>&times;</button>
                            <div class="row">
                                <div class="form-group col-lg-6 col-xs-12">
                                    <input type="text" name="cn_transport_code" id="cn_transport_code" class="form-control" required placeholder="@lang('form.placeholder_transport_code_china')">
                                </div>
                                <div class="form-group col-lg-6 col-xs-12">
                                    <input type="text" name="cn_order_date" id="cn_order_date" class="form-control js-date-picker" required placeholder="@lang('form.placeholder_order_in_china_date')">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4 col-xs-12">
                                    <textarea class="form-control " name="customer_note" id="customer_note" rows="2" required placeholder="@lang('form.placeholder_content')"></textarea>
                                </div>
                                <div class="form-group col-lg-4 col-xs-12">
                                    <textarea class="form-control " id="sender_info" name="sender_info" rows="2" required placeholder="@lang('form.placeholder_sender_info')" readonly>@lang('form.title_sender'){{ config('data.ha_khau_address') }}</textarea>
                                </div>
                                <div class="form-group col-lg-4 col-xs-12">
                                    <textarea class="form-control " id="receiver_info" name="receiver_info" rows="2" required placeholder="@lang('form.placeholder_receive_info')"></textarea>
                                </div>
                            </div>
                            <div class="row js-html-link-qty">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="submit" class="btn btn-primary">@lang('buttons.button_save')</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('buttons.button_close')</button>
                </div>
            </form>
        </div>
    </div>
</div>