<div class="modal fade" id="{{ NOTIFICATION_LOGIN_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_notification')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div>
                    <span>@lang('form.title_notification_login')</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-target="#{{ LOGIN_MODAL }}" data-toggle="modal" data-dismiss="modal">@lang('form.login_button')</button>
            </div>
        </div>
    </div>
</div>