<footer class="site-footer">
    <!-- Footer Top start -->
    <div class="footer-top-area wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer-wiz">
                        <h3 class="footer-logo"><img src="{{ asset('frontend/assets/images/logo.png') }}" alt="footer logo"/></h3>
                        <p></p>
                        <ul class="footer-contact">
                            <li><i class="fa fa-home"></i> 190 Nhạc Sơn - Kim Tân - TP Lào Cai</li>
                            <li><i class="fa fa-phone"></i> {{ $configs['hotline']->content }}</li>
                            <li><i class="fa fa-envelope"></i> {{ $configs['email']->content }}</li>
                        </ul>
                    </div>
                    <div class="top-social bottom-social">
                        <a href="{{ $configs['facebook_url']->content }}"><i class="fa fa-facebook"></i></a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer-wiz footer-menu">
                        <h3 class="footer-wiz-title">Hỗ trợ</h3>
                        <ul>
                            @foreach($cmsPages as $page)
                                <li><a href="{{ route('cms-pages.show', $page->slug) }}">{{ $page->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer-wiz">
                        <h3 class="footer-wiz-title">Giờ làm việc</h3>
                        <ul class="open-hours">
                            <li><span>Tất cả các ngày trong tuần:</span><br><span class="text-right"> Sáng từ 8h - 12h chiều từ 1h30 - 5h</span></li>
                        </ul>
                        <div class="newsletter">
                            <a href="#" target="_blank"><img src="{{ asset('frontend/assets/images/dathongbao.png') }}"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer top end -->

    <!-- copyright start -->
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">Copyright © 2018 <span>Hà Khẩu Logistics</span>. All Rights Reserved</div>
                <div class="col-12 col-lg-6 text-right">Được thiết kế và phát triển bởi: <a href="#" title="BSE Technology & Media JSC" target="_blank">BSE Technology & Media JSC</a></div>
            </div>
        </div>
    </div>
    <!-- copyright end -->
</footer>