<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('admin.index') }}" class="site_title">
                <img class="logo-lg" src="{{ asset('frontend/assets/images/logo.png') }}">
                <span class="logo-mini">{{ config('app.name') }}</span>
            </a>
        </div>

        <div class="clearfix"></div>

        <br>

        <!-- sidebar menu -->
        @if(Request::is('admin*'))
            @include('partials._admin_sidebar')
        @else
            Customer
        @endif
        <!-- /sidebar menu -->
    </div>
</div>