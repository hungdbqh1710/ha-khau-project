<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section active">
        <h3>General</h3>
        <ul class="nav side-menu" style="">
            @if(auth()->check() && auth()->user()->hasAnyPermission(['create_users', 'edit_users', 'delete_users']))
                <li>
                    <a href="{{ route('admin.users.index')}}"><i class="fa fa-users"></i> @lang('menu.manage_users') </a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['create_exchange_rates', 'edit_exchange_rates', 'delete_exchange_rates']))
                <li>
                    <a href="{{ route('admin.exchange-rate.index') }}"><i class="fa fa-money"></i>@lang('menu.manage_exchange_rate')</a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['create_slides', 'edit_slides', 'delete_slides']))
                <li class="js-menu-slide"><a><i class="fa fa-image"></i> @lang('menu.manage_slide')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin.slides.index', TYPE_TAO_BAO) }}"><i class="fa fa-image"></i>@lang('menu.manage_slide_tao_bao')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.slides.index', TYPE_1688) }}"><i class="fa fa-image"></i>@lang('menu.manage_slide_1688')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.slides.index', TYPE_TMALL) }}"><i class="fa fa-image"></i>@lang('menu.manage_slide_tmall')</a>
                        </li>
                    </ul>
                </li>
            @endif
            <li class="js-menu-slide"><a><i class="fa fa-list"></i> @lang('menu.title_order')<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li>
                        <a href="{{ route('admin.orders.purchases.index') }}">@lang('menu.manage_purchase_orders')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.transport-orders.index') }}">@lang('menu.manage_transport_orders')</a>
                    </li>
                </ul>
            </li>
            <li class="js-menu-slide"><a><i class="fa fa-file-text-o"></i> @lang('menu.ship_codes')<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li>
                        <a href="{{ route('admin.ship-codes.create') }}">@lang('menu.ship_codes_create')</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.ship-codes.index') }}">@lang('menu.ship_codes')</a>
                    </li>
                </ul>
            </li>
            @if(auth()->check() && auth()->user()->hasAnyPermission(['view_customer_info', 'create_customers']))
                <li>
                    <a href="{{ route('admin.customers.index') }}"><i class="fa fa-user"></i>@lang('menu.title_customer')</a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['view_feedback']))
                <li>
                    <a href="{{ route('admin.feedback.index') }}"><i class="fa fa-envelope"></i>@lang('menu.manage_feedback')</a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['create_blogs', 'edit_blogs', 'delete_blogs']))
                <li>
                    <a href="{{ route('admin.blogs.index') }}"><i class="fa fa-list-alt"></i>@lang('menu.manage_blog')</a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['create_roles', 'edit_roles', 'delete_roles']))
                <li class="js-menu-slide"><a><i class="fa fa-shield"></i> @lang('menu.manage_role_permission')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin.roles.index') }}"><i class="fa fa-shield"></i></i>@lang('menu.manage_roles')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.permissions.index') }}"><i class="fa fa-shield"></i>@lang('menu.manage_permission')</a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['edit_website_configs']))
                <li>
                    <a href="{{ route('admin.website-configs.index') }}"><i class="fa fa-cog"></i></i>@lang('menu.website_config')</a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['create_pages', 'edit_pages', 'delete_pages']))
                <li>
                    <a href="{{ route('admin.cms-pages.index') }}"><i class="fa fa-file"></i>@lang('menu.manage_cms_page')</a>
                </li>
            @endif
            @if(auth()->check() && auth()->user()->hasAnyPermission(['view_reports']))
                <li class="js-menu-slide"><a><i class="fa fa-balance-scale"></i> @lang('menu.reports')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li>
                            <a href="{{ route('admin.reports.debt') }}">@lang('menu.reports_debt')</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.reports.revenue') }}">@lang('menu.revenue')</a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</div>
