@if(isset($cmsPages))
    @foreach($cmsPages as $cmsPage)
        <li class="{{ request()->is($cmsPage->slug) ? 'nav-active' : '' }}"><a href="{{ route('cms-pages.show', $cmsPage->slug) }}">{{ $cmsPage->name }}</a></li>
    @endforeach
@endif