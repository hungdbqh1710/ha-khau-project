@if(!empty($url))
    <a
        href="{{ $url }}"
        class="btn btn-xs btn-info"
        title="Sửa">
        <i class="fa fa-pencil"></i>
    </a>
@else
    <a
        href="#"
        class="btn btn-xs btn-info js-btn-delete editor_edit"
        title="Sửa">
        <i class="fa fa-pencil"></i>
    </a>
@endif