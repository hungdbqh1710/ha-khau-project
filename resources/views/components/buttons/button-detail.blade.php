@if(!empty($url))
    <a
        href="{{ $url }}"
        class="btn btn-xs btn-primary"
        title="{{ $title }}">
        <i class="fa fa-info-circle"></i>
    </a>
@else
    <a href="#" class="btn btn-xs js-btn-detail btn-primary" data-data="{{ isset($query) ? $query : '' }}" title="Chi tiết"><i class="fa fa-info-circle"></i></a>
@endif
