<div class="modal fade" id="{{ ERROR_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_notification')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div>
                    <span>@lang('form.title_has_error')</span>
                </div>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">@lang('buttons.button_close')</button>
            </div>
        </div>
    </div>
</div>