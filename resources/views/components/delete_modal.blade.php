<div class="modal fade" id="{{ DELETE_MODAL }}" role="dialog">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">@lang('form.title_delete_order_item')</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div>
                    <span>@lang('form.confirm_delete')</span>
                </div>
                <form action="" class="js-form-delete" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="button" class="btn btn-default btn-sm pull-right ml-2" data-dismiss="modal">@lang('buttons.button_close')</button>
                    <button type="submit" class="btn btn-primary btn-sm pull-right">@lang('buttons.button_delete')</button>
                </form>
            </div>
        </div>
    </div>
</div>