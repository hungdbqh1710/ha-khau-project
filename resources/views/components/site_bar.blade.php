<div class="left-block left-menu">
    <ul>
        <li><a href="{{ route('customers.showProfile') }}"><i class="fa fa-user"></i> @lang('form.title_customer_profile')</a></li>
        <li><a href="{{ route('customers.showFormUpdatePassword') }}"><i class="fa fa-key"></i> @lang('form.title_update_password')</a></li>
        <li><a href="{{ route('orders.purchases.index') }}"><i class="fa fa-list-alt"></i> @lang('form.title_order_purchase_index')</a></li>
        <li><a href="{{ route('orders.indexTransportOrder') }}"><i class="fa fa-truck"></i> @lang('form.title_shipping_order_list')</a></li>
        <li><a href="{{ route('customers.debt') }}"><i class="fa fa-credit-card"></i> @lang('page-name.page_reports_debt')</a></li>
    </ul>
</div>