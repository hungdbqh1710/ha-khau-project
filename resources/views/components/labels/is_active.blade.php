@php
    $mapStatus = [
        1 => ['text' => trans('table.active'), 'class' => 'success'],
        0 => ['text' => trans('table.inactive'), 'class' => 'danger'],
    ]
@endphp
<span class="label label-{{ $mapStatus[$is_active]['class'] }}">
    {{ $mapStatus[$is_active]['text'] }}
</span>
