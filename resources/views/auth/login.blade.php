@extends('layouts.auth')

@section('body_class','login')

@section('content')
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    {{ Form::open(['route' => 'admin.login']) }}
                        <h1>@lang('form.login_header')</h1>

                        <div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' parsley-error' : ''}}" name="email" value="{{ old('email') }}"
                                   placeholder="@lang('form.email')" required autofocus>
                            @if($errors->has('email'))
                                <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required">{{ $errors->first('email') }}</li>
                                </ul>
                            @endif
                        </div>
                        <div>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' parsley-error' : ''}}" name="password"
                                   placeholder="@lang('form.password')" required>
                                @if($errors->has('email'))
                                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                                        <li class="parsley-required">{{ $errors->first('password') }}</li>
                                    </ul>
                               @endif
                        </div>
                        <div class="checkbox al_left text-left">
                            <label>
                                <input type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('form.remember')
                            </label>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div>
                            <button class="form-control btn btn-primary submit" type="submit">@lang('form.login_button')</button>

                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                        </div>
                        <a class="reset_pass" href="{{ route('admin.password.request') }}">
                            @lang('form.forgot_password')
                        </a>
                    {{ Form::close() }}
                </section>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @parent

    {{ Html::style(mix('assets/auth/css/login.css')) }}
@endsection
