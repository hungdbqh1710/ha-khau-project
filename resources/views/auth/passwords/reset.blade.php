@extends('layouts.auth')

@section('body_class','login')

@section('content')
    <div>
        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    {{ Form::open(['route' => 'admin.password.request']) }}
                    <h1>@lang('form.change_password')</h1>

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' parsley-error' : ''}}" name="email" value="{{ old('email') }}"
                               placeholder="@lang('form.email')" required autofocus>
                        @if($errors->has('email'))
                            <ul class="parsley-errors-list filled" id="parsley-id-5">
                                <li class="parsley-required">{{ $errors->first('email') }}</li>
                            </ul>
                        @endif
                    </div>
                    <div>
                        <input id="password" type="password" class="form-control" name="password"
                               placeholder="@lang('form.password')" required>
                        @if($errors->has('password'))
                            <ul class="parsley-errors-list filled" id="parsley-id-5">
                                <li class="parsley-required">{{ $errors->first('password') }}</li>
                            </ul>
                        @endif
                    </div>

                    <div>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               placeholder="@lang('form.password_confirmation')" required>
                        @if($errors->has('password_confirmation'))
                            <ul class="parsley-errors-list filled" id="parsley-id-5">
                                <li class="parsley-required">{{ $errors->first('password_confirmation') }}</li>
                            </ul>
                        @endif
                    </div>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        <button class="btn btn-primary submit form-control" type="submit">@lang('form.change_password')</button>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                    </div>
                    <a class="reset_pass" href="{{ route('admin.login') }}">
                        @lang('form.login_header')
                    </a>
                    {{ Form::close() }}
                </section>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @parent

    {{ Html::style(mix('assets/auth/css/passwords.css')) }}
@endsection