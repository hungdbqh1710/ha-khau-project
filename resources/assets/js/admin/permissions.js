$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/permissions',
    table: '#permissions',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [
        {
            label: 'Tên quyền',
            name: 'display_name',
            attr: {
                placeholder: 'Nhập tên quyền'
            }
        },
    ],
    i18n: {
        create: {
            button: 'Thêm',
            title: 'Thêm mới quyền',
            submit: 'Lưu lại',
        }
    }
});
$('#permissions').on('click', 'a.editor_edit', function(e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa thông tin quyền',
        buttons: [
            {
                text: 'Lưu lại',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function() {
                    this.close()
                }
            }
        ]
    });
    return false;
});
$('#permissions').on('click', 'a.editor_remove', function(e) {
    e.preventDefault();

    editor.remove($(this).closest('tr'), {
        title: 'Xóa bỏ quyền',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [
            {
                text: 'Xóa',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function() {
                    this.close()
                }
            }
        ]
    });
    return false;
});

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới quyền thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Chỉnh sửa quyền dùng thành công !');
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa người quyền thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return '<a href="#" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>'
        + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}
