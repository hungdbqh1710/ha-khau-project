$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
var initOptions = [];
$.ajax({
    url: '/api/roles',
    dataType: 'json',
    async: false,
    success: function (data) {
        data.forEach(function (role) {
            initOptions.push({
                label: role.display_name,
                value: role.id
            });
        })
    }
});
var editor = new $.fn.dataTable.Editor({
  ajax: '/admin/users',
  table: '#users',
  display: 'bootstrap',
  idSrc: 'id',
  fields: [
      {
          label: 'Tên',
          name: 'name',
          className: "required",
          attr: {
              placeholder: 'Nhập tên'
          }
      },
      {
          label: 'Địa chỉ email',
          name: 'email',
          className: "required",
          attr: {
              placeholder: 'Nhập địa chỉ email'
          }
      },
      {
          label: 'Trạng thái',
          name: 'is_active',
          type: 'select',
          className: "required",
          ipOpts: [
              {label: 'Hoạt động', value: 1},
              {label: 'Không hoạt động', value: 0}
          ],
      },
      {
          label: 'Số điện thoại',
          name: 'phone_number',
          className: "required",
          attr: {
              placeholder: 'Nhập số điện thoại'
          }
      },
      {
          label: 'Mật khẩu',
          name: 'password',
          type: 'password',
          attr: {
              placeholder: 'Nhập mật khẩu'
          }
      },
      {
          label: 'Vai trò',
          name: 'roles[].id',
          type: 'select2',
          options: initOptions,
          opts: {
              multiple: true,
              allowHtml: true,
              closeOnSelect: false
          }
      }
  ],
  i18n: {
    create: {
      button: 'Thêm',
      title: 'Thêm mới người dùng',
      submit: 'Lưu lại',
    }
  }
});
$('#users').on('click', 'a.editor_edit', function(e) {
  e.preventDefault();
  editor.edit($(this).closest('tr'), {
    title: 'Sửa thông tin người dùng',
    buttons: [
      {
        text: 'Lưu lại',
        className: 'btn-primary',
        fn: function() {
          this.submit();
        }
      },
      {
        text: 'Đóng',
        fn: function() {
          this.close()
        }
      }
    ]
  });
  return false;
});
$('#users').on('click', 'a.editor_remove', function(e) {
  e.preventDefault();

  editor.remove($(this).closest('tr'), {
    title: 'Xóa bỏ người dùng',
    message: 'Bạn có chắc chắn muốn xóa không ?',
    buttons: [
      {
        text: 'Xóa',
        className: 'btn-primary',
        fn: function() {
          this.submit();
        }
      },
      {
        text: 'Đóng',
        fn: function() {
          this.close()
        }
      }
    ]
  });
  return false;
});


$('#search-form').on('submit', function(e) {
    window.LaravelDataTables["users"].draw();
    e.preventDefault();
});

$('#refresh').on('click', function() {
    $('#filter-type').val('');
    window.LaravelDataTables["users"].search('');
    window.LaravelDataTables["users"].columns().search('');
    window.LaravelDataTables["users"].page.len(10).draw();
});
$('button.buttons-create').on( 'click', function () {
    editor
        .title('THÊM MỚI NGƯỜI DÙNG')
        .buttons([
            { label: 'Tạo mới', fn: function () { this.submit(); }, className: 'btn btn-primary'},
            { label: 'Đóng', fn: function () { this.close(); } }
        ])
        .create();
} );

editor.on('create', function(e, json, data) {
  showMessage('success', 'Thêm mới người dùng thành công !');
});

editor.on('edit', function(e, json, data) {
  showMessage('success', 'Chỉnh sửa người dùng thành công !');
});

editor.on('remove', function(e, json, data) {
  showMessage('success', 'Xóa người dùng thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
  editor.close();
  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});
editor.on('open', function(e, mode, action) {
    if(action == 'edit') {
        editor.field('password').input().attr('placeholder', 'Để trống nếu không muốn thay đổi');
    } else {
        $('.DTE_Field.DTE_Field_Type_password.DTE_Field_Name_password').addClass('required');
    }
});
editor.on('close', function() {
    $('.DTE_Field.DTE_Field_Type_password.DTE_Field_Name_password').removeClass('required');
    editor.field('password').input().attr('placeholder', '');
});
