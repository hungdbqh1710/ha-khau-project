$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/roles',
    table: '#roles',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [],
});

$('#roles').on('click', 'a.editor_remove', function(e) {
    e.preventDefault();

    editor.remove($(this).closest('tr'), {
        title: 'Xóa bỏ vai trò',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [
            {
                text: 'Xóa',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function() {
                    this.close()
                }
            }
        ]
    });
    return false;
});

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới vai trò thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Chỉnh sửa vai trò thành công !');
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa vai trò thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return `<a href="/admin/roles/${data}/edit" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>`
        + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}

function renderLabelPermissions(data) {
    return data.map((permission) => {
        return `<span class="label label-info">${permission.display_name}</span><br>`
    }).join('');
}