$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/order-items',
    table: '#order-details',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [
        {
            label: 'Mã vận đơn',
            name: 'transport_code',
            type: 'text',
            className: 'required',
            attr: {
                placeholder: 'Nhập mã vận đơn'
            }
        },
        {
            label: 'Mã đặt hàng TQ',
            name: 'china_order_code',
            type: 'text',
            className: 'required',
            attr: {
                placeholder: 'Nhập mã đặt hàng TQ'
            }
        },
    ],
    i18n: {}
});

$('#order-details').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Cập nhật chi tiết đơn hàng',
        buttons: [
            {
                text: 'Lưu lại',
                className: 'btn-primary',
                fn: function () {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                className: 'btn-default',
                fn: function () {
                    this.close();
                }
            },
        ]
    });
    return false;
});

function renderImage(image) {
    return `<image src="${image}" class="preview-table-img"></image>`
}

function renderProductLink(data) {
    return `<a href="${data}" target="_blank">Xem sản phẩm</a>`
}

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Cập nhật chi tiết đơn hàng thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});