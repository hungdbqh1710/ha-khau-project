$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/customers",
    table: "#customer-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [
        {
            label: "Họ tên",
            name: "name",
            className: "required",
            attr: {
                placeholder: 'Nhập họ tên'
            }
        },
        {
            label: "Email",
            name: "email",
            className: "required",
            attr: {
                placeholder: 'Nhập email'
            }
        },
        {
            label: "Số điện thoại",
            name: "phone_number",
            className: "required",
            attr: {
                placeholder: 'Nhập số điện thoại'
            }
        },
        {
            label: "Hoạt động",
            name: "is_active",
            type: "select",
            className: "required",
            options: [{label: "Hoạt động", value: 1}, {label: "Không hoạt động", value: 0}],
            def: 1
        },
        {
            label: "Mật khẩu",
            name: "password",
            type: "password",
            className: "required",
            attr: {
                placeholder: 'Nhập mật khẩu'
            }
        },
    ],
i18n: {
        create: {
            button: "Thêm",
            submit: "Tạo mới",
            title: "Thêm mới khách hàng",
        }
    }
});

function renderLabelOrder(data) {
    var mapStatus = {
        1: {
            text: 'Chưa gửi',
            class: 'default'
        },
        2: {
            text: 'Đang xử lý',
            class: 'warning'
        },
        3: {
            text: 'Đang về kho TQ',
            class: 'info'
        },
        4: {
            text: 'Đã báo giá',
            class: 'info'
        },
        5: {
            text: 'Đã đặt hàng',
            class: 'primary'
        },
        6: {
            text: 'Đã về kho Lào Cai',
            class: 'info'
        },
        7: {
            text: 'Đang giao hàng VN',
            class: 'primary'
        },
        8: {
            text: 'Thành công',
            class: 'success'
        },
        9: {
            text: 'Đã hủy',
            class: 'danger'
        },
    }
    return '<span class="label label-' + mapStatus[data]['class'] +  '">'
        + mapStatus[data]['text'] +
        '</span>';
}

function renderLabelCustomer(data) {
    var mapStatus = {
        1 : {text : 'Hoạt động', class : 'success'},
        0 : {text : 'Không hoạt động', class : 'danger'},
    }
    return '<span class="label label-' + mapStatus[data]['class'] +  '">'
        + mapStatus[data]['text'] +
        '</span>';
}

function renderLabelOrderType(data) {
    var mapStatus = {
        2 : {text : 'Đơn vận chuyển', class : 'success'},
        1 : {text : 'Đơn mua hộ', class : 'primary'},
    }
    return '<span class="label label-' + mapStatus[data]['class'] +  '">'
        + mapStatus[data]['text'] +
        '</span>';
}

$('#customer-table').on('click', 'tbody td:nth-child(4)', function (e) {
    editor.inline(this);
});

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới khách hàng thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Cập nhật hoạt động thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

$('#search-form').on('submit', function(e) {
    window.LaravelDataTables["customer-table"].draw();
    e.preventDefault();
});

$('#refresh').on('click', function() {
    $('#filter-type').val('');
    window.LaravelDataTables["customer-table"].search('');
    window.LaravelDataTables["customer-table"].columns().search('');
    window.LaravelDataTables["customer-table"].page.len(10).draw();
});
$('button.buttons-create').on( 'click', function () {
    editor
        .title('THÊM MỚI KHÁCH HÀNG')
        .buttons([
            { label: 'Tạo mới', fn: function () { this.submit(); }, className: 'btn btn-primary'},
            { label: 'Đóng', fn: function () { this.close(); } }
        ])
        .create();
} );

var editorTopup = new $.fn.dataTable.Editor({
    ajax: "/admin/transactions",
    table: "#customer-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [
        {
            label: "Tiền chuyển khoản",
            name: "amount_topup",
            className: "required format-price",
            attr: {
                placeholder: 'Nhập tiền khách chuyển khoản'
            }
        },
    ],
    i18n: {
        create: {
            button: "Thêm",
            submit: "Tạo mới",
            title: "Thêm tiền chuyển khoản",
        }
    }
});

var editRow;
$('#customer-table').on('click', 'a.editor_topup', function(e) {
    e.preventDefault();
    editRow = window.LaravelDataTables["customer-table"].row($(this).closest('tr')).data();
    editorTopup.title('Thêm tiền chuyển khoản')
        .buttons([
            { label: 'Tạo mới', fn: function () { this.submit(); }, className: 'btn btn-primary'},
            { label: 'Đóng', fn: function () { this.close(); } }
        ])
        .create();
    return false;
});
editorTopup.on( 'preSubmit', function ( e, d, type ) {
    if ( type === 'create') {
        d.data[0].customer_id = editRow.id;
    }
    d.data[0].amount_topup = $('#DTE_Field_amount_topup').unmask();
});

editorTopup.on('create', function(e, json, data) {
    showMessage('success', 'Thêm tiền chuyển khoản thành công !');
});
editorTopup.on('open', function(e, json, data) {
    formatPrice();
});

var transactionEditor = new $.fn.dataTable.Editor({
    ajax: "/admin/transactions",
    table: "#transaction-histories",
    display: "bootstrap",
    idSrc: 'id',
    fields: [{
        label: "Tiền chuyển khoản",
        name: "amount_topup",
        className: "required format-price",
        attr: {
            placeholder: 'Nhập số tiền chuyển khoản'
        }
    }],
    i18n: {}
});

$('#transaction-histories').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();
    transactionEditor.edit($(this).closest('tr'), {
        title: 'Sửa số tiền chuyển khoản',
        buttons: [{
            text: 'Lưu lại',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});

transactionEditor.on('preSubmit', function (e, d, type) {
    let firstKeyData = Object.keys(d.data)[0];
    d.data[firstKeyData].amount_topup = $('#DTE_Field_amount_topup').unmask();
});

transactionEditor.on('open', function (e, json, data) {
    formatPrice();
});

setInterval( function () {
    $('#transaction-histories').DataTable().ajax.reload();
}, 60000 );

function renderTimeRemaining(data) {
    if (data !== '') {
        let hours   = Math.floor((data * 1000 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let minutes = Math.floor((data * 1000 % (1000 * 60 * 60)) / (1000 * 60));
        return hours + " giờ " + minutes + " phút";
    }
    return '';
}
