$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/count-fees',
    table: '#count-fees',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [
        {
            label: 'Từ sản phẩm',
            name: 'from_product',
            attr: {
                placeholder: 'Nhập từ sản phẩm'
            }
        },
        {
            label: 'Đến sản phẩm',
            name: 'to_product',
            attr: {
                placeholder: 'Nhập đến sản phẩm'
            }
        },
        {
            label: 'Phí (SP > 10 tệ)',
            name: 'price_greater_10_fee',
            className: "required",
            attr: {
                placeholder: 'Nhập phí (SP > 10 tệ)'
            }
        },
        {
            label: 'Phí (SP < 10 tệ)',
            name: 'price_less_10_fee',
            className: "required",
            attr: {
                placeholder: 'Nhập phí (SP < 10 tệ)'
            }
        },
    ],
    i18n: {
        create: {
            button: 'Thêm',
            title: 'Thêm mới phí kiểm đếm',
            submit: 'Lưu lại',
        }
    }
});
$('#count-fees').on('click', 'a.editor_edit', function(e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa thông tin phí kiểm đếm',
        buttons: [
            {
                text: 'Lưu lại',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function() {
                    this.close()
                }
            }
        ]
    });
    return false;
});
$('#count-fees').on('click', 'a.editor_remove', function(e) {
    e.preventDefault();

    editor.remove($(this).closest('tr'), {
        title: 'Xóa bỏ phí kiểm đếm',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [
            {
                text: 'Xóa',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function() {
                    this.close()
                }
            }
        ]
    });
    return false;
});

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới phí kiểm đếm thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Chỉnh sửa phí kiểm đếm thành công !');
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa phí kiểm đếm thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return '<a href="#" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>'
        + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}
