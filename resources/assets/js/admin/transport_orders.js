$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var initOptions = [];
$.ajax({
    url: '/api/users',
    dataType: 'json',
    async: false,
    success: function (data) {
        data.forEach(function (user) {
            initOptions.push({
                label: user.name,
                value: user.id
            });
        })
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/transport-orders",
    table: "#transport-orders-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [
        {
            label: "Mã đơn hàng",
            name: "order_number",
        },
        {
            label: "Khách hàng",
            name: "customer_name",
        },
        {
            label: "Phương thức nhận",
            name: "transport_receive_type",
            type: 'select',
            ipOpts: [
                {
                    label: 'Nhận tại Lào Cai',
                    value: 0
                },
                {
                    label: 'Chuyển về nhà',
                    value: 1
                },
            ],
        },
        {
            label: "Nhân viên chăm sóc",
            name: "supporter_id",
            type: "select2",
            className: "required",
            options: initOptions,
            opts: {
                placeholder: 'Chọn nhân viên chăm sóc'
            }
        },
        {
            label: 'Trạng thái đơn',
            name: 'status',
            type: 'select',
            className: "required",
            ipOpts: [
                {
                    label: 'Đã xác nhận',
                    value: 2
                },
                {
                    label: 'Đang về kho TQ',
                    value: 3
                },
                {
                    label: 'Đã về kho LC',
                    value: 4
                },
                {
                    label: 'Đang giao hàng VN',
                    value: 7
                },
                {
                    label: 'Thành công',
                    value: 8
                },
                {
                    label: 'Đã hủy',
                    value: 9
                },
            ],
        },
        {
            label: 'Tổng tiền vận chuyển (VNĐ)',
            name: 'final_total_price',
            type: 'text',
            className: 'format-price'
        },{
            label: 'Mã vận đơn VN',
            name: 'vn_transport_code',
            attr: {
                placeholder: 'Nhập mã vận đơn VN'
            }
        },
        {
            label: 'Phí vận chuyển TQ',
            name: 'cn_transport_fee',
            className: 'format-price',
            attr: {
                placeholder: 'Nhập phí vận chuyển TQ'
            }
        },
        {
            label: 'Phí vận chuyển TQ về VN',
            name: 'via_border_transport_fee',
            attr: {
                placeholder: 'Nhập phí vận chuyển TQ về VN'
            },
            className: 'format-price'
        },
        {
            label: 'Số tiền thanh toán hộ',
            name: 'vn_transport_fee',
            attr: {
                placeholder: 'Nhập số tiền thanh toán hộ'
            },
            className: 'format-price'
        },
        {
            label: 'Hà Khẩu ghi chú',
            name: 'admin_note',
            type: 'textarea',
            attr: {
                placeholder: 'Nhập ghi chú'
            }
        },
    ],
    i18n: {}
});

$('#transport-orders-table').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();
    editor.edit($(this).closest('tr'), {
        title: 'Sửa thông tin đơn hàng vận chuyển',
        buttons: [
            {
                text: 'Lưu lại',
                className: 'btn-primary',
                fn: function () {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function () {
                    this.close()
                }
            }
        ]
    });
    return false;
});

$('#transport-orders-table').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: 'Xóa đơn hàng vận chuyển',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [
            {
                text: 'Xóa',
                className: 'btn-primary',
                fn: function () {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                fn: function () {
                    this.close()
                }
            }
        ]
    });
    return false;
});

editor.on('onSubmitError', function (e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

editor.on('preSubmit', function (e, d, data) {
    let row = transportOrdersTable.row( editor.modifier() ).data();
    let unmaskInputs = [
        'cn_transport_fee',
        'final_total_price',
        'via_border_transport_fee',
        'vn_transport_fee',
    ];
    unmaskFormatPrice(unmaskInputs, d, row, editor);
});

editor.on('open', function () {
    editor.field('order_number').disable();
    editor.field('customer_name').disable();
    editor.field('transport_receive_type').disable();
    editor.field('final_total_price').disable();
    if (!can('view_all_transport_orders')) {
        editor.field('supporter_id').disable();
    }
    formatPrice();
});

editor.on('edit', function (e, json, data) {
    showMessage('success', 'Cập nhật đơn hàng vận chuyển thành công !');
});

editor.on('remove', function (e, json, data) {
    showMessage('success', 'Xóa đơn hàng vận chuyển thành công !');
});

function renderActionButton(data, type, row, meta) {
    let btn = '';
    btn += `<a href="#" class="btn btn-xs btn-primary js-detail-link-qty" data-id="${row.id}" title="Chi tiết link và số lượng"><i class="fa fa-info-circle"></i></a>`;
    if (data.status < 8) {
        btn += '<a href="#" class="btn btn-xs btn-info editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>';
    }
    btn +=`<a href="/admin/transport-orders/${row.id}/export" class="btn btn-xs btn-success" title="Xuất excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>`;
    return btn;
}

function renderLabelOrder(data) {
    var mapStatus = {
        2: {
            text: 'Đã xác nhận',
            class: 'default'
        },
        3: {
            text: 'Đang về kho TQ',
            class: 'info'
        },
        4: {
            text: 'Đã về kho LC',
            class: 'warning'
        },
        7: {
            text: 'Đang giao hàng VN',
            class: 'primary'
        },
        8: {
            text: 'Thành công',
            class: 'success'
        },
        9: {
            text: 'Đã hủy',
            class: 'danger'
        }
    }
    return '<span class="label label-' + mapStatus[data]['class'] + '">' +
        mapStatus[data]['text'] +
        '</span>';
}

function renderReceiveType(data) {
    let mapType = {
        0: {
            text: 'Nhận tại Lào Cai',
            class: 'warning'
        },
        1: {
            text: 'Chuyển về nhà',
            class: 'info'
        },
    }
    return '<span class="label label-' + mapType[data]['class'] + '">' +
        mapType[data]['text'] +
        '</span>';
}

editor.dependent('status', function (val, data, callback) {
    var mapStatus = {
        2: [
            {
                label: 'Đã xác nhận',
                value: 2
            },
            {
                label: 'Đang về kho TQ',
                value: 3
            },
            {
                label: 'Đã hủy',
                value: 9
            }
        ],
        3: [
            {
                label: 'Đang về kho TQ',
                value: 3
            },
            {
                label: 'Đã về kho LC',
                value: 4
            },
        ],
        4: [
            {
                label: 'Đã về kho LC',
                value: 4
            },
            {
                label: 'Đang giao hàng VN',
                value: 7
            },
        ],
        7: [
            {
                label: 'Đang giao hàng VN',
                value: 7
            },
            {
                label: 'Thành công',
                value: 8
            }
        ],
        8: [
            {
                label: 'Thành công',
                value: 8
            }
        ],
        9: [
            {
                label: 'Đã hủy',
                value: 9
            }
        ],
    };
    return {"options": {"status": mapStatus[data.row.status]}}
});

var mapStatusItem = {
    1: {
        text: 'Chưa về kho',
        class: 'default'
    },
    2: {
        text: 'Đã về kho',
        class: 'info'
    },
    3: {
        text: 'Đang chuyển hàng',
        class: 'primary'
    },
    4: {
        text: 'Thành công',
        class: 'success'
    },
    5: {
        text: 'Đã hủy',
        class: 'danger'
    }
}

$(document).on('click', '.js-detail-link-qty', function (e) {
    e.preventDefault();
    let itemId = $(this).data('id');
    $.ajax({
        type: 'GET',
        url: 'transport-orders/link-qty',
        data: {itemId: itemId},
        success: function (response) {
            if (response.status === true) {
                $('#js-detail-link-qty-tbody').html(response.data);
            }
            $('#detail-link-qty-modal').modal('show');
        }
    })
});

var transportOrdersTable = $('#transport-orders-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: {
        url: '/admin/transport-orders',
        data: function (d) {
            d.status = $('select[name=status]').val();
        }
    },
    columns: [
        {data: 'order_number'},
        {data: 'customer_name', name: 'customer.name'},
        {data: 'supporter_name', name: 'supporter.name', editField: 'supporter_id'},
        {
            data: 'transport_receive_type', render: function (data, type, row, meta) {
                return renderReceiveType(data)
            }
        },
        {
            data: 'status', render: function (data, type, row, meta) {
                return renderLabelOrder(data, type, row, meta)
            }
        },
        {data: 'cn_transport_code'},
        {data: 'vn_transport_code'},
        {
            data: 'cn_transport_fee', render: function (data, type, row, meta) {
                return Number(data).format()
            }
        },
        {
            data: 'via_border_transport_fee', render: function (data, type, row, meta) {
                return Number(data).format()
            }
        },
        {
            data: 'vn_transport_fee', render: function (data, type, row, meta) {
                return Number(data).format()
            }
        },
        {
            data: 'final_total_price', render: function (data, type, row, meta) {
                return Number(data).format()
            }
        },
        {data: 'cn_order_date'},
        {data: 'receiver_info'},
        {data: 'admin_note'},
        {data: 'customer_note'},
        {data: 'created_at'},
        {
            defaultContent: null,
            data: null,
            name: 'action',
            render: function (data, type, row, meta) {
                return renderActionButton(data, type, row, meta)
            },
            orderable: false,
            searchable: false,
            exportable: false,
            printable: true,
            footer: '',
            width: '5%'
        },
    ],
    dom: "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>" +
    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    language: {
        url: urlLanguage
    },
    buttons: ['reset'],
    order: [[15, 'desc']]
});

$('#search-form').on('submit', function(e) {
    transportOrdersTable.draw();
    e.preventDefault();
});

$('#refresh').on('click', function() {
    $('#filter-type').val('');
    transportOrdersTable.search('');
    transportOrdersTable.columns().search('');
    transportOrdersTable.page.len(10).draw();
});
// var template = Handlebars.compile($("#details-template").html());
// $('#transport-orders-table tbody').on('click', '.details-control', function (e) {
//     e.preventDefault();
//     var tr = $(this).closest('tr');
//     var row = transportOrdersTable.row(tr);
//     var tableId = 'details-' + row.data().id;
//
//     if (row.child.isShown()) {
//         // This row is already open - close it
//         row.child.hide();
//         tr.removeClass('shown');
//     } else {
//         // Open this row
//         let data = {
//             ...row.data(),
//             transport_order_item: row.data().transport_order_item.map(item => {
//                 return {
//                     ...item,
//                     status: '<span class="label label-' + mapStatusItem[item.status]['class'] + '">' +
//                     mapStatusItem[item.status]['text'] +
//                     '</span>'
//                 }
//             })
//         }
//         row.child(template(data)).show();
//         initTable(tableId, data);
//         tr.addClass('shown');
//         tr.next('tr').addClass('highlight')
//     }
// });

//function initTable(tableId, data) {
    // var editorDetail = new $.fn.dataTable.Editor({
    //     ajax: '/admin/transport-order-items',
    //     table: '#' + tableId,
    //     display: 'bootstrap',
    //     idSrc: 'id',
    //     fields: [
    //         {
    //             label: 'Mã vận đơn VN',
    //             name: 'vn_transport_code',
    //             attr: {
    //                 placeholder: 'Nhập mã vận đơn VN'
    //             }
    //         },
    //         {
    //             label: 'Phí vận chuyển TQ',
    //             name: 'cn_transport_fee',
    //             attr: {
    //                 placeholder: 'Nhập phí vận chuyển TQ'
    //             }
    //         },
    //         {
    //             label: 'Phí vận chuyển TQ về VN',
    //             name: 'via_border_transport_fee',
    //             attr: {
    //                 placeholder: 'Nhập phí vận chuyển TQ về VN'
    //             }
    //         },
    //         {
    //             label: 'Phí vận chuyển VN',
    //             name: 'vn_transport_fee',
    //             attr: {
    //                 placeholder: 'Nhập phí vận chuyển VN'
    //             }
    //         },
    //         {
    //             label: 'Trạng thái',
    //             name: 'status',
    //             type: 'select',
    //             ipOpts: [
    //                 {
    //                     label: 'Chưa về kho',
    //                     value: 1
    //                 },
    //                 {
    //                     label: 'Đã về kho',
    //                     value: 2
    //                 },
    //                 {
    //                     label: 'Đang chuyển hàng',
    //                     value: 3
    //                 },
    //                 {
    //                     label: 'Thành công',
    //                     value: 4
    //                 },
    //                 {
    //                     label: 'Đã hủy',
    //                     value: 5
    //                 },
    //             ],
    //         },
    //         {
    //             label: 'Ghi chú của admin',
    //             attr: {
    //                 placeholder: 'Nhập ghi chú',
    //                 rows: 3
    //             },
    //             type: 'textarea',
    //             name: 'admin_note',
    //         },
    //     ],
    //     i18n: {}
    // });
    //
    // $('#' + tableId).on('click', 'a.editor_details_edit', function (e) {
    //     e.preventDefault();
    //     editorDetail.edit($(this).closest('tr'), {
    //         title: 'Sửa thông tin chi tiết đơn hàng vận chuyển',
    //         buttons: [
    //             {
    //                 text: 'Lưu lại',
    //                 className: 'btn-primary',
    //                 fn: function () {
    //                     this.submit();
    //                 }
    //             },
    //             {
    //                 text: 'Đóng',
    //                 fn: function () {
    //                     this.close()
    //                 }
    //             }
    //         ]
    //     });
    //     return false;
    // });
    //
    // $('#' + tableId).on('click', 'a.editor_detail_remove', function (e) {
    //     e.preventDefault();
    //     editorDetail.remove($(this).closest('tr'), {
    //         title: 'Xóa chi tiết đơn hàng vận chuyển',
    //         message: 'Bạn có chắc chắn muốn xóa không ?',
    //         buttons: [
    //             {
    //                 text: 'Xóa',
    //                 className: 'btn-primary',
    //                 fn: function () {
    //                     this.submit();
    //                 }
    //             },
    //             {
    //                 text: 'Đóng',
    //                 fn: function () {
    //                     this.close()
    //                 }
    //             }
    //         ]
    //     });
    //     return false;
    // });
    //
    // var orderTable = $('#transport-orders-table').DataTable();
    //
    // editorDetail.on('edit', function (e, json, data) {
    //     orderTable.ajax.reload();
    //     showMessage('success', 'Cập nhật chi tiết đơn hàng vận chuyển thành công !');
    // });
    //
    // editorDetail.on('remove', function (e, json, data) {
    //     orderTable.ajax.reload();
    //     showMessage('success', 'Xóa chi tiết đơn hàng vận chuyển thành công !');
    // });
    //
    // editorDetail.on('onSubmitError', function (e, json, data) {
    //     editorDetail.close();
    //     showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
    // });
    //
    // editorDetail.dependent('status', function (val, data, callback) {
    //     var mapStatus = {
    //         2: [
    //             {
    //                 label: 'Chưa về kho',
    //                 value: 1
    //             },
    //             {
    //                 label: 'Đã hủy',
    //                 value: 5
    //             },
    //         ],
    //         3: [
    //             {
    //                 label: 'Chưa về kho',
    //                 value: 1
    //             },
    //             {
    //                 label: 'Đã về kho',
    //                 value: 2
    //             }
    //         ],
    //         4: [
    //             {
    //                 label: 'Chưa về kho',
    //                 value: 1
    //             },
    //             {
    //                 label: 'Đã về kho',
    //                 value: 2
    //             }
    //         ],
    //         7: [
    //             {
    //                 label: 'Đã về kho',
    //                 value: 2
    //             },
    //             {
    //                 label: 'Đang chuyển hàng',
    //                 value: 3
    //             }
    //         ],
    //         8: [
    //             {
    //                 label: 'Thành công',
    //                 value: 4
    //             },
    //         ],
    //         9: [
    //             {
    //                 label: 'Đã hủy',
    //                 value: 5
    //             },
    //         ],
    //     };
    //     return {"options": {"status": mapStatus[data.row.order.status]}}
    // });

    // $('#' + tableId).DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: data.details_url,
    //     columns: [
    //         {data: 'cn_transport_code'},
    //         {data: 'vn_transport_code'},
    //         {
    //             data: 'cn_transport_fee', render: function (data, type, row, meta) {
    //                 return Number(data).format()
    //             }
    //         },
    //         {
    //             data: 'via_border_transport_fee', render: function (data, type, row, meta) {
    //                 return Number(data).format()
    //             }
    //         },
    //         {
    //             data: 'vn_transport_fee', render: function (data, type, row, meta) {
    //                 return Number(data).format()
    //             }
    //         },
    //         {
    //             data: 'status', render: function (data) {
    //                 return '<span class="label label-' + mapStatusItem[data]['class'] + '">' +
    //                     mapStatusItem[data]['text']+ '</span>';
    //             }
    //         },
    //         {data: 'cn_order_date'},
    //         {data: 'receiver_info'},
    //         {data: 'admin_note'},
    //         {data: 'customer_note'},
    //         {
    //             data: null, render: function (data, type, row, meta) {
    //                 return renderActionDetailButton(row);
    //             },
    //             orderable: false,
    //         }
    //     ],
    //     dom: "f",
    //     language: {
    //         url: urlLanguage
    //     },
    //     order: [[6, 'desc']]
    // });
//};

function renderActionDetailButton(row) {
    let btn = '';
    btn += `<a href="#" class="btn btn-xs btn-primary js-detail-link-qty" data-id="${row.id}" title="Chi tiết link và số lượng"><i class="fa fa-info-circle"></i></a>`;
    if (row.status < 4) {
        btn += '<a href="#" class="btn btn-xs btn-info editor_details_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>';
    }
    if (row.status == 5) {
        btn += '<a href="#" class="btn btn-xs btn-danger editor_detail_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
    }
    return btn;
}