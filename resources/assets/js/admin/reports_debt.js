$(function () {
    $('#filter-type').change(function (e) {
        if ($(this).val() == 1) {
            $('#single-date-group').addClass('hidden');
            $('#single-date').val('');
            $('#date-range-group').removeClass('hidden');
        }
        if ($(this).val() == 2) {
            $('#date-range-group').addClass('hidden');
            $('#date-range').val('');
            $('#single-date-group').removeClass('hidden');
        }
    });

    $('input[name="date_range"]').daterangepicker(
        {
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        }
    );

    $('input[name="date_range"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    $('input[name="date_range"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $('input[name="single_date"]').datetimepicker(
        {
            viewMode: 'years',
            format: 'MM/YYYY'
        }
    );

    var debTable = $('#reports-debt').DataTable(
        {
            processing: true,
            serverSide: true,
            ajax: {
                url: '/admin/reports/debt',
                data: function (d) {
                    d.date_range = $('#date-range').val();
                    d.single_date = $('#single-date').val();
                    d.customer_id = $('.select-customer-id').val();
                }
            },
            columns: [
                {data: 'created_at', name: 'created_at', title: 'Ngày tháng', visible: false},
                {
                    data: 'name',
                    name: 'name',
                    title: 'Khách hàng',
                },
                {
                    data: 'total_order',
                    name: 'total_order',
                    render: function (data, type, row, meta) {
                        return `<a href="#" title="Click để xem chi tiết" class="js-view-orders" data-id="${row.id}">${data ? Number(data).format() : 0}</a>`;
                    },
                    title: 'Tổng tiền mua hàng',
                    searchable: false
                },
                {
                    data: 'total_transaction',
                    name: 'total_transaction',
                    title: 'Tổng tiền chyển khoản',
                    render: function (data, type, row, meta) {
                        return `<a href="#" title="Click để xem chi tiết" class="js-view-transactions" data-id="${row.id}">${data ? Number(data).format() : 0}</a>`;
                    },
                    searchable: false
                },
                {
                    data: null,
                    name: 'total_debt',
                    title: 'Tổng tiền còn nợ',
                    render: function (data, type, row, meta) {
                        return Number((row.total_order ? row.total_order : 0) - (row.total_transaction ? row.total_transaction : 0)).format();
                    },
                    searchable: false,
                    orderable: false
                },
                {
                    data: null,
                    name: 'export_excel',
                    title: 'Xuất excel',
                    render: function (data, type, row, meta) {
                        return `<a href="#" title="Xuất excel" class="js-export-excel-debt btn btn-sm btn-success" data-id="${row.id}"><span class="glyphicon glyphicon-export"></span></a>`;
                    },
                    searchable: false,
                    orderable: false
                },
            ],
            language: {
                url: '/vendor/datatables/languages/Vietnamese.json',
            },
            order: [[0, 'desc']],
        }
    );

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click', '.js-export-excel-debt', function (e) {
        e.preventDefault();
        let dateRange = $('input[name=date_range]').val();
        let singleDate = $('input[name=single_date]').val();
        let customerId = $(this).data('id');

        $('#export-excel').find('input[name=date_range]').val(dateRange);
        $('#export-excel').find('input[name=single_date]').val(singleDate);
        $('#export-excel').find('input[name=customer_id]').val(customerId);

        $('#export-excel').submit();
    });

    var searchForm = function () {
        debTable.draw();
        let dateRange = $('input[name=date_range]').val();
        let singleDate = $('input[name=single_date]').val();
        let customerId = $('select[name=customer_id]').val();

        if (customerId != '') {
            $('#js-btn-payment-debt').closest('form').find('input[name=customer_id]').val(customerId);
        }
    }

    $('#search-form').on('submit', function (e) {
        e.preventDefault();
        searchForm();
    });

    $('#submit-export').on('click', function (e) {
        let dateRange = $('#date-range').val();
        let singleDate = $('#single-date').val();
        let customerId = $('.select-customer-id').val();

        $(this).closest('form').find('input[name=date_range]').val(dateRange);
        $(this).closest('form').find('input[name=single_date]').val(singleDate);
        $(this).closest('form').find('input[name=customer_id]').val(customerId);
        $(this).closest('form').submit();
    });
    var currentUrl;
    var currentType;
    $(document).on('click', '.js-view-orders', function() {
        let page = 1;
        let id = $(this).data('id');
        $('#view-detail-orders').modal('show');
        let url = '/api/customers/' + id +'/orders?page=';
        currentUrl = url;
        currentType = 'orders';
        loadData(url, page, currentType);
    });

    $(document).on('click', '.js-view-transactions', function() {
        let page = 1;
        let id = $(this).data('id');
        $('#view-detail-transactions').modal('show');
        let url = '/api/customers/' + id +'/transactions?page=';
        currentUrl = url;
        currentType = 'transactions';
        loadData(url, page, currentType);
    });
    var currentPage = 1;

    $('.modal-body').scroll(function() {
        if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight) {
            currentPage = currentPage + 1;
            loadData(currentUrl, currentPage, currentType );
        }
    });
    $('#view-detail-transactions').on('hidden.bs.modal', function () {
        currentPage = 1;
    });
    $('#view-detail-orders').on('hidden.bs.modal', function () {
        currentPage = 1;
    });

    function loadData(url, page, type){
        let data = {};
        data.date_range = $('#date-range').val();
        data.single_date = $('#single-date').val();
        $.ajax(
            {
                url: url + page,
                type: "get",
                data: data,
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                if(data.html == " "){
                    $('.ajax-load').html("No more records found");
                    return;
                }
                $('.ajax-load').hide();
                if (page == 1 && type == "transactions") {
                    $("#detail-transactions-info").html(data.html);
                } else if (type == "transactions") {
                    $("#detail-transactions-info").append(data.html);
                } else if (page == 1 && type == "orders") {
                    $("#detail-orders-info").html(data.html);
                } else {
                    $("#detail-orders-info").append(data.html);
                }
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                alert('Có lỗi xảy ra vui lòng thử lại sau...');
            });
    }
});
