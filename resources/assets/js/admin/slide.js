$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var slideGroup = $('.js-slide-group').val();

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/slides",
    table: "#slide-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [
        {
            name: "id",
            type: 'hidden',
            className: "required",
        },
        {
            label: "Tên",
            name: "name",
            className: "required",
            attr: {
                placeholder: 'Nhập tên'
            }
        },
        {
            label: "Link sản phẩm",
            name: "link",
            className: "required",
            attr: {
                placeholder: 'Nhập link sản phẩm'
            }
        },
        {
            label: "Vị trí",
            name: "position",
            className: "required",
            attr: {
                placeholder: 'Nhập vị trí'
            }
        },
        {
            label: "Hoạt động",
            name: "is_active",
            type: "select",
            className : "required",
            options: [{label: "Hoạt động", value: 1}, {label: "Không hoạt động", value: 0}],
            def: 0
        },
        {
            label: "Nhóm",
            name: "group",
            type: "hidden",
            className : "required",
            def: slideGroup
        },
        {
            label: "Ảnh",
            name: "src",
            type: "upload",
            className : "required",
            display: function (data) {
                return '<img src="/storage/app/public/slide-images/'+ data +'" class="img-responsive">';
            },
            clearText: "Clear",
            noImageText: 'No image'
        },
    ],
    i18n: {
        create: {
            button: "Thêm",
            submit: "Tạo mới",
            title: "Thêm mới slide " + slideGroup,
        }
    }
});

$('#slide-table').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: "Xóa slide",
        message: "Bạn chắc chắn xóa?",
        buttons: [
            {
                text: "Xóa", className: 'btn-primary', fn: function () {
                    this.submit()
                }
            },
            {
                text: "Đóng", className: 'btn-default', fn: function () {
                    this.close()
                }
            },
        ],
    });
});

$('#slide-table').on('click', 'a.editor_edit', function(e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa slide ' + slideGroup,
        buttons: [
            {
                text: 'Lưu lại',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                className: 'btn-default',
                fn: function() {
                    this.close();
                }
            }
        ]
    });
    return false;
});

function renderSlideImage(data) {
    return '<img src="/storage/app/public/slide-images/'+ data +'" class="preview-table-img">';
}


function renderSlideLink(data) {
    return `<a href="${data}" target="_blank">Xem sản phẩm</a>`
}

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới slide thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Cập nhật slide thành công !');
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa slide thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});
