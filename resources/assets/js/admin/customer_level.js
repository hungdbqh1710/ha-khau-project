$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/customer-level",
    table: "#customer-level-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [
        {
            label: "Cấp độ thành viên",
            name: "level",
            className: "required",
            attr: {
                placeholder: 'Nhập cấp độ thành viên'
            }
        },
        {
            label: "Tổng tiền từ (VNĐ)",
            name: "spent_money_from",
            attr: {
                placeholder: 'Nhập tổng tiền từ'
            }
        },
        {
            label: "Tổng tiền đến (VNĐ)",
            name: "spent_money_to",
            attr: {
                placeholder: 'Nhập tổng tiền đến'
            }
        },
        {
            label: "CK phí mua hàng (%)",
            name: "purchase_discount",
            className: "required",
            attr: {
                placeholder: 'Nhập chiết khấu phí mua hàng'
            }
        },
        {
            label: "CK phí vận chuyển (%)",
            name: "transport_discount",
            className: "required",
            attr: {
                placeholder: 'Nhập chiết khấu phí vận chuyển'
            }
        },
        {
            label: "Đặt cọc tối thiểu (%)",
            name: "deposit",
            className: "required",
            attr: {
                placeholder: 'Nhập đặt cọc tối thiểu'
            }
        },
    ],
    i18n: {
        create: {
            button: "Thêm",
            submit: "Tạo mới",
            title: "Thêm mới cấp độ thành viên",
        }
    }
});

$('#customer-level-table').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: "Xóa cấp độ thành viên",
        message: "Bạn chắc chắn xóa?",
        buttons: [
            {
                text: "Xóa", className: 'btn-primary', fn: function () {
                this.submit()
            }
            },
            {
                text: "Đóng", className: 'btn-default', fn: function () {
                this.close()
            }
            },
        ],
    });
});

$('#customer-level-table').on('click', 'a.editor_edit', function(e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa cấp độ thành viên ',
        buttons: [
            {
                text: 'Lưu lại',
                className: 'btn-primary',
                fn: function() {
                    this.submit();
                }
            },
            {
                text: 'Đóng',
                className: 'btn-default',
                fn: function() {
                    this.close();
                }
            }
        ]
    });
    return false;
});

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới cấp độ thành viên thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Cập nhật cấp độ thành viên thành công !');
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa cấp độ thành viên thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});
