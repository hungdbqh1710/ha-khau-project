$('#filter-type').change(function(e) {
    if ($(this).val() == 1) {
        $('#single-date-group').addClass('hidden');
        $('#single-date').val('');
        $('#date-range-group').removeClass('hidden');
    }
    if ($(this).val() == 2) {
        $('#date-range-group').addClass('hidden');
        $('#date-range').val('');
        $('#single-date-group').removeClass('hidden');
    }
});

$('input[name="date_range"]').daterangepicker({
    autoUpdateInput: false,
    locale: {
        cancelLabel: 'Clear'
    }
});

$('input[name="date_range"]').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
});

$('input[name="date_range"]').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
});

$('input[name="single_date"]').datetimepicker({
    viewMode: 'years',
    format: 'MM/YYYY'
});

$(document).ready(function () {
    let urlReportRevenue = $('#search-form').data('url-revenue');
    let table = $('#revenue-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: urlReportRevenue,
            data: function (d) {
                d.date_range = $('input[name=date_range]').val();
                d.single_date = $('input[name=single_date]').val();
                d.supporter_id = $('select[name=supporter_id]').val();
            }
        },
        columns: [
            {data: 'order_number', name: 'order_number'},
            {data: 'customer_name', name: 'customer_name'},
            {data: 'final_total_price', name: 'final_total_price'},
            {data: 'updated_at', name: 'updated_at'}
        ],
        language: {
            url: urlLanguage
        },
        order: [[3, 'desc']]
    });

    $('#search-form').on('submit', function(e) {
        table.draw();
        let dateRange = $('input[name=date_range]').val();
        let singleDate = $('input[name=single_date]').val();
        let supporterId = $('select[name=supporter_id]').val();
        let urlCalculate = $(this).data('url-calculate-revenue');

        $.ajax({
            type: 'GET',
            url: urlCalculate,
            data: {date_range: dateRange, single_date: singleDate, supporter_id: supporterId},
            success: function (response) {
                if (response && response.status !== false) {
                    $('.overview-calculate-contain').html();
                    $('.overview-calculate-contain').html(response.data);
                }
            }
        });
        e.preventDefault();
    });

    $('.js-btn-export-revenue').click(function () {
        let dateRange = $('.js-date-range').val();
        let singleDate = $('.js-single-date').val();
        let supporterId = $('.select-supporter-id').val();

        $(this).closest('form').find('input[name=date_range]').val(dateRange);
        $(this).closest('form').find('input[name=single_date]').val(singleDate);
        $(this).closest('form').find('input[name=supporter_id]').val(supporterId);
        $(this).closest('form').submit();
    });
});