var token = $('meta[name="csrf-token"]').attr('content');

$('#js-content-blog').ckeditor({
    height: 350,
    filebrowserImageBrowseUrl: routePrefix + '?type=Images',
    filebrowserImageUploadUrl: routePrefix + '/upload?type=Images&_token='+token,
    filebrowserBrowseUrl: routePrefix + '?type=Files',
    filebrowserUploadUrl: routePrefix + '/upload?type=Files&_token='+token,
});

CKEDITOR.config.extraPlugins = 'justify';
CKEDITOR.config.removePlugins = 'about';