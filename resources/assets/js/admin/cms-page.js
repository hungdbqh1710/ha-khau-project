$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/cms-pages",
    table: "#cms-page-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [],
    i18n: {}
});

$('#cms-page-table').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: "Xóa page",
        message: "Bạn chắc chắn xóa?",
        buttons: [
            {
                text: "Xóa", className: 'btn-primary', fn: function () {
                    this.submit()
                }
            },
            {
                text: "Đóng", className: 'btn-default', fn: function () {
                    this.close()
                }
            },
        ],
    });
});

function renderLabelActive(data) {
    var mapStatus = {
        1 : {text : 'Hoạt động', class : 'success'},
        0 : {text : 'Không hoạt động', class : 'danger'},
    }
    return '<span class="label label-' + mapStatus[data]['class'] +  '">'
        + mapStatus[data]['text'] +
        '</span>';
}

$('#cms-page-table').on('click', 'tbody td:nth-child(4)', function (e) {
    editor.inline(this);
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa page thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return `<a href="/admin/cms-pages/${data}/edit" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>`
        + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}