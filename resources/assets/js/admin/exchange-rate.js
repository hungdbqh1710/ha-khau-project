$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/exchange-rate",
    table: "#exchange-rate-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [
        {
            label: "VNĐ",
            name: "vnd",
            className: "required format-price",
            attr: {
                placeholder: 'Nhập VNĐ'
            }
        },
        {
            label: "NDT",
            name: "ndt",
            className: "required format-price",
            attr: {
                placeholder: 'Nhập NDT'
            }
        },
        {
            label: "Hoạt động",
            name: "is_active",
            type: "select",
            options: [{label: "Hoạt động", value: 1}, {label: "Không hoạt động", value: 0}],
            def: 0
        },
    ],
    i18n: {
        create: {
            button: "Thêm",
            submit: "Tạo mới",
            title: "Thêm mới tỷ giá",
        }
    }
});

$('#exchange-rate-table').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: "Xóa tỷ giá",
        message: "Bạn chắc chắn xóa?",
        buttons: [
            {
                text: "Xóa", className: 'btn-primary', fn: function () {
                    this.submit()
                }
            },
            {
                text: "Đóng", className: 'btn-default', fn: function () {
                    this.close()
                }
            },
        ],
    });
});

if (can('edit_exchange_rates')) {
    $('#exchange-rate-table').on('click', 'tbody td:nth-child(3)', function (e) {
        editor.inline(this);
    });
}

editor.on('preSubmit', function (e, d, data) {
    let row = window.LaravelDataTables["exchange-rate-table"].row(editor.modifier()).data();
    let unmaskInputs = [
        'vnd',
        'ndt',
    ];
    unmaskFormatPrice(unmaskInputs, d, row, editor, data);
});

editor.on('initCreate', function () {
    editor.show();
    editor.hide('is_active');
});

editor.on('open', function () {
    formatPrice();
});

editor.on('close', function () {
    editor.show('is_active');
});

editor.on('create', function(e, json, data) {
    showMessage('success', 'Thêm mới tỷ giá thành công !');
});

editor.on('edit', function(e, json, data) {
    showMessage('success', 'Cập nhật hoạt động thành công !');
});

editor.on('remove', function(e, json, data) {
    showMessage('success', 'Xóa tỷ giá thành công !');
});

editor.on('onSubmitError', function(e, json, data) {
    editor.close();  showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});