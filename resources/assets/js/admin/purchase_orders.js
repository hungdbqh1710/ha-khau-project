$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var initOptions = [];
    $.ajax({
        url: '/api/users',
        dataType: 'json',
        async: false,
        success: function (data) {
            data.forEach(function (user) {
                initOptions.push({
                    label: user.name,
                    value: user.id
                });
            })
        }
    });

    var editor = new $.fn.dataTable.Editor({
        ajax: '/admin/orders/purchases',
        table: '#orders',
        display: 'bootstrap',
        idSrc: 'id',
        fields: [{
            label: 'Nhân viên chăm sóc',
            name: 'supporter_id',
            type: 'select2',
            className: 'required',
            options: initOptions,
            opts: {
                placeholder: 'Chọn nhân viên chăm sóc'
            }
        },
            {
                label: 'Tổng tiền sản phẩm (VNĐ)',
                name: 'purchase_total_items_price',
                type: 'text',
                className: 'format-price'
            },
            {
                name: 'is_discounted',
                type: 'radio',
                options: [
                    { label: 'Giảm giá',  value: 1 },
                    { label: 'Không giảm giá', value: 0 }
                ]
            },
            {
                label: 'Trạng thái',
                name: 'status',
                type: 'select',
                className: "required",
                ipOpts: [{
                    label: 'Chưa gửi',
                    value: "1"
                },
                    {
                        label: 'Đang xử lý',
                        value: "2"
                    },
                    {
                        label: 'Đã báo giá',
                        value: "4"
                    },
                    {
                        label: 'Đã đặt hàng',
                        value: "5"
                    },
                    {
                        label: 'Đã về kho Lào Cai',
                        value: "6"
                    },
                    {
                        label: 'Thành công',
                        value: "8"
                    },
                    {
                        label: 'Đã hủy',
                        value: "9"
                    }
                ],
            },
            {
                label: 'Phần trăm phí dịch vụ (%)',
                attr: {
                    placeholder: 'Chọn phần trăm phí dịch vụ'
                },
                name: 'purchase_service_fee_percent',
                type: 'select',
                options: [
                    {
                        label: 'Chọn phần trăm phí dịch vụ',
                        value: 0
                    },
                    {
                        label: '0% tổng tiền sản phẩm',
                        value: -1
                    },
                    {
                        label: '1% tổng tiền sản phẩm',
                        value: 1
                    },
                    {
                        label: '2% tổng tiền sản phẩm',
                        value: 2
                    },
                    {
                        label: '3% tổng tiền sản phẩm',
                        value: 3
                    },
                    {
                        label: '4% tổng tiền sản phẩm',
                        value: 4
                    },
                    {
                        label: '5% tổng tiền sản phẩm',
                        value: 5
                    },
                    {
                        label: '6% tổng tiền sản phẩm',
                        value: 6
                    },
                    {
                        label: '7% tổng tiền sản phẩm',
                        value: 7
                    },
                    {
                        label: '8% tổng tiền sản phẩm',
                        value: 8
                    },
                    {
                        label: '9% tổng tiền sản phẩm',
                        value: 9
                    },
                    {
                        label: '10% tổng tiền sản phẩm',
                        value: 10
                    },
                ]
            },
            {
                label: 'Phí dịch vụ (VNĐ)',
                attr: {
                    placeholder: 'Nhập phí dịch vụ'
                },
                name: 'purchase_service_fee',
                data: function (row, type, val) {
                    return Number(row.purchase_service_fee).format();
                }
            },
            {
                label: 'Phí ship nội địa TQ (VNĐ)',
                attr: {
                    placeholder: 'Nhập phí ship nội địa TQ'
                },
                name: 'purchase_cn_transport_fee',
                className: 'format-price'
            },
            {
                label: 'Phí chuyển về VN (VNĐ)',
                attr: {
                    placeholder: 'Nhập phí vận chuyển về VN'
                },
                name: 'purchase_cn_to_vn_fee',
                className: 'format-price'
            },
            {
                label: 'Phí ship nội địa VN (VNĐ)',
                attr: {
                    placeholder: 'Nhập phí ship nội địa VN'
                },
                name: 'purchase_vn_transport_fee',
                className: 'format-price'
            },
            {
                label: 'Mã vận chuyển VN',
                attr: {
                    placeholder: 'Nhập mã vận chuyển VN',
                },
                name: 'purchase_vn_transport_code',
            },
            {
                label: 'Ghi chú của HK',
                attr: {
                    placeholder: 'Nhập ghi chú',
                    rows: 4
                },
                type: 'textarea',
                name: 'admin_note',
            },
            {
                label: 'Tổng giá cuối (VNĐ)',
                name: 'final_total_price',
                className: 'format-price'
            },
        ],
        i18n: {}
    });
    editor.field('final_total_price').disable();
    editor.field('purchase_service_fee').disable();
    editor.field('purchase_total_items_price').disable();
    editor.dependent('status', function (val, data, callback) {
        var mapStatus = {
            1: [
                {
                    label: 'Chưa gửi',
                    value: 1
                },
            ],
            2: [
                {
                    label: 'Đang xử lý',
                    value: 2
                },
                {
                    label: 'Đã báo giá',
                    value: 4
                },
                {
                    label: 'Đã hủy',
                    value: 9
                }
            ],
            4: [
                {
                    label: 'Đã báo giá',
                    value: 4
                },
                {
                    label: 'Đã đặt hàng',
                    value: 5
                },
                {
                    label: 'Đã hủy',
                    value: 9
                },
            ],
            5: [
                {
                    label: 'Đã đặt hàng',
                    value: 5
                },
                {
                    label: 'Đã về kho Lào Cai',
                    value: 6
                },
                {
                    label: 'Đã hủy',
                    value: 9
                }
            ],
            6: [
                {
                    label: 'Đã về kho Lào Cai',
                    value: 6
                },
                {
                    label: 'Thành công',
                    value: 8
                },
            ],
            8: [
                {
                    label: 'Thành công',
                    value: 8
                }
            ],
            9: [
                {
                    label: 'Đã hủy',
                    value: 9
                }
            ],
        };
        return {"options": {"status": mapStatus[data.row.status]}}
    });
    editor.dependent('purchase_service_fee', function (val, data, callback) {
        var totalItemPrice = data.row.purchase_total_items_price;
        var purchaseServiceFee = data.row.purchase_service_fee;

        $("#DTE_Field_purchase_service_fee_percent option").each(function() {
            $(this).prop('selected', false);
            if (Number($(this).val()) === Math.round(purchaseServiceFee/totalItemPrice*100)) {
                $(this).prop('selected', true);
            }
        });

        editor.field('purchase_service_fee_percent').input().on('click', function (e, d) {
            let valuePercent = $('#DTE_Field_purchase_service_fee_percent').val();
            if (Number(valuePercent) === -1) {
                $('#DTE_Field_purchase_service_fee').val(0);
            }
            if (Number(valuePercent) !== -1 && Number(valuePercent) !== 0) {
                $('#DTE_Field_purchase_service_fee').val(Number(valuePercent * totalItemPrice / 100).format());
            }
        });
        return [];
    });
    // editor.dependent('purchase_service_fee', function (val, data, callback) {
    //     let totalItemPrice = data.row.purchase_total_items_price;
    //     return {
    //         options: {
    //             "purchase_service_fee": [
    //                 {
    //                     label: '10% - ' + Number(totalItemPrice * 0.1).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.1
    //                 },
    //                 {
    //                     label: '9% - ' + Number(totalItemPrice * 0.09).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.09
    //                 },
    //                 {
    //                     label: '8% - ' + Number(totalItemPrice * 0.08).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.08
    //                 },
    //                 {
    //                     label: '7% - ' + Number(totalItemPrice * 0.07).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.07
    //                 },
    //                 {
    //                     label: '6% - ' + Number(totalItemPrice * 0.06).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.06
    //                 },
    //                 {
    //                     label: '5% - ' + Number(totalItemPrice * 0.05).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.05
    //                 },
    //                 {
    //                     label: '4% - ' + Number(totalItemPrice * 0.04).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.04
    //                 },
    //                 {
    //                     label: '3% - ' + Number(totalItemPrice * 0.03).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.03
    //                 },
    //                 {
    //                     label: '2% - ' + Number(totalItemPrice * 0.02).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.02
    //                 },
    //                 {
    //                     label: '1% - ' + Number(totalItemPrice * 0.01).format() + ' VNĐ',
    //                     value: totalItemPrice * 0.01
    //                 },
    //                 {
    //                     label: '0% - ' + 0 + ' VNĐ',
    //                     value: 0
    //                 }
    //             ]
    //         }
    //     }
    //
    // });
    editor.dependent( 'is_discounted', function ( val ) {
        return val === 1 ?
            { enable: 'purchase_total_items_price' } :
            { disable: 'purchase_total_items_price' };
    });
    $('#orders').on('click', 'a.editor_remove', function (e) {
        e.preventDefault();

        editor.remove($(this).closest('tr'), {
            title: 'Xóa bỏ đơn hàng',
            message: 'Bạn có chắc chắn muốn xóa không ?',
            buttons: [
                {
                    text: 'Xóa',
                    className: 'btn-primary',
                    fn: function () {
                        this.submit();
                    }
                },
                {
                    text: 'Hủy',
                    fn: function () {
                        this.close();
                    }
                }
            ]
        });
        return false;
    });

    $('#orders').on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        editor.edit($(this).closest('tr'), {
            title: 'Sửa thông tin đơn hàng',
            buttons: [
                {
                    text: 'Lưu lại',
                    className: 'btn-primary',
                    fn: function () {
                        this.submit();
                    }
                },
                {
                    text: 'Đóng',
                    fn: function () {
                        this.close()
                    }
                }
            ]
        });
        return false;
    });
    editor.on('edit', function (e, json, data) {
        showMessage('success', 'Cập nhật đơn hàng thành công !');
    });
    editor.on('preSubmit', function (e, d, data) {
        let row = orderTable.row( editor.modifier() ).data();
        let unmaskInputs = [
            'purchase_vn_transport_fee',
            'purchase_total_items_price',
            'purchase_cn_transport_fee',
            'purchase_cn_to_vn_fee',
            'final_total_price',
            'purchase_service_fee'
        ];
        unmaskFormatPrice(unmaskInputs, d, row, editor);
    });

    editor.on('open', function (e, json, data) {
        if (!can('view_all_purchase_orders')) {
            editor.field('supporter_id').disable();
        }
        let row = orderTable.row( editor.modifier() ).data();
        // if (row.status == 4 || row.status == 5 || row.status == 6 || row.status == 8) {
        //     editor.field('purchase_service_fee').disable();
        //     editor.field('purchase_cn_transport_fee').disable();
        //     editor.field('purchase_vn_transport_fee').disable();
        // }
        if (row.status == 8) {
            editor.field('purchase_cn_to_vn_fee').disable();
            editor.field('purchase_cn_transport_fee').disable();
            editor.field('purchase_vn_transport_fee').disable();
            editor.field('purchase_service_fee_percent').disable();
        }
        editor.field('purchase_service_fee').disable();
        formatPrice();
    });
    editor.on('postEdit postSubmit close', function() {
        editor.field('purchase_cn_transport_fee').enable();
        editor.field('purchase_vn_transport_fee').enable();
        editor.field('purchase_cn_to_vn_fee').enable();
        editor.field('purchase_service_fee_percent').enable();
    });

    editor.on('onSubmitError', function (e, json, data) {
        editor.close();
        showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
    });
    var orderTable = $('#orders').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/admin/orders/purchases',
            data: function (d) {
                d.status = $('select[name=status]').val();
            }
        },
        columns: [
            {data: 'order_number', title: 'Mã đơn hàng'},
            {data: 'purchase_vn_transport_code', title: 'Mã vận chuyển VN'},
            {data: 'customer_name', name: 'customer.name', title: 'Khách hàng'},
            {data: 'supporter_name', name: 'supporter.name', editField: 'supporter_id', title: 'Nhân viên chăm sóc'},
            {
                data: 'purchase_total_items_price', render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Tổng giá trị sản phẩm'
            },
            {
                data: 'status', render: function (data, type, row, meta) {
                    return renderLabelOrder(data, type, row, meta)
                },
                title: 'Trạng thái'
            },
            {
                data: 'transport_receive_type', render: function (data, type, row, meta) {
                    return renderReceiveType(data)
                },
                title: 'Phương thức nhận'
            },
            {data: 'confirm_date', title: 'Ngày xác nhận'},
            {
                data: 'purchase_service_fee', render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Phí dịch vụ'
            },
            {
                data: 'purchase_cn_transport_fee', render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Phí ship nội địa TQ'
            },
            {
                data: 'purchase_cn_to_vn_fee', render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Phí chuyển về VN'
            },
            {
                data: 'purchase_vn_transport_fee', render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Phí ship nội địa VN'
            },
            {
                data: 'final_total_price', render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Tổng giá cuối'
            },
            {data: 'created_at', title: 'Ngày tạo'},
            {
                defaultContent: null,
                title: 'Thao tác',
                data: null,
                name: 'action',
                render: function (data, type, row, meta) {
                    return renderActionButton(data, type, row, meta)
                },
                orderable: false,
                searchable: false,
                exportable: false,
                printable: false,
                footer: '',
                width: 'auto'
            },
        ],
        dom: "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>" +
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        language: {
            url: '/vendor/datatables/languages/Vietnamese.json'
        },
        buttons: [],
        order: [[13, 'desc']],
    });
    $('#search-form').on('submit', function(e) {
        orderTable.draw();
        e.preventDefault();
    });

    $('#refresh').on('click', function() {
        $('#filter-type').val('');
        orderTable.search('');
        orderTable.columns().search('');
        orderTable.page.len(10).draw();
    });

    var detailRows = [];
    var template = Handlebars.compile($("#details-template").html());
    $('#orders tbody').on('click', '.details-control', function (e) {
        e.preventDefault();

        var tr = $(this).closest('tr');
        var row = orderTable.row(tr);
        var tableId = 'details-' + row.data().id;
        var idx = $.inArray(tr.attr('id'), detailRows);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            detailRows.splice(idx, 1);

        } else {
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data(), orderTable);
            tr.addClass('shown');
            tr.next('tr').addClass('highlight');
            if (idx === -1) {
                detailRows.push(tr.attr('id'));
            }
        }
        return false;
    });

    orderTable.on('draw', function () {
        $.each(detailRows, function (i, id) {
            $('#' + id + ' td.details-control').trigger('click');
        });
    });
});

function initTable(tableId, data, orderTable) {
    var editorDetail = new $.fn.dataTable.Editor({
        ajax: '/admin/purchases-order-items',
        table: '#' + tableId,
        display: 'bootstrap',
        idSrc: 'id',
        fields: [
            {label: 'Mã vận chuyển TQ', name: 'cn_transport_code'},
            {label: 'Mã mua hàng TQ', name: 'cn_order_number'},
            {
                label: 'Ghi chú của HK',
                attr: {
                    placeholder: 'Nhập ghi chú',
                    rows: 4
                },
                type: 'textarea',
                name: 'admin_note',
            },
        ],
        i18n: {}
    });
    $('#' + tableId).on('click', 'a.editor_details_edit', function (e) {
        e.preventDefault();
        if (!$(this).attr('disabled')) {
            editorDetail.edit($(this).closest('tr'), {
                title: 'Sửa thông tin đơn hàng',
                buttons: [
                    {
                        text: 'Lưu lại',
                        className: 'btn-primary',
                        fn: function () {
                            this.submit();
                        }
                    },
                    {
                        text: 'Đóng',
                        fn: function () {
                            this.close()
                        }
                    }
                ]
            });
        } else {
            return false;
        }
        return false;
    });

    editorDetail.on('edit', function (e, json, data) {
        showMessage('success', 'Cập nhật chi tiết đơn hàng thành công !');
    });

    editorDetail.on('onSubmitError', function (e, json, data) {
        editorDetail.close();
        showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
    });

    let tableDetail = $('#' + tableId).DataTable({
        processing: true,
        serverSide: true,
        ajax: data.details_url,
        columns: [
            {
                data: 'product_image',
                render: function (data) {
                    return '<img class="preview-table-img" src="' + data + '"/>';
                },
                title: 'Ảnh sản phẩm'
            },
            {
                data: 'product_name', title: 'Tên sản phẩm',
                render: function (data) {
                    return '<span data-toggle="tooltip" title="' + data + '">' + data.substr(0, 10) + '...</span>';
                }
            },
            {
                data: 'product_link',
                render: function (data) {
                    return '<a href="' + data + '">Xem link sản phẩm</a>';
                },
                title: 'Link sản phẩm'
            },
            {
                data: 'array_property',
                render: function (data) {
                    return data && data.join('</br>');
                },
                title: 'Thuộc tính',
                orderable: false,
                searchable: false,
                exportable: false,
                printable: false,
                footer: '',
            },
            {data: 'qty', title: 'Số lượng'},
            {
                data: 'price',
                render: function (data, type, row, meta) {
                    return data && Number(data).format(2)
                },
                title: 'Đơn giá'
            },
            {
                data: 'current_rate',
                render: function (data, type, row, meta) {
                    return data && Number(data).format()
                },
                title: 'Tỷ giá'
            },
            {
                data: null,
                render: function (data, type, row, meta) {
                    return Number(row.price * row.qty * row.current_rate).format();
                },
                title: 'Thành tiền'
            },
            {
                data: 'customer_note', title: 'Ghi chú khách', render: function (data) {
                    return data && '<span data-toggle="tooltip" title="' + data + '">' + data.substr(0, 10) + '...</span>';
                }
            },
            {
                data: 'admin_note', title: 'Ghi chú admin', render: function (data) {
                    return data && '<span data-toggle="tooltip" title="' + data + '">' + data.substr(0, 10) + '...</span>';
                }
            },
            {data: 'cn_transport_code', title: 'Mã vận chuyển TQ'},
            {data: 'cn_order_number', title: 'Mã đặt hàng TQ'},
            {
                data: null,
                render: function (data, type, row, meta) {
                    return renderActionDetailButton(data, type, row, meta);
                },
                orderable: false,
                searchable: false,
                exportable: false,
                printable: false,
                footer: '',
                title: 'Thao tác'
            }
        ],
        dom: "f",
        language: {
            url: '/vendor/datatables/languages/Vietnamese.json'
        },
        order: [[1, 'asc']],
    });
}

function renderActionButton(data, type, row, meta) {
    let actionHtml = '';
    actionHtml += '<a href="#" class="btn btn-xs btn-primary details-control" title="Chi tiết"><i class="fa fa-info-circle"></i></a>';
    actionHtml += '<a href="#" class="btn btn-xs btn-success js-btn-delivery-address-detail" data-delivery-address="'+row.delivery_address+'" title="Địa chỉ giao hàng"><i class="fa fa-truck"></i></a>';
    if (row.status != 9 && row.status != 1) {
        actionHtml += '<a href="#" class="btn btn-xs btn-info editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>';
    }
    actionHtml += `<a href="/admin/orders/purchases/${row.id}/export" class="btn btn-xs btn-success" title="Xuất excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>`;
    return actionHtml;
}

function renderActionDetailButton(data, type, row, meta) {
    let btnEdit = '';
    if (row.order.status != 9) {
        btnEdit += '<a href="#" class="btn btn-xs btn-info editor_details_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>';
    }
    return btnEdit;
}

function renderLabelOrder(data) {
    var mapStatus = {
        1: {
            text: 'Chưa gửi',
            class: 'default'
        },
        2: {
            text: 'Đang xử lý',
            class: 'warning'
        },
        4: {
            text: 'Đã báo giá',
            class: 'info'
        },
        5: {
            text: 'Đã đặt hàng',
            class: 'primary'
        },
        6: {
            text: 'Đã về kho Lào Cai',
            class: 'info'
        },
        8: {
            text: 'Thành công',
            class: 'success'
        },
        9: {
            text: 'Đã hủy',
            class: 'danger'
        },
    }
    return '<span class="label label-' + mapStatus[data]['class'] + '">' +
        mapStatus[data]['text'] +
        '</span>';
}

function renderReceiveType(data) {
    let mapType = {
        0: {
            text: 'Nhận hàng tại Lào Cai',
            class: 'warning'
        },
        1: {
            text: 'Chuyển về nhà',
            class: 'info'
        },
    }
    return '<span class="label label-' + mapType[data]['class'] + '">' +
        mapType[data]['text'] +
        '</span>';
}

$(document).on('click', '.js-btn-delivery-address-detail', function (e) {
    e.preventDefault();
    $('#delivery-address-modal').modal('show');
    $('.js-delivery-address-content').html($(this).data('delivery-address'));
});