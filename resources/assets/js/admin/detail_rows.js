var template = Handlebars.compile($("#details-template").html());
$('#roles tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = window.LaravelDataTables["roles"].row( tr );

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        // Open this row
        row.child( template(row.data()) ).show();
        tr.addClass('shown');
    }
});