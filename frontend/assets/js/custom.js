/**
 * Class Application
 * @constructor
 */
function Application() {
    /**
     * Init date picker
     */
    this.initDatePicker = function () {
        $('.js-date-picker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
    };

    /**
     * Show modal when form contained in modal validate fail
     */
    this.showModalValidateFail = function (hasError, modalTarget) {
        if (hasError) {
            if (modalTarget !== '') {
                $("#" + modalTarget).modal('show');
            }
        }
    };
    /**
     * Init date picker and validate end date
     */
    this.initDatePickerValidateEndDate = function () {
        $(".js-start-date").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function (selected) {
            let startDate = new Date(selected.date.valueOf());
            $('.js-end-date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.js-end-date').datepicker('setStartDate', null);
        });

        $(".js-end-date").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function (selected) {
            let endDate = new Date(selected.date.valueOf());
            $('.js-start-date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
            $('.js-start-date').datepicker('setEndDate', null);
        });
    };

    /**
     * Build data to Update form after click edit button
     */
    this.buildDataUpdateModal = function () {
        $('.js-update-button').click(function () {
            let	formAction = $(this).data('url-update');
            let	urlFind    = $(this).data('url-find');
            let modal      = $(this).data('modal');

            $('.form-update').attr('action', formAction);

            $.ajax({
                type: 'GET',
                url: urlFind,
                success: function(response) {
                    if (response && response.status !== false) {
                        $(modal).modal('show');
                        // Fill value to input if field name equal id attribute of input
                        $.map(response.data, function(value, fieldName) {
                            let formItem = $('#' + fieldName);

                            if (formItem.attr('type') === 'checkbox') {
                                // Uncheck checkbox and make checked if checkbox has value equal field's value
                                formItem.prop('checked', false);
                                if (formItem.val() === value.toString()) {
                                    formItem.prop('checked', true);
                                }
                            } else if (formItem.attr('type') === 'radio') {
                                // Uncheck checkbox and make checked if checkbox has value equal field's value
                                formItem.prop('checked', false);
                                if (formItem.val() === value.toString()) {
                                    formItem.prop('checked', true);
                                } else {
                                    $('#' + fieldName + '_other').prop('checked', true);
                                }
                            } else {
                                formItem.val(value);
                            }
                        });

                        if (response.html) {
                            $('.js-html-link-qty').html(response.html);
                        }
                    } else {
                        $('#error-modal').modal('show');
                    }
                }
            });
        });
    };

    /**
     * Focus to the first input of modal which shown
     */
    this.focusFirstInputModal = function () {
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('form *').filter(':input:visible:first').focus();
        });
    };
};

/**
 * Class Customer
 * @constructor
 */
function Customer() {
    /**
     * Update is_default column of delivery address
     */
    this.updateDeliveryAddress = function () {
        $('.js-is-default-delivery-address').click(function () {
            let urlUpdate = $(this).data('url-update');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: urlUpdate,
                success: function (response) {
                    if (response && response.status !== false) {
                        $('#success-modal').modal('show');
                    } else {
                        $('#error-modal').modal('show');
                    }
                }
            });
        })
    }

    /**
     * Search product
     */
    this.searchProduct = function () {
        $('.input-search').keypress(function(e){
            if(e.keyCode == 13){
                $('.js-btn-search').click();
            }
        })

        $('.js-btn-search').click(function () {
            let typeSite  = $('.js-site-search').val();
            let keyword   = $('.input-search').val();
            let urlSearch = $(this).data('url-search');

            window.open(urlSearch + '?search=' + keyword + '&site=' + typeSite);
        });
    }

    /**
     * Get verify code
     */
    this.getVerifyCode = function () {
        $('.js-get-verify-code').click(function () {
            let phoneNumber       = $('.js-from-register .js-phone-number').val().trim();
            let email             = $('.js-from-register .js-email').val().trim();
            let urlGetVerifyCode  = $(this).data('url-get-verify-code');
            let timeCountDown     = $(this).data('total-time-countdown');
            let pinTimeToLive     = $('#js-pin-time-to-live');

            $('.js-from-register .text-danger').html('');
            if (phoneNumber === '') {
                $('.js-phone-number-message').html('Số điện thoại không được bỏ trống');
                return false;
            }

            if (!phoneNumber.match(/^0[0-9]{9,14}$/)) {
                $('.js-phone-number-message').html('Số điện thoại không hợp lệ');
                return false;
            }

            if (email === '') {
                $('.js-email-message').html('Email không được bỏ trống');
                return false;
            }

            if (!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                $('.js-email-message').html('Email không hợp lệ');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: urlGetVerifyCode,
                data: {phone: phoneNumber, email: email},
                success:function (response) {
                    if (response && response.status === true) {
                        let countDown = setInterval(function () {
                            pinTimeToLive.html(timeCountDown);
                            timeCountDown--;
                            if (timeCountDown < 0) {
                                $('#js-pin-time-to-live').closest('.text-primary').attr('hidden', true);
                                $('.js-phone-number-message').html('Bạn chưa nhận được mã? Vui lòng click vào Lấy mã xác nhận');
                                clearInterval(countDown);
                            }
                        }, 1000);
                        pinTimeToLive.closest('.text-primary').removeAttr('hidden');
                    } else if (response.status === false && response.type === 'phone') {
                        $('.js-phone-number-message').html(response.message);
                    } else if (response.status === false && response.type === 'email') {
                        $('.js-email-message').html(response.message);
                    } else {
                        $('#error-modal').modal('show');
                    }
                }
            })
        });
    }

    /**
     * Customer register
     */
    this.register = function () {
        $('.js-btn-register').click(function () {
            let urlVerifyCode         = $(this).data('url-verify-code');
            let pinCode               = $('.js-from-register .js-phone-verification').val().trim();
            let phoneNumber           = $('.js-from-register .js-phone-number').val().trim();
            let email                 = $('.js-from-register .js-email').val().trim();
            let name                  = $('.js-from-register .js-name').val().trim();
            let password              = $('.js-from-register .js-password').val().trim();
            let passwordConfirmation  = $('.js-from-register .js-password-confirmation').val().trim();

            $('.js-from-register .text-danger').html('');
            if (name === '') {
                $('.js-name-message').html('Họ và tên không được bỏ trống');
                return false;
            }

            if (email === '') {
                $('.js-email-message').html('Email không được bỏ trống');
                return false;
            }

            if (!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                $('.js-email-message').html('Email không hợp lệ');
                return false;
            }

            if (phoneNumber === '') {
                $('.js-phone-number-message').html('Số điện thoại không được bỏ trống');
                return false;
            }

            if (!phoneNumber.match(/^0[0-9]{9,14}$/)) {
                $('.js-phone-number-message').html('Số điện thoại không hợp lệ');
                return false;
            }

            if (pinCode === '') {
                $('.js-phone-verification-message').html('Mã xác nhận không được bỏ trống');
                return false;
            }

            if (!pinCode.match(/^[0-9]{4,10}$/)) {
                $('.js-phone-verification-message').html('Mã xác nhận không hợp lệ');
                return false;
            }

            if (password === '') {
                $('.js-password-message').html('Mật khẩu không được bỏ trống');
                return false;
            }

            // if (!password.match(/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z\d]).\S*$/)) {
            //     $('.js-password-message').html('Mật khẩu phải có độ dài nhỏ nhất 8 ký tự, gồm chữ hoa, chữ thường, ký tự đặc biết và không được có khoảng trống');
            //     return false;
            // }

            if (passwordConfirmation === '') {
                $('.js-password-confirmation-message').html('Mật khẩu xác nhận không được bỏ trống');
                return false;
            }

            if (password !== passwordConfirmation) {
                $('.js-password-message').html('Mật khẩu không khớp');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: urlVerifyCode,
                data: {pinCode: pinCode, phone: phoneNumber},
                success:function (response) {
                    if (response && response.status === true) {
                        $('.js-btn-register').closest('form').submit();
                    } else {
                        $('.js-phone-verification-message').html('Mã xác nhận không chính xác');
                    }
                }
            })
        });
    }

    /**
     * Init TawkTo chat
     */
    this.initTawkTo = function () {
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5bd674d965224c2640512de0/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    }

    /**
     * Prevent double submit form
     */
    this.preventDoubleSubmit = function () {
        $('.js-add-to-order').click(function (e) {
            e.preventDefault();
            $(this).attr('hidden', true);
            $(this).closest('form').submit();
        })
    }
}

/**
 * Class Order
 * @constructor
 */
function Order() {
    this.deleteOrderItem = function() {
        $('.delete-button').click(function(){
            $('#delete-modal').modal('show');
            var url = $(this).data('url-delete');
            $('.js-form-delete').attr('action', url);
            return false;
        });
    }

    /**
     * Add shipping order item html to form
     */
    this.addShippingOrderItemHtmlForm = function () {
        let numericalOrderForm = 0;
        let order = new Order();
        $('.js-btn-add-order').click(function () {
            numericalOrderForm++;
            $('#modal-body-content .js-remove-order-info:first-child').removeAttr('hidden');
            let orderItemForm = document.getElementById('modal-body-content');
            let orderItemHtmlForm = $('#modal-body-content :first').html();
            $('#modal-body-content').append(orderItemHtmlForm);
            $('.modal-info-order').last().attr('data-numerical-order', numericalOrderForm);
            orderItemForm.scrollTop = orderItemForm.scrollHeight;
            let app = new Application();
            app.initDatePicker();
            order.addNameInputWhenCreateTransportOrder();
            $('#modal-body-content div .modal-info-order .js-remove-order-info').attr('hidden', true);
        });

        $(document).on('click', '.js-btn-add-link-qty', function () {
            let linkQtyHtmlPattern = $('.js-link-qty-html').html();
            $(this).closest('.modal-info-order').append(linkQtyHtmlPattern);
            order.addNameInputWhenCreateTransportOrder()
        });

        $(document).on('click', '.js-btn-delete', function () {
            $(this).closest('.row').remove();
        });
    };

    this.addNameInputWhenCreateTransportOrder = function () {
        $('.js-cn-transport-code').focus(function () {
            let numericalOrder = $(this).closest('.modal-info-order').data('numerical-order');
            $(this).attr('name', 'order['+numericalOrder+'][cn_transport_code]')
        });

        $('.js-cn-order-date').focus(function () {
            let numericalOrder = $(this).closest('.modal-info-order').data('numerical-order');
            $(this).attr('name', 'order['+numericalOrder+'][cn_order_date]')
        });

        $('.js-customer-note').focus(function () {
            let numericalOrder = $(this).closest('.modal-info-order').data('numerical-order');
            $(this).attr('name', 'order['+numericalOrder+'][customer_note]')
        });

        $('.js-receiver-info').focus(function () {
            let numericalOrder = $(this).closest('.modal-info-order').data('numerical-order');
            $(this).attr('name', 'order['+numericalOrder+'][receiver_info]')
        });

        $('.js-link-order').focus(function () {
            let numericalOrder = $(this).closest('.modal-info-order').data('numerical-order');
            $(this).attr('name', 'order['+numericalOrder+'][link][]')
        });

        $('.js-qty-order').focus(function () {
            let numericalOrder = $(this).closest('.modal-info-order').data('numerical-order');
            $(this).attr('name', 'order['+numericalOrder+'][qty][]')
        });
    }

    this.detailLinkQtyTransportOrderItem = function () {
        $('.js-detail-link-qty').click(function () {
            let urlRenderLinkQty = $(this).data('url');
            $.ajax({
                type: 'GET',
                url: urlRenderLinkQty,
                success: function (response) {
                    if (response.status === true) {
                        $('#js-detail-link-qty-tbody').html(response.data);
                    }
                    $('#detail-link-qty-modal').modal('show');
                }
            })
        });
    }

    this.removeTransportOrderForm = function () {
        $(document).on('click', '.js-remove-order-info', function () {
            let transportOrderModal = $(this).closest('.modal-info-order');
            transportOrderModal.prop('hidden', true);
            transportOrderModal.find('input').each(function () {
                $(this).removeAttr('name').removeAttr('required');
            });
            transportOrderModal.find('textarea').each(function () {
                $(this).removeAttr('name').removeAttr('required');
            });
        });
    }
}

/**
 * Class Slide
 * @constructor
 */
function Slide() {
    /**
     * Init slide
     */
    this.initSlide = function () {
        $('.js-carousel-slide').owlCarousel({
            loop: true,
            margin: 30,
            autoplay: true,
            autoplayTimeout: 5000,
            nav: true,
            navText: ['<', '>'],
            responsive : {
                0 : {
                    items: 1,
                },
                480 : {
                    items: 2,
                },
                1024 : {
                    items: 4,
                }
            }
        })
    }
};

/**
 * Class Feedback
 * @constructor
 */
function Feedback() {
    /**
     * Show feedback popover
     */
    this.showFeedbackPopover = function () {
        $("[data-toggle=popover]").each(function() {
            $(this).popover({
                html: true,
                content: function() {
                    $('.js-btn-popover-submit').attr('hidden', false);
                    $('.js-feedback-message').attr('hidden', true);
                    return $('#js-popover-content-feedback').html();
                }
            });

            $(document).on('click', '.js-btn-popover-close', function () {
                $('.js-feedback-message').attr('hidden', true);
                $("[data-toggle=popover]").popover('hide');
            });
        });
    };

    /**
     * Send feedback
     */
    this.sendFeedback = function () {
        $(document).on('click', '.js-btn-popover-submit', function () {
            let urlSend = $(this).data('url-send-feedback');
            let phoneNumber = $(this).closest('.form-send-feedback').find('.js-feedback-phone-number').val().trim();
            let content = $(this).closest('.form-send-feedback').find('.js-feedback-content').val();

            $('.js-phone-number-error').attr('hidden', true);
            $('.js-content-error').attr('hidden', true);

            if (phoneNumber === '') {
                $('.js-phone-number-error').attr('hidden', false).html('Số điện thoại không được bỏ trống');
                return false;
            }

            if (!phoneNumber.match(/^0[0-9\-\+\(\)]{9,14}$/)) {
                $('.js-phone-number-error').attr('hidden', false).html('Số điện thoại không hợp lệ');
                return false;
            }

            if (content.trim() === '') {
                $('.js-content-error').attr('hidden', false);
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: urlSend,
                data: {'phone_number': phoneNumber, 'content': content},
                success: function (response) {
                    if (response && response.status !== false) {
                        $('.js-btn-popover-submit').attr('hidden', true);
                        $('.js-feedback-message').attr('hidden', false).addClass('text-primary').html('Cảm ơn bạn đã đóng góp ý kiến để giúp chúng tôi nâng cao chất lượng dịch vụ!')
                    } else {
                        $('.js-feedback-message').attr('hidden', false).addClass('text-danger').html('Xảy ra lỗi, vui lòng thử lại')
                    }
                }
            })
        });
    }
    this.showNotification = function () {
        $("[data-toggle=notification]").each(function() {
            $(this).popover({
                html: true,
                content: function() {
                    return $('#js-popover-content-notification').html();
                }
            });
        });
        $(document).on('click', 'body', function (e) {
                $('[data-toggle="notification"]').popover('hide');
        });
    };
}