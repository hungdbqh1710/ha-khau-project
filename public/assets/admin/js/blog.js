$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/blogs",
    table: "#blog-table",
    display: "bootstrap",
    idSrc: 'id',
    fields: [],
    i18n: {}
});

$('#blog-table').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: "Xóa bài viết",
        message: "Bạn chắc chắn xóa?",
        buttons: [{
            text: "Xóa", className: 'btn-primary', fn: function fn() {
                this.submit();
            }
        }, {
            text: "Đóng", className: 'btn-default', fn: function fn() {
                this.close();
            }
        }]
    });
});

function renderBlogImage(data) {
    return '<img src="/storage/app/public/blog-images/' + data + '" class="preview-table-img">';
}

$('#blog-table').on('click', 'tbody td:nth-child(4)', function (e) {
    editor.inline(this);
});

editor.on('create', function (e, json, data) {
    showMessage('success', 'Thêm mới bài viết thành công !');
});

editor.on('edit', function (e, json, data) {
    showMessage('success', 'Cập nhật bài viết thành công !');
});

editor.on('remove', function (e, json, data) {
    showMessage('success', 'Xóa bài viết thành công !');
});

editor.on('onSubmitError', function (e, json, data) {
    editor.close();showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return '<a href="/admin/blogs/' + data + '/edit" class="btn btn-xs btn-info js-btn-delete editor_edit" title="S\u1EEDa"> <i class="fa fa-pencil"></i> </a>' + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}

function previewImage() {
    var totalFiles = document.getElementById("js-image").files.length;
    if (totalFiles !== 0) {
        $('.preview-image img').remove();
        $('.preview-image').append("<img src='" + URL.createObjectURL(event.target.files[0]) + "'>");
    }
}