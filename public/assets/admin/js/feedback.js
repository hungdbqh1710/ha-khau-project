$(document).on('click', '.js-btn-detail', function () {
    var feedback = $(this).data('data');
    $('#detail-feedback-modal').modal('show');
    $('.js-feedback-phone-number').html(feedback['phone_number']);
    $('.js-feedback-content').html(feedback['content']);
});