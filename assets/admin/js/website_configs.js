$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = new $.fn.dataTable.Editor({
    ajax: "/admin/website-configs",
    table: "#website-configs",
    display: "bootstrap",
    idSrc: 'id',
    fields: [{
        name: "name",
        type: "hidden"
    }, {
        label: "Tên",
        name: "display_name",
        className: "required"
    }, {
        label: "Giá trị",
        name: "content",
        className: "required",
        attr: {
            placeholder: 'Nhập giá trị'
        }
    }, {
        name: "link",
        attr: {
            placeholder: 'Nhập đường dẫn'
        }
    }],
    i18n: {
        create: {
            button: "Thêm",
            submit: "Tạo mới",
            title: "Thêm mới"
        }
    }
});

$('#website-configs').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();
    editor.remove($(this).closest('tr'), {
        title: "Xóa cấu hình",
        message: "Bạn chắc chắn xóa?",
        buttons: [{
            text: "Xóa", className: 'btn-primary', fn: function fn() {
                this.submit();
            }
        }, {
            text: "Đóng", className: 'btn-default', fn: function fn() {
                this.close();
            }
        }]
    });
});

$('#website-configs').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();
    editor.clear('content');
    editor.clear('link');
    var row = $(this).closest('tr');
    var inputName = $('#website-configs').DataTable().row(row).data()['name'];
    if (inputName == 'payment_cash' || inputName == 'payment_transfer') {
        var inputType = 'textarea';
        var inputId = 'js-payment-content';
        $(document).ready(function () {
            $('.modal-dialog').addClass('modal-lg');
        });
    }
    if (inputName == 'popup_image') {
        var inputType = 'upload';
    }
    if (inputType == 'upload') {
        editor.add({
            label: "Ảnh",
            name: "content",
            type: "upload",
            display: function display(data) {
                return '<img src="/storage/app/public/slide-images/' + data + '" class="img-responsive">';
            },
            clearText: "Clear",
            noImageText: 'No image'
        });
        editor.add({
            label: "Đường dẫn",
            name: "link",
            attr: {
                placeholder: 'Nhập đường dẫn'
            }
        });
    } else {
        editor.add({
            type: inputType ? inputType : 'text',
            label: "Giá trị",
            name: "content",
            className: "required",
            attr: {
                id: inputId ? inputId : '',
                placeholder: 'Nhập giá trị'
            }
        });
        editor.add({
            name: "link",
            type: "hidden"
        });
    }

    editor.edit($(this).closest('tr'), {
        title: 'Sửa cấu hình website',
        buttons: [{
            text: 'Lưu lại',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            className: 'btn-default',
            fn: function fn() {
                this.close();
            }
        }]
    });

    var token = $('meta[name="csrf-token"]').attr('content');
    $('#js-payment-content').ckeditor({
        height: 200,
        filebrowserImageBrowseUrl: routePrefix + '?type=Images',
        filebrowserImageUploadUrl: routePrefix + '/upload?type=Images&_token=' + token,
        filebrowserBrowseUrl: routePrefix + '?type=Files',
        filebrowserUploadUrl: routePrefix + '/upload?type=Files&_token=' + token
    });
    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.config.removePlugins = 'about';

    return false;
});

editor.on('create', function (e, json, data) {
    showMessage('success', 'Thêm mới cấu hình thành công !');
});

editor.on('edit', function (e, json, data) {
    showMessage('success', 'Cập nhật cấu hình thành công !');
});

editor.on('remove', function (e, json, data) {
    showMessage('success', 'Xóa cấu hình thành công !');
});

editor.on('onSubmitError', function (e, json, data) {
    editor.close();showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

editor.on('initEdit', function () {
    editor.show();
    editor.field('display_name').disable();
});

editor.on('edit close', function (e, json, data) {
    $(document).ready(function () {
        $('.modal-dialog').removeClass('modal-lg');
    });
    editor.field('content').message('');
});

editor.on('open', function (e, json, data) {
    var row = window.LaravelDataTables["website-configs"].row(editor.modifier()).data();
    if (row.name == 'popup_image') {
        editor.field('content').error('Xóa bỏ ảnh nếu không muốn hiển thị popup ở trang chủ');
    }
});
function renderSlideImage(data, type, full, meta) {
    if (full.name == 'popup_image') {
        return full.content && '<img src="/storage/app/public/slide-images/' + full.content + '" class="preview-table-img">';
    }
    return data;
}