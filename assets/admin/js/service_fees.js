$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/service-fees',
    table: '#service-fees',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [{
        label: 'Từ giá (VNĐ)',
        name: 'from_price',
        attr: {
            placeholder: 'Nhập từ giá'
        }
    }, {
        label: 'Đến giá (VNĐ)',
        name: 'to_price',
        attr: {
            placeholder: 'Nhập đến giá'
        }
    }, {
        label: 'Phí (%)',
        className: "required",
        name: 'fee',
        attr: {
            placeholder: 'Nhập phí'
        }
    }],
    i18n: {
        create: {
            button: 'Thêm',
            title: 'Thêm mới phí dịch vụ',
            submit: 'Lưu lại'
        }
    }
});
$('#service-fees').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa thông tin phí dịch vụ',
        buttons: [{
            text: 'Lưu lại',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});
$('#service-fees').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();

    editor.remove($(this).closest('tr'), {
        title: 'Xóa bỏ phí dịch vụ',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [{
            text: 'Xóa',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});

editor.on('create', function (e, json, data) {
    showMessage('success', 'Thêm mới phí dịch vụ thành công !');
});

editor.on('edit', function (e, json, data) {
    showMessage('success', 'Chỉnh sửa phí dịch vụ thành công !');
});

editor.on('remove', function (e, json, data) {
    showMessage('success', 'Xóa phí dịch vụ thành công !');
});

editor.on('onSubmitError', function (e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return '<a href="#" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>' + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}