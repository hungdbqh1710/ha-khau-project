$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/transport-fees',
    table: '#transport-fees',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [{
        label: 'Từ cân (Kg)',
        name: 'from_weight',
        attr: {
            placeholder: 'Nhập từ cân'
        }
    }, {
        label: 'Đến cân (Kg)',
        name: 'to_weight',
        attr: {
            placeholder: 'Nhập đến cân'
        }
    }, {
        label: 'Phí về HN (VNĐ)',
        name: 'hn_fee',
        className: "required",
        attr: {
            placeholder: 'Nhập phí về HN'
        }
    }, {
        label: 'Phí về HCM (VNĐ)',
        name: 'hcm_fee',
        className: "required",
        attr: {
            placeholder: 'Nhập phí về HCM'
        }
    }],
    i18n: {
        create: {
            button: 'Thêm',
            title: 'Thêm mới phí vận chuyển',
            submit: 'Lưu lại'
        }
    }
});
$('#transport-fees').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa thông tin phí vận chuyển',
        buttons: [{
            text: 'Lưu lại',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});
$('#transport-fees').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();

    editor.remove($(this).closest('tr'), {
        title: 'Xóa bỏ phí vận chuyển',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [{
            text: 'Xóa',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});

editor.on('create', function (e, json, data) {
    showMessage('success', 'Thêm mới phí vận chuyển thành công !');
});

editor.on('edit', function (e, json, data) {
    showMessage('success', 'Chỉnh sửa phí vận chuyển thành công !');
});

editor.on('remove', function (e, json, data) {
    showMessage('success', 'Xóa phí vận chuyển thành công !');
});

editor.on('onSubmitError', function (e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

function renderActionButton(data) {
    return '<a href="#" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>' + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}