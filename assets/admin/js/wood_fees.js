$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var editor = new $.fn.dataTable.Editor({
    ajax: '/admin/wood-fees',
    table: '#wood-fees',
    display: 'bootstrap',
    idSrc: 'id',
    fields: [{
        label: 'Tên:',
        name: 'name',
        attr: {
            placeholder: 'Nhập tên'
        }
    }, {
        label: 'KG đầu tiên (VNĐ)',
        name: 'first_kg_fee',
        className: "required",
        attr: {
            placeholder: 'Nhập kg đầu tiên'
        }
    }, {
        label: 'KG tiếp theo (VNĐ)',
        name: 'next_kg_fee',
        className: "required",
        attr: {
            placeholder: 'Nhập kg tiếp theo'
        }
    }],
    i18n: {
        create: {
            button: 'Thêm',
            title: 'Thêm mới phí đóng gỗ',
            submit: 'Lưu lại'
        }
    }
});
$('#wood-fees').on('click', 'a.editor_edit', function (e) {
    e.preventDefault();

    editor.edit($(this).closest('tr'), {
        title: 'Sửa thông tin phí đóng gỗ',
        buttons: [{
            text: 'Lưu lại',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});
$('#wood-fees').on('click', 'a.editor_remove', function (e) {
    e.preventDefault();

    editor.remove($(this).closest('tr'), {
        title: 'Xóa bỏ phí đóng gỗ',
        message: 'Bạn có chắc chắn muốn xóa không ?',
        buttons: [{
            text: 'Xóa',
            className: 'btn-primary',
            fn: function fn() {
                this.submit();
            }
        }, {
            text: 'Đóng',
            fn: function fn() {
                this.close();
            }
        }]
    });
    return false;
});

editor.on('create', function (e, json, data) {
    showMessage('success', 'Thêm mới phí đóng gỗ thành công !');
});

editor.on('edit', function (e, json, data) {
    showMessage('success', 'Chỉnh sửa phí đóng gỗ thành công !');
});

editor.on('remove', function (e, json, data) {
    showMessage('success', 'Xóa phí đóng gỗ thành công !');
});

editor.on('onSubmitError', function (e, json, data) {
    editor.close();
    showMessage('error', 'Lỗi hệ thống, vui lòng thử lại sau !');
});

editor.on('initEdit', function () {
    editor.field('name').disable();
});

function renderActionButton(data) {
    return '<a href="#" class="btn btn-xs btn-info js-btn-delete editor_edit" title="Sửa"> <i class="fa fa-pencil"></i> </a>' + '<a href="#" class="btn btn-xs btn-danger js-btn-delete editor_remove" title="Xóa"><i class="fa fa-trash-o"></i></a>';
}