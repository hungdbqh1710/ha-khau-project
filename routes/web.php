<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
    Auth::routes();
    Route::middleware('auth')->group(function () {
        Route::get('/', 'OrderController@indexPurchaseOrder')->name('index');
        Route::group(
            [
                'middleware' => ['permission:create_users|edit_users|delete_users']
            ], function () {
            Route::get('users', 'UserController@index')->name('users.index');
            Route::post('users', 'UserController@action')->name('users.action');
        });

        Route::group(
            [
                'prefix' => 'exchange-rate', 'as' => 'exchange-rate.',
                'middleware' => ['permission:create_exchange_rates|edit_exchange_rates|delete_exchange_rates']
            ],
            function () {
                Route::get('/', 'ExchangeRateController@index')->name('index');
                Route::post('/', 'ExchangeRateController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'orders', 'as' => 'orders.'],
            function () {
                Route::get('/purchases/{id}/details', 'OrderController@showPurchaseOrderDetail')->name('purchases.detail');
                Route::get('/show-order/{customer_id}', 'OrderController@showByCustomerId')->name('showByCustomerId');
                Route::get('/purchases', 'OrderController@indexPurchaseOrder')->name('purchases.index');
                Route::post('/purchases', 'OrderController@actionPurchaseOrder')->name('purchases.action');
                Route::get('/purchases/{id}/export', 'OrderController@exportPurchaseOrder')->name('purchases.export');
                Route::delete('/purchases/{id}', 'OrderController@destroy')->name('delete');
            }
        );

        Route::group(['prefix' => 'purchases-order-items', 'as' => 'order-items.'],
            function () {
                Route::post('/', 'PurchaseOrderItemController@action')->name('action');
            }
        );

        Route::group(
            [
                'prefix' => 'customers', 'as' => 'customers.',
                'middleware' => 'permission:view_customer_info|create_customers'
            ],
            function () {
                Route::get('/', 'CustomerController@index')->name('index');
                Route::post('/', 'CustomerController@action')->name('action');
                Route::get('/{id}/transactions', 'CustomerController@showTransaction')->name('show-transactions');
            }
        );

        Route::group(
            [
                'prefix' => 'feedback', 'as' => 'feedback.',
                'middleware' => ['permission:view_feedback']
            ],
            function () {
                Route::get('/', 'FeedbackController@index')->name('index');
            }
        );

        Route::group(
            [
                'prefix' => 'slides', 'as' => 'slides.',
                'middleware' => ['permission:create_slides|edit_slides|delete_slides']
            ],
            function () {
                Route::get('/{group}', 'SlideController@index')->name('index');
                Route::post('/', 'SlideController@action')->name('action');
            }
        );
        Route::group(['prefix' => 'permissions', 'as' => 'permissions.'],
            function () {
                Route::get('/', 'PermissionController@index')->name('index');
                Route::post('/', 'PermissionController@action')->name('action');
            }
        );
        Route::group(
            [
                'prefix' => 'roles', 'as' => 'roles.',
                'middleware' => ['permission:create_roles|edit_roles|delete_roles']
            ],
            function () {
                Route::get('/', 'RoleController@index')->name('index');
                Route::post('/', 'RoleController@action')->name('action')->middleware('permission:delete_roles');
                Route::get('/{id}/edit', 'RoleController@edit')->name('edit')->middleware('permission:edit_roles');
                Route::post('/store', 'RoleController@store')->name('store')->middleware('permission:create_roles');
                Route::post('/{id}', 'RoleController@update')->name('update')->middleware('permission:edit_roles');
                Route::get('/create', 'RoleController@create')->name('create')->middleware('permission:create_roles');
            }
        );

        Route::group(['prefix' => 'blogs', 'as' => 'blogs.'], function () {
            Route::get('/', 'BlogController@index')->name('index')->middleware('permission:create_blogs|edit_blogs|delete_blogs');
            Route::get('/create', 'BlogController@create')->name('create')->middleware('permission:create_blogs');
            Route::post('/store', 'BlogController@store')->name('store')->middleware('permission:create_blogs');
            Route::get('/{id}/edit', 'BlogController@edit')->name('edit')->middleware('permission:edit_blogs');
            Route::post('/{id}/update', 'BlogController@update')->name('update')->middleware('permission:edit_blogs');
            Route::post('/', 'BlogController@action')->name('action');
        });

        Route::group(
            [
                'prefix' => 'website-configs', 'as' => 'website-configs.',
                'middleware' => ['permission:edit_website_configs']
            ],
            function () {
                Route::get('/', 'WebsiteConfigController@index')->name('index');
                Route::post('/', 'WebsiteConfigController@action')->name('action');
            }
        );

        Route::group(
            [
                'prefix' => 'cms-pages', 'as' => 'cms-pages.',
                'middleware' => ['permission:create_pages|edit_pages|delete_pages']
            ],
            function () {
                Route::get('/', 'CmsPageController@index')->name('index');
                Route::get('/create', 'CmsPageController@create')->name('create')->middleware('permission:create_pages');
                Route::post('/store', 'CmsPageController@store')->name('store')->middleware('permission:create_pages');
                Route::get('/{id}/edit', 'CmsPageController@edit')->name('edit')->middleware('permission:edit_pages');
                Route::post('/{id}/update', 'CmsPageController@update')->name('update')->middleware('permission:edit_pages');
                Route::post('/', 'CmsPageController@action')->name('action')->middleware('permission:delete_pages');
            }
        );

        Route::group(['prefix' => 'service-fees', 'as' => 'service-fees.'],
            function () {
                Route::get('/', 'ServiceFeeController@index')->name('index');
                Route::post('/', 'ServiceFeeController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'transport-fees', 'as' => 'transport-fees.'],
            function () {
                Route::get('/', 'TransportFeeController@index')->name('index');
                Route::post('/', 'TransportFeeController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'count-fees', 'as' => 'count-fees.'],
            function () {
                Route::get('/', 'CountFeeController@index')->name('index');
                Route::post('/', 'CountFeeController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'wood-fees', 'as' => 'wood-fees.'],
            function () {
                Route::get('/', 'WoodFeeController@index')->name('index');
                Route::post('/', 'WoodFeeController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'customer-level', 'as' => 'customer-level.'],
            function () {
                Route::get('/', 'CustomerLevelController@index')->name('index');
                Route::post('/', 'CustomerLevelController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'transport-orders', 'as' => 'transport-orders.'],
            function () {
                Route::get('/{id}/details', 'TransportOrderController@show')->name('detail');
                Route::get('/', 'TransportOrderController@index')->name('index');
                Route::post('/', 'TransportOrderController@action')->name('action');
                Route::get('/link-qty', 'TransportOrderController@renderLinkQty')->name('renderLinkQty');
                Route::get('/{id}/export', 'TransportOrderController@export')->name('export');
            }
        );

        Route::group(['prefix' => 'transport-order-items', 'as' => 'transport-order-items.'],
            function () {
                Route::get('/link-qty', 'TransportOrderItemController@renderLinkQty')->name('renderLinkQty');
                Route::post('/', 'TransportOrderItemController@action')->name('action');
            }
        );

        Route::group(
            [
                'prefix' => 'reports', 'as' => 'reports.',
                'middleware' => 'permission:view_reports'
            ],
            function() {
                Route::get('/debt', 'ReportController@debt')->name('debt');
                Route::post('/debt/export', 'ReportController@debtExport')->name('debt.export');
                Route::get('/revenue', 'ReportController@revenue')->name('revenue');
                Route::get('/export-revenue', 'ReportController@exportRevenue')->name('exportRevenue');
                Route::get('/calculation-revenue', 'ReportController@calculateRevenue')->name('calculateRevenue');
                Route::get('/calculation-debt', 'ReportController@calculateDebt')->name('calculateDebt');
                Route::post('/payment-debt', 'ReportController@paymentDebt')->name('paymentDebt');
            }
        );

        Route::group(['prefix' => 'transactions', 'as' => 'transactions.'],
            function () {
                Route::post('/', 'TransactionController@action')->name('action');
            }
        );

        Route::group(['prefix' => 'ship-codes', 'as' => 'ship-codes.'],
            function () {
                Route::get('/', 'ShipCodeController@index')->name('index');
                Route::get('/create-ship-code', 'ShipCodeController@formCreate')->name('create');
                Route::post('/edit', 'ShipCodeController@edit')->name('edit');
                Route::post('/create-update', 'ShipCodeController@createOrUpdate')->name('createOrUpdate');
                Route::get('/filter-customers', 'ShipCodeController@getCustomersByStatus')->name('filter');
                Route::get('/search', 'ShipCodeController@search')->name('search');
                Route::post('/get-excel', 'ShipCodeController@getExcel')->name('get-excel');
                Route::get('/page', 'ShipCodeController@pageItem')->name('page');
                Route::post('/delete', 'ShipCodeController@delete')->name('delete');
                Route::post('/import', 'ShipCodeController@import')->name('import');
            }
        );
    });
});

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/', 'HomeController@index')->name('home-page');

Route::group(['prefix' => config('auth.guard_customers.prefix'), 'namespace' => 'Customers', 'as' => 'customers.'], function () {
    Auth::routes();
    Route::get('/', 'CustomerController@showProfile')->name('showProfile');
    Route::get('/update-password', 'CustomerController@showFormUpdatePassword')->name('showFormUpdatePassword');
    Route::post('/update-password', 'CustomerController@updatePassword')->name('updatePassword');
    Route::get('/debt', 'CustomerController@debt')->name('debt');
    Route::post('/update/{customer_id}', 'CustomerController@update')->name('update');

    Route::group(['prefix' => 'delivery-address', 'as' => 'delivery-address.'], function () {
        Route::post('/new', 'CustomerController@createDeliveryAddress')->name('create');
        Route::post('/update', 'CustomerController@updateDeliveryAddress')->name('update');
        Route::get('/ajax-search/{id}', 'CustomerController@ajaxSearchAddress')->name('ajaxSearch');
        Route::post('/update-default/{id}', 'CustomerController@updateAddressDefault')->name('updateDefault');
    });

    Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function(){
        Route::get('/', 'NotificationController@index')->name('index');
        Route::get('/read/{id}', 'NotificationController@read')->name('read');
    });
});

Route::group(['prefix' => 'orders', 'as' => 'orders.', 'namespace' => 'Customers'], function () {
    Route::get('/export', 'OrderController@export')->name('export');
    Route::post('/temps', 'OrderTempController@store');
    Route::post('/update/transport/{id}', 'OrderController@updateTransport')->name('updateTransport');
    Route::get('/find/transport/{id}', 'OrderController@ajaxFindTransport')->name('ajaxFindTransport');
    Route::get('/temps', 'OrderTempController@show')->name('temps.show');
    Route::get('/transports', 'OrderController@indexTransportOrder')->name('indexTransportOrder');
    Route::get('/export-transports', 'OrderController@exportTransportOrder')->name('exportTransportOrder');
    Route::post('/transports/store', 'OrderController@storeTransportOrder')->name('storeTransportOrder');
    Route::get('/transports/{id}/detail', 'OrderController@showTransportOrderDetail')->name('showTransportOrderDetail');
    Route::get('/purchases', 'OrderController@indexPurchaseOrder')->name('purchases.index');
    Route::post('/purchases', 'OrderController@storePurchaseOrder')->name('purchases.store');
    Route::get('/purchases/{id}', 'OrderController@showPurchaseOrder')->name('purchases.show');
    Route::post('/send-order/{id}', 'OrderController@sendOrder')->name('send-order');
    Route::post('/cancel-order/{id}', 'OrderController@cancelOrder')->name('cancel-order');
    Route::get('/{id}/pay', 'OrderController@showDepositView')->name('show-deposit-view');
    Route::get('/{id}/link-qty', 'OrderController@renderLinkQty')->name('renderLinkQty');
    Route::delete('/{id}', 'OrderController@destroy')->name('delete');
});

Route::group(['prefix' => 'purchase-items', 'as' => 'purchase-items.', 'namespace' => 'Customers'], function () {
    Route::delete('/{id}', 'PurchaseOrderItemController@destroy')->name('delete');
});

Route::group(['prefix' => 'feedback', 'as' => 'feedback.', 'namespace' => 'Admin'],
    function () {
        Route::post('/sent', 'FeedbackController@send')->name('send');
    }
);

Route::group(['prefix' => 'blog', 'as' => 'blog.', 'namespace' => 'Customers'],
    function () {
        Route::get('/', 'BlogController@index')->name('index');
        Route::get('/{slug}', 'BlogController@show')->name('show');
    }
);
Route::group(['prefix' => 'ship-codes', 'as' => 'ship_codes.', 'namespace' => 'Customers'],
    function () {
        Route::get('/', 'ShipCodeController@index')->name('index');
        Route::get('/search', 'ShipCodeController@search')->name('search');
    }
);

Route::get('/search-product', 'Customers\CustomerController@searchProduct')->name('searchProduct');

Route::get('/api/transactions', 'Api\TransactionController@byCustomer')->name('api.transactions.byCustomer');
Route::get('/api/orders', 'Api\OrderController@byCustomer')->name('api.orders.byCustomer');
Route::get('/api/customers/{id}/orders', 'Api\CustomerController@findOrderByCustomer')->name('api.users.findOrderByCustomer');
Route::get('/api/customers/{id}/transactions', 'Api\CustomerController@findTransactionByCustomer')->name('api.users.findTransactionByCustomer');

Route::get('/{slug}', 'Admin\CmsPageController@show')->name('cms-pages.show');
