<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function(){
    Route::get('users', 'UserController@index');

    Route::get('permissions', 'PermissionController@index');

    Route::get('roles', 'RoleController@index');

    Route::post('/get-code', 'VerifyCodeController@getVerifyCode')->name('api.getVerifyCode');

    Route::post('/verify', 'VerifyCodeController@verifyCode')->name('api.verifyCode');

    Route::post('/createOrUpdate', 'ShipCodeController@createOrUpdate')->name('api.createOrUpdate');
});