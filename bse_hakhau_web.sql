-- MySQL dump 10.16  Distrib 10.1.35-MariaDB, for Linux (i686)
--
-- Host: localhost    Database: bse_hakhau_web
-- ------------------------------------------------------
-- Server version	10.1.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` VALUES (4,NULL,'Thông xe thí điểm tuyến xe khách quốc tế Côn minh (TQ) - Lào cai - Hà nội - Hải phòng','<p>S&aacute;ng 17-9, tại Bến xe kh&aacute;ch Thượng L&yacute; (TP Hải Ph&ograve;ng), Bộ Giao th&ocirc;ng vận tải phối hợp UBND th&agrave;nh phố Hải Ph&ograve;ng tổ chức khởi h&agrave;nh phương tiện chạy th&iacute; điểm tuyến vận tải h&agrave;nh kh&aacute;ch Hải Ph&ograve;ng - H&agrave; Nội - C&ocirc;n Minh (Trung Quốc).</p>\r\n\r\n<p>Việc th&iacute; điểm tuyến vận tải h&agrave;nh kh&aacute;ch Hải Ph&ograve;ng - H&agrave; Nội - C&ocirc;n Minh (Trung Quốc) v&agrave; tuyến vận tải h&agrave;ng h&oacute;a Th&acirc;m Quyến (Trung Quốc) - H&agrave; Nội nhằm th&uacute;c đẩy hoạt động vận tải qua lại bi&ecirc;n giới v&agrave;o s&acirc;u trong l&atilde;nh thổ mỗi quốc gia, đ&aacute;p ứng nhu cầu vận chuyển h&agrave;ng h&oacute;a, đi lại, du lịch của người d&acirc;n cũng như trao đổi h&agrave;ng h&oacute;a, ph&aacute;t triển kinh tế - thương mại giữa hai nước.</p>\r\n\r\n<p>Sau lễ khởi h&agrave;nh, Bộ Giao th&ocirc;ng vận tải hai nước đ&atilde; trao đổi giấy ph&eacute;p vận tải v&agrave; thực hiện cấp giấy ph&eacute;p vận tải cho c&aacute;c doanh nghiệp vận tải. Dự kiến, lễ khởi h&agrave;nh phương tiện chạy th&iacute; điểm tuyến vận tải h&agrave;ng h&oacute;a Th&acirc;m Quyến - H&agrave; Nội sẽ được Bộ Giao th&ocirc;ng vận tải Trung Quốc tổ chức tại Th&acirc;m Quyến v&agrave;o ng&agrave;y 19-9.</p>\r\n\r\n<p><img alt=\"HÃÂ¬nh Ã¡ÂºÂ£nh cÃÂ³ liÃÂªn quan\" src=\"http://trungquocsensetravel.com/view/at_tong-hop-kinh-nghiem-du-lich-con-minh-trung-quoc-2018_6d23a2db962d3c1b42a5e66f02fae495.jpg\" /></p>','1546477811.png',1,'thong-xe-thi-diem-tuyen-xe-khach-quoc-te-con-minh-tq-lao-cai-ha-noi-hai-phong-1546477811','2019-01-03 01:10:11','2019-01-03 01:10:11'),(5,NULL,'Chạy thi tranh dành làm thủ tục sang trung quốc tại cửa khẩu quốc tế Móng cái - Quảng Ninh','<p>H&agrave;ng trăm người chen lấn,&nbsp;x&ocirc; đẩy&nbsp;v&agrave; thi nhau&nbsp;chạy v&agrave;o khu vực l&agrave;m thủ tục xuất cảnh sang Trung Quốc l&agrave; h&igrave;nh ảnh rất phản cảm tại Cửa khẩu quốc tế M&oacute;ng C&aacute;i (Quảng Ninh) mỗi s&aacute;ng sớm.</p>\r\n\r\n<p><img alt=\"Káº¿t quáº£ hÃ¬nh áº£nh cho Cháº¡y thi tranh dÃ nh lÃ m thá»§ tá»¥c sang trung quá»c táº¡i cá»­a kháº©u quá»c táº¿ MÃ³ng cÃ¡i - Quáº£ng Ninh\" src=\"https://photo-3-baomoi.zadn.vn/w1000_r1/2018_05_28_19_26198828/72ac4c8f8ec967973ed8.jpg\" /></p>','1546477981.png',1,'chay-thi-tranh-danh-lam-thu-tuc-sang-trung-quoc-tai-cua-khau-quoc-te-mong-cai-quang-ninh-1546478192','2019-01-03 01:13:01','2019-01-03 01:16:32');
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_pages`
--

DROP TABLE IF EXISTS `cms_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Không hoạt động; 1: Hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cms_pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_pages`
--

LOCK TABLES `cms_pages` WRITE;
/*!40000 ALTER TABLE `cms_pages` DISABLE KEYS */;
INSERT INTO `cms_pages` VALUES (1,'Hướng dẫn đặt hàng','Hướng dẫn đặt hàng trên hệ thống Hà Khẩu Logistics','<p>Dưới đ&acirc;y l&agrave; hướng dẫn chi tiết c&aacute;ch đăng k&yacute; t&agrave;i khoản mới v&agrave; tạo đơn h&agrave;ng mới tr&ecirc;n hệ thống <strong>H&agrave; Khẩu Logistics</strong>.&nbsp;</p>\r\n<div>\r\n<video width=\"700\" height=\"400\" controls=\"\">\r\n  <source src=\"/frontend/assets/videos/QuyTrinhDatHang.mp4\" type=\"video/mp4\">\r\nYour browser does not support the video tag.\r\n</video>\r\n</div>\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>1. &nbsp;&nbsp;&nbsp; &nbsp;Tạo t&agrave;i khoản mới</strong></p>\r\n\r\n<p>Để đặt được h&agrave;ng, trước ti&ecirc;n bạn cần tạo một t&agrave;i khoản mới gồm th&ocirc;ng tin c&aacute; nh&acirc;n: T&ecirc;n, Số điện thoại.</p>\r\n\r\n<p><em><strong>Lưu &yacute;:</strong></em>&nbsp;Số điện thoại n&agrave;y cần ch&iacute;nh x&aacute;c v&igrave; kh&ocirc;ng thay đổi được v&agrave; sẽ l&agrave; th&ocirc;ng tin gốc để x&aacute;c thực v&agrave; đăng nhập t&agrave;i khoản của bạn trong suốt qu&aacute; tr&igrave;nh giao dịch v&agrave; đặt h&agrave;ng.&nbsp;Nếu đ&atilde; c&oacute; t&agrave;i khoản, bạn chỉ cần điền số điện thoại v&agrave; mật khẩu để đăng nhập.</p>\r\n\r\n<p><strong>2. &nbsp;&nbsp;&nbsp; &nbsp;C&agrave;i đặt extension</strong></p>\r\n\r\n<p>- Truy cập v&agrave;o link chrome extension <a href=\"https://chrome.google.com/webstore/detail/h%C3%A0-kh%E1%BA%A9u-logistics-%C4%91%E1%BA%B7t-h%C3%A0n/ghomkbcbllfkpicifgjlcgledhglgloj?hl=vi&amp;authuser=1\" target=\"_blank\">HaKhau extension</a>&nbsp;v&agrave; c&agrave;i đặt extension</p>\r\n\r\n<p><img alt=\"\" src=\"https://demo.bse-corp.com/laravel-filemanager/images/1/Annotation 2019-01-07 105438.jpg\" style=\"height:37px; width:500px\" /></p>\r\n\r\n<p><strong>3. &nbsp;&nbsp;&nbsp; &nbsp;Tạo v&agrave; gửi đơn h&agrave;ng</strong></p>\r\n\r\n<p><strong>H&agrave; Khẩu Logistics</strong>&nbsp;cho ph&eacute;p kh&aacute;ch h&agrave;ng trực tiếp t&igrave;m kiếm sản phẩm với từ kh&oacute;a bằng tiếng Việt. Bạn chỉ cần g&otilde; từ kh&oacute;a mặt h&agrave;ng cần t&igrave;m, lựa chọn trang chủ Taobao.com, 1688.com, Tmall.com , tr&igrave;nh duyệt sẽ tự động mở 1 tab mới v&agrave; hiển thị kết quả link tổng hợp từ kh&oacute;a bạn t&igrave;m kiếm.</p>\r\n\r\n<p><em><strong>a. Tạo đơn h&agrave;ng</strong></em></p>\r\n\r\n<p>Nếu đ&atilde; c&oacute; link sản phẩm, rất đơn giản, bạn chỉ cần copy link v&agrave;o &ocirc;&nbsp;<strong><em>Nhập link sản phẩm</em></strong>&nbsp;tr&ecirc;n hệ thống.&nbsp;Sau khi đ&atilde; copy link sản phẩm&nbsp;cần đặt, bạn thao t&aacute;c tạo đơn h&agrave;ng nhanh (Trung Quốc hoặc H&agrave;n Quốc), bạn chọn đầy đủ số lượng, th&ocirc;ng tin sản phẩm đặt h&agrave;ng tưng ứng. Tiếp theo, hệ thống sẽ tự động hiện c&aacute;c th&ocirc;ng tin sản phẩm li&ecirc;n quan để bạn chọn.</p>\r\n\r\n<p>Hoặc bạn c&oacute; thể ấn&nbsp;n&uacute;t&nbsp;<em><strong>Tạo đơn h&agrave;ng</strong></em>&nbsp;nằm tr&ecirc;n thanh menu bất k&igrave; l&uacute;c n&agrave;o để tạo đơn h&agrave;ng v&agrave; copy link đặt h&agrave;ng v&agrave;o đ&oacute;.</p>\r\n\r\n<p>Sau khi c&oacute; link sản phẩm, ấn&nbsp;n&uacute;t&nbsp;<em><strong>Lấy th&ocirc;ng th&ocirc;ng tin sản phẩm</strong></em>. C&oacute; hai h&igrave;nh thức lấy th&ocirc;ng tin sản phẩm:</p>\r\n\r\n<p>- Lấy th&ocirc;ng tin tự động: &Aacute;p dụng với đa số c&aacute;c link từ Taobao.com, 1688.com, Tmall.com. Với c&aacute;c link sản phẩm c&oacute; thể lấy link tự động, c&aacute;c thuộc t&iacute;nh của sản phẩm sẽ được hiện ra ra v&agrave; hiển thị tương tự như tr&ecirc;n 3 trang web kia, được dịch sang ng&ocirc;n ngữ tiếng Việt gi&uacute;p bạn dễ d&agrave;ng lựa chọn sản phẩm hơn. Bạn chỉ cần lựa chọn c&aacute;c thuộc t&iacute;nh sản phẩm v&agrave; số lượng sản phẩm tương ứng v&agrave; ấn n&uacute;t&nbsp;<em><strong>Th&ecirc;m v&agrave;o đơn h&agrave;ng</strong></em>&nbsp;để cập nhật c&aacute;c sản phẩm n&agrave;y v&agrave;o đơn h&agrave;ng hiện tại.</p>\r\n\r\n<p><em><strong>Lưu &yacute;:</strong></em>&nbsp;V&igrave; c&ocirc;ng cụ n&agrave;y lấy tin tự động n&ecirc;n một số từ ngữ chuyển từ tiếng Trung sang tiếng Việt c&oacute; thể kh&ocirc;ng ho&agrave;n to&agrave;n ch&iacute;nh x&aacute;c v&agrave; gi&aacute; sản phẩm nếu đang c&oacute; chương tr&igrave;nh khuyến m&atilde;i sẽ kh&ocirc;ng được tự động cập nhật, với trường hợp n&agrave;y <strong>H&agrave; Khẩu Logistics</strong>&nbsp;sẽ sửa trực tiếp lại cho kh&aacute;ch h&agrave;ng khi kiểm tra đơn.</p>\r\n\r\n<p>- Lấy th&ocirc;ng tin bằng tay: &Aacute;p dụng với c&aacute;c link sản phẩm c&ograve;n lại kh&ocirc;ng sử dụng được c&ocirc;ng cụ lấy th&ocirc;ng tin tự động. Th&ocirc;ng tin kh&ocirc;ng được tự động hiện ra n&ecirc;n bạn cần nhập đầy đủ c&aacute;c thuộc t&iacute;nh của sản phẩm m&igrave;nh muốn đặt như M&agrave;u, Size, G&oacute;i sản phẩm,... Mỗi nh&oacute;m thuộc t&iacute;nh kh&aacute;c nhau cần t&aacute;ch ra c&aacute;c d&ograve;ng kh&aacute;c nhau. Sau khi đ&atilde; th&ecirc;m đủ c&aacute;c link sản phẩm, ấn n&uacute;t&nbsp;<em><strong>Th&ecirc;m v&agrave;o đơn h&agrave;ng</strong></em>.&nbsp;</p>\r\n\r\n<p><em><strong>b. Gửi đơn h&agrave;ng</strong></em></p>\r\n\r\n<p>Sau khi tạo xong đơn h&agrave;ng, ấn n&uacute;t&nbsp;<em><strong>Gửi đơn h&agrave;ng</strong></em>&nbsp;để chuyển h&agrave;ng n&agrave;y tới CSKH của <strong>H&agrave; Khẩu Logistics</strong>..</p>\r\n\r\n<p>Nếu đ&atilde; c&oacute; t&agrave;i khoản, hệ thống sẽ mặc định lấy địa chỉ trong t&agrave;i khoản của bạn.</p>\r\n\r\n<p>Nếu chưa c&oacute; t&agrave;i khoản, h&atilde;y điền địa chỉ nhận h&agrave;ng&nbsp;v&agrave; nhập ch&iacute;nh x&aacute;c số điện thoại, ch&uacute;ng t&ocirc;i sẽ tạo t&agrave;i khoản cho bạn.</p>\r\n\r\n<p>Đặc biệt, bạn c&oacute; thể lựa chọn nh&acirc;n vi&ecirc;n chăm s&oacute;c, chờ b&aacute;o gi&aacute; v&agrave; đặt đơn h&agrave;ng,&nbsp;theo d&otilde;i lộ tr&igrave;nh đặt h&agrave;ng tr&ecirc;n thanh trạng th&aacute;i.</p>\r\n\r\n<p><em><strong>c. Nhận b&aacute;o gi&aacute; &nbsp;v&agrave; thanh to&aacute;n đơn h&agrave;ng</strong></em></p>\r\n\r\n<p>Sau khi gửi đơn h&agrave;ng th&agrave;nh c&ocirc;ng, CSKH <strong>H&agrave; Khẩu Logistics</strong>. sẽ b&aacute;o gi&aacute; cho bạn trong thời gian sớm nhất.</p>\r\n\r\n<p><em>Giao diện đơn h&agrave;ng đ&atilde; b&aacute;o gi&aacute;</em></p>\r\n\r\n<p>Tr&ecirc;n đ&acirc;y&nbsp;l&agrave; giao diện đơn h&agrave;ng đ&atilde; được b&aacute;o gi&aacute;, c&oacute; cập nhật đầy đủ tỷ gi&aacute;, ph&iacute; dịch vụ&nbsp;v&agrave; t&iacute;nh ri&ecirc;ng ship nội địa.&nbsp;Sau khi đ&atilde; b&aacute;o gi&aacute;, hệ th&ocirc;ng sẽ gửi th&ocirc;ng b&aacute;o cho bạn. Với đơn h&agrave;ng đầu ti&ecirc;n, ch&uacute;ng t&ocirc;i sẽ gửi tin nhắn th&ocirc;ng b&aacute;o cho bạn khi đơn h&agrave;ng được b&aacute;o gi&aacute;.&nbsp;Với c&aacute;c đơn h&agrave;ng tiếp theo, ch&uacute;ng t&ocirc;i sẽ th&ocirc;ng b&aacute;o qua hệ thống Th&ocirc;ng b&aacute;o tr&ecirc;n <strong>H&agrave; Khẩu Logistics</strong>., bạn n&ecirc;n truy cập hệ thống thường xuy&ecirc;n để nhận th&ocirc;ng tin đơn h&agrave;ng.</p>\r\n\r\n<p>Để thanh to&aacute;n đơn h&agrave;ng, &nbsp;bạn chỉ ấn n&uacute;t&nbsp;<em><strong>Thanh to&aacute;n&nbsp;</strong></em>v&agrave; lựa chọn h&igrave;nh thức thanh to&aacute;n ph&ugrave; hợp,&nbsp;số tiền cọc đơn h&agrave;ng,&nbsp;nội dung chuyển khoản cụ thể đều được hướng dẫn chi tiết. Sau khi bạn thanh to&aacute;n, đơn h&agrave;ng sẽ được tiến h&agrave;nh đặt trong v&ograve;ng 24 giờ.</p>\r\n\r\n<p>Lưu &yacute;: Khi&nbsp;kiểm tra lại đơn h&agrave;ng, nếu cần chỉnh sửa, bạn c&oacute; thể sửa trực tiếp tr&ecirc;n đơn (số lượng, m&agrave;u sắc, ghi ch&uacute; đặt h&agrave;ng....) v&agrave; gửi lại đơn. Đơn h&agrave;ng khi được gửi lại sẽ quay lại bước b&aacute;o gi&aacute; v&agrave; gửi lại cho tới khi bạn chấp nhận v&agrave; thanh to&aacute;n đơn h&agrave;ng.</p>\r\n\r\n<p>Với đơn h&agrave;ng đ&atilde; sửa đổi v&agrave; b&aacute;o gi&aacute; từ lần thứ hai, tr&ecirc;n đ&oacute;&nbsp;đều hiện chi tiết thay đổi từ hệ thống. Để biết cụ thể đơn h&agrave;ng đ&atilde; thay đổi như thế n&agrave;o, ấn xem&nbsp;<em><strong>Chi tiết thay đổi</strong>.</em></p>\r\n\r\n<p><em>Giao diện chi tiết thay đổi đơn h&agrave;ng</em></p>\r\n\r\n<p><em><strong>Lưu &yacute;: &nbsp;</strong></em>Về việc thay đổi địa chỉ nhận h&agrave;ng:</p>\r\n\r\n<p>Với đơn h&agrave;ng chưa được đặt: C&oacute; thể sửa địa chỉ nhận h&agrave;ng ngay tại phần địa chỉ, hệ thống sẽ tự động cập nhật.</p>\r\n\r\n<p>Với đơn h&agrave;ng đ&atilde; đặt: Vui l&ograve;ng li&ecirc;n hệ CSKH để thay đổi th&ocirc;ng tin nhận h&agrave;ng.</p>\r\n\r\n<p><strong>4. &nbsp;&nbsp;&nbsp; &nbsp;T&agrave;i khoản c&aacute; nh&acirc;n</strong></p>\r\n\r\n<p>- Th&ocirc;ng tin t&agrave;i khoản: Điền đầy đủ th&ocirc;ng tin theo mẫu, quan trọng nhất l&agrave; t&ecirc;n, địa chỉ v&agrave; số điện thoại v&igrave; đ&acirc;y l&agrave; c&aacute;c th&ocirc;ng tin được sử dụng li&ecirc;n tục v&agrave; l&agrave; th&ocirc;ng tin x&aacute;c thực t&agrave;i khoản của bạn.</p>\r\n\r\n<p>- Danh s&aacute;ch đơn h&agrave;ng: Theo d&otilde;i v&agrave; cập nhật đầy đủ c&aacute;c đơn h&agrave;ng đ&atilde; đặt, đang đặt h&agrave;ng, đang xử l&yacute;&hellip;.&nbsp;Danh s&aacute;ch đơn h&agrave;ng cho ph&eacute;p bạn lưu lại th&ocirc;ng tin đơn h&agrave;ng, lưu trữ link sản phẩm. Bạn c&oacute; thể t&igrave;m lại đơn h&agrave;ng bằng tra m&atilde; đơn h&agrave;ng, m&atilde; vận đơn, link sản phẩm...</p>\r\n\r\n<p><em>T&igrave;m kiếm theo m&atilde; đơn &nbsp;h&agrave;ng</em></p>\r\n\r\n<p>- Chi tiết giao dịch: Theo d&otilde;i t&agrave;i ch&iacute;nh từng đơn h&agrave;ng đ&atilde; đặt, dư&nbsp;nợ tr&ecirc;n hệ thống. Để theo d&otilde;i từng đơn h&agrave;ng, bạn lọc ng&agrave;y theo đơn để theo d&otilde;i dư nợ từng đơn, đảm bảo sự minh bạch tuyệt đối về t&agrave;i ch&iacute;nh.</p>\r\n\r\n<p>Mọi th&ocirc;ng tin thắc mắc về đơn h&agrave;ng, bạn vui l&ograve;ng li&ecirc;n hệ qua tổng đ&agrave;i duy nhất&nbsp;<em><strong>0123456789</strong></em>&nbsp;hoặc li&ecirc;n hệ trực tiếp tr&ecirc;n website <strong>H&agrave; Khẩu Logistics</strong>. để được tư vấn th&ecirc;m.</p>\r\n\r\n<p><em>C&aacute;m ơn Qu&yacute; kh&aacute;ch!</em></p>',1,'huong-dan-dat-hang',1,'2018-12-18 07:46:13','2019-01-09 03:31:30'),(2,'Quy định đặt hàng','Quy định đặt hàng','<p>Ch&agrave;o mừng bạn đến với <strong>H&agrave; Khẩu Logistics</strong>&nbsp;- Website cung cấp dịch vụ đặt h&agrave;ng, thanh to&aacute;n v&agrave; vận chuyển hộ đối với c&aacute;c website thương mại điện tử của Trung Quốc v&agrave; H&agrave;n Quốc: order h&agrave;ng Taobao, order h&agrave;ng alibaba, order h&agrave;ng Gmarket...&nbsp;Vượt qua r&agrave;o cản về ng&ocirc;n ngữ, thanh to&aacute;n, vận chuyển, ch&uacute;ng t&ocirc;i mang đến cho c&aacute;c bạn những nguồn h&agrave;ng đa dạng, phong ph&uacute; với gi&aacute; cả v&agrave; chi ph&iacute; thấp nhất phục vụ tối đa nhu cầu kinh doanh cũng như sử dụng.</p>\r\n\r\n<p>Để bắt đầu sử dụng dịch vụ, qu&yacute; kh&aacute;ch phải x&aacute;c nhận đ&atilde; đọc v&agrave; đồng &yacute; với c&aacute;c điều khoản v&agrave; quy định sau:</p>\r\n\r\n<p><strong>1. Về dịch vụ H&agrave; Khẩu Logistics</strong></p>\r\n\r\n<p>Ch&uacute;ng t&ocirc;i l&agrave; đơn vị trung gian cung cấp c&aacute;c dịch vụ về đặt h&agrave;ng, thanh to&aacute;n, vận chuyển giữa kh&aacute;ch h&agrave;ng v&agrave; nh&agrave; cung cấp. Gi&uacute;p việc nhập h&agrave;ng Trung Quốc, nhập h&agrave;ng Trung quốc&nbsp;trở n&ecirc;n đơn giản, dễ d&agrave;ng. Ch&uacute;ng t&ocirc;i kh&ocirc;ng chịu tr&aacute;ch nhiệm về bất cứ vấn c&aacute;c vấn đề kh&aacute;c ph&aacute;t sinh ngo&agrave;i c&aacute;c vấn đề kể tr&ecirc;n. Ch&uacute;ng t&ocirc;i kh&ocirc;ng chịu tr&aacute;ch nhiệm về c&aacute;c giao dịch n&agrave;o ngo&agrave;i hệ thống hoặc giao dịch sử dụng thương hiệu m&agrave; chưa được sự đồng &yacute; của <strong>H&agrave; Khẩu Logistics</strong>.</p>\r\n\r\n<p><em><strong>1.1. C&aacute;c mặt h&agrave;ng kh&ocirc;ng nhận đặt v&agrave; vận chuyển</strong></em></p>\r\n\r\n<p>-&nbsp;Ma t&uacute;y, c&aacute;c chất g&acirc;y nghiện, thuốc k&iacute;ch th&iacute;ch</p>\r\n\r\n<p>-&nbsp;Vũ kh&iacute;, đạn dược, c&aacute;c vận dụng g&acirc;y s&aacute;t thương kh&aacute;c</p>\r\n\r\n<p>-&nbsp;Thực phẩm, đồ gia dụng, đồ chơi trẻ em&hellip; kh&ocirc;ng x&aacute;c nhận nguồn gốc, c&oacute; chất g&acirc;y độc hại ảnh hưởng đến người sử dụng</p>\r\n\r\n<p>-&nbsp;C&aacute;c loại vật phẩm, ấn phẩm, t&agrave;i liệu đồi trụy, phản động</p>\r\n\r\n<p>-&nbsp;Động vật, thực vật&nbsp;sống</p>\r\n\r\n<p>-&nbsp;C&aacute;c loại h&agrave;ng h&oacute;a kh&aacute;c trong danh mục cấm vận chuyển, kinh doanh của theo quy định của Ph&aacute;p luật</p>\r\n\r\n<p><em><strong>1.2. C&aacute;c mặt h&agrave;ng đặc biệt</strong></em></p>\r\n\r\n<p>C&aacute;c loại h&agrave;ng đặc biệt phải th&ocirc;ng b&aacute;o với ch&uacute;ng t&ocirc;i khi đặt h&agrave;ng (nếu kh&ocirc;ng th&ocirc;ng b&aacute;o ch&uacute;ng t&ocirc;i sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm về vấn đề hỏng h&oacute;c, kh&ocirc;ng đ&uacute;ng thời hạn giao h&agrave;ng&hellip;)</p>\r\n\r\n<p>-&nbsp;Hạt giống, mầm c&acirc;y</p>\r\n\r\n<p>-&nbsp;Chất dễ ch&aacute;y nổ: Pin, ắc quy, di&ecirc;m, bật lửa đ&atilde; c&oacute; ga</p>\r\n\r\n<p>-&nbsp;Chất lỏng, h&oacute;a chất, c&aacute;c loại h&agrave;ng h&oacute;a chứa cồn</p>\r\n\r\n<p>-&nbsp;Loa đ&agrave;i, c&aacute;c h&agrave;ng h&oacute;a c&oacute; nam ch&acirc;m</p>\r\n\r\n<p>-&nbsp;C&aacute;c loại mặt h&agrave;ng dễ vỡ: chai lọ, b&aacute;t đĩa, đồ gốm, đồ nhựa mỏng, h&agrave;ng ho&aacute; chất liệu mica, gương k&iacute;nh,...</p>\r\n\r\n<p>- H&agrave;ng ho&aacute; m&aacute;y m&oacute;c, thiết bị điện tử,... đ&atilde; qua sử dụng</p>\r\n\r\n<p>-&nbsp;Đặc biệt: C&aacute;c loại h&agrave;ng nh&aacute;i (h&agrave;ng fake)&nbsp;c&aacute;c thương hiệu lớn, ch&uacute;ng t&ocirc;i kh&ocirc;ng khuyến kh&iacute;ch đặt c&aacute;c loại mặt h&agrave;ng n&agrave;y v&agrave; sẽ kh&ocirc;ng đảm bảo chất lượng cũng như h&igrave;nh d&aacute;ng bề ngo&agrave;i giống ảnh v&agrave; m&ocirc; tả tr&ecirc;n website</p>\r\n\r\n<p><strong>2. Bảo mật th&ocirc;ng tin</strong></p>\r\n\r\n<p>-&nbsp;Ch&uacute;ng t&ocirc;i cam kết bảo mật to&agrave;n bộ c&aacute;c th&ocirc;ng tin c&aacute; nh&acirc;n, mặt h&agrave;ng, nguồn h&agrave;ng đặt&hellip; với tất cả c&aacute;c kh&aacute;ch h&agrave;ng đăng k&yacute; v&agrave; sử dụng dịch vụ.</p>\r\n\r\n<p>-&nbsp;Kh&aacute;ch h&agrave;ng c&oacute; tr&aacute;ch nhiệm tự bảo mật c&aacute;c th&ocirc;ng tin về t&agrave;i khoản, mật khẩu. Tuyệt đối kh&ocirc;ng sử dụng t&agrave;i khoản của người kh&aacute;c hoặc cho người kh&aacute;c mượn t&agrave;i khoản.</p>\r\n\r\n<p><strong>3. Quy định về đặt h&agrave;ng, thanh to&aacute;n</strong></p>\r\n\r\n<p><em><strong>4. Thời gian chuyển h&agrave;ng</strong></em></p>\r\n\r\n<p>- C&aacute;c&nbsp;đơn h&agrave;ng th&ocirc;ng thường từ 3-5 ng&agrave;y sẽ về&nbsp;đến kho của h&agrave; khẩu logistics tại L&agrave;o cai</p>\r\n\r\n<p><strong>5. Ch&iacute;nh s&aacute;ch giải quyết khiếu nại</strong></p>\r\n\r\n<p>-&nbsp;Thời gian khiếu nại: Trong v&ograve;ng&nbsp;2 ng&agrave;y t&iacute;nh từ ng&agrave;y qu&yacute; kh&aacute;ch nhận được kiện h&agrave;ng.</p>\r\n\r\n<p>-&nbsp;Trường hợp h&agrave;ng sai: mẫu m&atilde;, mầu sắc, chủng loại, quy c&aacute;ch&hellip;do đặt h&agrave;ng. Ch&uacute;ng t&ocirc;i sẽ bồi thường 100% gi&aacute; trị h&agrave;ng h&oacute;a cho qu&yacute; kh&aacute;ch. Nếu h&agrave;ng sai do ph&iacute;a nh&agrave; cung cấp ph&aacute;t sai sản phẩm, ch&uacute;ng t&ocirc;i sẽ hỗ trợ tối đa trong việc giao dịch v&agrave; đ&agrave;m ph&aacute;n lại với đối t&aacute;c, đảm bảo qu&yacute; kh&aacute;ch h&agrave;ng kh&ocirc;ng phải chịu thiệt th&ograve;i; trường hợp phải gửi đổi trả h&agrave;ng với nh&agrave; cung cấp, qu&yacute; kh&aacute;ch vui l&ograve;ng thanh to&aacute;n chi ph&iacute; vận chuyển (Ph&iacute; n&agrave;y <strong>H&agrave; Khẩu Logistics</strong> sẽ b&aacute;o cụ thể với KH trước khi gửi chuyển h&agrave;ng)</p>\r\n\r\n<p>-&nbsp;Chất lượng sản phẩm: V&igrave; chỉ l&agrave; đơn vị trung gian giữa nh&agrave; cung cấp v&agrave; kh&aacute;ch h&agrave;ng n&ecirc;n ch&uacute;ng t&ocirc;i kh&ocirc;ng chịu tr&aacute;ch nhiệm về chất lượng sản phẩm. Nhưng ch&uacute;ng t&ocirc;i sẽ hỗ trợ tối đa trong việc khiếu nại với nh&agrave; cung cấp trong trường hợp h&agrave;ng h&oacute;a c&oacute; vấn đề về chất lượng. Ngo&agrave;i ra, nếu gặp rủi ro ch&uacute;ng t&ocirc;i sẽ hỗ trợ một phần gi&aacute; trị h&agrave;ng h&oacute;a.</p>\r\n\r\n<p>-&nbsp;Đối với h&agrave;ng h&oacute;a vỡ, hỏng, trầy xước do qu&aacute; tr&igrave;nh vận chuyển. Ch&uacute;ng t&ocirc;i sẽ t&ugrave;y thuộc v&agrave;o mức độ hỏng h&oacute;c m&agrave; đền b&ugrave; qu&yacute; kh&aacute;ch ở một mức thỏa đ&aacute;ng nhất.</p>\r\n\r\n<p>-&nbsp;Ch&uacute;ng t&ocirc;i ChỈ tiếp nhận xử l&yacute; khiếu nại khi c&oacute; đầy đủ h&igrave;nh ảnh (sản phẩm, giao dịch, vỏ kiện h&agrave;ng - l&agrave; m&atilde; vận đơn d&aacute;n tr&ecirc;n từng kiện h&agrave;ng) v&agrave; c&aacute;c th&ocirc;ng tin li&ecirc;n quan kh&aacute;c.</p>\r\n\r\n<p>-&nbsp;Qu&yacute; kh&aacute;ch n&ecirc;n thường xuy&ecirc;n v&agrave;o kiểm tra trạng th&aacute;i của từng đơn h&agrave;ng v&igrave; ch&uacute;ng t&ocirc;i chỉ th&ocirc;ng b&aacute;o t&igrave;nh trạng tr&ecirc;n 1 k&ecirc;nh duy nhất l&agrave; hệ thống chung.</p>\r\n\r\n<p><strong>6. &nbsp;Ngừng cung cấp dịch vụ</strong></p>\r\n\r\n<p>Trong c&aacute;c trường hợp sau, <strong>H&agrave; Khẩu Logistics</strong> c&oacute; quyền đơn phương từ chối cung cấp dịch vụ, bằng c&aacute;ch kh&oacute;a t&agrave;i khoản của qu&yacute; kh&aacute;ch tr&ecirc;n hệ thống m&agrave; kh&ocirc;ng cần c&oacute; sự đồng &yacute; trước:&nbsp;</p>\r\n\r\n<p>-&nbsp;Sử dụng th&ocirc;ng tin v&agrave; dịch vụ của <strong>H&agrave; Khẩu Logistics</strong> v&agrave;o c&aacute;c mục đ&iacute;ch kh&aacute;c, sai bản chất dịch vụ hoặc kh&ocirc;ng được sự đồng &yacute; trước của Ch&uacute;ng t&ocirc;i.&nbsp;</p>\r\n\r\n<p>-&nbsp;Bất cứ h&agrave;nh động gian lận n&agrave;o trong giao dịch: B&aacute;o thiếu khi nhận đủ, giả mạo thanh to&aacute;n, khiếu nại kh&ocirc;ng đ&uacute;ng với c&aacute;c quy định về khiếu nại.</p>\r\n\r\n<p>-&nbsp;Đăng tải hoặc cung cấp th&ocirc;ng tin sai sự thật, với mục đ&iacute;ch l&agrave;m giảm uy t&iacute;n của <strong>H&agrave; Khẩu Logistics</strong> đối với kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>-&nbsp;Đăng k&yacute; giả mạo c&aacute; nh&acirc;n hoặc tổ chức spam đơn h&agrave;ng li&ecirc;n tục g&acirc;y ảnh hưởng đến tốc độ xử l&yacute; đơn h&agrave;ng chung.</p>\r\n\r\n<p>- Sử dụng lời lẽ khiếm nh&atilde;, c&oacute; th&aacute;i độ bất lịch sự, g&acirc;y gổ với nh&acirc;n vi&ecirc;n c&ocirc;ng ty <strong>H&agrave; Khẩu Logistics</strong>.</p>\r\n\r\n<p>-&nbsp;C&oacute; những y&ecirc;u cầu kh&ocirc;ng ch&iacute;nh đ&aacute;ng, vượt ra ngo&agrave;i phạm vi quản l&yacute; của <strong>H&agrave; Khẩu Logistics</strong> v&agrave; sai lệch về bản chất của dịch vụ Nhập h&agrave;ng hộ.</p>',1,'quy-dinh-dat-hang',1,'2018-12-18 07:46:36','2019-01-03 00:52:52'),(3,'Bảng giá','Chi phí một đơn hàng','<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"text-align:center\"><strong>GIẢI TH&Iacute;CH</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\"><strong>Gi&aacute; web</strong></td>\r\n			<td>Gi&aacute; sản phẩm tr&ecirc;n website Trung Quốc</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\"><strong>Ph&iacute; mua hộ</strong></td>\r\n			<td>L&agrave; ph&iacute; trả cho H&agrave; khẩu Logistics&nbsp;để&nbsp;l&agrave;m việc&nbsp;với shop về c&aacute;c vấn&nbsp;đề li&ecirc;n quan&nbsp;đến h&agrave;ng h&oacute;a theo y&ecirc;u cầu của kh&aacute;ch h&agrave;ng</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\"><strong>Ship nội địa TQ</strong></td>\r\n			<td>Ph&iacute; tiền cước vận&nbsp;chuyển h&agrave;ng từ nh&agrave; cung cấp tới kho của H&agrave; khẩu logistics tại H&agrave; khẩu - TQ (Gi&aacute;p VN)</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\"><strong>Ship TQ - VN</strong></td>\r\n			<td>L&agrave; tiền cước vận chuyển từ H&agrave; khẩu - Trung quốc&nbsp;đến L&agrave;o cai - VN hoặc từ H&agrave; khẩu - TQ&nbsp;đến&nbsp;Địa chỉ kh&aacute;ch h&agrave;ng</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BẢNG GI&Aacute; DỊCH VỤ ORDER H&Agrave;NG TRUNG QUỐC</strong></p>\r\n\r\n<p>Ch&uacute;ng t&ocirc;i ở cửa khẩu L&agrave;o cai, trực tiếp sử l&yacute; đơn đặt h&agrave;ng, thuận tiện trong việc giải quyết mọi vướng mắc</p>\r\n\r\n<p><strong>1. T&iacute;nh theo c&acirc;n nặng, vận chuyển đến Tp. L&agrave;o cai</strong></p>\r\n\r\n<p>Tổng chi ph&iacute; gồm:</p>\r\n\r\n<p>=&nbsp;Gi&aacute; web</p>\r\n\r\n<p>+&nbsp;Ph&iacute; mua hộ:&nbsp;<strong><em>Dưới 30 triệu: 3%, Từ 30-50 triệu: 2%, Tr&ecirc;n 50 triệu: 1%</em></strong></p>\r\n\r\n<p>+&nbsp;Ship nội địa trung quốc</p>\r\n\r\n<p>+&nbsp;Ship H&agrave; khẩu &ndash; L&agrave;o cai</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">C&acirc;n nặng</td>\r\n			<td style=\"text-align:center\">1-3kg</td>\r\n			<td style=\"text-align:center\">Từ 3kg trở l&ecirc;n</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\">Đơn gi&aacute;</td>\r\n			<td style=\"text-align:center\">&nbsp; &nbsp; &nbsp;30k</td>\r\n			<td style=\"text-align:center\">10.000đ/1kg</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><strong>Lưu &yacute;:</strong></p>\r\n\r\n<p>+&nbsp;Ship l&agrave;o cai đến &nbsp;Kh&aacute;ch (<em>Ship việt nam qua chuyển ph&aacute;t Vnpost, viettelpost, xe kh&aacute;ch kh&aacute;ch h&agrave;ng tự thanh to&aacute;n</em>)</p>\r\n\r\n<p>+ H&agrave;ng dưới 3kg kh&ocirc;ng t&iacute;nh gộp đơn, 1 đơn l&agrave; 1 shop</p>\r\n\r\n<p>Tra cước l&agrave;o cai đi:&nbsp;</p>\r\n\r\n<p><a href=\"http://www.vnpost.vn/vi-vn/tra-cuu-gia-cuoc\">http://www.vnpost.vn/vi-vn/tra-cuu-gia-cuoc</a></p>\r\n\r\n<p><a href=\"https://www.viettelpost.com.vn/tinh-cuoc-van-chuyen\">https://www.viettelpost.com.vn/tinh-cuoc-van-chuyen</a></p>\r\n\r\n<p><strong>2. T&iacute;nh theo c&acirc;n nặng về đến H&agrave; Nội v&agrave; Hồ Ch&iacute; Minh</strong></p>\r\n\r\n<p>Tổng chi ph&iacute; gồm:</p>\r\n\r\n<p>=&nbsp;Gi&aacute; web</p>\r\n\r\n<p>+&nbsp;Ph&iacute; mua hộ:<em>&nbsp;<strong>Dưới 30 triệu: 3%, Từ 30-50 triệu: 2%, Tr&ecirc;n 50 triệu: 1%</strong></em></p>\r\n\r\n<p>+&nbsp;Ship nội địa trung quốc&nbsp;</p>\r\n\r\n<p>+&nbsp;Ship H&agrave; khẩu &ndash; H&agrave; Nội V&agrave; H&agrave; khẩu &ndash; Hồ CH&iacute; Minh</p>\r\n\r\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align:center\">C&acirc;n nặng</td>\r\n			<td style=\"text-align:center\">H&agrave; khẩu - H&agrave; nội</td>\r\n			<td style=\"text-align:center\">H&agrave; khẩu - Hồ ch&iacute; minh</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\">Dưới 10kg</td>\r\n			<td style=\"text-align:center\">150k</td>\r\n			<td style=\"text-align:center\">170k</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"text-align:center\">Từ 10kg trở l&ecirc;n</td>\r\n			<td style=\"text-align:center\">13k/1kg</td>\r\n			<td style=\"text-align:center\">17k/1kg</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><strong>Lưu &yacute;:</strong></p>\r\n\r\n<p><em>- 1 đơn h&agrave;ng l&agrave; tất cả c&aacute;c sản phẩm của 1 shop, c&aacute;c sản phẩm của một shop kh&aacute;c t&iacute;nh l&agrave; một đơn kh&aacute;c&nbsp;</em></p>\r\n\r\n<p><em>- H&agrave;ng cồng kềnh t&iacute;nh theo c&ocirc;ng thức quy đổi: (d&agrave;i x rộng x cao) : 4000</em></p>\r\n\r\n<p><strong>BẢNG GI&Aacute; DỊCH VỤ CHUYỂN - NHẬN TIỀN</strong></p>\r\n\r\n<p>1. Th&ocirc;ng qua ng&acirc;n h&agrave;ng ph&iacute; 30 tệ/ lần (kh&ocirc;ng qu&aacute; 2 vạn)</p>\r\n\r\n<p>2. Th&ocirc;ng qua alipay&nbsp;dưới 2 vạn ph&iacute; 30 tệ, từ 2 vạn đến 5 vạn ph&iacute; 50 tệ. Tr&ecirc;n 5 vạn ph&iacute; 0,1%.&nbsp;</p>\r\n\r\n<p><em><strong>3. Miễn ph&iacute; 100% ph&iacute; thanh to&aacute;n hộ qua alipay, wechat cho kh&aacute;ch h&agrave;ng sử dụng dịch vụ vận chuyển h&agrave;ng k&yacute; gửi</strong></em></p>\r\n\r\n<p>Tỉ gi&aacute;&nbsp;được cập nhật h&agrave;ng ng&agrave;y theo thị trường t&agrave;i ch&iacute;nh</p>\r\n\r\n<p>Sau khi đồng &yacute; với b&aacute;o gi&aacute; qu&yacute; kh&aacute;ch vui l&ograve;ng chuyển khoản từ 70 - 100% tiền h&agrave;ng để h&agrave; khẩu logistics tiến h&agrave;nh đặt h&agrave;ng</p>\r\n\r\n<p><strong>Th&ocirc;ng tin chuyển khoản:</strong></p>\r\n\r\n<p>1. Ng&acirc;n h&agrave;ng Thương mại cổ phần Ngoại thương Việt Nam - Vietcombank: Số t&agrave;i khoản 0951004175246 , chủ t&agrave;i khoản ĐỖ NGỌC MAI - VCB H&agrave; Nội- CN L&agrave;o Cai</p>\r\n\r\n<p>2. Ng&acirc;n h&agrave;ng N&ocirc;ng nghiệp v&agrave; Ph&aacute;t triển n&ocirc;ng th&ocirc;n Việt Nam - Agribank : Số t&agrave;i khoản: 8811205056907, chủ t&agrave;i khoản ĐỖ NGỌC MAI - CN Cốc Lếu</p>\r\n\r\n<p>3. Ng&acirc;n h&agrave;ng TMCP Kỹ thương Việt Nam - Techcombank: Số t&agrave;i khoản: 19021214819889, chủ t&agrave;i khoản ĐỖ NGỌC MAI - CN L&agrave;o Cai</p>',1,'bang-gia',1,'2018-12-18 07:52:20','2019-01-03 00:46:57'),(4,'Giới thiệu','Giới thiệu','<p>? Dịch vụ Order v&agrave; vận chuyển h&agrave;ng taobao, 1688, tmall chuy&ecirc;n nghiệp gi&aacute; rẻ</p>\r\n\r\n<p>? Miễn ph&iacute; 100% ph&iacute; thanh to&aacute;n hộ qua alipay, wechat cho kh&aacute;ch h&agrave;ng sử dụng dịch vụ vận chuyển h&agrave;ng k&yacute; gửi</p>\r\n\r\n<p>? Miễn ph&iacute; ph&iacute; mua hộ cho đơn h&agrave;ng đầu ti&ecirc;n,</p>\r\n\r\n<p>? Miễn ph&iacute; ph&iacute; mua hộ 1 đơn h&agrave;ng bất kỳ tr&ograve;ng th&aacute;ng</p>\r\n\r\n<p>? Ph&iacute; mua hộ chỉ từ 1-3%</p>\r\n\r\n<p>? Cước vận chuyển duy nhất 1 mức gi&aacute;: 10k/1kg về đến L&agrave;o cai, về Hn 13k/1kg, về Hcm 17k/1kg</p>\r\n\r\n<p>? Kh&ocirc;ng ph&iacute; lưu kho, kh&ocirc;ng ph&iacute; kiểm đếm, hỗ trợ gửi h&agrave;ng đổi trả, bảo h&agrave;nh miễn ph&iacute;</p>\r\n\r\n<p>? Kh&ocirc;ng bao giờ tắc bi&ecirc;n</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>? Cam kết đền tiền 100% nếu mất h&agrave;ng</p>\r\n\r\n<p>Rẻ - Uy t&iacute;n - An to&agrave;n - Nhanh ch&oacute;ng ➡️ Ch&uacute;ng t&ocirc;i ở L&agrave;o cai (th&agrave;nh phố cửa khẩu) trực tiếp sử l&yacute; đơn đặt h&agrave;ng</p>\r\n\r\n<p>---------</p>\r\n\r\n<p>? Hotline: 02142.246.888 - Zalo 0987777923 Add: 190 Nhạc sơn, Kim t&acirc;n, L&agrave;o cai</p>',1,'gioi-thieu',1,'2018-12-18 07:52:40','2019-01-03 00:08:40'),(5,'Liên hệ','Liên hệ','<p><strong>TR</strong><strong>Ụ SỞ</strong></p>\r\n\r\n<p>Địa chỉ: 190 Nhạc Sơn - Kim T&acirc;n - TP L&agrave;o Cai</p>\r\n\r\n<p>Điện thoại:&nbsp;<a href=\"tel:02142246888\">02142.246.888</a></p>\r\n\r\n<p>Tư vấn Việt Nam: <a href=\"tel:MrTrung 0977 773 299\">Mis</a>s Trang (Zalo): 0987777923 - Mrs: Th&uacute;y (Zalo): 0946328421 - Mrs H&agrave; (Zalo): 0888233011</p>\r\n\r\n<p>Tư vấn Trung Quốc:&nbsp;<a href=\"tel:Wu Chengzhong: 18687362843\">Wu Chengzhong: 18687362843</a></p>\r\n\r\n<p>Trả h&agrave;ng - Mrs H&agrave; (Zalo): 0888233011</p>\r\n\r\n<p>Thanh to&aacute;n hộ - Mrs Mai (Zalo): 0915802602.&nbsp;</p>\r\n\r\n<p>Tk thanh nhờ&nbsp;to&aacute;n hộ qua alipay 84-915802602</p>\r\n\r\n<p>M&atilde; Qr code tk alipay:&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://demo.bse-corp.com/laravel-filemanager/images/1/ec22f35e1e3cfd62a42d.jpg\" style=\"height:300px; width:190px\" /></p>\r\n\r\n<p><strong>VĂN PH&Ograve;NG H&Agrave; NỘI</strong></p>\r\n\r\n<p>H&agrave; khẩu logistics H&agrave; Nội</p>\r\n\r\n<p>Địa chỉ: 52 Ng&otilde; 100 Phố chợ kh&acirc;m thi&ecirc;n - Đống đa - H&agrave; nội</p>\r\n\r\n<p>Điện thoại:&nbsp;<a href=\"tel:01676325711\">01676325711</a></p>\r\n\r\n<p><strong>ĐỊA CHỈ TRUNG QUỐC</strong></p>\r\n\r\n<p>H&agrave; Khẩu logistics</p>\r\n\r\n<p>Địa chỉ: 收货地址：云南省 红河哈尼族彝族自治州 河口瑶族自治县 河口镇 云南省红河州河口县河边街15号</p>\r\n\r\n<p>Điện thoại:&nbsp;<a href=\"tel:18687362843\">18687362843</a></p>',1,'lien-he',1,'2018-12-18 07:52:54','2019-01-04 18:27:58');
/*!40000 ALTER TABLE `cms_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_profiles`
--

DROP TABLE IF EXISTS `customer_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT '0 : Nữ; 1 : Nam; 2 : Khác',
  `birthday` date DEFAULT NULL,
  `bank_account` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_owner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_sector` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remaining_amount` decimal(15,2) DEFAULT NULL,
  `avatar_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_profiles`
--

LOCK TABLES `customer_profiles` WRITE;
/*!40000 ALTER TABLE `customer_profiles` DISABLE KEYS */;
INSERT INTO `customer_profiles` VALUES (1,1,NULL,NULL,NULL,NULL,NULL,NULL,3000000.00,NULL,'2018-12-25 10:37:51','2018-12-28 06:36:21'),(2,2,NULL,NULL,NULL,NULL,NULL,NULL,9000000.00,NULL,'2018-12-27 07:03:28','2018-12-28 06:36:06'),(3,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-06 20:32:33','2019-01-06 20:32:33'),(4,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-06 20:42:13','2019-01-06 20:42:13');
/*!40000 ALTER TABLE `customer_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 : Không hoạt động; 1 : Hoạt động',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`),
  UNIQUE KEY `customers_phone_number_unique` (`phone_number`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Quảng','quangtd@bse-corp.com','$2y$10$SkxRgf1nNyau8Fu3FYPJg.yC7Qe5X7c4brPfAYPSxioMSTRyWOLke','01234567890',1,'VVINbXW74msfyWL6qbQECAoLK0WeRO783S8IseeSsoplTlFpODQLbtuxYDn4','2018-12-25 10:37:51','2018-12-25 10:37:51'),(2,'Vũ Thành Trung','trungvt0206@gmail.com','$2y$10$.6wuZvqT9ZqA/GqDczu2x.QLyJn0SIh6NiX7OtR6jVG1k8q4R/lU.','0977773299',1,'dNWFrJrz8FD8Q4GfzLDhKwKs3VIIsUkIym8L2t6RXMVnIGiy6Lq1QmrqGSEZ','2018-12-27 07:03:28','2018-12-27 07:03:28'),(3,'khachhang','khachhang@gmail.com','$2y$10$W6mn2kTtsYRq.gxrkgV6meFxOQHj0omlhrP3ey8HO84URg713tZ5W','0397962050',1,'VHRLcWmnBk6Ig4nLM39uknYLK9CQSTn0Q1fZOtpqLmLfoTdjsgNjWhEH21zG','2019-01-06 20:32:33','2019-01-06 20:32:33'),(4,'khachhangdemo','khachhangdemo@gmail.com','$2y$10$dTnWuH0LmhjwmFu0HoxX.O1gF1yVCvdZwbYr6fmD07WbUn4.1Lbke','0397368638',1,NULL,'2019-01-06 20:42:13','2019-01-06 20:42:13');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_addresses`
--

DROP TABLE IF EXISTS `delivery_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commune` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Không sử dụng, 1: Sử dụng',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_addresses`
--

LOCK TABLES `delivery_addresses` WRITE;
/*!40000 ALTER TABLE `delivery_addresses` DISABLE KEYS */;
INSERT INTO `delivery_addresses` VALUES (1,1,'Quảng','0978767256','hà nội','hà đông','a','123',1,'2018-12-26 03:04:31','2018-12-26 03:04:31'),(2,2,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên',1,'2018-12-27 07:12:26','2018-12-27 07:12:26'),(3,4,'khachhang','01234567890','Hà Nội','Thường tín','Nhị Khê','01',1,'2019-01-06 20:43:56','2019-01-06 20:43:56');
/*!40000 ALTER TABLE `delivery_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exchange_rates`
--

DROP TABLE IF EXISTS `exchange_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchange_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vnd` double(11,2) unsigned NOT NULL,
  `ndt` double(11,2) unsigned NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 : Không hoạt động, 1 : Hoạt động',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exchange_rates`
--

LOCK TABLES `exchange_rates` WRITE;
/*!40000 ALTER TABLE `exchange_rates` DISABLE KEYS */;
INSERT INTO `exchange_rates` VALUES (1,3450.00,1.00,0,'2018-12-18 07:01:27','2019-01-05 00:27:07'),(2,3500.00,1.00,1,'2019-01-05 00:27:02','2019-01-05 00:27:07');
/*!40000 ALTER TABLE `exchange_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_10_03_030142_create_exchange_rates_table',1),(4,'2018_10_05_152948_create_delivery_addresses_table',1),(5,'2018_10_05_153033_create_customers_table',1),(6,'2018_10_06_090542_create_customer_profiles_table',1),(7,'2018_10_08_091930_create_orders_table',1),(8,'2018_10_08_091945_create_order_addresses_table',1),(9,'2018_10_08_092142_create_purchase_order_items_table',1),(10,'2018_10_09_074104_create_order_temps_table',1),(11,'2018_10_10_225528_create_slides_table',1),(12,'2018_10_12_163624_create_feedback_table',1),(13,'2018_10_16_155402_create_blogs_table',1),(14,'2018_10_16_174227_create_permission_tables',1),(15,'2018_10_18_145729_create_cms_pages_table',1),(16,'2018_10_18_154634_create_notifications_table',1),(17,'2018_10_19_111101_create_website_configs_table',1),(18,'2018_10_23_154946_create_service_fees_table',1),(19,'2018_11_23_082344_create_order_groups_table',1),(20,'2018_11_24_094738_create_transport_order_items_table',1),(21,'2018_12_07_090414_add_display_name_column_to_website_configs_order',1),(22,'2018_12_11_141409_add_via_border_transport_fee_to_transport_order_items_table',1),(23,'2018_12_11_151533_add_shop_name_column_to_orders_table',1),(24,'2018_12_11_152723_add_order_id_column_to_purchase_order_items_table',1),(25,'2018_12_12_144745_add_purchase_cn_to_vn_fee_column_to_orders_table',1),(26,'2018_12_20_150002_add_current_rate_column_to_purchase_order_items_table',2),(27,'2018_12_21_110257_add_is_discounted_column_to_orders_table',2),(28,'2018_12_21_142831_create_transaction_histories_table',2),(29,'2018_12_21_152716_change_remaining_amount_column',2),(30,'2018_12_21_155105_add_columns_to_orders_table',2),(31,'2019_01_02_150404_change_content_nullable',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\Models\\User',1);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) unsigned NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES ('049537e8-f7b6-465d-83dc-eff4c71438c8','App\\Notifications\\PriceQuotation',3,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/11\",\"order_number\":\"00000011\"}','2019-01-06 20:35:05','2018-12-19 05:01:32','2019-01-06 20:35:05'),('0fd61edf-b803-4cd3-8d79-5ed9b110bace','App\\Notifications\\PriceQuotation',3,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/14\",\"order_number\":\"00000014\"}','2019-01-06 20:35:01','2018-12-20 02:56:47','2019-01-06 20:35:01'),('18c07770-3125-4051-8b3d-7b960b57544a','App\\Notifications\\PriceQuotation',2,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/2\",\"order_number\":\"00000002\"}',NULL,'2018-12-27 07:17:13','2018-12-27 07:17:13'),('2b0d7fe2-5903-4514-9bd4-1162a5bc6e03','App\\Notifications\\PriceQuotation',3,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/8\",\"order_number\":\"00000008\"}','2019-01-06 20:35:13','2018-12-19 04:57:22','2019-01-06 20:35:13'),('2dd496ad-35dd-4712-87b5-818f4b76c38e','App\\Notifications\\PriceQuotation',2,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/5\",\"order_number\":\"00000005\"}',NULL,'2018-12-27 07:26:54','2018-12-27 07:26:54'),('3d8552fe-4568-4de4-8ef7-e8b5296abc27','App\\Notifications\\PriceQuotation',3,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/10\",\"order_number\":\"00000010\"}','2019-01-06 20:35:21','2018-12-19 04:18:17','2019-01-06 20:35:21'),('554f9435-521f-441d-a2be-6fb549f71438','App\\Notifications\\PriceQuotation',2,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/3\",\"order_number\":\"00000003\"}',NULL,'2018-12-27 07:20:34','2018-12-27 07:20:34'),('8b55e3b1-aff8-4087-ac10-874c9209f93b','App\\Notifications\\PriceQuotation',1,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/4\",\"order_number\":\"00000004\"}',NULL,'2018-12-18 08:28:17','2018-12-18 08:28:17'),('b3631438-4add-47be-b78a-0e86c05492a7','App\\Notifications\\PriceQuotation',2,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/4\",\"order_number\":\"00000004\"}',NULL,'2018-12-27 07:24:10','2018-12-27 07:24:10'),('b461488f-6e63-4060-bcb6-0d60f9f85ab0','App\\Notifications\\PriceQuotation',1,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/3\",\"order_number\":\"00000003\"}',NULL,'2018-12-18 07:32:29','2018-12-18 07:32:29'),('bf939e75-a72f-413d-81d7-f6ce3765fea1','App\\Notifications\\PriceQuotation',2,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/7\",\"order_number\":\"00000007\"}',NULL,'2018-12-27 08:20:43','2018-12-27 08:20:43'),('cd49e7ca-96d7-45ea-8a8f-ee747708eb00','App\\Notifications\\PriceQuotation',1,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/7\",\"order_number\":\"00000007\"}',NULL,'2018-12-18 08:43:51','2018-12-18 08:43:51'),('d7a05d80-26c5-40c1-929f-b0dedf77de90','App\\Notifications\\PriceQuotation',3,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/9\",\"order_number\":\"00000009\"}','2019-01-06 20:35:19','2018-12-19 04:41:28','2019-01-06 20:35:19'),('e16f351b-08c9-4479-b204-0fbb1ed8d0f3','App\\Notifications\\PriceQuotation',1,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/1\",\"order_number\":\"00000001\"}',NULL,'2018-12-18 07:11:39','2018-12-18 07:11:39'),('e96be9f0-e885-43a2-b367-998d0791d8c7','App\\Notifications\\PriceQuotation',1,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/1\",\"order_number\":\"00000001\"}',NULL,'2018-12-28 06:38:35','2018-12-28 06:38:35'),('ee571476-66b5-492c-bd3d-159578ed8aa6','App\\Notifications\\PriceQuotation',1,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/2\",\"order_number\":\"00000002\"}',NULL,'2018-12-18 07:43:34','2018-12-18 07:43:34'),('fcef89ba-8ba0-40b9-8f91-c2aeb01e6d0f','App\\Notifications\\PriceQuotation',2,'App\\Models\\Customer','{\"link\":\"https:\\/\\/demo.bse-corp.com\\/orders\\/purchases\\/8\",\"order_number\":\"00000008\"}',NULL,'2018-12-27 08:22:10','2018-12-27 08:22:10');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_addresses`
--

DROP TABLE IF EXISTS `order_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commune` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_addresses`
--

LOCK TABLES `order_addresses` WRITE;
/*!40000 ALTER TABLE `order_addresses` DISABLE KEYS */;
INSERT INTO `order_addresses` VALUES (1,1,'Quảng','0978767256','hà nội','hà đông','a','123','2018-12-26 03:04:35','2018-12-26 03:04:35'),(2,4,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên','2018-12-27 07:12:29','2018-12-27 07:12:29'),(3,3,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên','2018-12-27 07:12:51','2018-12-27 07:12:51'),(4,2,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên','2018-12-27 07:12:57','2018-12-27 07:12:57'),(5,5,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên','2018-12-27 07:23:37','2018-12-27 07:23:37'),(6,7,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên','2018-12-27 08:13:24','2018-12-27 08:13:24'),(7,8,'Vũ Thành Trung','0977773299','Hà nội','Khâm thiên','Khâm thiên','Khâm thiên','2018-12-27 08:14:25','2018-12-27 08:14:25'),(8,16,'khachhang','01234567890','Hà Nội','Thường tín','Nhị Khê','01','2019-01-06 20:44:04','2019-01-06 20:44:04');
/*!40000 ALTER TABLE `order_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_groups`
--

DROP TABLE IF EXISTS `order_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `shop_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_groups`
--

LOCK TABLES `order_groups` WRITE;
/*!40000 ALTER TABLE `order_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_temps`
--

DROP TABLE IF EXISTS `order_temps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_temps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `property` text COLLATE utf8mb4_unicode_ci,
  `qty` int(11) NOT NULL,
  `price` double(12,2) NOT NULL,
  `total_price` double(14,2) NOT NULL,
  `price_range` text COLLATE utf8mb4_unicode_ci,
  `item_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_temps`
--

LOCK TABLES `order_temps` WRITE;
/*!40000 ALTER TABLE `order_temps` DISABLE KEYS */;
INSERT INTO `order_temps` VALUES (13,'https://cbu01.alicdn.com/img/ibank/2017/532/283/4663382235_555999837.400x400.jpg','https://detail.1688.com/offer/548388514757.html?spm=a360q.7751291.0.0.17533138aFSzJV','爆炸钩 盒装神刺荧光爆炸钩  炸弹钩 钓钩散装荧光钩 伊势尼鱼钩','[\"\\u89c4\\u683c: \\u795e\\u523a\\u7206\\u70b8\\u94a96#\"]',5,25634.00,128170.00,'[{\"begin\":\"12\",\"end\":\"59\",\"price\":\"7.43\"},{\"begin\":\"60\",\"end\":\"215\",\"price\":\"7.23\"},{\"begin\":\"216\",\"end\":null,\"price\":\"6.93\"}]','548388514757','芜湖市后梅荣鱼钩厂','2018-12-19 03:55:44','2018-12-19 03:55:44'),(14,'https://cbu01.alicdn.com/img/ibank/2017/532/283/4663382235_555999837.400x400.jpg','https://detail.1688.com/offer/548388514757.html?spm=a360q.7751291.0.0.17533138aFSzJV','爆炸钩 盒装神刺荧光爆炸钩  炸弹钩 钓钩散装荧光钩 伊势尼鱼钩','[\"\\u89c4\\u683c: \\u795e\\u523a\\u7206\\u70b8\\u94a97#\"]',5,25634.00,128170.00,'[{\"begin\":\"12\",\"end\":\"59\",\"price\":\"7.43\"},{\"begin\":\"60\",\"end\":\"215\",\"price\":\"7.23\"},{\"begin\":\"216\",\"end\":null,\"price\":\"6.93\"}]','548388514757','芜湖市后梅荣鱼钩厂','2018-12-19 03:55:44','2018-12-19 03:55:44'),(17,'//img.alicdn.com/imgextra/i4/3074463495/TB2a10maGSWBuNjSsrbXXa0mVXa_!!3074463495.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?id=564913136160&ali_refid=a3_430583_1006:1124969350:N:T%E6%81%A4%E5%A5%B3:2df09574532f2c071e320415f6faa62f&ali_trackid=1_2df09574532f2c071e320415f6faa62f&spm=a230r.1.14.1&skuId=3740762666829','夏装纯棉短袖t恤女2018新款韩范竹节棉宽松衣服半袖纯色大码上衣','[\"\\u5c3a\\u7801:XL\",\"\\u4e3b\\u8981\\u989c\\u8272:\\u6697\\u7c89\\u8272\"]',10,68655.00,686550.00,NULL,NULL,'诗凡秀旗舰店','2018-12-19 04:06:24','2018-12-19 04:06:24'),(20,'//img.alicdn.com/imgextra/i3/3415196073/TB2PXb7dSzqK1RjSZFLXXcn2XXa_!!3415196073.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?id=560525796874&ali_refid=a3_430583_1006:1151423423:N:%E7%94%B7%E9%9E%8B:8a2800f335477152f46d2acac5067e3a&ali_trackid=1_8a2800f335477152f46d2acac5067e3a&spm=a230r.1.14.1&skuId=3795751201572','冬季男士短靴英伦工装靴男大头鞋男鞋真皮加绒棉鞋男中帮马丁靴男','[\"\\u5c3a\\u7801:41\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u6d45\\u68d5\\u8272\"]',3,500250.00,1500750.00,NULL,NULL,'螺尚旗舰店','2018-12-19 04:07:54','2018-12-19 04:07:54'),(26,'https://cbu01.alicdn.com/img/ibank/2016/504/004/2712400405_1994684750.400x400.jpg','https://detail.1688.com/offer/525985582180.html?spm=b26110380.sw1688.0.0.701a2597hUMe71','丰田新凯美瑞锐志致炫雅力士花冠逸致皇冠威驰FS雷凌致享挡泥板',NULL,1,41400.00,41400.00,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"12.00\"},{\"begin\":\"30\",\"end\":\"79\",\"price\":\"11.50\"},{\"begin\":\"80\",\"end\":null,\"price\":\"6.50\"}]','525985582180','湖南丹尼汽车配件有限公司','2018-12-19 04:23:00','2018-12-19 04:23:00'),(27,'https://cbu01.alicdn.com/img/ibank/2016/504/004/2712400405_1994684750.400x400.jpg','https://detail.1688.com/offer/525985582180.html?spm=b26110380.sw1688.0.0.701a2597hUMe71','丰田新凯美瑞锐志致炫雅力士花冠逸致皇冠威驰FS雷凌致享挡泥板',NULL,4,41400.00,165600.00,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"12.00\"},{\"begin\":\"30\",\"end\":\"79\",\"price\":\"11.50\"},{\"begin\":\"80\",\"end\":null,\"price\":\"6.50\"}]','525985582180','湖南丹尼汽车配件有限公司','2018-12-19 04:23:09','2018-12-19 04:23:09'),(28,'https://cbu01.alicdn.com/img/ibank/2016/504/004/2712400405_1994684750.400x400.jpg','https://detail.1688.com/offer/525985582180.html?spm=b26110380.sw1688.0.0.701a2597hUMe71','丰田新凯美瑞锐志致炫雅力士花冠逸致皇冠威驰FS雷凌致享挡泥板',NULL,30,41400.00,1242000.00,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"12.00\"},{\"begin\":\"30\",\"end\":\"79\",\"price\":\"11.50\"},{\"begin\":\"80\",\"end\":null,\"price\":\"6.50\"}]','525985582180','湖南丹尼汽车配件有限公司','2018-12-19 04:23:22','2018-12-19 04:23:22'),(29,'https://cbu01.alicdn.com/img/ibank/2016/504/004/2712400405_1994684750.400x400.jpg','https://detail.1688.com/offer/525985582180.html?spm=b26110380.sw1688.0.0.701a2597hUMe71','丰田新凯美瑞锐志致炫雅力士花冠逸致皇冠威驰FS雷凌致享挡泥板',NULL,1,41400.00,41400.00,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"12.00\"},{\"begin\":\"30\",\"end\":\"79\",\"price\":\"11.50\"},{\"begin\":\"80\",\"end\":null,\"price\":\"6.50\"}]','525985582180','湖南丹尼汽车配件有限公司','2018-12-19 04:23:32','2018-12-19 04:23:32'),(30,'https://cbu01.alicdn.com/img/ibank/2016/504/004/2712400405_1994684750.400x400.jpg','https://detail.1688.com/offer/525985582180.html?spm=b26110380.sw1688.0.0.701a2597hUMe71','丰田新凯美瑞锐志致炫雅力士花冠逸致皇冠威驰FS雷凌致享挡泥板',NULL,80,41400.00,3312000.00,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"12.00\"},{\"begin\":\"30\",\"end\":\"79\",\"price\":\"11.50\"},{\"begin\":\"80\",\"end\":null,\"price\":\"6.50\"}]','525985582180','湖南丹尼汽车配件有限公司','2018-12-19 04:24:22','2018-12-19 04:24:22'),(31,'https://cbu01.alicdn.com/img/ibank/2016/504/004/2712400405_1994684750.400x400.jpg','https://detail.1688.com/offer/525985582180.html?spm=b26110380.sw1688.0.0.701a2597hUMe71','丰田新凯美瑞锐志致炫雅力士花冠逸致皇冠威驰FS雷凌致享挡泥板',NULL,80,41400.00,3312000.00,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"12.00\"},{\"begin\":\"30\",\"end\":\"79\",\"price\":\"11.50\"},{\"begin\":\"80\",\"end\":null,\"price\":\"6.50\"}]','525985582180','湖南丹尼汽车配件有限公司','2018-12-19 04:28:06','2018-12-19 04:28:06'),(36,'https://img.alicdn.com/imgextra/i3/2541080751/O1CN01vTS6YK1HPzvV58p9e_!!2541080751.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a1z09.8149145.0.0.273478eaO4rWUE&id=575785919169&skuId=3947981231539','02-08 Toyota cũ Vichy hộp tay vịn 03-07 Vios dành riêng trung tâm cầm tay hộp trung tâm điều khiển chuyển đổi phụ kiện','[\"Ph\\u00e2n lo\\u1ea1i m\\u00e0u:C\\u00e1c m\\u00f4 h\\u00ecnh c\\u00f3 th\\u1ec3 thu v\\u00e0o [\\u0111en] l\\u01b0u tr\\u1eef k\\u00e9p + c\\u00f3 th\\u1ec3 m\\u1edf r\\u1ed9ng + USB ph\\u00eda tr\\u01b0\\u1edbc + v\\u1edbi \\u00e1nh s\\u00e1ng + chi\\u1ec1u r\\u1ed9ng 13CM\"]',1,165600.00,165600.00,NULL,NULL,'Dongjia Auto Phụ kiện cửa hàng nhượng quyền','2018-12-25 02:13:38','2018-12-25 02:13:38'),(37,'https://img.alicdn.com/imgextra/i3/2541080751/O1CN01vTS6YK1HPzvV58p9e_!!2541080751.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a1z09.8149145.0.0.273478eaO4rWUE&id=575785919169&skuId=3947981231539','02-08 Toyota cũ Vichy hộp tay vịn 03-07 Vios dành riêng trung tâm cầm tay hộp trung tâm điều khiển chuyển đổi phụ kiện','[\"Ph\\u00e2n lo\\u1ea1i m\\u00e0u:C\\u00e1c m\\u00f4 h\\u00ecnh c\\u00f3 th\\u1ec3 thu v\\u00e0o [\\u0111en] l\\u01b0u tr\\u1eef k\\u00e9p + c\\u00f3 th\\u1ec3 m\\u1edf r\\u1ed9ng + USB ph\\u00eda tr\\u01b0\\u1edbc + v\\u1edbi \\u00e1nh s\\u00e1ng + chi\\u1ec1u r\\u1ed9ng 13CM\"]',1,165600.00,165600.00,NULL,NULL,'Dongjia Auto Phụ kiện cửa hàng nhượng quyền','2018-12-25 02:13:38','2018-12-25 02:13:38'),(49,'//img.alicdn.com/imgextra/i2/3287104402/O1CN011iO9xPmIc0kX777_!!3287104402.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224079','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','[\"\\u5c3a\\u7801:36\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u96c5\\u81f4\\u7ea2(\\u5973\\u6b3e\\uff09\"]',5,299.00,1495.00,NULL,NULL,'足力健旗舰店','2018-12-27 08:11:07','2018-12-27 08:11:07'),(50,'//img.alicdn.com/imgextra/i2/3287104402/O1CN011iO9xPmIc0kX777_!!3287104402.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224080','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','[\"\\u5c3a\\u7801:37\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u96c5\\u81f4\\u7ea2(\\u5973\\u6b3e\\uff09\"]',5,299.00,1495.00,NULL,NULL,'足力健旗舰店','2018-12-27 08:11:25','2018-12-27 08:11:25'),(51,'//img.alicdn.com/imgextra/i2/3287104402/O1CN011iO9xPmIc0kX777_!!3287104402.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224081','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','[\"\\u5c3a\\u7801:38\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u96c5\\u81f4\\u7ea2(\\u5973\\u6b3e\\uff09\"]',5,299.00,1495.00,NULL,NULL,'足力健旗舰店','2018-12-27 08:11:29','2018-12-27 08:11:29'),(52,'//img.alicdn.com/imgextra/i2/3287104402/O1CN011iO9xPmIc0kX777_!!3287104402.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224082','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','[\"\\u5c3a\\u7801:39\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u96c5\\u81f4\\u7ea2(\\u5973\\u6b3e\\uff09\"]',5,299.00,1495.00,NULL,NULL,'足力健旗舰店','2018-12-27 08:11:32','2018-12-27 08:11:32'),(53,'//img.alicdn.com/imgextra/i2/3287104402/O1CN011iO9xPmIc0kX777_!!3287104402.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224083','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','[\"\\u5c3a\\u7801:40\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u96c5\\u81f4\\u7ea2(\\u5973\\u6b3e\\uff09\"]',5,299.00,1495.00,NULL,NULL,'足力健旗舰店','2018-12-27 08:11:35','2018-12-27 08:11:35'),(59,'https://cbu01.alicdn.com/img/ibank/2016/445/282/2852282544_1798559658.400x400.jpg','https://detail.1688.com/offer/42065891608.html?spm=a360q.8274423.1130995625.138.49c84c9aa9m1IR','U型枕记忆棉慢回弹汽车用品颈椎枕 旅行护颈枕 脖子飞机U形靠枕','[\"\\u5c3a\\u5bf8\\u89c4\\u683c: 28*28*8cm\",\"\\u989c\\u8272: \\u9152\\u7ea2\"]',1000,6.80,6800.00,'[{\"begin\":\"1\",\"end\":\"9\",\"price\":\"7.5\"},{\"begin\":\"10\",\"end\":\"999\",\"price\":\"7.2\"},{\"begin\":\"1000\",\"end\":null,\"price\":\"6.8\"}]','42065891608','南通可彤美域电子商务有限公司','2018-12-28 03:54:01','2018-12-28 03:54:01'),(60,'https://cbu01.alicdn.com/img/ibank/2017/651/292/4649292156_107340832.400x400.jpg','https://detail.1688.com/offer/544571424313.html?spm=a2615.7691456.autotrace-offerGeneral.4.a5277cc1KDC7LV','创意倒流香炉陶瓷高山流水熏香炉室内仿古檀香炉观烟摆件香炉批发','[\"\\u89c4\\u683c: \\u9ad8\\u5c71\\u6d41\\u6c34\"]',320,9.80,3136.00,'[{\"begin\":\"1\",\"end\":\"79\",\"price\":\"14\"},{\"begin\":\"80\",\"end\":\"319\",\"price\":\"12\"},{\"begin\":\"320\",\"end\":null,\"price\":\"9.8\"}]','544571424313','福建省泉州市创雅香道有限公司','2019-01-02 18:37:21','2019-01-02 18:37:21'),(61,'//gd3.alicdn.com/imgextra/i1/1591525107/TB2VbVRhk.HL1JjSZFuXXX8dXXa_!!1591525107.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=2013.1.20141002.6.82612145VTifJ3&scm=1007.10009.70205.100200300000001&id=43493589236&pvid=570992ac-31d6-45f9-b775-d3570c98608d','模块 激光头传感器模块 KY-008 aruino 适用 鸿胜电子',NULL,20,1.50,30.00,NULL,NULL,'杨丽芹8876','2019-01-02 22:54:27','2019-01-02 22:54:27'),(62,'//img.alicdn.com/imgextra/i3/3217965353/TB2NQfCpfImBKNjSZFlXXc43FXa_!!3217965353.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a1z0d.6639537.1997196601.4.27ba7484rtPlRb&id=575553689245&skuId=3870423364574','冬季羽绒棉服女2018新款棉衣女中长款韩版修身过膝长款棉袄外套潮','[\"\\u5c3a\\u7801:M\\/160\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u7c73\\u767d\\u8272\\u7eaf\\u8272\\u6bdb\\u9886\"]',1,109.00,109.00,NULL,NULL,'恋薇路旗舰店','2019-01-02 23:14:00','2019-01-02 23:14:00'),(63,'https://cbu01.alicdn.com/img/ibank/2018/209/082/9248280902_690455116.400x400.jpg','https://detail.1688.com/offer/575560808752.html?spm=a360q.9889319.0.0.7bcf3748JUB2ZX','CT口红哑光雾面豆沙色滋润持久不脱色唇膏mac',NULL,2,65.00,130.00,'[{\"begin\":\"3\",\"end\":\"9\",\"price\":\"65.00\"},{\"begin\":\"10\",\"end\":\"99\",\"price\":\"60.00\"},{\"begin\":\"100\",\"end\":null,\"price\":\"50.00\"}]','575560808752','深圳市小杰护肤品有限公司','2019-01-03 00:32:09','2019-01-03 00:32:09'),(64,'//gd1.alicdn.com/imgextra/i4/1922212016/TB2lik6sFXXXXXMXpXXXXXXXXXX_!!1922212016.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a230r.1.14.42.2c2c7ac1W8mLgr&id=535642969976&ns=1&abbucket=14&fbclid=IwAR02edxysVidUqMx4dqJ7poQXoVN_4xGB4xh2tBKdt5BdLSaUcbvkOErOOA#detail','全新原装韩国SAMWHA 2.7V500F 2.7V 500F 超级 法拉电容 35*60MM','[\"\\u0110en:Hai ch\\u00e2n\"]',30,11.80,354.00,NULL,NULL,'来 米 西','2019-01-03 00:51:43','2019-01-03 00:51:43'),(65,'//img.alicdn.com/imgextra/i3/3818188777/O1CN01FpB1Jt2Ehuknk3FYU_!!3818188777.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?id=566541108228&sourceType=item&ttid=212200@taobao_android_8.2.1&ut_sk=1.WcIDqiqKko4DAAUKapcqEE7L_21646297_1546480275708.GoodsTitleURL.1&sp_tk=77%20lazRKc2JwQk5XZU7vv6U=&cpp=1&shareurl=true&spm=a313p.22.35h.1001913713118&short_name=h.3rcEHFZ&sm=c17af6&app=chrome&skuId=3602030881912','古雷米厨师工作服冬装酒店饭店餐厅厨师衣服烘焙后厨厨房白色棉男','[\"\\u5c3a\\u7801:S\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u767d\\u8272+\\u9ed1\"]',1,85.00,85.00,NULL,NULL,'古雷米旗舰店','2019-01-03 01:25:40','2019-01-03 01:25:40'),(66,'https://gd1.alicdn.com/imgextra/i3/277701383/TB20Hp2d3xlpuFjSszbXXcSVpXa_!!277701383.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a1z0d.6639537.1997196601.99.772e74841h417a&id=545040559874','陶瓷酒具套装黄酒酒具白酒酒杯酒壶分酒器白酒杯小酒盅清酒具防古','[\"\\u989c\\u8272\\u5206\\u7c7b:\\u534a\\u6708\\u9152\\u58f6\\u534a\\u65a48\\u676f\\u3010\\u6625\\u8272\\u3011\\u7b80\\u88c5\"]',1,25.00,25.00,NULL,NULL,'a168568988','2019-01-03 19:49:37','2019-01-03 19:49:37'),(67,'https://gd3.alicdn.com/imgextra/i4/277701383/TB2SWx9d80kpuFjSsppXXcGTXXa_!!277701383.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a1z0d.6639537.1997196601.99.772e74841h417a&id=545040559874','陶瓷酒具套装黄酒酒具白酒酒杯酒壶分酒器白酒杯小酒盅清酒具防古','[\"\\u989c\\u8272\\u5206\\u7c7b:\\u534a\\u6708\\u9152\\u58f6\\u534a\\u65a48\\u676f\\u3010\\u7af9\\u3011\\u7b80\\u88c5\"]',1,25.00,25.00,NULL,NULL,'a168568988','2019-01-03 19:50:03','2019-01-03 19:50:03'),(68,'https://cbu01.alicdn.com/img/ibank/2018/781/941/9675149187_2081593300.400x400.jpg','https://detail.1688.com/offer/582107714757.html?spm=a26d1.8091894.ji02bg81.15.50a34ea68v9yvm#','厂家直销新款大号猫砂盆 全封闭式后掀盖猫厕所 环保卫生使用方便','[\"\\u89c4\\u683c: \\u7c89\\u8272\"]',100,40.00,4000.00,'[{\"begin\":\"1\",\"end\":\"9\",\"price\":\"49\"},{\"begin\":\"10\",\"end\":\"99\",\"price\":\"44\"},{\"begin\":\"100\",\"end\":null,\"price\":\"40\"}]','582107714757','广州嘉禧宠物用品有限公司','2019-01-06 05:18:10','2019-01-06 05:18:10'),(69,'https://cbu01.alicdn.com/img/ibank/2017/038/169/4561961830_1769594335.400x400.jpg','https://detail.1688.com/offer/557255726131.html?spm=a2604.11786440.jjtsrjyt.16.192366c5EClT2l','2018秋季长袖运动套装男大码外套运动服套装女韩版女休闲情侣潮','[\"\\u5c3a\\u7801: L\",\"\\u989c\\u8272: \\u7537\\u9ed1\\u8272\"]',100,55.00,5500.00,'[{\"begin\":\"2\",\"end\":\"49\",\"price\":\"65\"},{\"begin\":\"50\",\"end\":\"99\",\"price\":\"60\"},{\"begin\":\"100\",\"end\":null,\"price\":\"55\"}]','557255726131','石狮市精品男人服装厂','2019-01-06 05:20:22','2019-01-06 05:20:22'),(70,'//img.alicdn.com/imgextra/i4/2907127182/O1CN0122vP0v4XSaZ8y1N_!!2907127182.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.1.5906232eU2YNJJ&id=577052284049&skuId=3869060589201&user_id=2907127182&cat_id=2&is_b=1&rn=ebdf243ab3dfb544a8498a108a3cd65c','初姗古装女仙女汉服改良襦裙广袖流仙裙中国风清新淡雅演出服装冬','[\"\\u5c3a\\u7801:\\u5c0f\\u7801M(\\u8eab\\u9ad8158-163cm\\uff09\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u7ea2\\u8272\"]',3,268.00,804.00,NULL,NULL,'初姗旗舰店','2019-01-06 20:15:28','2019-01-06 20:15:28'),(72,'//img.alicdn.com/imgextra/i2/1962127751/TB2_JpbuC8YBeNkSnb4XXaevFXa_!!1962127751.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.999.104.6c59523cMzuP2u&id=573079625262&ns=1&skuId=3904125871610','EXLUX纯头层绵羊皮海宁真皮皮衣男小圆领皮夹克春秋季款男装外套','[\"\\u5c3a\\u7801:M\",\"\\u989c\\u8272:\\u9ed1\\u8272\"]',1,698.00,698.00,NULL,NULL,'exlux旗舰店','2019-01-06 21:03:47','2019-01-06 21:03:47'),(73,'//img.alicdn.com/imgextra/i3/1600669341/O1CN017d1i6q2IsE7vEiVCN_!!1600669341.jpg_430x430q90.jpg','https://detail.tmall.com/item.htm?spm=a230r.1.999.49.6bd1523cywTv2L&id=563635129949&ns=1&skuId=3863185452169','南极人海宁真皮皮衣男短款帅气韩版修身羊皮夹克男士外套2018新款','[\"\\u5c3a\\u7801:M\",\"\\u989c\\u8272:\\u9ed1\\u8272\\u7acb\\u9886-\\u5939\\u68c9\\u6b3e\"]',1,698.00,698.00,NULL,NULL,'南极人懿想专卖店','2019-01-06 21:04:47','2019-01-06 21:04:47'),(74,'//gd3.alicdn.com/imgextra/i3/0/TB160sHAKuSBuNjSsplXXbe8pXa_!!0-item_pic.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a1z0d.6639537.1997196601.342.74207484ukGXo7&id=572755243425','日本进口 花王拉拉裤L44片大号44片尿不湿 婴儿学步裤 袋子微磨',NULL,1,69.00,69.00,NULL,NULL,'月月140518','2019-01-07 01:49:42','2019-01-07 01:49:42'),(75,'https://gd4.alicdn.com/imgextra/i4/2880209214/TB2RYXWu5MnBKNjSZFCXXX0KFXa_!!2880209214.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a230r.1.999.1.21c3523ce1HZzp&id=576768359765&ns=1#detail','休闲运动服套装女秋季2018新款韩版学生跑步长袖连帽卫衣两件套潮','[\"\\u5c3a\\u7801:M\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u7ea2\\u8272\"]',1,94.80,94.80,NULL,NULL,NULL,'2019-01-08 03:03:09','2019-01-08 03:03:09'),(76,'https://gd1.alicdn.com/imgextra/i4/2546052776/O1CN011WNRvd1ScL0qZBE_!!2546052776.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a230r.1.999.1.3d2d523c8o8zPp&id=575529610651&ns=1#detail','小香风包包女2018新款欧美时尚单肩手提斜挎多层简约托特仙女小包','[\"\\u989c\\u8272\\u5206\\u7c7b:\\u6b3e\\u5f0f\\u4e09\"]',1,32.90,32.90,NULL,NULL,'莉莉休闲女包','2019-01-08 03:13:11','2019-01-08 03:13:11'),(77,'https://gd2.alicdn.com/imgextra/i2/4107450890/O1CN01Y8hnOn1IRezDAd35W_!!4107450890.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a230r.1.999.3.23bb523c5Oh6Qw&id=584674618340&ns=1#detail','4d豪华智能电动按摩椅全自动家用太空舱全身揉捏多功能按摩器','[\"\\u989c\\u8272\\u5206\\u7c7b:\\u514d\\u5b89\\u88c5\\u9ed1\\u81f3\\u5c0a\\u7248\\u5168\\u8eab\\u6c14\\u56ca\\u5305\\u88f9\\u5939\\u634f\"]',1,2638.00,2638.00,NULL,NULL,'tb58890332','2019-01-09 01:45:47','2019-01-09 01:45:47'),(78,'https://gd2.alicdn.com/imgextra/i2/4107450890/O1CN01Y8hnOn1IRezDAd35W_!!4107450890.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a230r.1.999.1.2c45523cMX5S3j&id=584674618340&ns=1#detail','4d豪华智能电动按摩椅全自动家用太空舱全身揉捏多功能按摩器','[\"\\u989c\\u8272\\u5206\\u7c7b:\\u514d\\u5b89\\u88c5\\u9ed1\\u81f3\\u5c0a\\u7248\\u5168\\u8eab\\u6c14\\u56ca\\u5305\\u88f9\\u5939\\u634f\"]',1,2638.00,2638.00,NULL,NULL,'tb58890332','2019-01-09 02:09:18','2019-01-09 02:09:18'),(79,'https://gd4.alicdn.com/imgextra/i2/4107450890/O1CN01SsMps51IRezE3tbDr_!!4107450890.jpg_400x400.jpg','https://item.taobao.com/item.htm?spm=a230r.1.999.1.2c45523cMX5S3j&id=584674618340&ns=1#detail','4d豪华智能电动按摩椅全自动家用太空舱全身揉捏多功能按摩器','[\"\\u989c\\u8272\\u5206\\u7c7b:\\u514d\\u5b89\\u88c5\\u9ed1\\u65d7\\u8230\\u7248\\u5168\\u8eab\\u6c14\\u56ca\\u5305\\u88f9\\u5939\\u634f\"]',1,2278.00,2278.00,NULL,NULL,'tb58890332','2019-01-09 02:09:49','2019-01-09 02:09:49'),(80,'https://gd1.alicdn.com/imgextra/i2/2292801078/TB2HjmqbruWBuNjSszgXXb8jVXa_!!2292801078.jpg_400x400.jpg_.webp','https://item.taobao.com/item.htm?spm=a230r.1.14.229.450e4a41USPGSf&id=526477683646&ns=1&abbucket=19#detail','2019新款全自动花式棉花糖机摆摊用煤气商用推车式电动棉花糖机器',NULL,1,746.00,746.00,NULL,NULL,'香咖啡521','2019-01-09 04:18:34','2019-01-09 04:18:34');
/*!40000 ALTER TABLE `order_temps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `supporter_id` int(10) unsigned DEFAULT NULL,
  `transport_receive_type` tinyint(4) DEFAULT NULL,
  `purchase_cn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_vn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_total_items_price` double(14,2) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `confirm_date` datetime DEFAULT NULL,
  `purchase_service_fee` double(11,2) DEFAULT NULL,
  `purchase_cn_transport_fee` double(11,2) DEFAULT NULL,
  `purchase_vn_transport_fee` double(11,2) DEFAULT NULL,
  `purchase_returns_fee` double(11,2) DEFAULT NULL,
  `purchase_count_fee` double(11,2) DEFAULT NULL,
  `purchase_wood_fee` double(11,2) DEFAULT NULL,
  `final_total_price` double(15,2) DEFAULT NULL,
  `min_deposit` double(5,2) DEFAULT NULL,
  `deposited` double(15,2) DEFAULT NULL,
  `admin_note` text COLLATE utf8mb4_unicode_ci,
  `order_type` tinyint(4) NOT NULL COMMENT '1: Mua hộ, 2: Vận chuyển',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `shop_name` text COLLATE utf8mb4_unicode_ci,
  `purchase_cn_to_vn_fee` double(11,2) DEFAULT NULL,
  `is_discounted` tinyint(4) NOT NULL DEFAULT '0',
  `cn_transport_fee` double(11,2) DEFAULT NULL,
  `vn_transport_fee` double(11,2) DEFAULT NULL,
  `cn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_note` text COLLATE utf8mb4_unicode_ci,
  `cn_order_date` datetime DEFAULT NULL,
  `receiver_info` text COLLATE utf8mb4_unicode_ci,
  `link_qty` text COLLATE utf8mb4_unicode_ci,
  `via_border_transport_fee` double(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'00000001',1,1,1,NULL,NULL,1014400000.00,5,'2018-12-28 13:40:15',30432000.00,NULL,NULL,NULL,NULL,NULL,1046953212.00,NULL,NULL,'ewr',1,'2018-12-26 03:00:32','2019-01-05 00:26:51','深圳市龙岗区华之秀礼品厂',2121212.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'00000002',2,1,1,NULL,NULL,959100.00,5,'2018-12-27 14:34:37',28773.00,0.00,NULL,NULL,NULL,NULL,987873.00,NULL,NULL,'cân nặng tính sau',1,'2018-12-27 07:07:33','2018-12-27 07:34:37','骆驼男装旗舰店',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'00000003',2,1,1,NULL,NULL,1173000.00,5,'2018-12-27 14:35:24',35190.00,0.00,NULL,NULL,NULL,NULL,1238190.00,NULL,NULL,NULL,1,'2018-12-27 07:08:36','2018-12-27 07:35:24','emitdoog旗舰店',30000.00,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'00000004',2,1,1,NULL,NULL,6417000.00,9,NULL,192510.00,517500.00,NULL,NULL,NULL,NULL,7439010.00,NULL,NULL,'đã cân nặng',1,'2018-12-27 07:10:57','2018-12-27 08:15:42','温州俊斯特鞋业有限公司',312000.00,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'00000005',2,1,1,NULL,NULL,400000.00,6,'2018-12-27 14:35:31',12000.00,98325.00,NULL,NULL,NULL,NULL,1010325.00,NULL,NULL,'cân nặng tính sau: 500kg',1,'2018-12-27 07:23:13','2019-01-05 00:26:46','义乌市聚瑞电子商务商行',500000.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'00000006',2,NULL,NULL,NULL,NULL,8625000.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,8625000.00,NULL,NULL,NULL,1,'2018-12-27 07:24:11','2018-12-27 07:24:11','东莞市宇捷电子科技有限公司',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'00000007',2,1,1,NULL,NULL,15473250.00,4,NULL,464197.50,0.00,NULL,NULL,NULL,NULL,16093447.50,NULL,NULL,'cân nặng đã tính',1,'2018-12-27 08:12:18','2018-12-27 08:20:43','足力健旗舰店',156000.00,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'00000008',2,1,0,NULL,NULL,5157750.00,4,NULL,154732.50,0.00,NULL,NULL,NULL,NULL,5352482.50,NULL,NULL,'đã tính cân nặng',1,'2018-12-27 08:14:05','2019-01-05 00:26:44','足力健旗舰店',40000.00,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'00000009',2,NULL,NULL,NULL,NULL,4133100.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4133100.00,NULL,NULL,NULL,1,'2018-12-27 08:24:12','2018-12-27 08:24:12','艾莱诺旗舰店',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'00000010',2,1,0,NULL,NULL,NULL,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,130000.00,NULL,NULL,'Khách nhận hàng tự trả cước Lào cai - HN',2,'2018-12-28 03:19:19','2018-12-28 03:44:10',NULL,NULL,0,100000.00,NULL,'669530909078','VN123456789','Đồ câu','2018-12-26 00:00:00','Vũ trung','[{\"link\":\"https:\\/\\/trade.1688.com\\/order\\/offer_snapshot.htm?spm=a360q.8728381.content.45.59c06aa6Oh90aq&order_entry_id=257928646064376180\",\"qty\":\"10\"},{\"link\":\"https:\\/\\/trade.1688.com\\/order\\/offer_snapshot.htm?spm=a360q.8728381.content.45.59c06aa6Oh90aq&order_entry_id=257928646064376180\",\"qty\":\"20\"},{\"link\":\"https:\\/\\/trade.1688.com\\/order\\/offer_snapshot.htm?spm=a360q.8728381.content.49.59c06aa6Oh90aq&order_entry_id=257928646066376180\",\"qty\":\"50\"}]',30000.00),(12,'00000012',2,1,1,NULL,NULL,NULL,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL,NULL,NULL,2,'2018-12-28 03:21:36','2018-12-28 07:55:53',NULL,NULL,0,NULL,NULL,'3909546030249',NULL,'Gối tựa đầu','2018-12-26 00:00:00','Vũ trung','[{\"link\":\"https:\\/\\/detail.1688.com\\/offer\\/42065891608.html?spm=a360q.8274423.1130995625.138.49c84c9aa9m1IR\",\"qty\":\"50\"}]',NULL),(13,'00000013',2,1,0,NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,NULL,NULL,NULL,2,'2018-12-28 06:58:42','2018-12-28 07:33:15',NULL,NULL,0,NULL,NULL,'70178507501605',NULL,'Test','2018-12-25 00:00:00','Lào cai','[{\"link\":\"https:\\/\\/detail.1688.com\\/offer\\/44483526294.html?spm=a360q.8274423.1130995625.204.49c84c9aDon4ZM\",\"qty\":\"100\"}]',NULL),(15,'00000014',2,1,1,NULL,NULL,NULL,8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,600000.00,NULL,NULL,'từ lào cai đi khách tự trả',2,'2018-12-28 07:39:09','2018-12-28 07:55:45',NULL,NULL,0,100000.00,NULL,'70178507501604','123456','abc','2018-11-29 00:00:00','125','[{\"link\":\"https:\\/\\/detail.1688.com\\/offer\\/44483526294.html?spm=a360q.8274423.1130995625.204.49c84c9aDon4ZM\",\"qty\":\"1000\"}]',500000.00),(16,'00000015',4,NULL,1,NULL,NULL,672000.00,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,672000.00,NULL,NULL,NULL,1,'2019-01-06 20:43:06','2019-01-06 20:44:04','奥德摩卫浴专营店',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'create_users','Thêm mới người dùng','web','2018-12-18 06:53:35','2018-12-18 06:53:35'),(2,'edit_users','Sửa thông tin người dùng','web','2018-12-18 06:53:35','2018-12-18 06:53:35'),(3,'delete_users','Xóa người dùng','web','2018-12-18 06:53:35','2018-12-18 06:53:35'),(4,'create_exchange_rates','Thêm mới tỷ giá','web','2018-12-18 06:53:35','2018-12-18 06:53:35'),(5,'edit_exchange_rates','Sửa tỷ giá','web','2018-12-18 06:53:35','2018-12-18 06:53:35'),(6,'delete_exchange_rates','Xóa tỷ giá','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(7,'create_slides','Thêm mới slide','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(8,'edit_slides','Sửa slide','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(9,'delete_slides','Xóa slide','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(10,'edit_orders','Sửa đơn hàng','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(11,'view_all_purchase_orders','Xem tất cả đơn hàng mua hộ','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(12,'view_all_transport_orders','Xem tất cả đơn hàng vận chuyển','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(13,'view_customer_info','Xem thông tin khách hàng','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(14,'create_customers','Thêm khách hàng','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(15,'view_feedback','Xem hòm thư góp ý','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(16,'create_blogs','Thêm mới bài viết','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(17,'edit_blogs','Sửa bài viết','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(18,'delete_blogs','Xóa bài viết','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(19,'create_roles','Thêm mới vai trò','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(20,'edit_roles','Sửa vai trò','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(21,'delete_roles','Xóa vai trò','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(22,'edit_website_configs','Sửa cấu hình website','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(23,'create_pages','Thêm page','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(24,'edit_pages','Sửa page','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(25,'delete_pages','Xóa page','web','2018-12-18 06:53:36','2018-12-18 06:53:36'),(26,'view_reports','Xem báo cáo','web','2018-12-18 06:53:36','2018-12-18 06:53:36');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_order_items`
--

DROP TABLE IF EXISTS `purchase_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property` text COLLATE utf8mb4_unicode_ci,
  `qty` int(11) NOT NULL,
  `price` double(14,2) NOT NULL,
  `customer_note` text COLLATE utf8mb4_unicode_ci,
  `admin_note` text COLLATE utf8mb4_unicode_ci,
  `price_range` text COLLATE utf8mb4_unicode_ci,
  `cn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cn_order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `current_rate` double(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_order_items`
--

LOCK TABLES `purchase_order_items` WRITE;
/*!40000 ALTER TABLE `purchase_order_items` DISABLE KEYS */;
INSERT INTO `purchase_order_items` VALUES (1,'https://cbu01.alicdn.com/img/ibank/2013/433/567/987765334_95650897.400x400.jpg','新款毛绒玩具定制动物酒店吉祥物泰迪熊小号毛巾布穿浴袍小熊公仔','https://detail.1688.com/offer/1294080671.html?spm=b26110380.sw1688.mof001.131.63aa783bcbfqxJ','1294080671','[\"\\u9ad8\\u5ea6: 15cm\",\"\\u989c\\u8272: \\u68d5\\u8272\"]',30000,9.80,NULL,'rffwfwe','[{\"begin\":\"20\",\"end\":\"29999\",\"price\":\"16\"},{\"begin\":\"30000\",\"end\":\"49999\",\"price\":\"9.8\"},{\"begin\":\"50000\",\"end\":null,\"price\":\"7\"}]',NULL,NULL,'1','2018-12-26 03:00:32','2018-12-27 07:38:23',1,3450.00),(2,'https://cbu01.alicdn.com/img/ibank/2013/433/567/987765334_95650897.400x400.jpg','新款毛绒玩具定制动物酒店吉祥物泰迪熊小号毛巾布穿浴袍小熊公仔','https://detail.1688.com/offer/1294080671.html?spm=b26110380.sw1688.mof001.131.63aa783bcbfqxJ','1294080671','[\"\\u9ad8\\u5ea6: 15cm\",\"\\u989c\\u8272: \\u68d5\\u8272\"]',3,9.80,NULL,NULL,'[{\"begin\":\"20\",\"end\":\"29999\",\"price\":\"16\"},{\"begin\":\"30000\",\"end\":\"49999\",\"price\":\"9.8\"},{\"begin\":\"50000\",\"end\":null,\"price\":\"7\"}]',NULL,NULL,'1','2018-12-26 03:00:43','2018-12-26 03:00:43',1,3450.00),(3,'//img.alicdn.com/imgextra/i2/839919086/O1CN012GzR7ahWoaHX54h_!!839919086.jpg_430x430q90.jpg','骆驼男装 秋季弹力牛仔裤男宽松直筒小脚青年韩版修身休闲裤子潮','https://detail.tmall.com/item.htm?id=554152328217&ali_refid=a3_430583_1006:1104095586:N:%E7%89%9B%E4%BB%94%E8%A3%A4:4eb4c69ff2a9d55a1272e00d7aba5488&ali_trackid=1_4eb4c69ff2a9d55a1272e00d7aba5488&spm=a230r.1.14.1&skuId=3962055055477',NULL,'[\"\\u5c3a\\u7801:29\",\"\\u989c\\u8272:D7X316526\\u84dd\\u8272\"]',1,139.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 07:07:33','2018-12-27 07:07:33',2,3450.00),(4,'//img.alicdn.com/imgextra/i2/839919086/O1CN012GzR7ahWoaHX54h_!!839919086.jpg_430x430q90.jpg','骆驼男装 秋季弹力牛仔裤男宽松直筒小脚青年韩版修身休闲裤子潮','https://detail.tmall.com/item.htm?id=554152328217&ali_refid=a3_430583_1006:1104095586:N:%E7%89%9B%E4%BB%94%E8%A3%A4:4eb4c69ff2a9d55a1272e00d7aba5488&ali_trackid=1_4eb4c69ff2a9d55a1272e00d7aba5488&spm=a230r.1.14.1&skuId=3962055055478',NULL,'[\"\\u5c3a\\u7801:30\",\"\\u989c\\u8272:D7X316526\\u84dd\\u8272\"]',1,139.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 07:07:45','2018-12-27 07:07:45',2,3450.00),(5,'//img.alicdn.com/imgextra/i2/4037193719/O1CN011dLLQvWMCkmY51C_!!4037193719.jpg_430x430q90.jpg','EMITDOOG现代简约北欧轻奢挂钟客厅卧室艺术家用时钟创意时尚钟表','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.12236d1fDTlCPs&id=576307566504&cm_id=140105335569ed55e27b&abbucket=10&skuId=3957864490444',NULL,'[\"\\u5c3a\\u5bf8:12\\u82f1\\u5bf8\",\"\\u989c\\u8272\\u5206\\u7c7b:A\\u6b3e\"]',5,68.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 07:08:36','2018-12-27 07:08:36',3,3450.00),(6,'https://cbu01.alicdn.com/img/ibank/2018/890/088/9201880098_573552641.400x400.jpg','2018秋季新款皮鞋正装男鞋商务皮鞋男士真皮系带鞋子厂家直销代发','https://detail.1688.com/offer/44274722406.html?spm=b26110380.sw1688.mof001.14.63e63a7e3XLsxO&sk=consign','44274722406','[\"\\u5c3a\\u7801: 38\",\"\\u989c\\u8272: \\u84dd\\u8272\"]',10,62.00,NULL,NULL,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"72\"},{\"begin\":\"30\",\"end\":\"59\",\"price\":\"67\"},{\"begin\":\"60\",\"end\":null,\"price\":\"62\"}]',NULL,NULL,'1','2018-12-27 07:10:57','2018-12-27 07:10:57',4,3450.00),(7,'https://cbu01.alicdn.com/img/ibank/2018/890/088/9201880098_573552641.400x400.jpg','2018秋季新款皮鞋正装男鞋商务皮鞋男士真皮系带鞋子厂家直销代发','https://detail.1688.com/offer/44274722406.html?spm=b26110380.sw1688.mof001.14.63e63a7e3XLsxO&sk=consign','44274722406','[\"\\u5c3a\\u7801: 39\",\"\\u989c\\u8272: \\u84dd\\u8272\"]',10,62.00,NULL,NULL,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"72\"},{\"begin\":\"30\",\"end\":\"59\",\"price\":\"67\"},{\"begin\":\"60\",\"end\":null,\"price\":\"62\"}]',NULL,NULL,'1','2018-12-27 07:10:57','2018-12-27 07:10:57',4,3450.00),(8,'https://cbu01.alicdn.com/img/ibank/2018/890/088/9201880098_573552641.400x400.jpg','2018秋季新款皮鞋正装男鞋商务皮鞋男士真皮系带鞋子厂家直销代发','https://detail.1688.com/offer/44274722406.html?spm=b26110380.sw1688.mof001.14.63e63a7e3XLsxO&sk=consign','44274722406','[\"\\u5c3a\\u7801: 40\",\"\\u989c\\u8272: \\u84dd\\u8272\"]',10,62.00,NULL,NULL,'[{\"begin\":\"1\",\"end\":\"29\",\"price\":\"72\"},{\"begin\":\"30\",\"end\":\"59\",\"price\":\"67\"},{\"begin\":\"60\",\"end\":null,\"price\":\"62\"}]',NULL,NULL,'1','2018-12-27 07:10:57','2018-12-27 07:10:57',4,3450.00),(9,'https://cbu01.alicdn.com/img/ibank/2018/994/345/8541543499_1909741668.400x400.jpg','雨刮器（非玻璃现货 wiper wizard 汽车雨刮修复器清洁刷 TV雨刮','https://detail.1688.com/offer/565067064003.html?spm=a360q.7751291.0.0.49933138sWHGdU','565067064003','[\"\\u5c3a\\u5bf8: \\u5438\\u5851\\u88c5\"]',10,7.80,NULL,'20kg',NULL,NULL,NULL,'1','2018-12-27 07:23:13','2018-12-28 02:38:23',5,3450.00),(10,'https://cbu01.alicdn.com/img/ibank/2018/994/345/8541543499_1909741668.400x400.jpg','雨刮器（非玻璃现货 wiper wizard 汽车雨刮修复器清洁刷 TV雨刮','https://detail.1688.com/offer/565067064003.html?spm=a360q.7751291.0.0.49933138sWHGdU','565067064003','[\"\\u5c3a\\u5bf8: OPP\\u88c5\"]',10,6.80,NULL,'50kg',NULL,NULL,NULL,'1','2018-12-27 07:23:13','2018-12-28 02:38:35',5,3450.00),(11,'https://cbu01.alicdn.com/img/ibank/2017/293/239/3795932392_413313507.400x400.jpg','丰田汽车专用双USB车充插座TOYOTA原装12V24V车载手机充电器4.2A','https://detail.1688.com/offer/544316552593.html?spm=a360q.7751291.0.0.49933138sWHGdU','544316552593','[\"\\u578b\\u53f7: \\u65b0\\u6b3e\\u4e30\\u7530\\u8f66\\u7528\\u53cc\\u8f66\\u5e26\\u76d6\"]',100,25.00,NULL,NULL,'[{\"begin\":\"5\",\"end\":\"299\",\"price\":\"25\"},{\"begin\":\"300\",\"end\":\"999\",\"price\":\"22\"},{\"begin\":\"1000\",\"end\":null,\"price\":\"20\"}]',NULL,NULL,'1','2018-12-27 07:24:11','2018-12-27 07:24:11',6,3450.00),(12,'//img.alicdn.com/imgextra/i4/3287104402/O1CN011iO9xHjzFhVK4qQ_!!3287104402.jpg_430x430q90.jpg','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224087',NULL,'[\"\\u5c3a\\u7801:37\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u65f6\\u5c1a\\u7070\\uff08\\u5973\\u6b3e\\uff09\"]',5,299.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 08:12:18','2018-12-27 08:12:18',7,3450.00),(13,'//img.alicdn.com/imgextra/i4/3287104402/O1CN011iO9xHjzFhVK4qQ_!!3287104402.jpg_430x430q90.jpg','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224088',NULL,'[\"\\u5c3a\\u7801:38\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u65f6\\u5c1a\\u7070\\uff08\\u5973\\u6b3e\\uff09\"]',5,299.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 08:12:54','2018-12-27 08:12:54',7,3450.00),(14,'//img.alicdn.com/imgextra/i4/3287104402/O1CN011iO9xHjzFhVK4qQ_!!3287104402.jpg_430x430q90.jpg','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224089',NULL,'[\"\\u5c3a\\u7801:39\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u65f6\\u5c1a\\u7070\\uff08\\u5973\\u6b3e\\uff09\"]',5,299.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 08:12:57','2018-12-27 08:12:57',7,3450.00),(15,'//img.alicdn.com/imgextra/i2/3287104402/O1CN011iO9xPmIc0kX777_!!3287104402.jpg_430x430q90.jpg','足力健安全老人鞋正品男爸爸健步鞋中老年防滑鞋子夹棉冬季运动鞋','https://detail.tmall.com/item.htm?spm=a230r.1.14.6.492a156bDy3ubU&id=559847446080&cm_id=140105335569ed55e27b&abbucket=11&skuId=3492491224079',NULL,'[\"\\u5c3a\\u7801:36\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u96c5\\u81f4\\u7ea2(\\u5973\\u6b3e\\uff09\"]',5,299.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 08:14:05','2018-12-27 08:14:05',8,3450.00),(16,'//img.alicdn.com/imgextra/i1/2089328478/TB2f1bWnsIrBKNjSZK9XXagoVXa_!!2089328478.jpg_430x430q90.jpg','2018秋冬新款颗粒羊剪绒皮草外套女短款海宁皮毛大衣时尚一体韩版','https://detail.tmall.com/item.htm?spm=a230r.1.14.17.4b7644e7m7OG7l&id=575408222693&cm_id=140105335569ed55e27b&abbucket=11&skuId=3773767232828',NULL,'[\"\\u5c3a\\u7801:M\",\"\\u989c\\u8272\\u5206\\u7c7b:\\u6c99\\u6f20\\u7ea2\"]',1,1198.00,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 08:24:12','2018-12-27 08:24:12',9,3450.00),(17,'//img.alicdn.com/imgextra/i3/2030534297/TB2VYyOaisd61BjSZFHXXXfJXXa_!!2030534297.jpg_430x430q90.jpg','欧式牙刷杯架 镀金色 漱口杯双杯金色五金挂件杯架口杯牙杯免打孔','https://detail.tmall.com/item.htm?spm=a230r.1.14.31.5e9d55980E8uCx&id=539834425498&ns=1&abbucket=9&skuId=3235913689615',NULL,'[\"\\u989c\\u8272\\u5206\\u7c7b:\\u91d1\\u8272\\uff08\\u6253\\u5b54\\u6b3e\\uff09\"]',2,96.00,NULL,NULL,NULL,NULL,NULL,'1','2019-01-06 20:43:06','2019-01-06 20:43:06',16,3500.00);
/*!40000 ALTER TABLE `purchase_order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','admin','web','2018-12-18 06:53:36','2018-12-18 06:53:36');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_fees`
--

DROP TABLE IF EXISTS `service_fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_fees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_price` double(12,2) DEFAULT NULL,
  `to_price` double(12,2) DEFAULT NULL,
  `fee` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_fees`
--

LOCK TABLES `service_fees` WRITE;
/*!40000 ALTER TABLE `service_fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `src` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
INSERT INTO `slides` VALUES (1,'taobao','Vòng Tay','https://item.taobao.com/item.htm?spm=a230r.1.14.21.ee3a147fT51AU3&id=579992452805&ns=1&abbucket=9#detail','1545118766.png',1,1,'2018-12-18 07:39:27','2018-12-18 07:39:27'),(2,'taobao','Gối','https://item.taobao.com/item.htm?spm=a230r.1.14.55.1f2c4c2b3viZA9&id=575665773758&ns=1&abbucket=9#detail','1545118783.png',2,1,'2018-12-18 07:39:44','2018-12-18 07:39:44'),(3,'taobao','Gấu Bông','https://item.taobao.com/item.htm?spm=a230r.1.0.0.4909e18aOsYHb0&id=545892504759&ns=1#detail','1545118810.png',3,1,'2018-12-18 07:40:11','2018-12-18 07:40:11'),(4,'taobao','Áo nữ','https://item.taobao.com/item.htm?spm=a230r.1.0.0.4909e18aOsYHb0&id=570479096061&ns=1#detail','1545118826.png',4,1,'2018-12-18 07:40:29','2018-12-18 07:40:29'),(5,'taobao','Cốc Đôi','https://item.taobao.com/item.htm?spm=a230r.1.0.0.55a0e18akeLAk7&id=575821568243&ns=1#detail','1545118848.png',5,1,'2018-12-18 07:40:49','2018-12-18 07:40:49'),(6,'1688','Tủ lạnh','https://detail.1688.com/offer/552651052394.html?spm=b26110380.sw1688.mof001.9.761462bbHBES16','1545118876.png',1,1,'2018-12-18 07:41:17','2018-12-18 07:41:17'),(7,'1688','Dầu oliu','https://detail.1688.com/offer/532534778405.html?spm=b26110380.sw1688.mof001.19.307f4ae6upYot2','1545118893.png',2,1,'2018-12-18 07:41:34','2018-12-18 07:41:34'),(8,'1688','Áo Khoác','https://detail.1688.com/offer/575360601486.html?spm=b26110380.sw1688.mof001.23.57cd36deX3tXWW','1545118907.png',3,1,'2018-12-18 07:41:49','2018-12-18 07:41:49'),(9,'1688','Balo','https://detail.1688.com/offer/556184701721.html?spm=b26110380.sw1688.0.0.36aa2484QCVwFl&tracelog=p4p&clickid=9c2a314945e34833968ec91d22e28961&sessionid=8d15a8a46b778e85ef8e1fd953ad473f','1545118925.png',4,1,'2018-12-18 07:42:07','2018-12-18 07:42:07'),(10,'1688','Bánh xe tập','https://detail.1688.com/offer/546857718161.html?spm=b26110380.sw1688.0.0.36aa2484QCVwFl&tracelog=p4p&clickid=a88c37fc2d9947cb94949d2a0277a18b&sessionid=8d15a8a46b778e85ef8e1fd953ad473f','1545118945.png',5,1,'2018-12-18 07:42:26','2018-12-18 07:42:26'),(11,'tmall','Áo nam','https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.1.39b15f4cluJ3YP&id=522128736149&skuId=3632653647482&user_id=1022008754&cat_id=2&is_b=1&rn=14d809b2168df03a895a5d499fe48069','1545118966.png',1,1,'2018-12-18 07:42:47','2018-12-18 07:42:47'),(12,'tmall','Rượu vang','https://detail.tmall.com/item.htm?id=573816517977&ali_refid=a3_430008_1006:1122949817:N:%E5%8A%A0%E5%B7%9E%E4%B9%90%E4%BA%8B%203l:d7ee945a595f8ef6ba4c488264a2590b&ali_trackid=1_d7ee945a595f8ef6ba4c488264a2590b&spm=a230r.1.0.0','1545118981.png',2,1,'2018-12-18 07:43:02','2018-12-18 07:43:02'),(13,'tmall','Cốc đôi','https://detail.tmall.com/item.htm?spm=a230r.1.14.31.5e9d55980E8uCx&id=539834425498&ns=1&abbucket=9','1545118998.png',3,1,'2018-12-18 07:43:18','2018-12-18 07:43:18'),(14,'tmall','Quần Nữ','https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.41.330e5c21aUzYXj&id=569035633613&skuId=3962002438623&user_id=1695037642&cat_id=2&is_b=1&rn=7e95044c138e804a0890283d59a349f6','1545119015.png',4,1,'2018-12-18 07:43:36','2018-12-18 07:43:36'),(15,'tmall','Áo nữ','https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.5.330e5c21aUzYXj&id=577007232858&skuId=3810092409607&user_id=2095392386&cat_id=2&is_b=1&rn=7e95044c138e804a0890283d59a349f6','1545119033.png',5,1,'2018-12-18 07:43:54','2018-12-18 07:43:54'),(16,'taobao','2345345','https://detail.tmall.com/item.htm?spm=a220m.1000858.1000725.5.330e5c21aUzYXj&id=577007232858&skuId=3810092409607&user_id=2095392386&cat_id=2&is_b=1&rn=7e95044c138e804a0890283d59a349f6','1545733544.png',100,0,'2018-12-25 10:25:51','2018-12-25 10:25:51');
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_histories`
--

DROP TABLE IF EXISTS `transaction_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `amount_before` double(12,2) NOT NULL,
  `amount_topup` double(12,2) NOT NULL,
  `amount_after` double(12,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_histories`
--

LOCK TABLES `transaction_histories` WRITE;
/*!40000 ALTER TABLE `transaction_histories` DISABLE KEYS */;
INSERT INTO `transaction_histories` VALUES (1,2,1,0.00,5000000.00,5000000.00,'2018-12-27 07:29:29','2018-12-27 07:29:29'),(2,2,1,5000000.00,2500000.00,7500000.00,'2018-12-27 09:14:04','2018-12-27 09:14:04'),(3,2,1,7500000.00,500000.00,8000000.00,'2018-12-28 03:32:25','2018-12-28 03:32:25'),(4,2,1,8000000.00,1000000.00,9000000.00,'2018-12-28 06:36:06','2018-12-28 06:36:06'),(5,1,1,0.00,3000000.00,3000000.00,'2018-12-28 06:36:21','2018-12-28 06:36:21');
/*!40000 ALTER TABLE `transaction_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport_order_items`
--

DROP TABLE IF EXISTS `transport_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transport_order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `cn_transport_fee` double(11,2) DEFAULT NULL,
  `vn_transport_fee` double(11,2) DEFAULT NULL,
  `cn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vn_transport_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_note` text COLLATE utf8mb4_unicode_ci,
  `admin_note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `cn_order_date` datetime NOT NULL,
  `receiver_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_qty` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `via_border_transport_fee` double(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport_order_items`
--

LOCK TABLES `transport_order_items` WRITE;
/*!40000 ALTER TABLE `transport_order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `transport_order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin@gmail.com','$2y$10$kCsCUwM9jUTn7tNyH6GaZu6YcSM1YHla/WaYqxitD9qioXp8XMHii',1,'0123456789','Mu6KDjYWfb774YadcxGe2yDgWlhwSez0EgPRW5yGsdiD956HMgEPvmt8mczv','2018-12-18 06:53:36','2018-12-18 06:53:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `website_configs`
--

DROP TABLE IF EXISTS `website_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `website_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `website_configs_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `website_configs`
--

LOCK TABLES `website_configs` WRITE;
/*!40000 ALTER TABLE `website_configs` DISABLE KEYS */;
INSERT INTO `website_configs` VALUES (1,'hotline','02142246888','2019-01-03 23:10:37','2019-01-04 18:30:47','Đường dây nóng',NULL),(2,'facebook_url','https://facebook.com','2019-01-03 23:10:37','2019-01-03 23:10:37','Đường dẫn facebook',NULL),(3,'email','Email@example.com','2019-01-03 23:10:37','2019-01-03 23:10:37','Địa chỉ email',NULL),(4,'payment_cash','190 Nhạc Sơn - Kim Tân - TP Lào Cai','2019-01-03 23:10:37','2019-01-03 23:10:37','Thanh toán tiền mặt',NULL),(5,'payment_transfer','Ngân hàng: Vietcombank. Chủ tài khoản: Nguyễn Văn Anh. Thông tin tài khoản: 3215487984 - Chi nhánh Thanh Xuân - Hà Nội','2019-01-03 23:10:37','2019-01-03 23:10:37','Thanh toán chuyển khoản',NULL),(6,'popup_image','1546557078.png','2019-01-03 23:10:37','2019-01-04 00:24:13','Popup Quảng cáo','https://demo.bse-corp.com/blog');
/*!40000 ALTER TABLE `website_configs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-09 16:46:01

--
-- Cấu trúc bảng cho bảng `shipping`
--
DROP TABLE IF EXISTS `shipping`;
CREATE TABLE `shipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `ship_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buy_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'in-china',
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` double(12,2) DEFAULT NULL,
  `price` double(12,2) DEFAULT NULL,
  `cn_transport_fee` double(11,2) DEFAULT NULL,
  `exchange_rates` double(11,2) UNSIGNED DEFAULT NULL,
  `total_price` double(14,2) DEFAULT NULL,
  `admin_note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
