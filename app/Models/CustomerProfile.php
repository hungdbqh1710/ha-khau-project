<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerProfile
 *
 * @package App\Models
 */
class CustomerProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'gender', 'birthday', 'bank_account', 'bank_name', 'bank_account_owner', 'business_sector', 'remaining_amount', 'avatar_url',
    ];

    protected $casts = [
        'remaining_amount' => 'float'
    ];
}
