<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipCodes extends Model
{
    /**
     * @var array
     */
    protected $table = 'shipping';
    protected $fillable = ['ship_code', 'package_code', 'buy_code', 'size', 'weight', 'status', 'price', 'cn_transport_fee', 'exchange_rates', 'total_price', 'admin_note', 'customer_id'];
}