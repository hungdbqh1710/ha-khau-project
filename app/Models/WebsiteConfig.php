<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebsiteConfig extends Model
{
    protected $fillable = ['name', 'content', 'display_name', 'link'];
}
