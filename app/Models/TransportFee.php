<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportFee extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['from_weight', 'to_weight', 'hn_fee', 'hcm_fee'];
}
