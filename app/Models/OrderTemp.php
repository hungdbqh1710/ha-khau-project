<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTemp extends Model
{
    protected $fillable = [
        'product_image',
        'product_link',
        'price',
        'product_name',
        'qty',
        'property',
        'total_price',
        'price_range',
        'item_id',
        'shop_name'
    ];
}
