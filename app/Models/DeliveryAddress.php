<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryAddress
 *
 * @package App\Models
 */
class DeliveryAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'name', 'phone_number', 'city', 'district', 'commune', 'street', 'is_default',
    ];
}
