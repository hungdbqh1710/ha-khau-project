<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WoodFee extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'first_kg_fee', 'next_kg_fee'];
}
