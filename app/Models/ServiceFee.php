<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceFee extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['from_price', 'to_price', 'fee'];
}
