<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    protected $fillable = ['customer_id', 'name', 'phone_number', 'city', 'district', 'commune', 'street'];
}
