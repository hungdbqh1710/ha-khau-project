<?php

namespace App\Models;

use App\Traits\Filter;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TransactionHistory
 * @package App\Models
 */
class TransactionHistory extends Model
{
    use Filter;

    protected $fillable = [
        'customer_id',
        'created_by',
        'amount_before',
        'amount_topup',
        'amount_after'
    ];

    protected $appends = [
        'amount_topup_format',
        'created_at_format'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return string
     */
    public function getAmountTopupFormatAttribute()
    {
        return number_format($this->amount_topup);
    }

    /**
     * @return mixed
     */
    public function getCreatedAtFormatAttribute()
    {
        return $this->created_at->format('d/m/Y h:i:s');
    }
}
