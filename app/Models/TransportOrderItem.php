<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TransportOrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'cn_transport_fee',
        'vn_transport_fee',
        'cn_transport_code',
        'vn_transport_code',
        'customer_note',
        'admin_note',
        'status',
        'cn_order_date',
        'receiver_info',
        'link_qty',
        'via_border_transport_fee',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * @param $value
     * @return string
     */
    public function getCnOrderDateAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }
}
