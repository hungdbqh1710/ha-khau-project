<?php

namespace App\Models;

use App\Notifications\ResetPasswordCustomerNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Customer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordCustomerNotification($token));
    }

    /**
     * Get the customer profile record associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customerProfile()
    {
        return $this->hasOne('App\Models\CustomerProfile');
    }

    /**
     * Get the delivery address record associated with the customer.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveryAddress()
    {
        return $this->hasMany('App\Models\DeliveryAddress');
    }

    /** Get default delivery address
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object
     */
    public function getDefaultDeliveryAddress()
    {
        return $this->deliveryAddress()->where('is_default', STATUS_ACTIVE)->first();
    }

    /**
     * Check customer is active
     * @return bool
     */
    public function isActive()
    {
        return $this->is_active == STATUS_ACTIVE;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    /**
     * @return mixed
     */
    public function totalSpentMoney()
    {
        return $this->orders()->where('status', STATUS_SUCCESS)->sum('final_total_price');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shippingOrders()
    {
        return $this->hasMany('App\Models\ShippingOrder', 'customer_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::created(function($builder) {
            $builder->customerProfile()->create([]);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactionHistories()
    {
        return $this->hasMany(TransactionHistory::class, 'customer_id');
    }
}
