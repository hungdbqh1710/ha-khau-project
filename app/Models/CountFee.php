<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountFee extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['from_product', 'to_product', 'price_greater_10_fee', 'price_less_10_fee'];
}
