<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerLevel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level',
        'spent_money_from',
        'spent_money_to',
        'purchase_discount',
        'transport_discount',
        'deposit'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'spent_money_from'   => 'float',
        'spent_money_to'     => 'float',
        'purchase_discount'  => 'float',
        'transport_discount' => 'float',
        'deposit'            => 'float'
    ];
}
