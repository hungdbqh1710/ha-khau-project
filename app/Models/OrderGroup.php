<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderGroup extends Model
{
    protected $fillable = [
        'order_id',
        'shop_name'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchaseOrderItems()
    {
        return $this->hasMany(PurchaseOrderItem::class, 'order_group_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
