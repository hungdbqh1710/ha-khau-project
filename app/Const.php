<?php

const STATUS_ACTIVE = 1;
const STATUS_INACTIVE = 0;
const STATUS_UNSENT = 1;
const STATUS_PROCESSING = 2;
const STATUS_CONFIRMED = 3;
const STATUS_PRICE_QUOTATION = 4;
const STATUS_ORDERED = 5;
const STATUS_IN_WAREHOUSE = 6;
const STATUS_TRANSPORTING = 7;
const STATUS_SUCCESS = 8;
const STATUS_CANCEL = 9;

const GENDER_FEMALE = 0;
const GENDER_MALE = 1;
const GENDER_OTHER = 2;

const VALUE_TYPE_TRUE = 1;
const VALUE_TYPE_FALSE = 0;

const CREATE_DELIVERY_ADDRESS_MODAL = 'create-delivery-address-modal';
const UPDATE_DELIVERY_ADDRESS_MODAL = 'update-delivery-address-modal';
const ERROR_MODAL = 'error-modal';
const SUCCESS_MODAL = 'success-modal';
const LOGIN_MODAL = 'login-modal';
const REGISTER_MODAL = 'register-modal';
const DETAIL_FEEDBACK_MODAL = 'detail-feedback-modal';
const DELETE_MODAL = 'delete-modal';
const TRANSPORT_ORDER_MODAL = 'transport-order-modal';
const NOTIFICATION_LOGIN_MODAL = 'notification-login-modal';
const DETAIL_LINK_QTY_MODAL = 'detail-link-qty-modal';
const DELIVERY_ADDRESS_MODAL = 'delivery-address-modal';
const PAYMENT_DEBT_MODAL = 'payment-debt-modal';
const UPDATE_TRANSPORT_MODAL = 'update-transport-modal';

const TYPE_TAO_BAO = 'taobao';
const TYPE_1688 = '1688';
const TYPE_TMALL = 'tmall';
const DEFAULT_DEPOSIT = 100;

const STATUS_TRANSPORT_ITEM_NOT_RECEIVE = 1;
const STATUS_TRANSPORT_ITEM_RECEIVED = 2;
const STATUS_TRANSPORT_ITEM_TRANSPORTING = 3;
const STATUS_TRANSPORT_ITEM_STATUS = 4;
const STATUS_TRANSPORT_ITEM_CANCEL = 5;

const STATUS_TRANSPORT_CONFIRMED = 2;
const STATUS_TRANSPORT_TO_TQ = 3;
const STATUS_TRANSPORT_TO_LC = 4;
const STATUS_TRANSPORT_TO_CUSTOMER = 7;
const STATUS_TRANSPORT_SUCCESS = 8;
const STATUS_TRANSPORT_CANCEL = 9;

const STATUS_ORDER_ITEM_RECEIVED = 1;
const STATUS_ORDER_ITEM_RETURNS = 2;

const TYPE_ORDER_PURCHASE = 1;
const TYPE_ORDER_TRANSPORT = 2;