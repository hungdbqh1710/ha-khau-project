<?php

namespace App\Filters;

use App\Helper;
use Carbon\Carbon;

class OrderFilter extends Filters
{
    protected $filters = [
        'status',
        'order_number',
        'transport_code',
        'product_link',
        'item_id',
        'product_name',
        'start_date',
        'end_date'
    ];

    /**
     * @param $status
     * @return mixed
     */
    public function status($status)
    {
        return $this->builder->where('status', $status);
    }

    /**
     * @param $orderNumber
     * @return mixed
     */
    public function orderNumber($orderNumber)
    {
        $orderNumber = Helper::clearPrefixOrder($orderNumber);
        return $this->builder->where('order_number', 'LIKE', '%'.$orderNumber.'%');
    }

    /**
     * @param $transportCode
     * @return mixed
     */
    public function transportCode($transportCode)
    {
        return $this->builder->where('purchase_vn_transport_code', 'LIKE', '%'.$transportCode.'%');
    }

    /**
     * @param $productLink
     * @return mixed
     */
    public function productLink($productLink)
    {
        return $this->builder->whereHas('purchaseOrderItems', function($query) use($productLink){
            $query->where('product_link','LIKE', '%'.$productLink.'%');
        });
    }

    /**
     * @param $productName
     * @return mixed
     */
    public function productName($productName)
    {
        return $this->builder->whereHas('purchaseOrderItems', function($query) use($productName){
            $query->where('product_name','LIKE', '%'.$productName.'%');
        });
    }

    /**
     * @param $startDate
     * @return mixed
     */
    public function startDate($startDate)
    {
        return $this->builder->whereDate('created_at', '>=', Carbon::parse($startDate)->startOfDay());
    }

    /**
     * @param $endDate
     * @return mixed
     */
    public function endDate($endDate)
    {
        return $this->builder->whereDate('created_at', '<=', Carbon::parse($endDate)->startOfDay());
    }

    /**
     * @param $id
     * @return mixed
     */
    public function itemId($id)
    {
        return $this->builder->whereHas('purchaseOrderItems', function ($query) use ($id) {
            $query->where('product_id', 'LIKE', '%' . $id . '%');
        });
    }
}