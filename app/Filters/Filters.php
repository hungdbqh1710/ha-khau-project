<?php

namespace App\Filters;

abstract class Filters
{
    protected $request;
    protected $builder;

    /**
     * Filters constructor.
     */
    public function __construct()
    {
        $this->request = app('request');
    }

    /**
     * @param $builder
     * @return mixed
     */
    public function apply($builder)
    {
        $this->builder = $builder;
        foreach ($this->getFilter() as $filter => $value) {
            if (method_exists($this, camel_case($filter))) {
                $this->{camel_case($filter)}($value);
            }
        }
        return $this->builder;
    }
    /**
     * @return array
     */
    public function getFilter(): array
    {
        return array_filter($this->request->only($this->filters));
    }
}
