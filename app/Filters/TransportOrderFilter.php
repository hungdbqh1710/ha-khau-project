<?php

namespace App\Filters;

use App\Helper;
use Carbon\Carbon;

class TransportOrderFilter extends Filters
{
    protected $filters = [
        'status',
        'order_number',
        'cn_transport_code',
        'start_date',
        'end_date'
    ];

    /**
     * @param $status
     * @return mixed
     */
    public function status($status)
    {
        return $this->builder->where('status', $status);
    }

    /**
     * @param $orderNumber
     * @return mixed
     */
    public function orderNumber($orderNumber)
    {
        $orderNumber = Helper::clearPrefixOrder($orderNumber);
        return $this->builder->where('order_number', 'LIKE', '%'.$orderNumber.'%');
    }

    /**
     * @param $cnTransportCode
     * @return mixed
     */
    public function CnTransportCode($cnTransportCode)
    {
        return $this->builder->whereHas('transportOrderItem', function ($query) use ($cnTransportCode) {
            $query->where('cn_transport_code', 'LIKE', '%' . $cnTransportCode . '%');
        });
    }

    /**
     * @param $startDate
     * @return mixed
     */
    public function startDate($startDate)
    {
        return $this->builder->whereDate('created_at', '>=', Carbon::parse($startDate)->startOfDay());
    }

    /**
     * @param $endDate
     * @return mixed
     */
    public function endDate($endDate)
    {
        return $this->builder->whereDate('created_at', '<=', Carbon::parse($endDate)->startOfDay());
    }
}