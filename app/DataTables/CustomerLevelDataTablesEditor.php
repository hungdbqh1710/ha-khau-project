<?php

namespace App\DataTables;

use App\Models\CustomerLevel;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class CustomerLevelDataTablesEditor extends DataTablesEditor
{
    protected $model = CustomerLevel::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'level'              => 'required|empty_html|unique:customer_levels',
            'spent_money_from'   => 'required_without:spent_money_to|numeric|digits_between:1,12|nullable',
            'spent_money_to'     => 'required_without:spent_money_from|numeric|digits_between:1,12|nullable|greater_than_field:spent_money_from',
            'purchase_discount'  => 'required|numeric|max:100',
            'transport_discount' => 'required|numeric|max:100',
            'deposit'            => 'required|numeric|max:100',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'level'              => 'required|empty_html|' . Rule::unique($model->getTable())->ignore($model->getKey()),
            'spent_money_from'   => 'required_without:spent_money_to|numeric|digits_between:1,12|nullable',
            'spent_money_to'     => 'required_without:spent_money_from|numeric|digits_between:1,12|nullable|greater_than_field:spent_money_from',
            'purchase_discount'  => 'required|numeric|max:100',
            'transport_discount' => 'required|numeric|max:100',
            'deposit'            => 'required|numeric|max:100',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }
}
