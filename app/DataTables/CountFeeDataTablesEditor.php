<?php

namespace App\DataTables;

use App\Models\CountFee;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class CountFeeDataTablesEditor extends DataTablesEditor
{
    protected $model = CountFee::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'from_product'         => 'required_without_all:to_product|numeric|digits_between:1,9|nullable',
            'to_product'           => 'required_without_all:from_product|numeric|digits_between:1,9|nullable|greater_than_field:from_product',
            'price_greater_10_fee' => 'required|digits_between:1,9',
            'price_less_10_fee'    => 'required|digits_between:1,9'
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'from_product'         => 'required_without_all:to_product|numeric|integer|digits_between:1,9|nullable',
            'to_product'           => 'required_without_all:from_product|numeric|integer|digits_between:1,9|nullable|greater_than_field:from_product',
            'price_greater_10_fee' => 'required|digits_between:1,9',
            'price_less_10_fee'    => 'required|digits_between:1,9'
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Get create messages
     * @return array
     */
    public function createMessages()
    {
        return $this->getMessage();
    }

    /**
     * Get edit messages
     * @return array
     */
    public function editMessages()
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    protected function getMessage()
    {
        return [
            'from_product.required_without_all'   => 'Trường từ sản phẩm không được bỏ trống khi trường đến sản phẩm không có giá trị',
            'to_product.required_without_all' => 'Trường đến sản phẩm không được bỏ trống khi trường từ sản phẩm không có giá trị'
        ];
    }
}
