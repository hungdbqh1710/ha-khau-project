<?php

namespace App\DataTables;

use App\Models\Slide;
use App\Rules\PositionSlideRule;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class SlideDataTablesEditor extends DataTablesUploadEditor
{
    protected $model = Slide::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'name'      => 'required|empty_html|max:50',
            'link'      => 'required|url',
            'src'       => 'required',
            'position'  => ['required', 'int', 'min:1', new PositionSlideRule(request()->all())],
            'is_active' => 'in:0,1',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'name'      => 'required|empty_html|max:50',
            'link'      => 'required|url',
            'src'       => 'required',
            'position'  => ['required', 'int', 'min:1', new PositionSlideRule(request()->all())],
            'is_active' => 'in:0,1',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Pre-delete action event hook.
     *
     * @param Model $model
     * @param $data
     * @return mixed
     */
    public function deleting(Model $model, $data)
    {
        Storage::delete('public/slide-images/'.$data['src']);

        return $data;
    }
}
