<?php

namespace App\DataTables;

use App\Models\TransactionHistory;
use App\User;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TranSactionDataTablesEditor
 * @package App\DataTables
 */
class TranSactionDataTablesEditor extends DataTablesEditor
{
    /**
     * @var string
     */
    protected $model = TransactionHistory::class;
    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    protected $customerRepository;

    /**
     * TranSactionDataTablesEditor constructor.
     */
    public function __construct()
    {
        $this->customerRepository = app('CustomerRepositoryInterface');
    }

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'customer_id'  => 'required|exists:customers,id',
            'amount_topup' => 'required|numeric|max:999999999',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'amount_topup' => 'required|numeric|max:999999999',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * @param $model
     * @param $data
     * @return mixed
     */
    public function creating($model, $data)
    {
        $customer = $this->customerRepository->getOneById($data['customer_id']);
        $data['created_by'] = auth()->id();
        $data['amount_before'] = $customer->customerProfile->remaining_amount ?? 0;
        $data['amount_after'] = (float)$data['amount_topup'] + (float)$customer->customerProfile->remaining_amount ?? 0;
        return $data;
    }

    /**
     * @param $model
     * @param $data
     * @return mixed
     */
    public function created($model, $data)
    {
        $customer = $this->customerRepository->getOneById($data['customer_id']);
        $customerProfile = $customer->customerProfile;
        $amountTopup = $customerProfile['remaining_amount'] + $data['amount_topup'];
        $customer->customerProfile()->update([
            'remaining_amount' => $amountTopup
        ]);
        return $data;
    }

    /**
     * @param $model
     * @param $data
     * @return mixed
     */
    public function updating($model, $data)
    {
        $customer = $this->customerRepository->getOneById($model['customer_id']);
        $customerProfile = $customer->customerProfile;
        //tài khoản dư trừ đi số tiền trước đấy, cộng với số tiền update
        $amountTopup = $customerProfile['remaining_amount'] - $model['amount_topup'] + $data['amount_topup'];
        $customer->customerProfile()->update([
            'remaining_amount' => $amountTopup
        ]);
        return $data;
    }
}
