<?php

namespace App\DataTables;

use App\Helper;
use App\Models\Customer;
use Yajra\DataTables\Services\DataTable;

/**
 * Class CustomerDataTable
 *
 * @package App\DataTables
 */
class CustomerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function($query) {
                $url = route('admin.customers.show-transactions', $query->id);
                $title = trans('form.view_transaction_histories');
                return view('frontend.customer.button-list-order', compact('query'))->render() . view('components.buttons.topup')->render() . view('components.buttons.button-detail', compact('url', 'title'))->render();
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Customer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Customer $model)
    {
        $query = $model->newQuery()->select('id', 'name', 'email', 'phone_number', 'is_active', 'created_at', 'updated_at');
        if (request()->filled('status')) {
            $query->where('is_active', request()->status);
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax(route('admin.customers.index'), null, [], ['data' => 'function (d) {d.status = $("select[name=status]").val();}'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['title' => trans('form.title_name')],
            'email' => ['title' => trans('form.email')],
            'phone_number' => ['title' => trans('form.title_phone_number')],
            'is_active' => [
                'title' => trans('form.title_active'),
                'data' => 'is_active',
                'render' => [
                    'renderLabelCustomer(data)'
                ]
            ],
            'created_at' => ['title' => trans('form.title_created_at')],
            'updated_at' => ['title' => trans('form.title_updated_at')],
            'action'       => [
                'defaultContent' => 'null',
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Customer_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => [],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                4 , 'desc'
            ]
        ];
    }
}
