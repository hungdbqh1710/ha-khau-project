<?php

namespace App\DataTables;

use App\Models\Feedback;
use Yajra\DataTables\Services\DataTable;

class FeedbackDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('limit-content', function ($query) {
                return str_limit($query->content, config('data.limit_content_in_admin'));
            })
            ->addColumn('action', function ($query) {
                return view('components.buttons.button-detail', compact('query'))->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Feedback $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Feedback $model)
    {
        return $model->newQuery()->select('id', 'phone_number', 'content', 'created_at', 'updated_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['title' => trans('form.title_action')])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'phone_number'  => ['title' => trans('form.title_phone_number')],
            'content'       => ['title' => trans('form.title_content'), 'data' => 'limit-content', 'name' => 'content', 'width' => '40%'],
            'created_at'    => ['title' => trans('form.title_created_at')],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Feedback_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => [
                'reset',
            ],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                2 , 'desc'
            ]
        ];
    }
}
