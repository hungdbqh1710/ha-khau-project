<?php

namespace App\DataTables;

use App\Models\ShipCodes;
use Yajra\DataTables\Services\DataTable;

class ShipCodeTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
                ->editColumn('order_id', function($query){
                    return $query->order->order_number;
                })
                ->editColumn('property', function($query){
                    return collect(json_decode($query->property,true))->implode('<br>');
                })
                ->addColumn('action', function () {
                    return view('components.buttons.button-edit')->render();
                })
                ->rawColumns(['property', 'product_image', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param OrderItem $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(OrderItem $model)
    {
        return $model->newQuery()
            ->select('order_items.*')
            ->whereOrderId($this->id);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'code' => ['title' => trans('table.order_number')],
            'name' => ['title' => trans('table.customer_name')],
            'weight' => ['title' => trans('table.weight')],
            'status' => [
                'title' => trans('form.title_transport_code')
            ],
            'china_order_code' => [
                'title' => trans('table.status')
            ],
            'updated_at' => ['title' => trans('table.updated_at')],
            'action' => [
                'data'           => 'action',
                'title'          => trans('form.title_action'),
                'name'           => 'action',
                'searchable'     => false,
                'orderable'      => false
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ShipCode_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => [
                [
                    'text' => 'function() { return renderBackButton(); }',
                    'action' => 'function ( e, dt, node, config ) {
                        window.location.href=' . '"' . url()->previous() . '"' . ';
                    }'
                ],
            ],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                12 , 'desc'
            ]
        ];
    }
}
