<?php

namespace App\DataTables;

use App\Models\ServiceFee;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class ServiceFeeDataTablesEditor extends DataTablesEditor
{
    protected $model = ServiceFee::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'from_price' => 'required_without_all:to_price|numeric|digits_between:1,9|nullable',
            'to_price'   => 'required_without_all:from_price|numeric|digits_between:1,9|nullable|greater_than_field:from_price',
            'fee'        => 'required|numeric|max:100',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'from_price' => 'required_without_all:to_price|numeric|numeric|digits_between:1,9|nullable',
            'to_price'   => 'required_without_all:from_price|numeric|digits_between:1,9|nullable|greater_than_field:from_price',
            'fee'        => 'required|numeric|max:100',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Get create messages
     * @return array
     */
    public function createMessages()
    {
        return $this->getMessage();
    }

    /**
     * Get edit messages
     * @return array
     */
    public function editMessages()
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    protected function getMessage()
    {
        return [
            'fee.max' => 'Trường phí tối đa là 100 %',
            'from_price.required_without_all'   => 'Trường từ giá không được bỏ trống khi trường đến giá không có giá trị',
            'to_price.required_without_all' => 'Trường đến giá không được bỏ trống khi trường từ giá không có giá trị'
        ];
    }
}
