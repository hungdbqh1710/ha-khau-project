<?php

namespace App\DataTables;

use App\Models\ServiceFee;
use Yajra\DataTables\Services\DataTable;

class ServiceFeeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param ServiceFee $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ServiceFee $model)
    {
        return $model->newQuery()->select(['service_fees.*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     * @throws \Throwable
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     * @throws \Throwable
     */
    protected function getColumns()
    {
        return [
            'from_price' => [
                'title' => trans('table.from_price'),
                'render' => [
                    'Number(data).format(3)'
                ]
            ],
            'to_price'   => [
                'title' => trans('table.to_price'),
                'render' => [
                    'Number(data).format(3)'
                ]
            ],
            'fee'        => [
                'title' => trans('table.fee'),
                'render' => [
                    'Number(data).format(2) + " %"'
                ]
            ],
            'created_at' => ['title' => trans('table.created_at')],
            'updated_at' => ['title' => trans('table.created_at')],
            'action'       => [
                'defaultContent' => view('components.buttons.button-edit')->render() . view('components.buttons.button-delete'),
                'data'           => 'null',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ServiceFee_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'dom'        => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'order'      => [1, 'asc'],
            'select'     => [
                'style'    => 'os',
                'selector' => 'td:first-child',
            ],
            'buttons'    => [
                [
                    'extend'      => 'create',
                    'editor'      => 'editor',
                    'text' => 'function() { return renderCreateButton(); }',
                    'formButtons' => [
                        [
                            'text'      => trans('form.button_create'),
                            'className' => 'btn-primary',
                            'action'    => 'function() { this.submit(); }'
                        ],
                        [
                            'text'      => trans('buttons.button_close'),
                            'action'    => 'function() { this.close(); }'
                        ]
                    ],

                ],
                'reset',
            ],
            'language'   => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
        ];
    }
}
