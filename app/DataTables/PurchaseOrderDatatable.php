<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Services\DataTable;

/**
 * Class OrderOfCustomerDataTable
 *
 * @package App\DataTables
 */
class PurchaseOrderDatatable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('supporter_name', function ($query) {
                return optional($query->supporter)->name;
            })
            ->addColumn('customer_name', function ($query) {
                return optional($query->customer)->name;
            })
            ->addColumn('details_url', function($query) {
                return route('admin.orders.purchases.detail', $query->id);
            })
            ->addColumn('delivery_address', function($query) {
                $deliveryAddress = $query->orderAdress;
                return $deliveryAddress->name . ' - ' . $deliveryAddress->phone_number . ' - ' . $deliveryAddress->street . ' - ' . $deliveryAddress->commune . ' - ' . $deliveryAddress->district . ' - ' . $deliveryAddress->city;
            })
            ->setRowId('id');

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        $query = $model->newQuery()
            ->where('orders.order_type', TYPE_ORDER_PURCHASE)
            ->where('orders.status', '<>', STATUS_UNSENT)
            ->with(
                [
                    'customer',
                    'supporter',
                    'purchaseOrderItems'
                ]
            )
            ->select('orders.*');
        if (request()->filled('status')) {
            $query->where('status', request()->status);
        }
        if (auth()->user()->hasPermissionTo('view_all_purchase_orders')) {
            return $query;
        }
        return $query->where('supporter_id', auth()->id());
    }
}