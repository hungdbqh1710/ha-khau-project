<?php

namespace App\DataTables;

use App\Models\Order;
use App\Notifications\PriceQuotation;
use App\Rules\DepositedRule;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDataTablesEditor extends DataTablesEditor
{
    protected $model = Order::class;
    protected $oldStatus;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'supporter_id'               => 'required|exists:users,id',
            'status'                     => 'required|in:1,2,4,5,6,8,9',
            'purchase_service_fee'       => 'numeric|max:999999999|nullable',
            'purchase_cn_transport_fee'  => 'numeric|max:999999999|nullable',
            'purchase_vn_transport_fee'  => 'numeric|max:999999999|nullable',
            'admin_note'                 => 'empty_html',
            'purchase_vn_transport_code' => 'empty_html',
            'purchase_cn_to_vn_fee'      => 'numeric|max:999999999|nullable',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    public function updating(Model $model, $data)
    {
        $this->oldStatus = $model->status;
        return $data;
    }
    /**
     * @param Model $model
     */
    public function updated(Model $model)
    {
        if ($model->status == STATUS_PRICE_QUOTATION && $this->oldStatus != STATUS_PRICE_QUOTATION) {
            $model->customer->notify(new PriceQuotation($model));
        }
        if ($model->status == STATUS_ORDERED && $this->oldStatus != STATUS_ORDERED) {
            $model->confirm_date = now();
        }
        $model->final_total_price = $model->purchase_total_items_price + $model->purchase_service_fee + $model->purchase_cn_transport_fee + $model->purchase_vn_transport_fee + $model->purchase_cn_to_vn_fee;
        $model->save();
    }
}
