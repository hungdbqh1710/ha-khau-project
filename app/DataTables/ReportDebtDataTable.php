<?php

namespace App\DataTables;

use App\Models\Customer;
use App\Models\Order;
use App\Models\TransactionHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Services\DataTable;

class ReportDebtDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Customer $customer
     * @param Request  $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Customer $customer, Request $request)
    {
        $query = $customer->newQuery()
            ->select('customers.*')
            ->withCount(['orders as total_order' => function ($query) use($request){
                $query->select(\DB::raw("SUM(final_total_price)"))->where('status', '>=', STATUS_TRANSPORT_CONFIRMED)->where('status', '<', STATUS_CANCEL);
                if ($request->filled('single_date')) {
                    $month = explode('/', request()->single_date)[0];
                    $year =  explode('/', request()->single_date)[1];
                    $query->whereMonth('created_at' , $month);
                    $query->whereYear('created_at', $year);
                }
                if ($request->filled('date_range')) {
                    $dataRange = explode('-', request()->date_range);
                    $query->whereDate('created_at', '>=', Carbon::parse($dataRange[0])->startOfDay());
                    $query->whereDate('created_at', '<=', Carbon::parse($dataRange[1])->endOfDay());
                }
            }])
            ->withCount(['transactionHistories as total_transaction' => function ($query) use($request){
                $query->select(\DB::raw("SUM(amount_topup)"));
                if ($request->filled('single_date')) {
                    $month = explode('/', request()->single_date)[0];
                    $year =  explode('/', request()->single_date)[1];
                    $query->whereMonth('created_at' , $month);
                    $query->whereYear('created_at', $year);
                }
                if ($request->filled('date_range')) {
                    $dataRange = explode('-', request()->date_range);
                    $query->whereDate('created_at', '>=', Carbon::parse($dataRange[0])->startOfDay());
                    $query->whereDate('created_at', '<=', Carbon::parse($dataRange[1])->endOfDay());
                }
            }]);

        if (request()->filled('customer_id')) {
            $query->where('id', request()->customer_id);
        }
        return $query;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ReportDebt_' . date('YmdHis');
    }
}
