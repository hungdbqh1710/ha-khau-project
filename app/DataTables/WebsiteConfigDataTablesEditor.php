<?php

namespace App\DataTables;

use App\Models\WebsiteConfig;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class WebsiteConfigDataTablesEditor extends DataTablesUploadEditor
{
    protected $model = WebsiteConfig::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        $rule = [];
        $input = collect(request('data'))->first();
        if ($input['name'] == 'hotline') {
            $rule['content'] = 'required|min:8|max:15';
        }
        if ($input['name'] == 'email' || $input['name'] == 'facebook_url') {
            $rule['content'] = 'required|max:255';
        }
        if ($input['name'] == 'payment_cash' || $input['name'] == 'payment_transfer') {
            $rule['content'] = 'required';
        }
        return $rule;
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Pre-delete action event hook.
     *
     * @param Model $model
     * @param $data
     * @return mixed
     */
    public function deleting(Model $model, $data)
    {
        Storage::delete('public/slide-images/'.$data['src']);

        return $data;
    }
}
