<?php

namespace App\DataTables;

use App\Models\Order;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class ReportRevenueDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('customer_name', function ($query) {
                return optional($query->customer)->name;
            })
            ->editColumn('final_total_price', function ($query) {
                return number_format($query->final_total_price);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param $order
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $order)
    {
        $orders = $order->newQuery()->select('id', 'supporter_id', 'customer_id', 'order_number', 'status', 'final_total_price', 'order_type', 'updated_at')->where('status', STATUS_SUCCESS);

        if ($this->single_date) {
            $month = explode('/', $this->single_date)[0];
            $year = explode('/', $this->single_date)[1];
            $orders = $orders->whereMonth('updated_at', $month)->whereYear('updated_at', $year);
        }

        if ($this->date_range) {
            $startDate = explode('-', $this->date_range)[0];
            $endDate = explode('-', $this->date_range)[1];
            $orders = $orders->whereBetween('updated_at', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()]);
        }

        if ($this->supporter_id) {
            $orders = $orders->where('supporter_id', $this->supporter_id);
        }

        return $orders;
    }
}
