<?php

namespace App\DataTables;

use App\Models\Role;
use Yajra\DataTables\Services\DataTable;

class RoleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($query) {
                $actionHtml = '';
                if (auth()->user()->can('edit_roles')) {
                    $url = route('admin.roles.edit', $query->id);
                    $actionHtml .= view('components.buttons.button-edit', compact('url'))->render();
                }
                if (auth()->user()->can('delete_roles')) {
                    $actionHtml .= view('components.buttons.button-delete')->render();
                }
                return $actionHtml;
            })
            ->rawColumns(['action']);;
    }

    /**
     * Get query source of dataTable.
     *
     * @param Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Role $model)
    {
        return $model->newQuery()
            ->with('permissions')
            ->selectRaw('distinct roles.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'permissions_name' => [
                'title'          => trans('form.title_permission'),
                "className"      => 'details-control',
                "orderable"      => false,
                "searchable"     => false,
                "data"           => null,
                "defaultContent" => 'Click để xem',
                "width"          => '20%'
            ],
            'name' => ['visible' => false],
            'display_name' => ['title' => trans('table.role_display_name')],
            'created_at' => ['title' => trans('table.created_at')],
            'updated_at' => ['title' => trans('table.updated_at')],
            'action'       => [
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Role_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $buttons = [];
        if (auth()->user()->can('create_roles')) {
            $buttons[] = 'createRole';
        }
        $buttons[] = 'reset';
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'    => [
                'createRole',
                'reset',
            ],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                4 , 'desc'
            ]
        ];
    }
}
