<?php

namespace App\DataTables;

use App\Models\WoodFee;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class WoodFeeDataTablesEditor extends DataTablesEditor
{
    protected $model = WoodFee::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'first_kg_fee' => 'required|numeric|digits_between:1,9',
            'next_kg_fee'  => 'required|numeric|digits_between:1,9',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'first_kg_fee' => 'required|numeric|digits_between:1,9',
            'next_kg_fee'  => 'required|numeric|digits_between:1,9',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }
}
