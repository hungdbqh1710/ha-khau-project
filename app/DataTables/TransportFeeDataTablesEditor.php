<?php

namespace App\DataTables;

use App\Models\TransportFee;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class TransportFeeDataTablesEditor extends DataTablesEditor
{
    protected $model = TransportFee::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'from_weight' => 'required_without_all:to_weight|numeric|digits_between:1,9|nullable',
            'to_weight'   => 'required_without_all:from_weight|numeric|digits_between:1,9|nullable|greater_than_field:from_weight',
            'hn_fee'      => 'required|numeric|digits_between:1,8',
            'hcm_fee'     => 'required|numeric|digits_between:1,8',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'from_weight' => 'required_without_all:to_weight|numeric|digits_between:1,9|nullable',
            'to_weight'   => 'required_without_all:from_weight|numeric|digits_between:1,9|nullable|greater_than_field:from_weight',
            'hn_fee'      => 'required|numeric|digits_between:1,8',
            'hcm_fee'     => 'required|numeric|digits_between:1,8',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Get create messages
     * @return array
     */
    public function createMessages()
    {
        return $this->getMessage();
    }

    /**
     * Get edit messages
     * @return array
     */
    public function editMessages()
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    protected function getMessage()
    {
        return [
            'from_weight.required_without_all'   => 'Trường từ cân không được bỏ trống khi trường đến cân không có giá trị',
            'to_weight.required_without_all' => 'Trường đến cân không được bỏ trống khi trường từ cân không có giá trị'
        ];
    }
}
