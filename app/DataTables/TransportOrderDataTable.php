<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Services\DataTable;

class TransportOrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('supporter_name', function ($query) {
                return optional($query->supporter)->name;
            })
            ->addColumn('customer_name', function ($query) {
                return optional($query->customer)->name;
            })
            ->addColumn('details_url', function($query) {
                return route('admin.transport-orders.detail', $query->id);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        $query =  $model
            ->newQuery()
            ->where('order_type', TYPE_ORDER_TRANSPORT)
            ->with('customer')
            ->with('supporter')
            ->with('transportOrderItem')
            ->select('orders.*');
        if (request()->filled('status')) {
            $query->where('status', request()->status);
        }
        if (auth()->user()->hasPermissionTo('view_all_transport_orders')) {
            return $query;
        }
        return $query->where('supporter_id', auth()->id());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'TransportOrder_' . date('YmdHis');
    }
}
