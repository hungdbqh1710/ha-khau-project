<?php

namespace App\DataTables;

use App\Helper;
use App\Models\User;
use Yajra\DataTables\Services\DataTable;

class UserDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('roles_name', function ($query) {
                return $query->roles->map(function ($roles) {
                    return $roles->display_name;
                })->implode(', ');
            })
            ->addColumn('action', function ($query) {
                $actionHtml = '';
                if (auth()->user()->can('edit_users')) {
                    $actionHtml .= view('components.buttons.button-edit')->render();
                }
                if ($query->id != auth()->id() && auth()->user()->can('delete_users')) {
                    $actionHtml .= view('components.buttons.button-delete')->render();
                }
                return $actionHtml;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $query =  $model->newQuery()
            ->with('roles')
            ->select('id', 'name', 'email', 'phone_number', 'is_active', 'created_at', 'updated_at');
        if (request()->filled('status')) {
            $query->where('is_active', request()->status);
        }
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     * @throws \Throwable
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax(route('admin.users.index'), null, [], ['data' => 'function (d) {d.status = $("select[name=status]").val();}'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     * @throws \Throwable
     */
    protected function getColumns()
    {
        return [
            'name'         => ['title' => trans('table.name')],
            'email'        => ['title' => trans('table.email')],
            'is_active'    => [
                'title'  => trans('table.status'),
                'data'   => 'is_active',
                'render' => [
                    'renderLabel(data)'
                ]
            ],
            'created_at'   => ['title' => trans('table.created_at')],
            'updated_at'   => ['title' => trans('table.updated_at')],
            'phone_number' => ['title' => trans('table.phone_number')],
            'roles'        => ['title' => trans('table.role'), 'data' => 'roles_name', 'name' => 'roles.name', 'orderable' => false],
            'action'       => [
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'dom'        => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                            "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                            "<'row'<'col-sm-12'tr>>" .
                            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'order'      => [3, 'desc'],
            'select'     => [
                'style'    => 'os',
                'selector' => 'td:first-child',
            ],
            'buttons'    => [],
            'language'   => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
        ];
    }
}
