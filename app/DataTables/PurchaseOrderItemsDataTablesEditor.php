<?php

namespace App\DataTables;

use App\Models\PurchaseOrderItem;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderItemsDataTablesEditor extends DataTablesEditor
{
    protected $model = PurchaseOrderItem::class;
    protected $oldStatus;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'cn_transport_code'   => 'empty_html|nullable|max:30',
            'cn_order_number' => 'empty_html|nullable|max:30',
            'admin_note'      => 'empty_html'
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * @param Model $model
     * @param       $data
     * @return mixed
     */
    public function updating(Model $model, $data)
    {
        $this->oldStatus = $model->status;
        if ($this->oldStatus == STATUS_ORDER_ITEM_RETURNS && isset($data['status'])) {
            unset($data['status']);
        }
        return $data;
    }
}
