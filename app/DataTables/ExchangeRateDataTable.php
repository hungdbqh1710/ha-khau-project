<?php

namespace App\DataTables;

use App\Helper;
use App\Models\ExchangeRate;
use Yajra\DataTables\Services\DataTable;

/**
 * Class ExchangeRateDataTable
 *
 * @package App\DataTables
 */
class ExchangeRateDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('is_active', 'components.labels.is_active')
            ->editColumn('vnd', function($query) {
                return number_format($query->vnd, config('data.number_decimals'));
            })
            ->editColumn('ndt', function($query) {
                return number_format($query->ndt, config('data.number_decimals'));
            })
            ->addColumn('action', function ($query) {
                $actionHtml = '';
                if (auth()->user()->can('delete_exchange_rates')) {
                    $actionHtml .= view('components.buttons.button-delete')->render();
                }
                return $actionHtml;
            })
            ->rawColumns(['is_active', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ExchangeRate $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ExchangeRate $model)
    {
        return $model->newQuery()->select('id', 'vnd', 'ndt', 'is_active', 'created_at', 'updated_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'vnd'        => ['title' => trans('form.title_vnd')],
            'ndt'        => ['title' => trans('form.title_ndt')],
            'is_active'  => ['title' => trans('form.title_active')],
            'created_at' => ['title' => trans('form.title_created_at')],
            'updated_at' => ['title' => trans('form.title_updated_at')],
            'action'     => [
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ExchangeRate_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $buttons = Helper::generateDatatableButton('create_exchange_rates');
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => $buttons,
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                3 , 'desc'
            ]
        ];
    }
}
