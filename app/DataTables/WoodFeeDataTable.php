<?php

namespace App\DataTables;

use App\Models\WoodFee;
use Yajra\DataTables\Services\DataTable;

class WoodFeeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param WoodFee $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(WoodFee $model)
    {
        return $model->newQuery()->select(['wood_fees.*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     * @throws \Throwable
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     * @throws \Throwable
     */
    protected function getColumns()
    {
        return [
            'name'         => ['title' => trans('table.name')],
            'first_kg_fee' => [
                'title'  => trans('table.first_kg_fee'),
                'render' => [
                    'Number(data).format(3)'
                ]
            ],
            'next_kg_fee'  => [
                'title'  => trans('table.next_kg_fee'),
                'render' => [
                    'Number(data).format(3)'
                ]
            ],
            'created_at'   => ['title' => trans('table.created_at')],
            'updated_at'   => ['title' => trans('table.updated_at')],
            'action'       => [
                'defaultContent' => view('components.buttons.button-edit')->render() . view('components.buttons.button-delete'),
                'data'           => 'null',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'WoodFee_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'dom'        => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>" .
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'order'      => [1, 'asc'],
            'select'     => [
                'style'    => 'os',
                'selector' => 'td:first-child',
            ],
            'buttons'    => [
                'reset',
            ],
            'language'   => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
        ];
    }
}
