<?php

namespace App\DataTables;

use App\Models\Blog;
use Yajra\DataTables\Services\DataTable;

class BlogDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('creator', function ($query) {
                return $query->creator->name;
            })
            ->editColumn('content', function ($query) {
                return strip_tags(str_limit($query->content, config('data.limit_content_in_admin')));
            })
            ->editColumn('title', function ($query) {
                return strip_tags(str_limit($query->title, config('data.limit_content_in_admin')));
            })
            ->addColumn('action', function ($query) {
                $actionHtml = '';
                if (auth()->user()->can('edit_blogs')) {
                    $url = route('admin.blogs.edit', $query->id);
                    $actionHtml .= view('components.buttons.button-edit', compact('url'))->render();
                }
                if (auth()->user()->can('delete_blogs')) {
                    $actionHtml .= view('components.buttons.button-delete')->render();
                }
                return $actionHtml;
            })
            ->rawColumns(['content', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Blog $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Blog $model)
    {
        return $model
            ->newQuery()
            ->select(
                'id',
                'category_id',
                'title',
                'content',
                'image',
                'created_by',
                'slug',
                'created_at',
                'updated_at'
            )->with('creator');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title'      => ['title' => trans('form.title'), 'width' => '25%'],
            'content'    => ['title' => trans('form.title_content'), 'width' => '25%', 'searchable' => false],
            'image'    => [
                'title'  => trans('form.title_src'),
                'data'   => 'image',
                'render' => [
                    'renderBlogImage(data)'
                ],
                'max-width' => '12%',
            ],
            'created_by' => ['title' => trans('form.title_created_by'), 'data' => 'creator', 'name' => 'creator.name'],
            'created_at' => ['title' => trans('form.title_created_at')],
            'action'       => [
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Blog_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $buttons = [];
        if (auth()->user()->can('create_blogs')) {
            $buttons[] = 'createCustom';
        }
        $buttons[] = 'reset';
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'      => $buttons,
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                4 , 'desc'
            ]
        ];
    }
}
