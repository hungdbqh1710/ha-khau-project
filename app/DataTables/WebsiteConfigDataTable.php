<?php

namespace App\DataTables;

use App\Models\WebsiteConfig;
use Yajra\DataTables\Services\DataTable;

class WebsiteConfigDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($query) {
                $actionHtml = '';
                if (auth()->user()->can('edit_website_configs')) {
                    $actionHtml .= view('components.buttons.button-edit')->render();
                }
                return $actionHtml;
            })
            ->addColumn('strip_content', function ($query) {
                return str_limit(strip_tags($query->content), config('data.limit_content'));
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param WebsiteConfig $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(WebsiteConfig $model)
    {
        return $model->newQuery()->select(['*']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     * @throws \Throwable
     */
    protected function getColumns()
    {
        return [
            'display_name' => ['title' => trans('table.config_name') ],
            'content' => [
                'title' => trans('table.config_content'),
                'data' => 'strip_content',
                'render' => [
                    'renderSlideImage(data,type,full,meta)'
                ],
            ],
            'created_at' => ['title' => trans('table.created_at')],
            'updated_at' => ['title' => trans('table.updated_at')],
            'action'       => [
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'WebsiteConfig_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'order'      => [3, 'desc'],
            'select'     => [
                'style'    => 'os',
                'selector' => 'td:first-child',
            ],
            'buttons'    => [
                'reset',
            ],
            'language'   => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
        ];
    }
}
