<?php

namespace App\DataTables;

use App\Models\Order;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class TransportOrderDataTablesEditor extends DataTablesEditor
{
    protected $model = Order::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'supporter_id'             => 'required',
            'status'                   => 'required',
            'admin_note'               => 'nullable|max:255',
            'cn_transport_code'        => 'nullable|max:50',
            'vn_transport_code'        => 'nullable|max:50',
            'cn_transport_fee'         => 'nullable|numeric|min:0|max:999999999',
            'vn_transport_fee'         => 'nullable|numeric|min:0|max:999999999',
            'via_border_transport_fee' => 'nullable|numeric|min:0|max:999999999',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Updated
     *
     * @param Model $model
     * @param $data
     * @return array
     */
    public function updated($model, $data)
    {
        $model->final_total_price = $data['cn_transport_fee'] + $data['vn_transport_fee'] + $data['via_border_transport_fee'];
        $model->save();

        return $data;
    }
}
