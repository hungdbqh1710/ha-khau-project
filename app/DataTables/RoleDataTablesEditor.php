<?php

namespace App\DataTables;

use App\Models\Role;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class RoleDataTablesEditor extends DataTablesEditor
{
    protected $model = Role::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'display_name'  => 'required|max:255|unique:permissions',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'display_name' => 'required|max:255|' . Rule::unique($model->getTable())->ignore($model->getKey()),
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * @param Model $model
     * @param       $data
     */
    public function created(Model $model, $data)
    {
        if(!empty($data['permissions'])) {
            $model->givePermissionTo($data['permissions']);
        }
    }

    public function updated(Model $model, $data)
    {
        if(empty($data['permissions'])) {
            $model->syncPermissions([]);
        } else {
            $model->syncPermissions($data['permissions']);
        }
    }
}
