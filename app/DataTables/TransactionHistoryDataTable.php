<?php

namespace App\DataTables;

use App\Models\TransactionHistory;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class TransactionHistoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('time-remaining', function ($query) {
                $startDay = Carbon::today();
                $tomorrow = Carbon::tomorrow();
                $now = Carbon::now();
                if ($startDay <= $query->created_at) {
                    return strtotime($tomorrow) - strtotime($now);
                }
                return "";
            })
            ->addColumn('action', function ($query) {
                $startDay = Carbon::today();
                if ($startDay <= $query->created_at) {
                    return view('components.buttons.button-edit')->render();
                }
                return "";
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param TransactionHistory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TransactionHistory $model)
    {
        return $model->newQuery()->select(['*'])->where('customer_id', $this->id)->with('customer:id,name', 'user:id,name');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'customer_id' => [
                'title' => trans('form.title_customer_name'),
                'render' => [
                    'full && full.customer.name'
                ],
                'name' => 'customer.name'
            ],
            'amount_topup' => [
                'title' => trans('form.title_amount_topup'),
                'render' => [
                    'Number(data).format()'
                ]
            ],
            'created_by' => [
                'title' => trans('form.title_created_by'),
                'render' => [
                    'full && full.user.name'
                ],
                'name' => 'user.name'
            ],
            'created_at' => ['title' => trans('form.title_topup_date')],
            'updated_at' => ['title' => trans('form.title_updated_at')],
            'time-remaining'    => [
                'title'  => trans('form.title_time_remaining'),
                'data'   => 'time-remaining',
                'render' => [
                    'renderTimeRemaining(data)'
                ],
                'className'      => 'time-remaining'
            ],
            'action'       => [
                'defaultContent' => 'null',
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'TransactionHistory_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => [
                [
                    'text' => 'function() { return renderBackButton(); }',
                    'action' => 'function ( e, dt, node, config ) {
                        window.location.href=' . '"' . url()->previous() . '"' . ';
                    }'
                ],
            ],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                4, 'desc'
            ]
        ];
    }
}
