<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Services\DataTable;

/**
 * Class OrderOfCustomerDataTable
 *
 * @package App\DataTables
 */
class OrderOfCustomerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('supporter_name', function($query) {
                if (isset($query->supporter->name)) {
                    return $query->supporter->name;
                }
            })
            ->editColumn('final_total_price', function($query) {
                if (!empty($query->final_total_price)) {
                    return number_format($query->final_total_price);
                }
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery()->select(['*'])->where('customer_id', $this->id)->with('supporter');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'order_number'      => ['title' => trans('form.title_order_number')],
            'supporter_id'      => [
                'title' => trans('form.title_caring_staff'),
                'data'  => 'supporter_name',
                'name'  => 'supporter.name'
            ],
            'order_type'        => [
                'title'  => trans('form.title_order_type'),
                'data'   => 'order_type',
                'render' => [
                    'renderLabelOrderType(data)'
                ]
            ],
            'status'            => [
                'title'  => trans('form.title_status'),
                'data'   => 'status',
                'render' => [
                    'renderLabelOrder(data)'
                ]
            ],
            'final_total_price' => ['title' => trans('form.title_final_total_price')],
            'created_at'        => ['title' => trans('form.title_created_at')],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'OrderOfCustomer_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => [
                [
                    'text' => 'function() { return renderBackButton(); }',
                    'action' => 'function ( e, dt, node, config ) {
                        window.location.href=' . '"' . url()->previous() . '"' . ';
                    }'
                ],
            ],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                5, 'desc'
            ]
        ];
    }
}
