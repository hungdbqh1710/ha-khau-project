<?php

namespace App\DataTables;

use App\Models\Customer;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomerDataTableEditor
 *
 * @package App\DataTables
 */
class CustomerDataTableEditor extends DataTablesEditor
{
    protected $model = Customer::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'name'          => 'required|max:30|empty_html',
            'email'         => 'required|email|unique:customers',
            'is_active'     => 'in:0,1',
            'phone_number'  => 'required|min:10|max:15|regex:/^0([0-9\-\+\(\)]*)$/|unique:customers',
            // 'password'      => 'required|min:8|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z\d]).\S*$/'
            'password'      => 'required|min:8'
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'is_active' => 'in:0,1',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Pre-create action event hook.
     *
     * @param Model $model
     * @return array
     */
    public function creating(Model $model, array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return $data;
    }

    /**
     * Pre-update action event hook.
     *
     * @param Model $model
     * @return array
     */
    public function updating(Model $model, array $data)
    {
        $data['password'] = $model->password;

        return $data;
    }
}
