<?php

namespace App\DataTables;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class UsersDataTablesEditor extends DataTablesEditor
{
    protected $model = User::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'email' => 'required|email|unique:users|empty_html',
            'name'  => 'required|max:30|empty_html',
            'is_active' => 'required|in:0,1',
            'phone_number' => 'required|min:10|max:15|regex:/^([0-9\ x20\-\+\(\)]*)$/|unique:users',
            'password'  => 'required|password',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'email' => 'required|email|empty_html|' . Rule::unique($model->getTable())->ignore($model->getKey()),
            'name'  => 'required|max:30|empty_html',
            'is_active' => 'required|in:0,1',
            'phone_number' => 'required|min:10|max:15|regex:/^([0-9\ x20\-\+\(\)]*)$/|' . Rule::unique($model->getTable())->ignore($model->getKey()),
            'password'  => 'sometimes|password'
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
    * Pre-create action event hook.
    *
    * @param Model $model
    * @return array
    */
    public function creating(Model $model, array $data)
    {
        $data['password'] = bcrypt($data['password']);
        return $data;
    }

    /**
    * Pre-update action event hook.
    *
    * @param Model $model
    * @return array
    */
    public function updating(Model $model, array $data)
    {
        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = bcrypt($data['password']);
        }
        return $data;
    }

    /**
     * @param Model $model
     * @param array $data
     */
    public function created(Model $model, array $data) {
        if(!empty($data['roles'])) {
            $model->assignRole($data['roles']);
        }
    }

    /**
     * @param Model $model
     * @param array $data
     */
    public function updated(Model $model, array $data) {
        if(empty($data['roles'])) {
            $model->syncRoles([]);
        } else {
            $model->syncRoles($data['roles']);
        }
    }
}
