<?php

namespace App\DataTables;

use App\Helper;
use App\Models\Slide;
use App\User;
use Yajra\DataTables\Services\DataTable;

class SlideDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($query) {
                $actionHtml = '';
                if (auth()->user()->can('edit_slides')) {
                    $actionHtml .= view('components.buttons.button-edit')->render();
                }
                if (auth()->user()->can('delete_slides')) {
                    $actionHtml .= view('components.buttons.button-delete')->render();
                }
                return $actionHtml;
            });;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Slide $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Slide $model)
    {
        return $model->newQuery()->select('id', 'name', 'link', 'src', 'position', 'is_active', 'created_at', 'updated_at')->where('group', $this->group);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['title' => trans('form.title_name_src')],
            'link'    => [
                'title'  => trans('form.title_link'),
                'data'   => 'link',
                'render' => [
                    'renderSlideLink(data)'
                ]
            ],
            'src'    => [
                'title'  => trans('form.title_src'),
                'data'   => 'src',
                'render' => [
                    'renderSlideImage(data)'
                ],
                'max-width' => '15%',
            ],
            'position' => ['title' => trans('form.title_position')],
            'is_active'    => [
                'title'  => trans('form.title_active'),
                'data'   => 'is_active',
                'render' => [
                    'renderLabel(data)'
                ]
            ],
            'created_at' => ['title' => trans('form.title_created_at')],
            'action'       => [
                'defaultContent' => 'null',
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Slide_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $buttons = Helper::generateDatatableButton('create_slides');
        return [
            'responsive' => true,
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'order'      => [5, 'desc'],
            'select'     => [
                'style'    => 'os',
                'selector' => 'td:first-child',
            ],
            'buttons'    => $buttons,
            'language'   => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
        ];
    }
}
