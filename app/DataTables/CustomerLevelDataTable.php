<?php

namespace App\DataTables;

use App\Models\CustomerLevel;
use Yajra\DataTables\Services\DataTable;

class CustomerLevelDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\CustomerLevel $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CustomerLevel $model)
    {
        return $model
            ->newQuery()
            ->select(
                'id',
                'level',
                'spent_money_from',
                'spent_money_to',
                'purchase_discount',
                'transport_discount',
                'deposit',
                'created_at',
                'updated_at'
            );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'level'              => ['title' => trans('form.title_level')],
            'spent_money_from'   => [
                'title'  => trans('form.title_spent_money_from'),
                'render' => [
                    'Number(data).format(3)'
                ]
            ],
            'spent_money_to'     => [
                'title'  => trans('form.title_spent_money_to'),
                'render' => [
                    'Number(data).format(3)'
                ]
            ],
            'purchase_discount'  => [
                'title'  => trans('form.title_purchase_discount'),
                'render' => [
                    'Number(data).format(2) + " %"'
                ]
            ],
            'transport_discount' => [
                'title'  => trans('form.title_transport_discount'),
                'render' => [
                    'Number(data).format(2) + " %"'
                ]
            ],
            'deposit'            => [
                'title'  => trans('form.title_deposit'),
                'render' => [
                    'Number(data).format(2) + " %"'
                ]
            ],
            'created_at'         => ['title' => trans('form.title_created_at')],
            'action'       => [
                'defaultContent' => view('components.buttons.button-edit')->render() . view('components.buttons.button-delete'),
                'data'           => 'null',
                'name'           => 'action',
                'title'          => trans('form.title_action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CustomerLevel_' . date('YmdHis');
    }

    /**
     * Get default builder parameters.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => "<'row btn-table '<'col-sm-6'><'col-sm-6 dataTables_filter'B>>".
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" .
                "<'row'<'col-sm-12'tr>>" .
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            'buttons'  => [
                [
                    'extend' => 'create',
                    'editor' => 'editor',
                    'text' => 'function() { return renderCreateButton(); }',
                    'formButtons' => [
                        [
                            'text'      => trans('buttons.button_add'),
                            'className' => 'btn-primary',
                            'action'    => 'function() { this.submit(); }',
                        ],
                        [
                            'text'      => trans('buttons.button_close'),
                            'className' => 'btn-default',
                            'action'    => 'function() { this.close(); }',
                        ],
                    ],
                ],
                'reset',
            ],
            'language' => [
                'url' => asset('vendor/datatables/languages/Vietnamese.json')
            ],
            'order'    => [
                2 , 'desc'
            ]
        ];
    }
}
