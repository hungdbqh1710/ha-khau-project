<?php

namespace App\DataTables;

use App\Models\TransportOrderItem;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

class TransportOrderItemDataTablesEditor extends DataTablesEditor
{
    protected $model = TransportOrderItem::class;

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [
            'status'            => 'required',
            'cn_transport_code' => 'max:50',
            'vn_transport_code' => 'max:50',
            'cn_transport_fee'  => 'nullable|numeric|min:0|max:999999999',
            'vn_transport_fee'  => 'nullable|numeric|min:1|max:999999999',
            'admin_note'        => 'nullable|max:255',
        ];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Updated
     *
     * @param $model
     * @param $data
     * @return mixed
     */
    public function updated($model, $data)
    {
        $order = $model->order;
        $itemsCount = $model->order->transportOrderItem->count();
        $itemsCancelCount = $model->order->transportOrderItem->where('status', STATUS_TRANSPORT_ITEM_CANCEL)->count();
        if ($itemsCount == $itemsCancelCount) {
            $order->status = STATUS_CANCEL;
        }
        $itemsNotCancel = $order->transportOrderItem->where('status', '<>', STATUS_TRANSPORT_ITEM_CANCEL);
        $order->final_total_price = $itemsNotCancel->pluck('cn_transport_fee')->sum() + $itemsNotCancel->pluck('vn_transport_fee')->sum() + $itemsNotCancel->pluck('via_border_transport_fee')->sum();
        $order->save();

        return $data;
    }
}
