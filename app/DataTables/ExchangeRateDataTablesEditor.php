<?php

namespace App\DataTables;

use App\Models\ExchangeRate;
use Yajra\DataTables\DataTablesEditor;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ExchangeRateDataTablesEditor
 *
 * @package App\DataTables
 */
class ExchangeRateDataTablesEditor extends DataTablesEditor
{
    protected $model = ExchangeRate::class;

    protected $exchangeRateRepository;

    /**
     * ExchangeRateDataTablesEditor constructor.
     */
    public function __construct()
    {
        $this->exchangeRateRepository = app('ExchangeRateRepositoryInterface');
    }

    /**
     * Get create action validation rules.
     *
     * @return array
     */
    public function createRules()
    {
        return [
            'vnd' => 'required|numeric|min:0|max:999999999',
            'ndt' => 'required|numeric|min:0|max:999999999',
        ];
    }

    /**
     * Get edit action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function editRules(Model $model)
    {
        return [];
    }

    /**
     * Get remove action validation rules.
     *
     * @param Model $model
     * @return array
     */
    public function removeRules(Model $model)
    {
        return [];
    }

    /**
     * Deactivate the exchange rate
     *
     * @param Model $model
     * @param array $data
     * @return array
     */
    public function creating(Model $model, array $data)
    {
        $activeExchangeRate = $this->exchangeRateRepository->findByField('is_active', STATUS_ACTIVE)->first();

        if (empty($activeExchangeRate)) {
            $data['is_active'] = STATUS_ACTIVE;
        } else if (!empty($activeExchangeRate) && $data['is_active'] == STATUS_ACTIVE) {
            $activeExchangeRate->is_active = STATUS_INACTIVE;
            $activeExchangeRate->save();
        }

        return $data;
    }

    /**
     * Deactivate the exchange rate
     *
     * @param Model $model
     * @param array $data
     * @return array
     */
    public function updated(Model $model, array $data)
    {
        $activeExchangeRate = $this->exchangeRateRepository->findByField('is_active', STATUS_ACTIVE)->where('id', '<>', $model->id)->first();

        if (!empty($activeExchangeRate) && $data['is_active'] == STATUS_ACTIVE) {
            $activeExchangeRate->is_active = STATUS_INACTIVE;
            $activeExchangeRate->save();
        }

        return $data;
    }

    /**
     * Active the exchange rate
     *
     * @param Model $model
     * @param array $data
     * @return array
     */
    public function deleted(Model $model, array $data)
    {
        if ($model->is_active == STATUS_ACTIVE) {
            $lastExchangeRate = $this->exchangeRateRepository->getAll()->last();
            $lastExchangeRate->is_active = STATUS_ACTIVE;
            $lastExchangeRate->save();
        }

        return $data;
    }
}
