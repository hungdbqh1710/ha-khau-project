<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Repositories\Eloquents\ShipCodeRepository;
use App\Helper;

class ShipCodeImport implements ToCollection
{

    protected $shipCodeRepository;

    public function __construct()
    {
        $this->shipCodeRepository = app('ShipCodeRepositoryInterface');
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $data)
    {
        //dd($data);
        if($data->count() > 0)
        {
            $b = false;
            foreach($data->toArray() as $key => $row)
            {
                if(!$b) {
                    $b = true;
                    continue;
                }
                $this->shipCodeRepository->create([
                    'ship_code' => $row[0].'',
                    'package_code' => $row[1].'',
                    'buy_code' => $row[2].'',
                    'status' => $row[3].'',
                    'size' => $row[4].'',
                    'weight' => trim($row[5].''),
                    'price' => $row[6],
                    'cn_transport_fee' => $row[7],
                    'exchange_rates' => $row[8],
                    'total_price' => $row[9],
                    'admin_note' => $row[10].'',
                    'customer_id' => $row[11]
                ]);      
            }
        }
    }
}
