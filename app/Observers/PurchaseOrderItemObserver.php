<?php

namespace App\Observers;

use App\Models\PurchaseOrderItem;

class PurchaseOrderItemObserver
{
    /**
     * Handle the User "creating" event.
     *
     * @param PurchaseOrderItem $orderItem
     * @return void
     */
    public function creating(PurchaseOrderItem $orderItem)
    {
        $price = str_replace(',', '', $orderItem->price);
        $orderItem->price = (float)$price;
    }
    /**
     * Handle the OrderItem "created" event.
     *
     * @param  \App\Models\PurchaseOrderItem $orderItem
     * @return void
     */
    public function created(PurchaseOrderItem $orderItem)
    {
        $order = $orderItem->order;
        $total_price = $order->purchase_total_items_price + $orderItem->totalPrice();
        $updateInfo = [
            'purchase_total_items_price' => $total_price,
            'final_total_price' => $total_price + $order->purchase_service_fee + $order->purchase_cn_transport_fee + $order->purchase_vn_transport_fee + $order->purchase_cn_to_vn_fee
        ];
        $order->update($updateInfo);
    }
    /**
     * Handle the OrderItem "updated" event.
     *
     * @param  \App\Models\PurchaseOrderItem $orderItem
     * @return void
     */
    public function updated(PurchaseOrderItem $orderItem)
    {
    }

    /**
     * Handle the OrderItem "deleted" event.
     *
     * @param PurchaseOrderItem $orderItem
     * @return void
     */
    public function deleted(PurchaseOrderItem $orderItem)
    {
        $exchangeRateRepository = app('ExchangeRateRepositoryInterface');
        $currentRate = $exchangeRateRepository->findActiveExchangeRate()->getCurrentRate();
        $order = $orderItem->order;
        if (!empty($order)) {
            $updateInfo = [];
            $orderItems = $order->purchaseOrderItems;
            if ($orderItems->count() == 0 && ($order->status == STATUS_PROCESSING || $order->status == STATUS_PRICE_QUOTATION)) {
                $updateInfo['status'] = STATUS_CANCEL;
            }
            if (!empty($orderItem->price_range)) {
                $this->updateItemPrice($order, $orderItem, $orderItems);
            }
            $totalPrice = $order->fresh()->purchaseOrderItems->reduce(function ($total, $item) use ($currentRate){
                return $total + $item->qty * $item->price * $currentRate;
            }, 0);

            $updateInfo['purchase_total_items_price'] = $totalPrice;
            $updateInfo['final_total_price'] = $totalPrice + $order->purchase_service_fee + $order->purchase_cn_transport_fee + $order->purchase_vn_transport_fee + $order->purchase_cn_to_vn_fee;
            $order->update($updateInfo);
        }
    }

    /**
     * Update item price after delete
     * @param $order
     * @param $orderItem
     * @param $orderItems
     */
    private function updateItemPrice($order, $orderItem, $orderItems)
    {
        $productId = $orderItem->product_id;
        $orderItems = $orderItems->where('product_id', $productId);
        $totalItem = $orderItems->sum('qty');
        $newItemPrice = null;
        $priceRange = json_decode($orderItem->price_range);
        foreach ($priceRange as $range) {
            if ($range->end != null) {
                if ($totalItem >= $range->begin && $totalItem <= $range->end) {
                    $newItemPrice = $range->price;
                }
            } elseif ($totalItem >= $range->begin) {
                $newItemPrice = $range->price;
            }
        }
        if (empty($newItemPrice)) {
            $newItemPrice = $priceRange[0]->price;
        }
        $order->purchaseOrderItems()
            ->where('product_id', $productId)
            ->toBase()
            ->update(['purchase_order_items.updated_at' => now(), 'purchase_order_items.price' => $newItemPrice]);
    }
}