<?php

namespace App\Observers;

use App\Models\OrderTemp;

class OrderTempObserver
{
    /**
     * Handle the User "creating" event.
     *
     * @param OrderTemp $orderTemp
     * @return void
     */
    public function creating(OrderTemp $orderTemp)
    {
        $price = $orderTemp->price;
        if (!is_numeric(substr($orderTemp->price, -3))) {
            $price = substr_replace($orderTemp->price, '.', -3, 1);
        }
        if (!is_numeric(substr($orderTemp->price, -2))) {
            $price = substr_replace($orderTemp->price, '.', -2, 1);
        }
        $price = str_replace(',', '', $price);
        $orderTemp->price = (float)$price;
        $orderTemp->qty = (int)$orderTemp->qty;
        $orderTemp->total_price = $orderTemp->price * $orderTemp->qty;
        if (!empty($orderTemp->property)) {
            $orderTemp->property = json_encode($orderTemp->property);
        }
        if (!empty($orderTemp->price_range)) {
            $orderTemp->price_range = json_encode($orderTemp->price_range);
        }
    }
    /**
     * Handle the OrderTemp "created" event.
     *
     * @param  \App\Models\OrderTemp  $orderItem
     * @return void
     */
    public function created(OrderTemp $orderItem)
    {
    }
    /**
     * Handle the OrderTemp "updated" event.
     *
     * @param  \App\Models\OrderTemp  $orderItem
     * @return void
     */
    public function updated(OrderTemp $orderItem)
    {
    }

    /**
     * Handle the OrderTemp "deleted" event.
     *
     * @param OrderTemp $orderItem
     * @return void
     */
    public function deleted(OrderTemp $orderItem)
    {
    }
}
