<?php

namespace App\Observers;

use App\Models\Order;

class OrderObserver
{
    /**
     * Handle the User "creating" event.
     *
     * @param Order $order
     * @return void
     */
    public function creating(Order $order)
    {
    }
    /**
     * Handle the Order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
    }
    /**
     * Handle the Order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        if ($order->order_type == TYPE_ORDER_PURCHASE) {
            $order->orderItems->each->delete();
            $order->orderAdress->delete();
        }
    }
}
