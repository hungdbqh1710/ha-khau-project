<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\PurchaseOrderItem;
use App\Observers\OrderObserver;
use App\Models\OrderTemp;
use App\Observers\OrderTempObserver;
use App\Observers\PurchaseOrderItemObserver;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Maatwebsite\Excel\Sheet;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        PurchaseOrderItem::observe(PurchaseOrderItemObserver::class);
        Order::observe(OrderObserver::class);
        OrderTemp::observe(OrderTempObserver::class);

        $this->getExtendValidation();
        
        $this->app->bind('path.public', function() {
            // return base_path().'/../public_html';
            return base_path();
        });

        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Blade::directive('render', function ($component) {
            return "<?php echo (app($component))->toHtml(); ?>";
        });
    }

    /**
     * Extend validation rule
     */
    protected function getExtendValidation()
    {
        \Validator::extend('empty_html', function ($attribute, $value) {
            if($value != strip_tags($value)) {
                return false;
            }
            return true;
        });
        \Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            if(!empty($value)) {
                // return preg_match('/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z\d]).\S*$/', (string)$value);
            }
            return true;
        });
        \Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
            if(!empty($value)) {
                $min_field = $parameters[0];
                $data = $validator->getData();
                $min_value = $data[$min_field];
                return $value > $min_value;
            }
            return true;
        });

        \Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', trans('validation.attributes.'. $parameters[0]), $message);
        });
    }
}
