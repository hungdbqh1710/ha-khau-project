<?php

namespace App\Providers;

use App\Exports\MattExcel;
use App\Repositories\Eloquents\BlogRepository;
use App\Repositories\Eloquents\CmsPageRepository;
use App\Repositories\Eloquents\CustomerLevelRepository;
use App\Repositories\Eloquents\CustomerRepository;
use App\Repositories\Eloquents\DeliveryAddressRepository;
use App\Repositories\Eloquents\FeedbackRepository;
use App\Repositories\Eloquents\NotificationRepository;
use App\Repositories\Eloquents\PurchasePurchaseOrderItemRepository;
use App\Repositories\Eloquents\OrderRepository;
use App\Repositories\Eloquents\PermissionRepository;
use App\Repositories\Eloquents\RoleRepository;
use App\Repositories\Eloquents\ServiceFeeRepository;
use App\Repositories\Eloquents\SlideRepository;
use App\Repositories\Eloquents\TransactionHistoriesRepository;
use App\Repositories\Eloquents\TransportOrderItemRepository;
use App\Services\Translate\TranslateChina;
use App\Services\TwoFactorAuthentication\SpeedSms;
use App\Repositories\Eloquents\WebsiteConfigRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Eloquents\ExchangeRateRepository;
use App\Repositories\Eloquents\UserRepository;
use App\Repositories\Eloquents\OrderTempRepository;
use App\Repositories\Eloquents\ShipCodeRepository;


class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind('UserRepository', UserRepository::class);
        app()->bind('ExchangeRateRepositoryInterface', ExchangeRateRepository::class);
        app()->bind('CustomerRepositoryInterface', CustomerRepository::class);
        app()->bind('DeliveryAddressRepositoryInterface', DeliveryAddressRepository::class);
        app()->bind('OrderTempRepositoryInterface', OrderTempRepository::class);
        app()->bind('OrderRepositoryInterface', OrderRepository::class);
        app()->bind('ExportInterface', MattExcel::class);
        app()->bind('SlideRepositoryInterface', SlideRepository::class);
        app()->bind('FeedbackRepositoryInterface', FeedbackRepository::class);
        app()->bind('RoleRepositoryInterface', RoleRepository::class);
        app()->bind('PermissionRepositoryInterface', PermissionRepository::class);
        app()->bind('BlogRepositoryInterface', BlogRepository::class);
        app()->bind('WebsiteConfigRepositoryInterface', WebsiteConfigRepository::class);
        app()->bind('NotificationRepositoryInterface', NotificationRepository::class);
        app()->bind('CmsPageRepositoryInterface', CmsPageRepository::class);
        app()->bind('TwoFactorAuthentication', SpeedSms::class);
        app()->bind('PurchaseOrderItemRepositoryInterface', PurchasePurchaseOrderItemRepository::class);
        app()->bind('TranslateInterface', TranslateChina::class);
        app()->bind('CustomerLevelInterface', CustomerLevelRepository::class);
        app()->bind('ServiceFeeInterface', ServiceFeeRepository::class);
        app()->bind('TransportOrderInterface', TransportOrderItemRepository::class);
        app()->bind('App\Repositories\Contracts\TransactionHistoryInterface', TransactionHistoriesRepository::class);
        app()->bind('ShipCodeRepositoryInterface', ShipCodeRepository::class);
    }
}
