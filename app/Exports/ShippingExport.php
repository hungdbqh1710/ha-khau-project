<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Repositories\Eloquents\ShipCodeRepository;
use App\Helper;

class ShippingExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $shipCodeRepository;
    protected $request;
    protected $data;

    /**
     * ShippingExport constructor.
     */
    public function __construct(Array $data)
    {
        $this->shipCodeRepository = app('ShipCodeRepositoryInterface');
        $this->request = app('request');
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */

    public function collection()
    {
        return collect($this->data);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Mã vận đơn',
            'Mã bao', 
            'Mã đơn hàng mua hộ', 
            'Trạng thái', 
            'Ngày giờ', 
            'Kích thước',
            'Cân nặng', 
            'Đơn giá', 
            'Cước TQ', 
            'Tỷ giá', 
            'Tổng cước', 
            'Ghi chú',
            'Khách hàng'
        ];
    }
}
