<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $orderRepository;
    protected $request;

    /**
     * OrderExport constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
        $this->request = app('request');
    }

    /**
     * @return \Illuminate\Support\Collection
     */

    public function collection()
    {
        $orders = $this->orderRepository->getFilterOrder(
            [
                'order_number',
                'purchase_total_items_price',
                'status',
                'confirm_date',
                'purchase_service_fee',
                'purchase_cn_transport_fee',
                'purchase_vn_transport_fee',
                'purchase_cn_to_vn_fee',
                'final_total_price',
                'created_at'
            ]
        )->toArray();
        return collect($orders)->map(function ($order) {
            $order['status'] = config('data.order_status')[$order['status']];
            unset($order['final_total_price_format']);
            unset($order['created_at_format']);
            return $order;
        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('form.order_number'),
            trans('form.total_price'),
            trans('form.status'),
            trans('form.confirm_date'),
            trans('form.purchase_service_fee'),
            trans('form.cn_transport_fee'),
            trans('form.vn_transport_fee'),
            trans('form.title_purchase_cn_to_vn_fee'),
            trans('form.final_total_price'),
            trans('form.title_created_at'),
        ];
    }
}
