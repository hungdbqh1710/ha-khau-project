<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportRevenueExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $userRepository;
    protected $orderRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
        $this->userRepository = app('UserRepository');
    }

    /**
     * @return \Illuminate\Support\Collection
     */

    public function collection()
    {
        $supporters = $this->userRepository->getAll();
        $dataRevenue = $this->orderRepository->exportRevenue(request()->only(['supporter_id', 'single_date', 'date_range']), $supporters);

        return collect($dataRevenue);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [];
    }
}