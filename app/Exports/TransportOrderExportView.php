<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class TransportOrderExportView implements FromView, ShouldAutoSize, WithEvents, WithColumnFormatting
{
    protected $orderRepository;

    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Order::all();
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $data = $this->orderRepository->getOneById(request()->id);
        return view('admins.exports.transport_orders', compact('data'));
    }

    public function registerEvents(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
        ];
    }
}
