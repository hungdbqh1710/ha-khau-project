<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class PurchaseOrderExport implements FromView, ShouldAutoSize, WithEvents, WithColumnFormatting
{
    protected $orderRepository;
    protected $webSiteRepository;

    /**
     * PurchaseOrderExport constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
        $this->webSiteRepository = app('WebsiteConfigRepositoryInterface');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Order::all();
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $data = $this->orderRepository->getOneById(request()->id)->load('orderAdress', 'purchaseOrderItems');
        $configs = $this->webSiteRepository->getAll()->groupBy('name')->map(function($config) {
            return $config->first();
        });
        return view('admins.exports.purchase_orders', compact('data', 'configs'));
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1',
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'A7',
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
                $event->sheet->styleCells(
                    'A16:J16',
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
            },
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => '#,##0',
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
        ];
    }
}
