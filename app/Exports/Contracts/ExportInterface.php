<?php

namespace App\Exports\Contracts;

interface ExportInterface
{
    /**
     * @return mixed
     */
    public function import();

    /**
     * @param $export
     * @param $fileName
     * @return mixed
     */
    public function export($export, $fileName);
}
