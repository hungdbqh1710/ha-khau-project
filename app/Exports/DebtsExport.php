<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Repositories\Contracts\TransactionHistoryInterface;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class DebtsExport implements FromView, ShouldAutoSize, WithEvents, WithColumnFormatting
{
    protected $orderRepository;
    protected $transactionHistory;
    protected $customerRepository;

    public function __construct(TransactionHistoryInterface $transactionHistory)
    {
        $this->orderRepository = app('OrderRepositoryInterface');
        $this->customerRepository = app('CustomerRepositoryInterface');
        $this->transactionHistory = $transactionHistory;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Order::all();
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $orders = $this->orderRepository->calculateDebt();
        $transactionHistories = $this->transactionHistory->findByCustomerId(request()->customer_id);
        $customerName = null;
        $filterDate = null;
        if (request()->filled('customer_id')) {
            $customerName = $this->customerRepository->getOneById(request()->customer_id)->name;
        }
        if (request()->filled('single_date')) {
            $filterDate = request()->single_date;
        } elseif (request()->filled('date_range')) {
            $filterDate = request()->date_range;
        }

        $count = $orders->count();
        $type = 'order';
        
        if ($orders->count() < $transactionHistories->count()) {
            $count = $transactionHistories->count();
            $type = 'transaction';
        }

        return view('admins.exports.debt', compact('orders', 'customerName', 'filterDate', 'transactionHistories', 'count', 'type'));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A3:I3',
                    [
                        'font' => [
                            'bold' => true
                        ]
                    ]
                );
            },
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'E' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
        ];
    }
}