<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TransportOrderExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $orderRepository;
    protected $request;

    /**
     * OrderExport constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * @return \Illuminate\Support\Collection
     */

    public function collection()
    {
        $orders = $this->orderRepository->getFilterTransportOrder(
            [
                'order_number',
                'status',
                'final_total_price',
                'admin_note',
                'created_at'
            ]
        )->toArray();
        return collect($orders)->map(function ($order) {
            $order['status'] = config('data.order_status')[$order['status']];
            $order['final_total_price'] = number_format($order['final_total_price']);
            unset($order['order_type_text']);
            unset($order['final_total_price_format']);
            unset($order['created_at_format']);
            return $order;
        });
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            trans('form.order_number'),
            trans('form.status'),
            trans('form.final_total_price'),
            trans('form.title_note_admin'),
            trans('form.title_created_at'),
        ];
    }
}