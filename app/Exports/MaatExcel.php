<?php

namespace App\Exports;

use App\Exports\Contracts\ExportInterface;
use Maatwebsite\Excel\Excel;

class MattExcel implements ExportInterface
{
    protected $excel;

    /**
     * MattExcel constructor.
     * @param Excel $excel
     */
    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * @return mixed|void
     */
    public function import()
    {
    }


    /**
     * @param $export
     * @param $fileName
     * @return mixed|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function export($export, $fileName)
    {
        return $this->excel->download($export, $fileName);
    }
}
