<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Helper
{
    /**
     * Generate order number
     * @return string
     */
    public static function generateOrderNR()
    {
        $orderObj = DB::table('orders')->select('order_number')->latest('id')->first();
        if ($orderObj) {
            $orderNumber = $orderObj->order_number;
            $removed1char = substr($orderNumber, 1);
            $generateOrder_nr = str_pad($removed1char + 1, 8, "0", STR_PAD_LEFT);
        } else {
            $generateOrder_nr = str_pad(1, 8, "0", STR_PAD_LEFT);
        }
        return $generateOrder_nr;
    }

    /**
     * Generate search url
     *
     * @param $keyword
     * @param $typeSite
     * @return string
     */
    public static function generateSearchUrl($keyword, $typeSite)
    {
        foreach (config('data.list_sites') as $key => $site) {
            if ($typeSite == $key && $site['site_name'] == TYPE_1688) {
                return $site['url_search'] . mb_convert_encoding($keyword, 'gb2312', 'utf-8');
            }
            if ($typeSite == $key) {
                return $site['url_search'] . $keyword;
            }
        }

        return route('home-page');
    }

    /**
     * Generate button for datatable
     * @param $permission
     */
    public static function generateDatatableButton($permission)
    {
        $buttons = [];
        if (auth()->user()->can($permission)) {
            $buttons[] = [
                'extend'      => 'create',
                'editor'      => 'editor',
                'text' => 'function() { return renderCreateButton(); }',
                'formButtons' => [
                    [
                        'text'      => trans('form.button_create'),
                        'className' => 'btn-primary',
                        'action'    => 'function() { this.submit(); }'
                    ],
                    [
                        'text'      => trans('buttons.button_close'),
                        'action'    => 'function() { this.close(); }'
                    ]
                ],

            ];
        }
        $buttons[] = 'reset';
        return $buttons;
    }

    /**
     * @param $keyWord
     * @return bool|string
     */
    public static function clearPrefixOrder($keyWord)
    {
        if ($keyWord[0] == config('data.prefix_order_number')) {
            $keyWord = substr($keyWord, 1);
        }
        return $keyWord;
    }

    /**
     * Change status when update shipping
     * @param $current_stt
     * @return status
     */
    public static function changeStatusShipping($current_stt){
        $status = array('in-cn', 'out-cn', 'in-vn', 'out-vn');
        $length = count($status);
        $index = array_search($current_stt, $status);
        if($index+1 < $length){
            return $status[$index + 1];
        }
        return $status[$index];
    }

    public static function getNameCustomer($id){
        if($id != '')
            return \App\Models\Customer::find($id)['name'];
        else
            return '';
    }

    public static function getNameStatus($stt){
        $status = array('in-cn', 'out-cn', 'in-vn', 'out-vn');
        $index = array_search($stt, $status);
        if($stt == $status[0]){
            return 'Đã về kho TQ';
        }
        if($stt == $status[1]){
            return 'Đã xuất kho TQ';
        }
        if($stt == $status[2]){
            return 'Đã về kho VN';
        }
        if($stt == $status[3]){
            return 'Đã xuất kho VN';
        }
    }
}
