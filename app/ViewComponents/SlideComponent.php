<?php

namespace App\ViewComponents;

use Illuminate\Contracts\Support\Htmlable;

class SlideComponent implements Htmlable
{
    protected $slide;

    /**
     * SlideComponent constructor.
     */
    public function __construct()
    {
        $this->slide = app('SlideRepositoryInterface');
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        return view('frontend.slide')
            ->with('slidesTaobao', $this->slide->getSlideTaobao())
            ->with('slides1688', $this->slide->getSlide1688())
            ->with('slidesTmall', $this->slide->getSlideTmall())
            ->render();
    }
}