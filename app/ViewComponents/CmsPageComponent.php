<?php

namespace App\ViewComponents;

use Illuminate\Contracts\Support\Htmlable;

class CmsPageComponent implements Htmlable
{
    protected $cmsPage;

    /**
     * SlideComponent constructor.
     */
    public function __construct()
    {
        $this->cmsPage = app('CmsPageRepositoryInterface');
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        return view('partials._cms_page')
            ->with('cmsPages', $this->cmsPage->getCmsPage())
            ->render();
    }
}