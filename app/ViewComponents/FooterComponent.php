<?php

namespace App\ViewComponents;

use Illuminate\Contracts\Support\Htmlable;

class FooterComponent implements Htmlable
{
    protected $websiteConfigRepository;
    protected $cmsPageRepository;

    public function __construct()
    {
        $this->websiteConfigRepository = app('WebsiteConfigRepositoryInterface');
        $this->cmsPageRepository = app('CmsPageRepositoryInterface');
    }

    /**
     * Get content as a string of HTML.
     *
     * @return string
     * @throws \Throwable
     */
    public function toHtml()
    {

        $configs = $this->websiteConfigRepository->getAll()->groupBy('name')->map(function($config) {
            return $config->first();
        });
        $cmsPages = $this->cmsPageRepository->findWhere(['is_active' => STATUS_ACTIVE]);
        return view('partials.frontend._footer', compact('configs', 'cmsPages'))->render();
    }
}