<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PositionSlideRule implements Rule
{
    protected $data;

    protected $slideRepository;

    /**
     * Create a new rule instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->slideRepository = app('SlideRepositoryInterface');
        $this->data = $data;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $slide = collect($this->data['data'])->first();
        $group = $slide['group'];
        $positions = $this->slideRepository->findByField('group', $group)->where('id', '<>', $slide['id'])->pluck('position')->toArray();

        if (in_array($value, $positions)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('message.position_has_exist');
    }
}
