<?php

namespace App\Services\Translate\Contracts;

/**
 * Interface TranslateInterface
 * @package App\Sevices\Translate\Contracts
 */
interface TranslateInterface
{
    /**
     * @param string $keyword
     * @return mixed
     */
    public function translate($keyword);
}