<?php

namespace App\Services\Translate;

use App\Services\Translate\Contracts\TranslateInterface;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Stichoza\GoogleTranslate\TranslateClient;

class TranslateChina implements TranslateInterface
{
    /**
     * @param string $keyword
     * @return string
     */
    public function translate($keyword)
    {
        try {
            $translate = new TranslateClient('vi', 'zh-CN');

            return ($keyword != null) ? $translate->translate($keyword) : '';
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi dịch từ khóa sang ngôn ngữ Trung Quốc',
                    'context' => [
                        'keyword'       => $keyword,
                        'error_message' => $exception->getMessage(),
                    ]
                ]
            );
            return '';
        }
    }
}