<?php

namespace App\Services\TwoFactorAuthentication;

use App\Services\TwoFactorAuthentication\Contracts\TwoFactorAuthentication;
use GuzzleHttp\Client;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;

/**
 * Class SpeedSms
 * @package App\Services\TwoFactorAuthentication
 */
class SpeedSms implements TwoFactorAuthentication
{
    protected $client;

    /**
     * SpeedSms constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Get verify code
     *
     * @param $phoneNumber
     * @return null
     */
    public function getVerifyCode($phoneNumber)
    {
        try {
            $url  = config('speed-sms.url-get-verify-code');

            $params['headers'] = [
                'Content-Type'  => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic '.base64_encode(config('speed-sms.api-access-token').':'.config('speed-sms.password')),
            ];
            $params['form_params'] = ['to' => $phoneNumber, 'content' => config('speed-sms.content'), 'app_id' => config('speed-sms.app-id')];

            $result = $this->client->post($url, [
                'headers' => $params['headers'],
                'body'    => json_encode($params['form_params']),
            ]);

            $result = $result->getBody()->getContents();
            $result = json_decode($result, true);

            if ($result['status'] === 'success') {
                return $result['data']['pin_code'];
            }

            return null;
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi lấy mã xác nhận',
                    'context' => [
                        'phone_number'  => $phoneNumber,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return null;
        }
    }

    /**
     * Verify code
     *
     * @param $phoneNumber
     * @param $pinCode
     * @return null
     */
    public function verifyCode($phoneNumber, $pinCode)
    {
        try {
            $url = config('speed-sms.url-verify-code');

            $params['headers'] = [
                'Content-Type'  => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic '.base64_encode(config('speed-sms.api-access-token').':'.config('speed-sms.password')),
            ];
            $params['form_params'] = ['phone' => $phoneNumber, 'app_id' => config('speed-sms.app-id'), 'pin_code' => $pinCode];

            $result = $this->client->post($url, [
                'headers' => $params['headers'],
                'body'    => json_encode($params['form_params']),
            ]);

            $result = $result->getBody()->getContents();
            $result = json_decode($result, true);
            if ($result['status'] === 'success' && $result['data']['verified'] === true) {
                return true;
            }

            return false;
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi xác nhận mã',
                    'context' => [
                        'phone_number'  => $phoneNumber,
                        'pin_code'      => $pinCode,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return false;
        }
    }
}