<?php

namespace App\Services\TwoFactorAuthentication\Contracts;

/**
 * Interface TwoFactorAuthentication
 * @package App\Services\TwoFactorAuthentication\Contracts
 */
interface TwoFactorAuthentication
{
    /**
     * @param integer $phoneNumber
     * @return mixed
     */
    public function getVerifyCode($phoneNumber);

    /**
     * @param integer $phoneNumber
     * @param integer $pinCode
     * @return mixed
     */
    public function verifyCode($phoneNumber, $pinCode);
}