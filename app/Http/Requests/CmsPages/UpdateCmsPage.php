<?php

namespace App\Http\Requests\CmsPages;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCmsPage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|max:25|empty_html',
            'slug'    => 'required|empty_html|unique:cms_pages,slug,'.$this->id,
            'title'   => 'required|empty_html|max:100',
            'content' => 'required',
        ];
    }
}
