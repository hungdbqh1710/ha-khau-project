<?php

namespace App\Http\Requests\Blogs;

use App\Rules\ImageRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreBlog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => 'required|empty_html|max:100',
            'image'   => ['required', new ImageRule()],
            'content' => 'required',
        ];
    }
}
