<?php

namespace App\Http\Requests\Customers;

use App\Rules\BirthdayRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class UpdateCustomer
 *
 * @package App\Http\Requests\Customers
 */
class UpdateCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|max:30|empty_html',
            'phone_number' => 'required|min:10|max:15|regex:/^0([0-9\-\+\(\)]*)$/|unique:customers,phone_number,' . Auth::id(),
            'email'        => 'required|email|unique:customers,email,' . Auth::id(),
            'bank_account' => 'nullable|numeric|digits_between:8,20',
            'birthday'     => ['nullable', new BirthdayRule()],
        ];
    }
}
