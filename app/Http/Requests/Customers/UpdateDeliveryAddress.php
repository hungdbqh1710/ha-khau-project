<?php

namespace App\Http\Requests\Customers;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateDeliveryAddress
 *
 * @package App\Http\Requests\Customers
 */
class UpdateDeliveryAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->session()->flash('modal_target', UPDATE_DELIVERY_ADDRESS_MODAL);
        return [
            'name'         => 'required',
            'phone_number' => 'required|min:10|max:15|regex:/^0([0-9\-\+\(\)]*)$/',
            'city'         => 'required',
            'district'     => 'required',
            'commune'      => 'required',
            'street'       => 'required',
        ];
    }
}
