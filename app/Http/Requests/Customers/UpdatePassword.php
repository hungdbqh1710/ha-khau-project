<?php

namespace App\Http\Requests\Customers;

use App\Rules\ComparePasswordRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePassword
 *
 * @package App\Http\Requests\Customers
 */
class UpdatePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => [new ComparePasswordRule($this)],
            'password'         => 'required|string|min:8|confirmed',
            // 'password'         => 'required|string|min:8|confirmed|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^a-zA-Z\d]).\S*$/',
        ];
    }
}
