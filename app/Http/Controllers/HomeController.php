<?php

namespace App\Http\Controllers;

use App\Models\WebsiteConfig;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the homepage/
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $popup = WebsiteConfig::where('name', 'popup_image')->first();
        return view('frontend.home', compact('popup'));
    }
}
