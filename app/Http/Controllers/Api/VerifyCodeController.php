<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class VerifyCodeController extends Controller
{
    protected $twoFactorAuthentication;

    protected $customerRepository;

    /**
     * VerifyCodeController constructor.
     */
    public function __construct()
    {
        $this->twoFactorAuthentication = app('TwoFactorAuthentication');
        $this->customerRepository = app('CustomerRepositoryInterface');
    }

    /**
     * Get verify code
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVerifyCode(Request $request)
    {
        try {
            $customer = $this->customerRepository->findByPhoneNumber($request->phone);
            if (!empty($customer)) {
                return response()->json([
                    'status'  => false,
                    'type'    => 'phone',
                    'message' => trans('message.phone_number_already_exists'),
                ]);
            }

            $customer = $this->customerRepository->findByEmail($request->email);
            if (!empty($customer)) {
                return response()->json([
                    'status'  => false,
                    'type'    => 'email',
                    'message' => trans('message.email_already_exists'),
                ]);
            }

            $pinCode = $this->twoFactorAuthentication->getVerifyCode($request->phone);
            if (empty($pinCode)) {
                return response()->json([
                    'status' => false,
                ]);
            }

            return response()->json([
                'status' => true,
                'data'   => $pinCode
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi lấy mã xác thực',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false,
            ]);
        }
    }

    /**
     * Verify code
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyCode(Request $request)
    {
        $result = $this->twoFactorAuthentication->verifyCode($request->phone, $request->pinCode);

        if ($result === false) {
            return response()->json([
                'status' => false,
            ]);
        }

        return response()->json([
            'status' => true,
        ]);
    }
}
