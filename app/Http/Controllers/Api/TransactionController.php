<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Contracts\TransactionHistoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    protected $transactionHistoryRepository;

    /**
     * TransactionController constructor.
     * @param TransactionHistoryInterface $transactionHistoryRepository
     */
    public function __construct(TransactionHistoryInterface $transactionHistoryRepository)
    {
        $this->transactionHistoryRepository = $transactionHistoryRepository;
    }

    public function byCustomer()
    {
        $transactionHistories = $this->transactionHistoryRepository->findByCustomer(['customer_id' => auth('customers')->id()]);
        return response()->json(
            $transactionHistories
        );
    }
}
