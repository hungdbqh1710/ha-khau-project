<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $userRepository;

    /**
     * PermissionController constructor.
     */
    public function __construct()
    {
        $this->userRepository = app('UserRepository');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->userRepository->getActiveUser();
    }
}
