<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    protected $permissionRepository;

    /**
     * PermissionController constructor.
     */
    public function __construct()
    {
        $this->permissionRepository = app('PermissionRepositoryInterface');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->permissionRepository->getAll();
    }
}
