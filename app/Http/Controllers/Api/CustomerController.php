<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Models\TransactionHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    protected $orderRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function findOrderByCustomer($id)
    {
        $orders = Order::where('customer_id', $id)->where('status', '>=', STATUS_TRANSPORT_CONFIRMED)->where('status', '<', STATUS_CANCEL)->filterByDate(request())->latest()->paginate(config('data.default_paginate'));
        $response = view('admins.reports.ajax.orders', compact('orders'))->render();
        return response()->json(['html' => $response]);
    }

    public function findTransactionByCustomer($id)
    {
        $transactions = TransactionHistory::where('customer_id', $id)->filterByDate(request())->latest()->paginate(config('data.default_paginate'));
        $response = view('admins.reports.ajax.transactions', compact('transactions'))->render();
        return response()->json(['html' => $response]);
    }
}
