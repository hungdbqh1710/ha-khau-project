<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    protected $roleRepository;

    /**
     * PermissionController constructor.
     */
    public function __construct()
    {
        $this->roleRepository = app('RoleRepositoryInterface');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->roleRepository->getAll();
    }
}
