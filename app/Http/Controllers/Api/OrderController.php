<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    protected $orderRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    public function byCustomer()
    {
        $orders = $this->orderRepository->findPriceQuotation();
        return response()->json($orders);
    }
}
