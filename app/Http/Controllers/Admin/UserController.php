<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\UserDatatable;
use App\Http\Controllers\Controller;
use App\DataTables\UsersDataTablesEditor;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param UserDatatable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index(UserDatatable $dataTable)
    {
        return $dataTable->render('admins.users.index');
    }

    /**
     * Process action to database.
     *
     * @param UsersDataTablesEditor $editor
     * @return \Illuminate\Http\Response
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(UsersDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
