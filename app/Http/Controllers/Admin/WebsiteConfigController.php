<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\WebsiteConfigDataTable;
use App\DataTables\WebsiteConfigDataTablesEditor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebsiteConfigController extends Controller
{
    /**
     * @param WebsiteConfigDataTable $dataTable
     * @return mixed
     */
    public function index(WebsiteConfigDataTable $dataTable)
    {
        return $dataTable->render('admins.website_configs.index');
    }

    /**
     * Process action to database.
     *
     * @param WebsiteConfigDataTablesEditor $editor
     * @return \Illuminate\Http\Response
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(WebsiteConfigDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
