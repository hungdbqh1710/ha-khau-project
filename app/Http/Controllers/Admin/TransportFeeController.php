<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TransportFeeDataTable;
use App\DataTables\TransportFeeDataTablesEditor;
use App\Http\Controllers\Controller;

class TransportFeeController extends Controller
{
    /**
     * @param TransportFeeDataTable $dataTable
     * @return mixed
     */
    public function index(TransportFeeDataTable $dataTable)
    {
        return $dataTable->render('admins.transport_fees.index');
    }

    /**
     * Action transport fees
     * @param TransportFeeDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(TransportFeeDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
