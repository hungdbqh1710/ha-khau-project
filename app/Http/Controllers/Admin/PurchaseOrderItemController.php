<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PurchaseOrderItemsDataTablesEditor;
use App\Http\Controllers\Controller;

class PurchaseOrderItemController extends Controller
{
    /**
     * @param PurchaseOrderItemsDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(PurchaseOrderItemsDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
