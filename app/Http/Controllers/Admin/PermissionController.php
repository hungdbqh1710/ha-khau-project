<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PermissionDataTable;
use App\DataTables\PermissionDataTablesEditor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * @param PermissionDataTable $dataTable
     * @return mixed
     */
    public function index(PermissionDataTable $dataTable)
    {
        return $dataTable->render('admins.permissions.index');
    }

    /**
     * Action permissions
     * @param PermissionDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(PermissionDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
