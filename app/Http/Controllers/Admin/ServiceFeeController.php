<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ServiceFeeDataTable;
use App\DataTables\ServiceFeeDataTablesEditor;
use App\Http\Controllers\Controller;

class ServiceFeeController extends Controller
{
    /**
     * @param ServiceFeeDataTable $dataTable
     * @return mixed
     */
    public function index(ServiceFeeDataTable $dataTable)
    {
        return $dataTable->render('admins.service_fees.index');
    }

    /**
     * Action service fees
     * @param ServiceFeeDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(ServiceFeeDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
