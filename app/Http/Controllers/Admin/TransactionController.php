<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TranSactionDataTablesEditor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class TransactionController
 * @package App\Http\Controllers\Admin
 */
class TransactionController extends Controller
{
    /**
     * @param TranSactionDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(TranSactionDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
