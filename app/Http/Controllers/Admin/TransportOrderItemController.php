<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TransportOrderItemDataTablesEditor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class TransportOrderItemController extends Controller
{
    protected $transportOrderRepository;

    /**
     * TransportOrderItemController constructor.
     */
    public function __construct()
    {
        $this->transportOrderRepository = app('TransportOrderInterface');
    }

    /**
     * Action service fees
     * @param TransportOrderItemDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(TransportOrderItemDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * Render link and qty of transport order item
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderLinkQty(Request $request)
    {
        try {
            $linkQties = $this->transportOrderRepository->getLinkQty($request->itemId);
            $renderLinkQtyHtml = view('frontend.transport-orders.render_link_qty', compact('linkQties'))->render();

            return response()->json([
                'status' => true,
                'data'   => $renderLinkQtyHtml
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi render link, số lượng',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false
            ]);
        }
    }
}
