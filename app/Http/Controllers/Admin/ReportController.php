<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ReportDebtDataTable;
use App\DataTables\ReportRevenueDataTable;
use App\Exports\DebtsExport;
use Illuminate\Http\Request;
use App\Exports\ReportRevenueExport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Exception;

class ReportController extends Controller
{
    protected $userRepository;
    protected $orderRepository;
    protected $customerRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
        $this->userRepository = app('UserRepository');
        $this->customerRepository = app('CustomerRepositoryInterface');
    }

    /**
     * Report revenue
     *
     * @param ReportRevenueDataTable $dataTable
     * @return mixed
     */
    public function revenue(ReportRevenueDataTable $dataTable)
    {
        $users = $this->userRepository->getAll();
        $calculateOrderRevenue = $this->orderRepository->calculateOrderRevenue(request()->only(['supporterId', 'singleDate', 'dateRange']));

        return $dataTable->with(request()->only(['supporter_id', 'single_date', 'date_range']))->render('admins.reports.revenue', compact('users', 'calculateOrderRevenue'));
    }

    /**
     * Calculate revenue
     *
     * @return string
     */
    public function calculateRevenue()
    {
        try {
            $calculateOrderRevenue = $this->orderRepository->calculateOrderRevenue(request()->only(['supporter_id', 'single_date', 'date_range']));
            $renderRevenueHtml = view('admins.reports.calculate-revenue', compact('calculateOrderRevenue'))->render();

            return response()->json([
                'status' => true,
                'data'   => $renderRevenueHtml
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi tính toán doanh thu',
                    'context' => [
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false
            ]);
        }
    }

    /**
     * Calculate debt
     *
     * @return string
     */
    public function calculateDebt()
    {
        try {
            $calculateOrdersDebt = $this->orderRepository->calculateDebtByConditions(request()->only(['customer_id', 'single_date', 'date_range']));
            $renderDebtHtml = view('admins.reports.calculate-debt', compact('calculateOrdersDebt'))->render();

            return response()->json([
                'status' => true,
                'data'   => $renderDebtHtml
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi tính toán công nợ',
                    'context' => [
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false
            ]);
        }
    }

    /**
     * Export revenue
     *
     * @param ReportRevenueExport $data
     * @return mixed
     */
    public function exportRevenue(ReportRevenueExport $data)
    {
        $export = app('ExportInterface');
        return $export->export($data, trans('excels.revenue_file_name') . now()->format('d-m-Y') . '.xlsx');
    }

    /**
     * Show report debt
     *
     * @param ReportDebtDataTable $dataTable
     * @return mixed
     */
    public function debt(ReportDebtDataTable $dataTable)
    {
        $customers = $this->customerRepository->getAll();
        $calculateOrdersDebt = $this->orderRepository->calculateDebtByConditions(request()->only(['customer_id', 'single_date', 'date_range']));
        return $dataTable->render('admins.reports.debt', compact('customers', 'calculateOrdersDebt'));
    }

    /**
     * @param DebtsExport $order
     * @return
     */
    public function debtExport(DebtsExport $order)
    {
        $export = app('ExportInterface');
        return $export->export($order, 'Báo cáo công nợ ' . now()->format('d-m-Y') .'.xlsx');
    }

    /**
     * Payment debt
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function paymentDebt()
    {
        try {
            $this->orderRepository->paymentDebt(request()->only(['customer_id']));

            return response()->json([
                'status' => true
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi thanh toán công nợ',
                    'content' => [
                        'data'          => request()->all(),
                        'error_message' => $exception->getMessage()
                    ]
                ]
            );

            return response()->json([
                'status' => false
            ]);
        }
    }
}
