<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TransportOrderDataTable;
use App\DataTables\TransportOrderDataTablesEditor;
use App\Exports\TransportOrderExportView;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Yajra\DataTables\DataTables;

class TransportOrderController extends Controller
{
    protected $orderRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * @param TransportOrderDataTable $dataTable
     * @return mixed
     */
    public function index(TransportOrderDataTable $dataTable)
    {
        return $dataTable->render('admins.transport-orders.index');
    }

    /**
     * Action service fees
     * @param TransportOrderDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(TransportOrderDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * Show transport order item
     *
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function show($id)
    {
        $transportOrderItems = $this->orderRepository->getOneById($id)->transportOrderItem->load('order');
        return DataTables::of($transportOrderItems)->make(true);
    }

    /**
     * @param                          $id
     * @param TransportOrderExportView $order
     * @return mixed
     */
    public function export($id, TransportOrderExportView $order)
    {
        $orderNumber = $this->orderRepository->getOneById($id)->order_number;
        $export = app('ExportInterface');
        return $export->export($order, 'Đơn hàng vận chuyển ' . $orderNumber .'.xlsx');
    }

    /**
     * Render link and qty of transport order item
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderLinkQty(Request $request)
    {
        try {
            $linkQties = $this->orderRepository->getLinkQty($request->itemId);
            $renderLinkQtyHtml = view('frontend.transport-orders.render_link_qty', compact('linkQties'))->render();

            return response()->json([
                'status' => true,
                'data'   => $renderLinkQtyHtml
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi render link, số lượng',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false
            ]);
        }
    }
}
