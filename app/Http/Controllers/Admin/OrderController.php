<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PurchaseOrderDatatable;
use App\DataTables\PurchaseOrderDataTablesEditor;
use App\DataTables\OrderOfCustomerDataTable;
use App\Exports\PurchaseOrderExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

/**
 * Class OrderController
 *
 * @package App\Http\Controllers\Admin
 */
class OrderController extends Controller
{
    protected $customerRepository;
    protected $orderRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->customerRepository = app('CustomerRepositoryInterface');
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * @param PurchaseOrderDatatable $dataTable
     * @return mixed
     */
    public function indexPurchaseOrder(PurchaseOrderDatatable $dataTable)
    {
        return $dataTable->render('admins.orders.index');
    }

    /**
     * Action order
     *
     * @param PurchaseOrderDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function actionPurchaseOrder(PurchaseOrderDataTablesEditor $editor)
    {
        return $editor->process(request());
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function showPurchaseOrderDetail($id)
    {
        $orderItems = $this->orderRepository->getOneById($id)->purchaseOrderItems->load('orderGroup', 'order');
        return Datatables::of($orderItems)
                            ->addColumn('shop_name', function ($item) {
                                return optional($item->orderGroup)->shop_name;
                            })
                            ->skipPaging()->make(true);
    }

    /**
     * Show orders of customer
     *
     * @param $customerId
     * @param OrderOfCustomerDataTable $dataTable
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showByCustomerId($customerId, OrderOfCustomerDataTable $dataTable)
    {
        $customer = $this->customerRepository->getOneById($customerId);

        return $dataTable->with(['id' => $customerId])->render('admins.orders.orders_of_customer', compact('customer'));
    }

    /**
     * @param                     $id
     * @param PurchaseOrderExport $order
     * @return mixed
     */
    public function exportPurchaseOrder($id, PurchaseOrderExport $order)
    {
        $orderNumber = $this->orderRepository->getOneById($id)->order_number;
        $export = app('ExportInterface');
        return $export->export($order, 'Đơn hàng mua hộ ' . $orderNumber .'.xlsx');
    }

    /**
     * Delete transport order
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $this->orderRepository->delete($id);
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.delete_transport_order_success')]);
            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi xóa đơn vận chuyển',
                    'context' => [
                        'id'          => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.delete_transport_order_error')]);
            return back();
        }
    }
}
