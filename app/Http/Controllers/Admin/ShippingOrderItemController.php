<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ShippingOrderItemDataTablesEditor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShippingOrderItemController extends Controller
{
    /**
     * Action service fees
     * @param ShippingOrderItemDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(ShippingOrderItemDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
