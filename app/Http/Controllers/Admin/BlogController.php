<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BlogDataTable;
use App\DataTables\BlogDataTablesEditor;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blogs\StoreBlog;
use App\Http\Requests\Blogs\UpdateBlog;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class BlogController extends Controller
{
    protected $blogRepository;

    /**
     * BlogController constructor.
     */
    public function __construct()
    {
        $this->blogRepository = app('BlogRepositoryInterface');
    }

    /**
     * Display a listing of the resource.
     *
     * @param BlogDataTable $dataTable
     * @return mixed
     */
    public function index(BlogDataTable $dataTable)
    {
        return $dataTable->render('admins.blogs.index');
    }

    /**
     * Action exchange rate
     *
     * @param BlogDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function action(BlogDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * Show form create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admins.blogs.create');
    }

    /**
     * Store blog
     *
     * @param StoreBlog $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreBlog $request)
    {
        try {
            $this->blogRepository->storeBlog($request->all());

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.create_blog_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi tạo mới bài viết',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.create_blog_error')]);

            return back();
        }
    }

    /**
     * Show form edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $blog = $this->blogRepository->getOneById($id);

        return view('admins.blogs.update', compact('blog'));
    }

    /**
     * Update blog
     *
     * @param $id
     * @param UpdateBlog $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateBlog $request, $id)
    {
        try {
            $this->blogRepository->updateBlog($id, $request->except('_token'));

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_blog_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi cập nhật bài viết',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.update_blog_error')]);

            return back();
        }
    }
}