<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ShippingOrderDataTable;
use App\DataTables\ShippingOrderDataTablesEditor;
use App\DataTables\ShippingOrderItemDataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShippingOrderController extends Controller
{
    /**
     * @param ShippingOrderDataTable $dataTable
     * @return mixed
     */
    public function index(ShippingOrderDataTable $dataTable)
    {
        return $dataTable->render('admins.shipping_orders.index');
    }

    /**
     * Action service fees
     * @param ShippingOrderDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(ShippingOrderDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * @param ShippingOrderItemDataTable $dataTable
     * @param                            $id
     * @return mixed
     */
    public function show(ShippingOrderItemDataTable $dataTable, $id)
    {
        $shippingOrderRepository = app('OrderRepositoryInterface');
        $order = $shippingOrderRepository->getOneById($id);
        return $dataTable
            ->with(
                [
                    'id'    => $id,
                    'order' => $order
                ]
            )
            ->render('admins.shipping_orders.detail', compact('order'));
    }
}
