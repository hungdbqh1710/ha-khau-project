<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\RoleDataTable;
use App\DataTables\RoleDataTablesEditor;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Session\Store;
use Mockery\Exception;
use Session;

class RoleController extends Controller
{
    protected $roleRepository;
    protected $permissionRepository;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->roleRepository = app('RoleRepositoryInterface');
        $this->permissionRepository = app('PermissionRepositoryInterface');
    }

    /**
     * @param RoleDataTable $dataTable
     * @return mixed
     */
    public function index(RoleDataTable $dataTable)
    {
        return $dataTable->render('admins.roles.index');
    }

    /**
     * Action permissions
     * @param RoleDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(RoleDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $role = $this->roleRepository->getOneById($id)->load('permissions');
        $permissions = $this->permissionRepository->getAll();
        return view('admins.roles.edit', compact('role', 'permissions'));
    }

    /**
     * @param                   $id
     * @param UpdateRoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRoleRequest $request)
    {
        try {
            $this->roleRepository->update($id, $request->only('display_name'));
            $this->roleRepository->updatePermission($id, $request->permissions);
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_roles_success')]);
            return redirect()->back();
        } catch (Exception $exception) {
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi cập nhật role',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.update_roles_error')]);
            return back();
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $permissions = $this->permissionRepository->getAll();
        return view('admins.roles.create', compact('permissions'));
    }

    /**
     * @param StoreRoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRoleRequest $request)
    {
        try {
            $role = $role = $this->roleRepository->create(
                [
                    'name'  => str_slug($request->display_name),
                    'display_name' => $request->display_name,
                ]
            );
            $this->roleRepository->updatePermission($role->id, $request->permissions);
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.create_roles_success')]);
            return redirect()->back();
        } catch (Exception $exception) {
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi thêm mới role',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.create_roles_error')]);
            return back();
        }
    }
}
