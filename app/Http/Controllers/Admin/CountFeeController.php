<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CountFeeDataTable;
use App\DataTables\CountFeeDataTablesEditor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountFeeController extends Controller
{
    /**
     * @param CountFeeDataTable $dataTable
     * @return mixed
     */
    public function index(CountFeeDataTable $dataTable)
    {
        return $dataTable->render('admins.count_fees.index');
    }

    /**
     * Action count fees
     * @param CountFeeDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(CountFeeDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
