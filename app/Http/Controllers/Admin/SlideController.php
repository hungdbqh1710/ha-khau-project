<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SlideDataTable;
use App\DataTables\SlideDataTablesEditor;
use App\Http\Controllers\Controller;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $group
     * @param SlideDataTable $dataTable
     * @return \Illuminate\Http\Response
     */
    public function index($group, SlideDataTable $dataTable)
    {
        return $dataTable->with(['group' => $group])->render('admins.slides.index', compact('group'));
    }

    /**
     * Process action to database.
     *
     * @param SlideDataTablesEditor $editor
     * @return \Illuminate\Http\Response
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(SlideDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
