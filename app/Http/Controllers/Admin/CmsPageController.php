<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CmsPageDataTable;
use App\DataTables\CmsPageDataTablesEditor;
use App\Http\Controllers\Controller;
use App\Http\Requests\CmsPages\StoreCmsPage;
use App\Http\Requests\CmsPages\UpdateCmsPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class CmsPageController extends Controller
{
    protected $cmsPageRepository;

    /**
     * BlogController constructor.
     */
    public function __construct()
    {
        $this->cmsPageRepository = app('CmsPageRepositoryInterface');
    }

    /**
     * Display a listing of the resource.
     *
     * @param CmsPageDataTable $dataTable
     * @return mixed
     */
    public function index(CmsPageDataTable $dataTable)
    {
        return $dataTable->render('admins.cms-pages.index');
    }

    /**
     * Action exchange rate
     *
     * @param CmsPageDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function action(CmsPageDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * Show form create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admins.cms-pages.create');
    }

    /**
     * Store page
     *
     * @param StoreCmsPage $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCmsPage $request)
    {
        try {
            $this->cmsPageRepository->storeCmsPage($request->all());

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.create_cms_page_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi tạo mới page',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.create_cms_page_error')]);

            return back();
        }
    }

    /**
     * Show form edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $cmsPage = $this->cmsPageRepository->getOneById($id);

        return view('admins.cms-pages.update', compact('cmsPage'));
    }

    /**
     * Update page
     *
     * @param $id
     * @param UpdateCmsPage $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCmsPage $request, $id)
    {
        try {
            $this->cmsPageRepository->updateCmsPage($id, $request->except('_token'));

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_cms_page_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi cập nhật page',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.update_cms_page_error')]);

            return back();
        }
    }

    /**
     * Show cms page
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $cmsPage = $this->cmsPageRepository->firstOrFail(['slug' => $slug]);

        return view('frontend.cms-pages.detail', compact('cmsPage'));
    }
}
