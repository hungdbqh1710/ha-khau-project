<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\WoodFeeDataTable;
use App\DataTables\WoodFeeDataTablesEditor;
use App\Http\Controllers\Controller;

class WoodFeeController extends Controller
{
    /**
     * @param WoodFeeDataTable $dataTable
     * @return mixed
     */
    public function index(WoodFeeDataTable $dataTable)
    {
        return $dataTable->render('admins.wood_fees.index');
    }

    /**
     * Action wood fees
     * @param WoodFeeDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Yajra\DataTables\DataTablesEditorException
     */
    public function action(WoodFeeDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
