<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\FeedbackDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class FeedbackController extends Controller
{
    protected $feedbackRepository;

    /**
     * FeedbackController constructor.
     */
    public function __construct()
    {
        $this->feedbackRepository = app('FeedbackRepositoryInterface');
    }

    /**
     * Display a listing of the resource.
     *
     * @param FeedbackDataTable $dataTable
     * @return mixed
     */
    public function index(FeedbackDataTable $dataTable)
    {
        return $dataTable->render('admins.feedback.index');
    }

    /**
     * Send feedback
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(Request $request)
    {
        try {
            $this->feedbackRepository->create($request->all());

            return response()->json([
                'status' => true
            ]);
        } catch (Exception $exception) {
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => "Lỗi khi khách hàng gửi phản hồi",
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ],
            ]);
            return response()->json([
                'status' => false
            ]);
        }
    }
}
