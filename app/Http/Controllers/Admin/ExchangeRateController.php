<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ExchangeRateDataTable;
use App\DataTables\ExchangeRateDataTablesEditor;
use App\Http\Controllers\Controller;

/**
 * Class ExchangeRateController
 *
 * @package App\Http\Controllers
 */
class ExchangeRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ExchangeRateDataTable $dataTable
     * @return mixed
     */
    public function index(ExchangeRateDataTable $dataTable)
    {
        return $dataTable->render('exchange-rate.index');
    }

    /**
     * Action exchange rate
     *
     * @param ExchangeRateDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function action(ExchangeRateDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
