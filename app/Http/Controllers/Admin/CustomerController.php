<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CustomerDataTable;
use App\DataTables\CustomerDataTableEditor;
use App\DataTables\TransactionHistoryDataTable;
use App\Http\Controllers\Controller;

/**
 * Class CustomerController
 *
 * @package App\Http\Controllers\Admin
 */
class CustomerController extends Controller
{
    protected $customerRepository;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->customerRepository = app('CustomerRepositoryInterface');
    }

    /**
     * Display a listing of the resource.
     *
     * @param CustomerDataTable $dataTable
     * @return mixed
     */
    public function index(CustomerDataTable $dataTable)
    {
        return $dataTable->render('admins.customers.index');
    }

    /**
     * Action exchange rate
     *
     * @param CustomerDataTableEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function action(CustomerDataTableEditor $editor)
    {
        return $editor->process(request());
    }

    /**
     * Show transaction histories
     * @param                             $id
     * @param TransactionHistoryDataTable $dataTable
     * @return mixed
     */
    public function showTransaction($id, TransactionHistoryDataTable $dataTable)
    {
        $customer = $this->customerRepository->getOneById($id);
        return $dataTable->with(['id' => $id])->render('admins.transactions.transaction_histories', compact('customer'));
    }
}
