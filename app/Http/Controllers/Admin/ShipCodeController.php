<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ShippingExport;
use App\Imports\ShipCodeImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquents\ShipCodeRepository;
use DateTime;
use App\Helper;
use Maatwebsite\Excel\Facades\Excel;
use DB;

/**
 * Class ShipCodeController
 *
 * @package App\Http\Controllers\Admin
 */
class ShipCodeController extends Controller
{
    protected $shipCodeRepository;
    protected $customerRepository;
    public $successStatus = 200;
    public $noContentStatus = 204;
    public $validateStatus = 400;
    public $authStatus = 401;
    public $exceptionStatus = 500;
    private $excel;

    /**
     * ShipCodeController constructor.
     */
    public function __construct(ShipCodeRepository $shipCodeRepository)
    {
        $this->shipCodeRepository = $shipCodeRepository;
        $this->customerRepository = app('CustomerRepositoryInterface');
    }

    /**
     * Get list with paginate
     * @return mixed
     */
    public function index()
    {
        $per_page = request()->per_page ? request()->per_page : config('data.default_paginate');
        $ship_codes = $this->shipCodeRepository->index($per_page);
        return view('admins.ship-codes.index', compact('ship_codes', 'per_page'));
    }

    public function formCreate(){
        return view('admins.ship-codes.create');
    }

    public function search(){
        if(request()->search_content){
            $ship_codes = $this->shipCodeRepository->search(request()->search_content);
        }
        else{
            $ship_codes = $this->shipCodeRepository->index(config('data.default_paginate'));
        }
        $names = array();
        foreach($ship_codes as $ship){
            array_push($names, Helper::getNameCustomer($ship->customer_id));
        }
        return response()->json(['status' => 200, 'data' => $ship_codes, 'names' => $names], 200);
    }

    /**
     * Create or update shipping
     * @param $request
     */
    public function createOrUpdate(Request $request) {
        $result = $this->shipCodeRepository->createOrUpdate($request);
        $status = $result == 'success' ? $this->successStatus : $this->validateStatus;
        return response()->json(['status' => $status, 'data' => $result], $status);
    }

    /**
     * Filter by order status and date
     * @param $request
     */
    public function getCustomersByStatus(){
        $customer_filter = request()->states[0];
        $customer_id = $customer_filter  ? explode("-", $customer_filter)[0] : '';
        $order_status = request()->order_status != null ? request()->order_status : '';
        $date_range = explode("-",request()->daterange);
        $from = trim($date_range[0]);
        $to = trim($date_range[1]);
        $day_from = DateTime::createFromFormat('d/m/Y', $from)->format('Y-m-d');
        $day_from = $day_from.' 00:00:00';
        $day_to = DateTime::createFromFormat('d/m/Y', $to)->format('Y-m-d H:i:s');
        $ship_codes = $this->shipCodeRepository->getAllWithFilter($order_status, $customer_id, $day_from, $day_to); 
        return view('admins.ship-codes.index', compact('ship_codes', 'order_status', 'from', 'to', 'customer_filter'));
    }

    /**
     * Get file excel by filter
     * @param $request
     */
   public function getExcel(Request $request){
        $data = json_decode($request->data);
        if(isset($data->current_page)){ 
            $data = $data->data;
        }
        if(count($data)){
            foreach ($data as $dt) {
                $excel_data[] = array(
                    'Mã vận đơn' => $dt->ship_code,
                    'Mã bao' => $dt->package_code,
                    'Mã đơn hàng mua hộ' => $dt->buy_code,
                    'Trạng thái' => Helper::getNameStatus($dt->status),
                    'Ngày giờ' => $dt->updated_at,
                    'Kích thước' => $dt->size,
                    'Cân nặng' => $dt->weight,
                    'Đơn giá' => number_format($dt->price),
                    'Cước TQ' => $dt->cn_transport_fee,
                    'Tỷ giá' => number_format($dt->exchange_rates),
                    'Tổng cước' => number_format($dt->total_price),
                    'Ghi chú' => $dt->admin_note,
                    'Khách hàng' => $dt->customer_id ? Helper::getNameCustomer($dt->customer_id) : '',
                );
            }
            $export = app('ExportInterface');
            return $export->export(new ShippingExport($excel_data), 'Mã vận đơn ' . now()->format('d-m-Y') . '.xlsx');
        }
    }

    public function delete(Request $request){
        $data = json_decode($request->data);
        $id = $data->delete_id;
        $this->shipCodeRepository->deleteShipCode($id);
        return response()->json(['status' => 200, 'data' => 'Delete'], 200);
    }

    public function import(Request $request)
    {
        $this->validate($request, [
        'select_file'  => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file');
        $data = Excel::import(new ShipCodeImport, $path);
        return back()->with('success', 'Excel Data Imported successfully.');
    }
}
