<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CustomerLevelDataTable;
use App\DataTables\CustomerLevelDataTablesEditor;
use App\Http\Controllers\Controller;

class CustomerLevelController extends Controller
{
    protected $customerLevelInterface;

    /**
     * CustomerLevelController constructor.
     */
    public function __construct()
    {
        $this->customerLevelInterface = app('CustomerLevelInterface');
    }

    /**
     * Display a listing of the resource.
     *
     * @param CustomerLevelDataTable $dataTable
     * @return mixed
     */
    public function index(CustomerLevelDataTable $dataTable)
    {
        return $dataTable->render('admins.customer-level.index');
    }

    /**
     * Action exchange rate
     *
     * @param CustomerLevelDataTablesEditor $editor
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function action(CustomerLevelDataTablesEditor $editor)
    {
        return $editor->process(request());
    }
}
