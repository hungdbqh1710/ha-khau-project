<?php

namespace App\Http\Controllers\Customers;

use App\Models\OrderTemp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderTempController extends Controller
{
    protected $orderTempReposiory;
    protected $exchangeRateReposiory;

    /**
     * OrderTempController constructor.
     */
    public function __construct()
    {
        $this->orderTempReposiory = app('OrderTempRepositoryInterface');
        $this->exchangeRateReposiory = app('ExchangeRateRepositoryInterface');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->data['selected'];
        $orderTempIds = $this->orderTempReposiory->createProduct($data);
        return response()->json(
            [
                'url' => route(
                    'orders.temps.show',
                    [
                        'id' => $orderTempIds->implode(','),
                        'redirect_to' => urlencode(route('orders.temps.show', ['id' => $orderTempIds->implode(',')]))
                    ]
                )
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $currentRate = $this->exchangeRateReposiory->findActiveExchangeRate()->getCurrentRate();
        $orderTemps = $this->orderTempReposiory->getByIds(explode(',', $request->id));
        $tempIds = $orderTemps->map(function($temp) {
           return $temp['id'];
        })->implode(',');
        return view('frontend.orders.temp', compact('orderTemps', 'tempIds', 'currentRate'));
    }
}
