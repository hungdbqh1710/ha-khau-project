<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    protected $notificationRepository;

    /**
     * NotificationController constructor.
     */
    public function  __construct()
    {
        $this->notificationRepository = app('NotificationRepositoryInterface');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $notifications =  $this->notificationRepository->getFilterCustomerNotification();
        return view('frontend.notifications.index', compact('notifications'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function read($id)
    {
        $this->notificationRepository->markAsRead($id);
        return redirect()->to(request()->redirect);
    }
}
