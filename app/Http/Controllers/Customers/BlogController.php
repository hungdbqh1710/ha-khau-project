<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    protected $blogRepository;

    /**
     * BlogController constructor.
     */
    public function __construct()
    {
        $this->blogRepository = app('BlogRepositoryInterface');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $blogs = $this->blogRepository->paginate(config('data.default_paginate'));

        return view('frontend.blogs.blog', compact('blogs'));
    }

    /**
     * Show detail blog
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $blog = $this->blogRepository->findByField('slug', $slug)->first();

        return view('frontend.blogs.detail', compact('blog'));
    }
}