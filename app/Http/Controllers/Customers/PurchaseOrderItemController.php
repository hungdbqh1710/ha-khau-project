<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Models\PurchaseOrderItem;
use Illuminate\Http\Request;
use Session;
use DB;

class PurchaseOrderItemController extends Controller
{
    protected $purchaseOrderItemRepository;

    public function __construct()
    {
        $this->purchaseOrderItemRepository = app('PurchaseOrderItemRepositoryInterface');
    }

    /**
     * Delete order
     * @param         $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $this->purchaseOrderItemRepository->delete($id);
            DB::commit();
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.delete_order_item_success')]);
            return back();
        } catch (\Exception $exception) {
            \Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi xóa chi tiết hóa đơn',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            DB::rollBack();
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.delete_order_item_error')]);
            return back();
        }
    }
}
