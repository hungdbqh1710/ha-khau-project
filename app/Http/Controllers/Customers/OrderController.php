<?php

namespace App\Http\Controllers\Customers;

use App\Exports\OrderExport;
use App\Exports\TransportOrderExport;
use App\Http\Requests\CheckAuthorizeOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Log;
use Session;

class OrderController extends Controller
{
    protected $orderRepository;
    protected $exchangeRateReposiory;
    protected $deliveryAddressRepository;
    protected $customerLevelRepository;
    protected $websiteConfigRepository;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:customers');
        $this->orderRepository = app('OrderRepositoryInterface');
        $this->exchangeRateReposiory = app('ExchangeRateRepositoryInterface');
        $this->deliveryAddressRepository = app('DeliveryAddressRepositoryInterface');
        $this->customerLevelRepository = app('CustomerLevelInterface');
        $this->websiteConfigRepository = app('WebsiteConfigRepositoryInterface');
    }

    /**
     * Show all customer order
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexPurchaseOrder(Request $request)
    {
        $allOrders = $this->orderRepository->findWhere(
            [
                'customer_id' => auth('customers')->id(),
                'order_type'  => TYPE_ORDER_PURCHASE
            ]
        );
        $allOrderCount = $allOrders->count();
        $unsentOrderCount = $allOrders->where('status', STATUS_UNSENT)->count();
        $processingOrderCount = $allOrders->where('status', STATUS_PROCESSING)->count();
        $priceQuotationOrderCount = $allOrders->where('status', STATUS_PRICE_QUOTATION)->count();
        $inWareHouseOrderCount = $allOrders->where('status', STATUS_IN_WAREHOUSE)->count();
        $orderedOrderCount = $allOrders->where('status', STATUS_ORDERED)->count();
        $orderSuccessCount = $allOrders->where('status', STATUS_SUCCESS)->count();
        $orderCancelCount = $allOrders->where('status', STATUS_CANCEL)->count();
        $orders = $this->orderRepository->getOrderWithItem($request);
        return view(
            'frontend.orders.index',
            compact(
                'orders',
                'allOrderCount',
                'unsentOrderCount',
                'processingOrderCount',
                'priceQuotationOrderCount',
                'inWareHouseOrderCount',
                'orderedOrderCount',
                'orderSuccessCount',
                'orderCancelCount'
            )
        );
    }

    /**
     * Store order and order item
     * @param StoreOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePurchaseOrder(StoreOrderRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->orderRepository->createOrder($request);
            DB::commit();
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.create_order_item_success')]);
            return redirect()->route('orders.purchases.index');
        } catch (Exception $exception) {
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi tạo order',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            DB::rollback();
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.create_order_error')]);
            return back();
        }
    }

    /**
     * Show order detail
     * @param                            $id
     * @param CheckAuthorizeOrderRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPurchaseOrder($id, CheckAuthorizeOrderRequest $request)
    {
        $order = $this->orderRepository->getOneById($id)->load('purchaseOrderItems');
        $activeExchangeRate = $this->exchangeRateReposiory->findActiveExchangeRate();
        $defaultDeliveryAddress = $this->deliveryAddressRepository->findDefaultDeliveryAddress();
        $deposited =  $order->final_total_price != 0 ? ($order->deposited / $order->final_total_price * 100) : 0;
        return view('frontend.orders.show', compact('order', 'activeExchangeRate', 'defaultDeliveryAddress', 'deposited'));
    }

    /**
     * @param                                 $id
     * @param CheckAuthorizeOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendOrder($id, CheckAuthorizeOrderRequest $request)
    {
        try {
            $order = $this->orderRepository->getOneById($id);

            if ($order->purchaseOrderItems()->count() == VALUE_TYPE_FALSE) {
                Session::flash('notification', ['status' => 'danger', 'message' => trans('message.order_has_not_item')]);
                return back();
            }

            $this->orderRepository->sendOrder($id, $request);
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.send_order_success')]);
            return redirect()->route('orders.purchases.index');
        } catch (Exception $exception) {
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi gửi đơn hàng',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.send_order_error')]);
            return back();
        }
    }

    /**
     * @param                            $id
     * @param CheckAuthorizeOrderRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDepositView($id, CheckAuthorizeOrderRequest $request)
    {
        $order = $this->orderRepository->getOneById($id);
        $paymentCashInfo = $this->websiteConfigRepository->getPaymentInfo('payment_cash');
        $paymentTransferInfo = $this->websiteConfigRepository->getPaymentInfo('payment_transfer');

        return view('frontend.orders.deposit', compact('order', 'paymentCashInfo', 'paymentTransferInfo'));
    }

    public function export(OrderExport $order)
    {
        $export = app('ExportInterface');
        return $export->export($order, 'Đơn hàng ' . now()->format('d-m-Y') . '.xlsx');
    }

    /**
     * Show all transport order
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexTransportOrder(Request $request)
    {
        $transportOrders = $this->orderRepository->findTransportOrders();
        $transportOrderCount = $transportOrders->count();
        $processingOrderCount = $transportOrders->where('status', STATUS_PROCESSING)->count();
        $confirmedOrderCount = $transportOrders->where('status', STATUS_CONFIRMED)->count();
        $priceQuotationOrderCount = $transportOrders->where('status', STATUS_PRICE_QUOTATION)->count();
        $priceTransportingCount = $transportOrders->where('status', STATUS_TRANSPORTING)->count();
        $orderSuccessCount = $transportOrders->where('status', STATUS_SUCCESS)->count();
        $orderCancelCount = $transportOrders->where('status', STATUS_CANCEL)->count();
        $orders = $this->orderRepository->getTransportOrderWithItem($request);
        return view(
            'frontend.transport-orders.index',
            compact(
                'orders',
                'transportOrders',
                'transportOrderCount',
                'processingOrderCount',
                'priceQuotationOrderCount',
                'priceTransportingCount',
                'confirmedOrderCount',
                'orderSuccessCount',
                'orderCancelCount'
            )
        );
    }

    /**
     * Create transport order
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTransportOrder(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->orderRepository->createOrder($request);
            DB::commit();
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.create_transport_order_success')]);
            return redirect()->route('orders.indexTransportOrder');
        } catch (Exception $exception) {
            DB::rollback();
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi tạo đơn hàng vận chuyển',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.create_transport_order_error')]);
            return back();
        }
    }

    /**
     * Show transport order detail
     *
     * @param                                 $id
     * @param CheckAuthorizeOrderRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showTransportOrderDetail($id, CheckAuthorizeOrderRequest $request)
    {
        $order = $this->orderRepository->getOneById($id);
        $transportOrderItems = $order->transportOrderItem;

        return view('frontend.transport-orders.show', compact('order', 'transportOrderItems'));
    }

    /**
     * Export transport order
     *
     * @param TransportOrderExport $order
     * @return mixed
     */
    public function exportTransportOrder(TransportOrderExport $order)
    {
        $export = app('ExportInterface');
        return $export->export($order, 'order.xlsx');
    }

    /**
     * Cancel order
     * @param                    $id
     * @param CheckAuthorizeOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelOrder($id, CheckAuthorizeOrderRequest $request)
    {
        try {
            $this->orderRepository->cancelOrder($id, $request);
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.cancel_order_success')]);
            return redirect()->route('orders.purchases.index');
        } catch (Exception $exception) {
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi hủy đơn hàng',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.cancel_order_error')]);
            return back();
        }
    }

    /**
     * Render link and qty of transport order item
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderLinkQty($id)
    {
        try {
            $linkQties = $this->orderRepository->getLinkQty($id);
            $renderLinkQtyHtml = view('frontend.transport-orders.render_link_qty', compact('linkQties'))->render();

            return response()->json([
                'status' => true,
                'data'   => $renderLinkQtyHtml
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi render link, số lượng',
                    'context' => [
                        'id'          => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false
            ]);
        }
    }

    /**
     * Delete transport order
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $this->orderRepository->delete($id);
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.delete_transport_order_success')]);
            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi xóa đơn vận chuyển',
                    'context' => [
                        'id'          => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.delete_transport_order_error')]);
            return back();
        }
    }

    /**
     * Find transport order
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function ajaxFindTransport($id)
    {
        try {
            $data = $this->orderRepository->getOneById($id);
            $linkQtys = json_decode($data->link_qty, true);
            $linkQtyHtml = view('frontend.transport-orders.link_qty_update_render', compact('linkQtys'))->render();

            return response()->json([
                'status' => true,
                'data'   => $data,
                'html'   => $linkQtyHtml,
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi tìm kiếm đơn vận chuyển bằng ajax',
                    'context' => [
                        'id'            => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );

            return response()->json([
                'status' => false,
            ]);
        }
    }

    /**
     * Update transport order
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTransport(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $order = $this->orderRepository->getOneById($id);
            $order->transport_receive_type = $data['transport_receive_type'];
            $order->order_type = $data['order_type'];
            $order->cn_transport_code = $data['cn_transport_code'];
            $order->cn_order_date = $data['cn_order_date'];
            $order->customer_note = $data['customer_note'];
            $order->receiver_info = $data['receiver_info'];
            if (!empty($data['link']) && !empty($data['qty'])) {
                $linkQty = array_map(function ($link, $qty) {
                    return array(
                        'link' => $link,
                        'qty'  => $qty
                    );
                }, $data['link'], $data['qty']);
                $order->link_qty = json_encode($linkQty);
            }

            $order->save();
            DB::commit();
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_transport_order_success')]);
            return back();
        } catch (Exception $exception) {
            DB::rollback();
            Log::error([
                'method'  => __METHOD__,
                'line'    => __LINE__,
                'message' => 'Lỗi khi sửa đơn hàng vận chuyển',
                'context' => [
                    'data'          => $request->all(),
                    'error_message' => $exception->getMessage()
                ]
            ]);
            Session::flash('notification', ['status' => 'danger', 'message' => trans('message.update_transport_order_error')]);
            return back();
        }
    }

    public function searchShipCode(){
        if(request()->transport_code){

        }
    }
}
