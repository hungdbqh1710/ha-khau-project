<?php

namespace App\Http\Controllers\Customers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class TransportOrderItemController extends Controller
{
    protected $transportOrderRepository;
    protected $orderRepository;

    public function __construct()
    {
        $this->transportOrderRepository = app('TransportOrderInterface');
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * Render link and qty of transport order item
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderLinkQty($id)
    {
        try {
            $linkQties = $this->transportOrderRepository->getLinkQty($id);
            $renderLinkQtyHtml = view('frontend.transport-orders.render_link_qty', compact('linkQties'))->render();

            return response()->json([
                'status' => true,
                'data'   => $renderLinkQtyHtml
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi render link, số lượng',
                    'context' => [
                        'id'          => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            return response()->json([
                'status' => false
            ]);
        }
    }

    /**
     * Delete transport order
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $order = $this->transportOrderRepository->getOneById($id)->order;
            $this->transportOrderRepository->delete($id);
            $this->orderRepository->updateFinalTotalPrice($order);
            DB::commit();
            Session::flash('notification', ['status' => 'success', 'message' => trans('message.delete_transport_order_item_success')]);
            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi xóa item đơn vận chuyển',
                    'context' => [
                        'id'          => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            DB::rollBack();
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.delete_transport_order_item_error')]);
            return back();
        }
    }
}
