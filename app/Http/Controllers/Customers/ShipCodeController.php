<?php

namespace App\Http\Controllers\Customers;

use App\Exports\ShippingExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquents\ShipCodeRepository;
use DateTime;
use App\Helper;
use App\Http\Requests\CheckAuthorizeOrderRequest;
use Exception;
use Illuminate\Support\Facades\DB;
use Log;
use Session;

/**
 * Class ShipCodeController
 *
 * @package App\Http\Controllers\Admin
 */
class ShipCodeController extends Controller
{
    protected $shipCodeRepository;
    protected $customerRepository;
    public $successStatus = 200;
    public $noContentStatus = 204;
    public $validateStatus = 400;
    public $authStatus = 401;
    public $exceptionStatus = 500;
    private $excel;

    /**
     * ShipCodeController constructor.
     */
    public function __construct(ShipCodeRepository $shipCodeRepository)
    {
        $this->shipCodeRepository = $shipCodeRepository;
        $this->customerRepository = app('CustomerRepositoryInterface');
    }

    public function index(){
        return view('frontend.ship-codes.index');
    } 

    public function search()
    {
        $ship_codes = $this->shipCodeRepository->findByShipCode(request()->ship_code);
        return view('frontend.ship-codes.index',compact('ship_codes'));
    }
}