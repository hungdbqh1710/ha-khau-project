<?php

namespace App\Http\Controllers\Customers;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\StoreDeliveryAddress;
use App\Http\Requests\Customers\UpdateCustomer;
use App\Http\Requests\Customers\UpdateDeliveryAddress;
use App\Http\Requests\Customers\UpdatePassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

/**
 * Class CustomerController
 *
 * @package App\Http\Controllers\Customers
 */
class CustomerController extends Controller
{
    protected $customerRepository;

    protected $deliveryAddressRepository;

    protected $translateInterface;

    protected $orderRepository;

    /**
     * CustomerController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:'.config('auth.guard_customers.model'))->except('searchProduct');
        $this->customerRepository = app('CustomerRepositoryInterface');
        $this->deliveryAddressRepository = app('DeliveryAddressRepositoryInterface');
        $this->translateInterface = app('TranslateInterface');
        $this->orderRepository = app('OrderRepositoryInterface');
    }

    /**
     * Show customer profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfile()
    {
        $customerId        = auth('customers')->id();
        $customer          = $this->customerRepository->getOneById($customerId);
        $customerProfile   = $customer->customerProfile;
        $deliveryAddresses = $customer->deliveryAddress()->paginate(config('data.default_paginate'));
        $urlUpdate         = route('customers.update', $customerId);

        return view('frontend.customer.customer_profile', compact('customer', 'customerProfile', 'urlUpdate', 'deliveryAddresses'));
    }

    /**
     * Update customer
     *
     * @param UpdateCustomer $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCustomer $request)
    {
        DB::beginTransaction();
        try {
            $this->customerRepository->updateCustomerAndProfile($request);

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_customer_success')]);

            DB::commit();
            return back();
        } catch (Exception $exception) {
            DB::rollback();
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi cập nhật thông tin khách khàng',
                    'context' => [
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.update_customer_error')]);

            return back();
        }
    }

    /**
     * Show form update password
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFormUpdatePassword()
    {
        return view('frontend.customer.update_password');
    }

    /**
     * Update password
     *
     * @param UpdatePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(UpdatePassword $request)
    {
        try {
            $this->customerRepository->updatePassword($request);

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_password_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi đổi mật khẩu khách hàng',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.update_password_error')]);

            return back();
        }
    }

    /**
     * Create delivery address
     *
     * @param StoreDeliveryAddress $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createDeliveryAddress(StoreDeliveryAddress $request)
    {
        try {
            $this->deliveryAddressRepository->createDeliveryAddress($request->all());

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.create_delivery_address_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi thêm mới địa chỉ khách hàng',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.create_delivery_address_error')]);

            return back();
        }
    }

    /**
     * Update delivery address
     *
     * @param UpdateDeliveryAddress $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateDeliveryAddress(UpdateDeliveryAddress $request)
    {
        try {
            $this->deliveryAddressRepository->updateDeliveryAddress($request);

            Session::flash('notification', ['status' => 'success', 'message' => trans('message.update_delivery_address_success')]);

            return back();
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi cập nhật địa chỉ khách hàng',
                    'context' => [
                        'data'          => $request->all(),
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );
            Session::flash('notification', ['status' => 'error', 'message' => trans('message.update_delivery_address_error')]);

            return back();
        }
    }

    /**
     * Find delivery address for ajax
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxSearchAddress($id)
    {
        try {
            $data = $this->deliveryAddressRepository->getOneById($id);

            return response()->json([
                'status' => true,
                'data'   => $data
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi tìm kiếm địa chỉ giao hàng bằng ajax',
                    'context' => [
                        'id'            => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );

            return response()->json([
                'status' => false,
            ]);
        }
    }

    /**
     * Update is_default column of delivery address
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAddressDefault($id)
    {
        try {
            $this->deliveryAddressRepository->updateAddressDefault($id);

            return response()->json([
                'status' => true
            ]);
        } catch (Exception $exception) {
            Log::error(
                [
                    'method'  => __METHOD__,
                    'line'    => __LINE__,
                    'message' => 'Lỗi khi cập nhật địa chỉ giao hàng',
                    'context' => [
                        'id'            => $id,
                        'error_message' => $exception->getMessage()
                    ],
                ]
            );

            return response()->json([
                'status' => false,
            ]);
        }
    }

    /**
     * Search product
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function searchProduct()
    {
        $keyword = request()->input('search');
        $typeSite = request()->input('site');
        $translatedKeyword = $this->translateInterface->translate($keyword);
        $urlSearch = Helper::generateSearchUrl($translatedKeyword, $typeSite);

        return redirect($urlSearch);
    }

    /**
     * Show report debt
     *
     * @return mixed
     */
    public function debt()
    {
        $orders = $this->orderRepository->getOrderPriceQuotation();
        return view('frontend.debt.index', compact('orders'));
    }
}
