<?php

namespace App\Traits;

use Carbon\Carbon;

trait Filter {
    /**
     * @param $query
     * @param $request
     * @return mixed
     */
    public function scopeFilterByDate($query, $request)
    {
        if ($request->filled('single_date')) {
            $month = explode('/', $request->single_date)[0];
            $year =  explode('/', $request->single_date)[1];
            $query->whereMonth('created_at' , $month);
            $query->whereYear('created_at', $year);
        }
        if ($request->filled('date_range')) {
            $dataRange = explode('-', $request->date_range);
            $query->whereDate('created_at', '>=', Carbon::parse($dataRange[0])->startOfDay());
            $query->whereDate('created_at', '<=', Carbon::parse($dataRange[1])->endOfDay());
        }
        return $query;
    }
}