<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\ServiceFeeInterface;

class ServiceFeeRepository extends BaseRepository implements ServiceFeeInterface
{

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\ServiceFee';
    }
}