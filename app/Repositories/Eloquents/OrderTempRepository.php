<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\OrderTempInterface;

class OrderTempRepository extends BaseRepository implements OrderTempInterface
{
    public function getModelClass(): string
    {
        return 'App\Models\OrderTemp';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createProduct(array $data)
    {
        $orderTempIds = [];
        if (isset($data[0]['site']) && $data[0]['site'] == '1688') {
            foreach ($data as $item) {
                if (!empty($item->price_range)) {
                    $newItemPrice = null;
                    $priceRange = $item->price_range;
                    foreach ($priceRange as $range) {
                        if ($range->end != null) {
                            if ($item->qty >= $range->begin && $item->qty <= $range->end) {
                                $newItemPrice = $range->price;
                            }
                        } elseif ($item->qty >= $range->begin) {
                            $newItemPrice = $range->price;
                        }
                    }
                    if (empty($newItemPrice)) {
                        $newItemPrice = $priceRange[0]->price;
                    }
                    $item['price'] = $newItemPrice;
                }
                $orderTempIds[] = $this->create($item)->id;
            }
        } else {
            if (!empty($data->price_range)) {
                $newItemPrice = null;
                $priceRange = $data->price_range;
                foreach ($priceRange as $range) {
                    if ($range->end != null) {
                        if ($data->qty >= $range->begin && $data->qty <= $range->end) {
                            $newItemPrice = $range->price;
                        }
                    } elseif ($data->qty >= $range->begin) {
                        $newItemPrice = $range->price;
                    }
                }
                if (empty($newItemPrice)) {
                    $newItemPrice = $priceRange[0]->price;
                }
                $data['price'] = $newItemPrice;
            }
            $orderTempIds[] = $this->create($data)->id;
        }
        return collect($orderTempIds);
    }
}
