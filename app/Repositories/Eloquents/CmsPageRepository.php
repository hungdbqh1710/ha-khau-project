<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\BlogInterface;
use App\Repositories\Contracts\CmsPageInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class CmsPageRepository
 *
 * @package App\Repositories\Eloquents
 */
class CmsPageRepository extends BaseRepository implements CmsPageInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\CmsPage';
    }

    /**
     * Store
     *
     * @param $data
     * @return mixed
     */
    public function storeCmsPage($data)
    {
        $data['created_by'] = Auth()->id();
        $data['slug'] = str_slug($data['name']);

        $countBySlug = $this->findByField('slug', $data['slug'])->count();

        if ($countBySlug > VALUE_TYPE_FALSE) {
            $data['slug'] = $data['slug'].'-'.time();
        }

        return $this->create($data);
    }

    /**
     * Update
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateCmsPage($id, $data)
    {
        $data['created_by'] = Auth()->id();

        return $this->update($id, $data);
    }

    /**
     * Get cms page
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCmsPage()
    {
        return $this->findByField('is_active', VALUE_TYPE_TRUE);
    }

    /**
     * @param array $attribute
     * @return mixed
     */
    public function firstOrFail(array $attribute)
    {
        return $this->model->where($attribute)->firstOrFail();
    }
}