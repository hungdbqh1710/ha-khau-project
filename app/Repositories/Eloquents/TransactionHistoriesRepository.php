<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\TransactionHistoryInterface;
use Carbon\Carbon;

/**
 * Class TransactionHistoriesRepository
 * @package App\Repositories\Eloquents
 */
class TransactionHistoriesRepository extends BaseRepository implements TransactionHistoryInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\TransactionHistory';
    }

    /**
     * @param array $where
     * @return mixed
     */
    public function findByCustomer(array $where)
    {
        return $this->model->where($where)->latest()->paginate(config('data.default_paginate'));
    }

    /**
     * @param id
     * @return mixed
     */
    public function findByCustomerId($id)
    {
        $query = $this->model->newQuery();

        if (request()->filled('single_date')) {
            $month = explode('/', request()->single_date)[0];
            $year = explode('/', request()->single_date)[1];
            $query->whereMonth('created_at', $month);
            $query->whereYear('created_at', $year);
        }
        if (request()->filled('date_range')) {
            $dataRange = explode('-', request()->date_range);
            $query->whereDate('created_at', '>=', Carbon::parse($dataRange[0])->startOfDay());
            $query->whereDate('created_at', '<=', Carbon::parse($dataRange[1])->endOfDay());
        }
        if (request()->filled('customer_id')) {
            $query->where('customer_id', request()->customer_id);
        }

        return $query->latest()->get();
    }
}