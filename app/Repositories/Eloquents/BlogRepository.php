<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\BlogInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class CustomerRepository
 *
 * @package App\Repositories\Eloquents
 */
class BlogRepository extends BaseRepository implements BlogInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\Blog';
    }

    /**
     * Store
     *
     * @param $data
     * @return mixed
     */
    public function storeBlog($data)
    {
        $fileName = time().'.png';
        $data['image']->storeAs('public/blog-images', $fileName);
        $data['image'] = $fileName;
        $data['created_by'] = Auth()->id();
        $data['slug'] = str_slug($data['title'], '-').'-'.time();

        return $this->create($data);
    }

    /**
     * Update
     *
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateBlog($id, $data)
    {
        if (isset($data['image'])) {
            $fileName = time().'.png';
            $data['image']->storeAs('public/blog-images', $fileName);
            $data['image'] = $fileName;
        }
        $data['created_by'] = Auth()->id();
        $data['slug'] = str_slug($data['title'], '-').'-'.time();

        return $this->update($id, $data);
    }
}