<?php

namespace App\Repositories\Eloquents;


use App\Repositories\Contracts\PurchaseOrderItemInterface;

class PurchasePurchaseOrderItemRepository extends BaseRepository implements PurchaseOrderItemInterface
{

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\PurchaseOrderItem';
    }
}