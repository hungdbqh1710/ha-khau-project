<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\FeedbackInterface;

/**
 * Class FeedbackRepository
 *
 * @package App\Repositories\Eloquents
 */
class FeedbackRepository extends BaseRepository implements FeedbackInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\Feedback';
    }
}