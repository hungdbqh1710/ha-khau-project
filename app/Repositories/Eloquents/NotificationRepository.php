<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\NotificationRepositoryInterface;
use Carbon\Carbon;

class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    public function getModelClass(): string
    {
        return 'App\Models\Notification';
    }

    /**
     * Update read_at column
     * @return mixed
     */
    public function markAsRead($id)
    {
        auth('customers')->user()->notifications()->findOrFail($id)->markAsRead();
    }

    /**
     * Get all notification of customer
     * @return mixed
     */
    public function getFilterCustomerNotification()
    {
        $request = app('request');
        return auth('customers')
                ->user()
                ->notifications()
                ->when($request->filled('status'), function($query) use($request){
                    if($request->status == 0) {
                        return $query->where('read_at', null);
                    }
                    if($request->status == 1) {
                        return $query->where('read_at', '<>', null);
                    }
                    if($request->status == -1) {
                        return $query;
                    }
                })
                ->when($request->filled('start_date'), function($query) use($request) {
                    $query->whereDate('created_at', '>=', Carbon::parse($request->start_date)->startOfDay());
                })
                ->when($request->filled('end_date'), function($query) use($request) {
                    $query->whereDate('created_at', '<=', Carbon::parse($request->end_date)->startOfDay());
                })
                ->paginate(5);
    }
}
