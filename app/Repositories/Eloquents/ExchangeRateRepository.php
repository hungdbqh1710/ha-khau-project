<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\ExchangeRateInterface;

/**
 * Class ExchangeRateRepository
 *
 * @package App\Repositories\Eloquents
 */
class ExchangeRateRepository extends BaseRepository implements ExchangeRateInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\ExchangeRate';
    }

    /** Get active exchange rate
     * @return mixed
     */
    public function findActiveExchangeRate()
    {
        return $this->findByField('is_active', STATUS_ACTIVE)->first();
    }
}