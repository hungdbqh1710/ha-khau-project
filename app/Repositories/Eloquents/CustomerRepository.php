<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\CustomerInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Class CustomerRepository
 *
 * @package App\Repositories\Eloquents
 */
class CustomerRepository extends BaseRepository implements CustomerInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\Customer';
    }

    /**
     * Update customer and customer profile
     *
     * @param $request
     * @return $this
     */
    public function updateCustomerAndProfile($request)
    {
        $dataCustomer = $request->only(['name', 'email', 'phone_number']);
        $dataCustomerProfile = $request->only(['gender', 'birthday', 'bank_account', 'bank_name', 'bank_account_owner', 'business_sector']);
        $this->update(Auth::id(), $dataCustomer);
        if (isset(Auth::user()->customerProfile)) {
            Auth::user()->customerProfile()->update($dataCustomerProfile);
        } else {
            Auth::user()->customerProfile()->create($dataCustomerProfile);
        }

        return $this;
    }

    /**
     * Update password
     *
     * @param $request
     * @return $this
     */
    public function updatePassword($request)
    {
        $data['password'] = Hash::make($request->get('password'));
        return $this->update(Auth::id(), $data);
    }

    /**
     * Find by phone number
     *
     * @param $phoneNumber
     * @return mixed
     */
    public function findByPhoneNumber($phoneNumber)
    {
        return $this->findByField('phone_number', $phoneNumber)->first();
    }

    /**
     * Find by email
     *
     * @param $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        return $this->findByField('email', $email)->first();
    }

    /**
     * Get order success
     *
     * @return mixed
     */
    public function getOrderSuccess()
    {
        return $this->model->where('id', auth()->id())->first()->orders()->where('status', STATUS_SUCCESS)->latest()->paginate(config('data.default_paginate', 20));
    }

    /**
     * Get customers by status
     *
     * @return mixed
     */
    public function getByStatus($status)
    {
        if($status != '')
            return $this->model->where('is_active', $status)->orderBy('created_at', 'DESC')->get();
        else 
            return $this->model->orderBy('created_at', 'DESC')->get();
    }
}