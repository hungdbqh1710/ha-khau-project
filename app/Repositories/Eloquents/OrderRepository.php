<?php

namespace App\Repositories\Eloquents;

use App\Filters\TransportOrderFilter;
use App\Helper;
use App\Models\Order;
use App\Repositories\Contracts\OrderInterface;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Filters\OrderFilter;
use Illuminate\Support\Facades\DB;

/**
 * Class ExchangeRateRepository
 *
 * @package App\Repositories\Eloquents
 */
class OrderRepository extends BaseRepository implements OrderInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\Order';
    }

    /**
     * @return mixed
     */
    public function findUnSentOrder(): Collection
    {
        return $this->model->where(['status' => STATUS_UNSENT, 'order_type' => TYPE_ORDER_PURCHASE])->byCurrentCustomer()->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getOrderWithItem(): LengthAwarePaginator
    {
        return $this->model->filter(new OrderFilter())
                            ->byCurrentCustomer()
                            ->where('order_type', TYPE_ORDER_PURCHASE)
                            ->latest()
                            ->with(['purchaseOrderItems' => function ($query) {
                                return $query->latest();
                            }])
                            ->paginate(config('repository.limit', 5));
    }

    /**
     * Store order
     * @param Request $request
     * @return mixed
     */
    public function createOrder(Request $request)
    {
        $exchangeRateRepository = app('ExchangeRateRepositoryInterface');
        $orderTempRepository = app('OrderTempRepositoryInterface');
        $data = collect($request->except(['_token']));
        if ($data['order_type'] == TYPE_ORDER_PURCHASE) {
            unset($data['order_type']);
            $currentRate = $exchangeRateRepository->findActiveExchangeRate()->getCurrentRate();
            $orderTemps = $orderTempRepository->getByIds(explode(',', $data['temp_ids']));
            $orderTemps = collect(array_map(function($item) {
                $item['product_id'] = $item['item_id'];
                unset($item['item_id']);
                return $item;
            }, $orderTemps->toArray()));
            $orderTempRepository->delete(explode(',', $data['temp_ids']));
            unset($data['temp_ids']);
            $this->createPurchaseOrder($orderTemps, $currentRate);
        } else {
            $this->createTransportOrder($data);
        }
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function getFilterOrder($columns = ['*'])
    {
        return $this->model
            ->filter(new OrderFilter())
            ->byCurrentCustomer()
            ->get($columns);
    }

    /**
     * @param         $id
     * @param Request $request
     * @return mixed|void
     */
    public function sendOrder($id, Request $request)
    {
        $order = $this->getOneById($id);
        $updateInfo = [
            'status' => STATUS_PROCESSING,
            'transport_receive_type' => $request->transport_receive_type
        ];
        $order->update($updateInfo);
        $order->orderAdress()->create($request->except('_token', 'transport_receive_type'));
    }

    /**
     * @param Collection $data
     * @param            $currentRate
     */
    protected function createPurchaseOrder(Collection $data, $currentRate)
    {
        $unSentOrders = $this->findUnSentOrder();
        if ($unSentOrders->isEmpty()) {
            $data = $data->groupBy('shop_name');
            foreach ($data as $shop => $item) {
                $order = $this->create([
                    'customer_id'  => auth('customers')->id(),
                    'order_number' => Helper::generateOrderNR(),
                    'order_type'   => TYPE_ORDER_PURCHASE,
                    'shop_name'    => $shop
                ]);
                $order->purchaseOrderItems()->createMany($item->map(function ($item) use ($currentRate) {
                    $item['current_rate'] = $currentRate;
                    return $item;
                })->toArray());
            }
        } else {
            $itemsHasRange = $data->where('price_range', '<>', null);
            $itemsNoRange = $data->where('price_range', null);
            if ($itemsHasRange->isNotEmpty()) {
                $orderSameShop = $unSentOrders->filter(function ($order) use ($itemsHasRange) {
                    return $order->shop_name == $itemsHasRange->first()['shop_name'];
                });
                if ($orderSameShop->isEmpty()) {
                    $itemsHasRange = $itemsHasRange->groupBy('shop_name');
                    foreach ($itemsHasRange as $shop => $item) {
                        $order = $this->create([
                            'customer_id'  => auth('customers')->id(),
                            'order_number' => Helper::generateOrderNR(),
                            'order_type'   => TYPE_ORDER_PURCHASE,
                            'shop_name'    => $shop
                        ]);
                        $order->purchaseOrderItems()->createMany($item->map(function ($item) use ($currentRate) {
                            $item['current_rate'] = $currentRate;
                            return $item;
                        })->toArray());
                    }
                } else {
                    $unSentOrder = $orderSameShop->first();
                    $items = $unSentOrder->purchaseOrderItems;
                    $oldItemIds = $items->pluck('product_id')->unique()->toArray();
                    $newItemIds = $itemsHasRange->pluck('product_id')->unique()->toArray();
                    foreach ($oldItemIds as $itemId) {
                        if (in_array($itemId, $newItemIds)) {
                            $oldItemCount = $items->where('product_id', $itemId)->sum('qty');
                            $newItemCount = $itemsHasRange->sum('qty');
                            $priceRange = json_decode($itemsHasRange->where('product_id', $itemId)->first()['price_range']);
                            $totalItem = $oldItemCount + $newItemCount;
                            $itemPrice = null;
                            foreach ($priceRange as $range) {
                                if ($range->end != null) {
                                    if ($totalItem >= $range->begin && $totalItem <= $range->end) {
                                        $itemPrice = $range->price;
                                    }
                                } elseif ($totalItem >= $range->begin) {
                                    $itemPrice = $range->price;
                                }
                            }
                            if (empty($itemPrice)) {
                                $itemPrice = $priceRange[0]->price;
                            }
                            $unSentOrder
                                ->purchaseOrderItems()
                                ->where('product_id', $itemId)
                                ->toBase()
                                ->update(['purchase_order_items.updated_at' => now(), 'purchase_order_items.price' => $itemPrice]);
                            $itemsHasRange = $itemsHasRange->where('product_id', $itemId)
                                ->map(function ($item) use ($itemPrice) {
                                    $item['price'] = $itemPrice;
                                    return $item;
                                });
                        }
                    }
                    $itemsHasRange = $itemsHasRange->groupBy('shop_name');
                    foreach ($itemsHasRange as $shopName => $item) {
                        $unSentOrder->purchaseOrderItems()->createMany($item->map(function ($item) use ($currentRate) {
                            $item['current_rate'] = $currentRate;
                            return $item;
                        })->toArray());
                    }
                    $items = $unSentOrder->fresh()->purchaseOrderItems;
                    $totalPrice = $items->reduce(function ($total, $item) use ($currentRate){
                        return $total + $item->qty * $item->price * $currentRate;
                    }, 0);
                    $updateInfo = [
                        'purchase_total_items_price' => $totalPrice,
                        'final_total_price'          => $totalPrice + $unSentOrder->purchase_service_fee + $unSentOrder->purchase_cn_transport_fee + $unSentOrder->purchase_vn_transport_fee + $unSentOrder->purchase_returns_fee
                    ];
                    $unSentOrder->update($updateInfo);
                }

            }

            if ($itemsNoRange->isNotEmpty()) {
                $itemsNoRange = $itemsNoRange->groupBy('shop_name');
                foreach ($itemsNoRange as $shopName => $item) {
                    if ($unSentOrders->where('shop_name', $shopName)->isEmpty()) {
                        $order = $this->create([
                            'customer_id'  => auth('customers')->id(),
                            'order_number' => Helper::generateOrderNR(),
                            'order_type'   => TYPE_ORDER_PURCHASE,
                            'shop_name'    => $shopName
                        ]);
                        $order->purchaseOrderItems()->createMany($item->map(function ($item) use ($currentRate) {
                            $item['current_rate'] = $currentRate;
                            return $item;
                        })->toArray());
                    } else {
                        $unSentOrders->where('shop_name', $shopName)->first()->purchaseOrderItems()->createMany($item->map(function ($item) use ($currentRate) {
                            $item['current_rate'] = $currentRate;
                            return $item;
                        })->toArray());
                    }
                }
            }
        }
    }

    /**
     * Find transport orders
     *
     * @return mixed
     */
    public function findTransportOrders()
    {
        return $this->model->where('customer_id', auth('customers')->id())->where('order_type', TYPE_ORDER_TRANSPORT)->get();
    }

    /**
     * Create transport order
     *
     * @param $data
     * @return $this
     */
    public function createTransportOrder($data)
    {
        foreach ($data['order'] as $value) {
            $order = new Order();
            $order->order_number = Helper::generateOrderNR();
            $order->customer_id = auth('customers')->id();
            $order->transport_receive_type = $data['transport_receive_type'];
            $order->order_type = $data['order_type'];
            $order->status = STATUS_PROCESSING;
            $order->cn_transport_code = $value['cn_transport_code'];
            $order->cn_order_date = $value['cn_order_date'];
            $order->customer_note = $value['customer_note'];
            $order->receiver_info = $value['receiver_info'];
            if (!empty($value['link']) && !empty($value['qty'])) {
                $linkQty = array_map(function ($link, $qty) {
                    return array(
                        'link' => $link,
                        'qty'  => $qty
                    );
                }, $value['link'], $value['qty']);
                $order->link_qty = json_encode($linkQty);
            }
            $order->save();
        }
        return $this;
    }

    /**
     * Update final total price
     *
     * @param $order
     * @return mixed
     */
    public function updateFinalTotalPrice($order)
    {
        $transportOrderItems = $order->transportOrderItem;
        $totalPrice = 0;
        if ($transportOrderItems->count() > VALUE_TYPE_FALSE) {
            $totalPrice =+ $transportOrderItems->pluck('cn_transport_fee')->sum() + $transportOrderItems->pluck('vn_transport_fee')->sum();
        }
        $order->final_total_price = $totalPrice;
        return $order->save();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getTransportOrderWithItem(): LengthAwarePaginator
    {
        return $this->model->filter(new TransportOrderFilter())
            ->byCurrentCustomer()
            ->getTransportOrder()
            ->latest()
            ->with(['transportOrderItem' => function ($query) {
                return $query->latest();
            }])
            ->paginate(config('repository.limit', 5));
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function getFilterTransportOrder($columns = ['*'])
    {
        return $this->model
            ->filter(new TransportOrderFilter())
            ->byCurrentCustomer()
            ->getTransportOrder()
            ->get($columns);
    }
    /**
     * @return mixed
     */
    public function calculateDebt()
    {
        $query = $this->model
            ->filter(new OrderFilter())
            ->where('status', '!=', STATUS_CANCEL)
            ->where(function ($querySub) {
                $querySub->where('order_type', TYPE_ORDER_PURCHASE)->where('status', '>=', STATUS_PRICE_QUOTATION)->orWhere('order_type', TYPE_ORDER_TRANSPORT);
            });
        if (request()->filled('single_date')) {
            $month = explode('/', request()->single_date)[0];
            $year = explode('/', request()->single_date)[1];
            $query->whereMonth('created_at', $month);
            $query->whereYear('created_at', $year);
        }
        if (request()->filled('date_range')) {
            $dataRange = explode('-', request()->date_range);
            $query->whereDate('created_at', '>=', Carbon::parse($dataRange[0])->startOfDay());
            $query->whereDate('created_at', '<=', Carbon::parse($dataRange[1])->endOfDay());
        }
        if (request()->filled('customer_id')) {
            $query->where('customer_id', request()->customer_id);
        }

        return $query->latest()->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function calculateOrderRevenue($data)
    {
        $orders = $this->getOrdersBySupporterAndDate($data);

        $totalItem = 0;
        $totalPriceReturns = 0;
        $totalOrderReturns = 0;
        $totalItemReturns = 0;
        foreach ($orders->get() as $order) {
            if ($order->order_type == TYPE_ORDER_TRANSPORT) {
                $countItem = $order->transportOrderItem->where('status', '<>', STATUS_TRANSPORT_ITEM_CANCEL)->count();
                $totalItem += $countItem;
            }
            if ($order->order_type == TYPE_ORDER_PURCHASE) {
                $orderItems = $order->purchaseOrderItems;

                $totalItem += $orderItems->count();

                $itemsReturns = $orderItems->where('status', STATUS_ORDER_ITEM_RETURNS)->toArray();
                $priceItems = array_reduce($itemsReturns, function ($total, $item) {
                    return $total + $item['qty'] * $item['price'];
                }, 0);
                $totalPriceReturns += $priceItems;

                $countItemReturns = $orderItems->where('status', STATUS_ORDER_ITEM_RETURNS)->count();
                if ($countItemReturns > VALUE_TYPE_FALSE) {
                    $totalOrderReturns += 1;
                    $totalItemReturns += $countItemReturns;
                }
            }
        }
        $totalPrice = $orders->pluck('final_total_price')->sum();

        $revenue = [
            'total_order'         => $orders->count(),
            'total_price'         => number_format($totalPrice),
            'total_item'          => $totalItem,
            'total_price_returns' => number_format($totalPriceReturns),
            'total_order_returns' => $totalOrderReturns,
            'total_item_returns'  => $totalItemReturns,
        ];

        return $revenue;
    }

    /**
     * Export revenue
     * @param $data
     * @param $supporters
     * @return array
     */
    public function exportRevenue($data, $supporters)
    {
        $finalResult = [];

        // Lấy dữ liệu doanh thu từ 1 nhân viên chăm sóc hoặc tất cả nhân viên
        if (isset($data['supporter_id']) && $data['supporter_id'] != null) {
            $finalResult = $this->getRevenueBySupporterId($data['supporter_id'], $data);
        } else {
            foreach ($supporters as $supporter) {
                $result = $this->getRevenueBySupporterId($supporter->id, $data);
                $finalResult = array_merge($finalResult, $result);
            }
        }

        // Tính toán tổng doanh thu, tổng đơn hàng, tổng giá của tất cả nhân viên
        $revenueSupporters = collect($finalResult)->filter(function ($data) {
            return isset($data['total_price']);
        });
        $totalRevenueAllSupporter = [
            'total_price'         => $revenueSupporters->sum('total_price'),
            'total_order'         => $revenueSupporters->sum('total_order'),
            'total_item'          => $revenueSupporters->sum('total_item'),
        ];
        array_unshift($finalResult, $totalRevenueAllSupporter);
        array_unshift($finalResult, trans('excels.title_header_revenue'));

        if (isset($data['single_date']) && $data['single_date'] != null) {
            array_unshift($finalResult, [trans('excels.title_month', ['month' => $data['single_date']])]);
        }
        if (isset($data['date_range']) && $data['date_range'] != null) {
            $startDate = explode('-', $data['date_range'])[0];
            $endDate = explode('-', $data['date_range'])[1];
            array_unshift($finalResult, [trans('excels.title_date_range', ['startDate' => $startDate, 'endDate' => $endDate])]);
        }

        return $finalResult;
    }

    /**
     * @param $supporterId
     * @param $data
     * @return array
     */
    protected function getRevenueBySupporterId($supporterId, $data)
    {
        $result = [];
        $orders = $this->getOrdersBySupporterAndDate($data, $supporterId);

        if($orders->count() === VALUE_TYPE_FALSE) {
            return $result;
        }
        $customer = app('CustomerRepositoryInterface');

        foreach ($orders->get() as $order) {
            $customerName = $customer->getOneById($order->customer_id)->name;
            $order = $order->toArray();
            unset($order['id']);
            unset($order['transport_order_item']);
            unset($order['purchase_order_items']);
            unset($order['order_type']);
            unset($order['customer_id']);
            array_splice($order, 2, 0, $customerName);
            array_push($result, $order);
        }

        // Tính doanh thu của một nhân viên
        $totalAmount = $orders->pluck('final_total_price')->sum();
        $revenueOfSupporter = [
            'total_price'         => $totalAmount,
            'total_order'         => $orders->count(),
        ];
        array_unshift($result, trans('excels.title_order_revenue'));
        array_unshift($result, $revenueOfSupporter);
        array_unshift($result, trans('excels.title_header_revenue'));
        $user = app('UserRepository');
        array_unshift($result, [], [trans('excels.title_employee', ['employee' => $user->getOneById($supporterId)->name])]);

        return $result;
    }

    /**
     * Get orders by supporter and date
     *
     * @param $supporterId
     * @param $data
     * @return mixed
     */
    public function getOrdersBySupporterAndDate($data, $supporterId = null)
    {
        $orders = $this->model->where('status', STATUS_SUCCESS)
            ->select(['id', 'order_number', 'updated_at', 'customer_id', 'order_type', 'final_total_price']);

        if (isset($data['single_date']) && $data['single_date'] != null) {
            $month = explode('/', $data['single_date'])[0];
            $year = explode('/', $data['single_date'])[1];
            $orders = $orders->whereMonth('updated_at', $month)->whereYear('updated_at', $year);
        }

        if (isset($data['date_range']) && $data['date_range'] != null) {
            $startDate = explode('-', $data['date_range'])[0];
            $endDate = explode('-', $data['date_range'])[1];
            $orders = $orders->whereBetween('updated_at', [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()]);
        }

        if (isset($data['supporter_id']) && $data['supporter_id'] != null && $supporterId == null) {
            $orders = $orders->where('supporter_id', $data['supporter_id']);
        }

        if ($supporterId != null) {
            $orders = $orders->where('supporter_id', $supporterId);
        }

        return $orders;
    }

    /**
     * @param         $id
     * @param Request $request
     * @return mixed
     */
    public function cancelOrder($id, Request $request)
    {
        $order = $this->getOneById($id);
        $updateInfo = [
            'status' => STATUS_CANCEL,
        ];
        $order->update($updateInfo);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function calculateDebtByConditions($data)
    {
        $orders = $this->model->newQuery();
        if (isset($data['single_date']) && $data['single_date'] != null) {
            $month = explode('/', $data['single_date'])[0];
            $year = explode('/', $data['single_date'])[1];
            $orders->whereMonth('created_at', $month)->whereYear('created_at', $year);
        }

        if (isset($data['date_range']) && $data['date_range'] != null) {
            $startDate = explode('-', $data['date_range'])[0];
            $endDate = explode('-', $data['date_range'])[1];
            $orders->whereBetween('created_at',
                [Carbon::parse($startDate)->startOfDay(), Carbon::parse($endDate)->endOfDay()]);
        }

        if (isset($data['customer_id']) && $data['customer_id'] != null) {
            $orders->where('customer_id', $data['customer_id']);
        }
        $totalTopup = $orders->first()->customer->customerProfile->remaining_amount ?? 0;
        return [
            'total_order_price' => number_format($orders->pluck('final_total_price')->sum()),
            'total_deposited' => number_format($totalTopup),
            'total_debt' => number_format($orders->pluck('final_total_price')->sum() - $totalTopup),
        ];
    }

    /**
     * Payment debt
     *
     * @param array $customerId
     * @return mixed
     */
    public function paymentDebt($customerId)
    {
        return $this->model->where('customer_id', $customerId)
            ->where('status', STATUS_SUCCESS)
            ->where(function ($query) {
                $query->whereColumn('deposited', '<>', 'final_total_price')
                    ->orWhere('deposited', null);
            })
            ->update(['deposited' => DB::raw('final_total_price')]);
    }

    /**
     * @param $id
     * @return array
     */
    public function getLinkQty($id)
    {
        $orderTransport = $this->getOneById($id);
        return json_decode($orderTransport->link_qty, true);
    }

    /**
     * @param $where
     * @return mixed
     */
    public function findByCustomer($where)
    {
        return $this->model->where($where)->latest()->paginate(config('data.default_paginate'));
    }

    /**
     * @return mixed
     */
    public function findPriceQuotation()
    {
        return $this->model->where('customer_id', auth('customers')->id())->where('status', '>=', STATUS_TRANSPORT_CONFIRMED)->where('status', '<', STATUS_CANCEL)->latest()->paginate(config('data.default_paginate'));
    }

    /**
     * @return mixed
     */
    public function getOrderPriceQuotation()
    {
        return $this->model->where('customer_id', auth('customers')->id())->where('status', '>=', STATUS_TRANSPORT_CONFIRMED)->where('status', '<', STATUS_CANCEL);
    }
}