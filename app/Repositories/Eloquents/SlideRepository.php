<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\SlideRepositoryInterface;

/**
 * Class ExchangeRateRepository
 *
 * @package App\Repositories\Eloquents
 */
class SlideRepository extends BaseRepository implements SlideRepositoryInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\Slide';
    }

    /**
     * Get slide 1688
     *
     * @return mixed
     */
    public function getSlideTaobao()
    {
        return $this->model->where(['group' => TYPE_TAO_BAO, 'is_active' => STATUS_ACTIVE])->orderBy('position')->get();
    }

    /**
     * Get slide 1688
     *
     * @return mixed
     */
    public function getSlide1688()
    {
        return $this->model->where(['group' => TYPE_1688, 'is_active' => STATUS_ACTIVE])->orderBy('position')->get();
    }

    /**
     * Get slide Tmall
     *
     * @return mixed
     */
    public function getSlideTmall()
    {
        return $this->model->where(['group' => TYPE_TMALL, 'is_active' => STATUS_ACTIVE])->orderBy('position')->get();
    }
}