<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\RoleInterface;

class RoleRepository extends BaseRepository implements RoleInterface
{

    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\Role';
    }

    /**
     * Update permission of role
     * @param       $id
     * @param array $permissions
     * @return mixed
     */
    public function updatePermission($id, $permissions = [])
    {
        $role = $this->model->find($id);
        $role->syncPermissions($permissions);
    }
}