<?php

namespace App\Repositories\Eloquents;

use App\Models\TransportOrderItem;
use App\Repositories\Contracts\TransportOrderItemInterface;

class TransportOrderItemRepository extends BaseRepository implements TransportOrderItemInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\TransportOrderItem';
    }

    /**
     * @param $data
     * @param int $orderId
     * @return $this
     */
    public function createItems($data, $orderId)
    {
        foreach ($data as $value) {
            $transportOrderItem = new TransportOrderItem();
            $transportOrderItem->order_id = $orderId;
            $transportOrderItem->cn_transport_code = $value['cn_transport_code'];
            $transportOrderItem->cn_order_date = $value['cn_order_date'];
            $transportOrderItem->customer_note = $value['customer_note'];
            $transportOrderItem->receiver_info = $value['receiver_info'];
            $transportOrderItem->status = STATUS_TRANSPORT_ITEM_NOT_RECEIVE;
            if (!empty($value['link']) && !empty($value['qty'])) {
                $linkQty = array_map(function ($link, $qty) {
                    return array(
                        'link' => $link,
                        'qty'  => $qty
                    );
                }, $value['link'], $value['qty']);
                $transportOrderItem->link_qty = json_encode($linkQty);
            }
            $transportOrderItem->save();
        }
        return $this;
    }

    /**
     * @param $id
     * @return array
     */
    public function getLinkQty($id)
    {
        $orderTransportItem = $this->getOneById($id);
        return json_decode($orderTransportItem->link_qty, true);
    }
}