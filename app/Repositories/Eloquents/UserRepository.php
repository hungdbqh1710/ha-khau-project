<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\UserRepositoryInterface;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function getModelClass(): string
    {
        return 'App\Models\User';
    }

    /**
     * Get all active users
     * @return mixed
     */
    public function getActiveUser()
    {
        return $this->model->active()->get();
    }

}
