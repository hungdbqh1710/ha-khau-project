<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\CustomerLevelInterface;

class CustomerLevelRepository extends BaseRepository implements CustomerLevelInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\CustomerLevel';
    }

    /**
     * @return mixed
     */
    public function getMinimumDepositByCustomer($customer)
    {
        $levels = $this->model->latest('spent_money_from')->get();
        return $levels->where('spent_money_from', '<=', $customer->totalSpentMoney())
                        ->first()
                        ->deposit ?? DEFAULT_DEPOSIT;
    }
}