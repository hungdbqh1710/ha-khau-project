<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\WebsiteConfigRepositoryInterface;

class WebsiteConfigRepository extends BaseRepository implements WebsiteConfigRepositoryInterface
{
    public function getModelClass(): string
    {
        return 'App\Models\WebsiteConfig';
    }

    /**
     * Get payment info
     *
     * @param $typePayment
     * @return mixed
     */
    public function getPaymentInfo($typePayment)
    {
        return $this->model->where('name', $typePayment)->first();
    }
}
