<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\DeliveryAddressInterface;

/**
 * Class DeliveryAddressRepository
 *
 * @package App\Repositories\Eloquents
 */
class DeliveryAddressRepository extends BaseRepository implements DeliveryAddressInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\DeliveryAddress';
    }

    /**
     * Create delivery address
     *
     * @param $data
     * @return mixed
     */
    public function createDeliveryAddress($data)
    {
        if ((auth('customers')->user()->deliveryAddress)->count() == VALUE_TYPE_FALSE) {
            $data['is_default'] = VALUE_TYPE_TRUE;
        }
        $data['customer_id'] = auth('customers')->id();

        return $this->create($data);
    }

    /**
     * Update delivery address
     *
     * @param $request
     * @return mixed
     */
    public function updateDeliveryAddress($request)
    {
        $data = $request->except('_token');

        return $this->model->where('id', $data['id'])->update($data);
    }

    /**
     * Update delivery address default
     *
     * @param $id
     * @return mixed
     */
    public function updateAddressDefault($id)
    {
        $defaultAddress = $this->model->where(
            [
                'is_default'  => VALUE_TYPE_TRUE,
                'customer_id' => auth('customers')->id()
            ]
        )->first();
        $defaultAddress->is_default = VALUE_TYPE_FALSE;
        $defaultAddress->save();

        $deliveryAddress = $this->getOneById($id);
        $deliveryAddress->is_default = VALUE_TYPE_TRUE;
        $deliveryAddress->save();
    }

    /** find default delivery address of customer
     * @return mixed
     */
    public function findDefaultDeliveryAddress()
    {
        return auth('customers')->user()->getDefaultDeliveryAddress();
    }
}