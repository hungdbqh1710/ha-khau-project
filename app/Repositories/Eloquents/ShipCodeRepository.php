<?php

namespace App\Repositories\Eloquents;

use App\Helper;
use App\Models\ShipCodes;
use App\Models\Order;
use App\Models\PurchaseOrderItem;
use App\Repositories\Contracts\ShipCodeInterface;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\Filters\ShippingFilter;
use Illuminate\Support\Facades\DB;

/**
 * Class ShipCodeRepository
 *
 * @package App\Repositories\Eloquents
 */
class ShipCodeRepository extends BaseRepository implements ShipCodeInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return 'App\Models\ShipCodes';
    }
    
    /**
     * @return Object
     */
    public function index($per_page)
    {
        return $this->model->orderBy('created_at', 'DESC')->paginate($per_page ? $per_page : config('data.default_paginate'));
    }
    
    /**
     * Function search ship code
     * @return Object
     */
    public function search($search_content){
        $aColumns = array( 'ship_code', 'buy_code', 'package_code', 'weight', 'size', 'status', 'admin_note', 'price', 'total_price', 'cn_transport_fee', 'exchange_rates');
        $query = ShipCodes::select('*');
        foreach($aColumns as $column)
        {
            $query->orWhere($column, 'LIKE', '%'.$search_content.'%');
        }
        $models = $query->paginate(config('data.default_paginate'));
        return $models;
    }
    /**
     * Get all ship code not paginate
     * @return Object
     */
    public function getAllNotPaginate(){
        return $this->model->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Create or Update shipping
     */
    public function createOrUpdate($request){
        $rq = json_decode($request->data);
        $ship_code = $rq->ship_code;
        $check_buy_code = PurchaseOrderItem::where(['cn_transport_code' => $ship_code])->first();
        $check_package_code = Order::where(['cn_transport_code' => $ship_code])->first();
        $shipping = $this->model->where(['ship_code' => $ship_code])->first();
        if(!$shipping && !$check_buy_code && !$check_package_code){
            // update shipping after 10s
            return $this->createShipping($ship_code);
        }
        else{
            // create new shipping
            $check_code = $check_buy_code ? $check_buy_code : $check_package_code;
            return $this->updateShipping($check_code, $rq);
        }
    }

    /**
     * Create Shipping with new ship-code
     */
    public function createShipping($code){
        $exchangeRateRepository = app('ExchangeRateRepositoryInterface');
        $currentRate = $exchangeRateRepository->findActiveExchangeRate()->getCurrentRate();
        $this->model->create([
            'ship_code' => $code,
            'status' => 'in-cn',
            'exchange_rates' => $currentRate,
            'total_price' => 0,
        ]);
        return true;
    }

    /**
     * Update ship-code has existed
     */
    public function updateShipping($typeOrder, $request){
        $ship_code = $request->ship_code;
        $shipping = $this->model->where(['ship_code' => $ship_code])->first();
        if(!$shipping){
            $exchangeRateRepository = app('ExchangeRateRepositoryInterface');
            $currentRate = $exchangeRateRepository->findActiveExchangeRate()->getCurrentRate();
            $package_code = Order::where('cn_transport_code', $ship_code)->first();
            $this->model->create([
                'ship_code' => $ship_code,
                'buy_code' => isset($typeOrder->order) ? $typeOrder->order->order_number : '',
                'package_code' => isset($package_code) ? $package_code->order_number : '',
                'exchange_rates' => $currentRate,
                'total_price' => 0,
                'status'    => 'in-cn',
            ]);
        }else{
            $begin = Carbon::parse($shipping->updated_at);
            $end = $begin->addMinutes(10);
            $now = Carbon::now();
            if($now->gte($end) && $request->type == 'update-status'){
                if($shipping->status == 'out-vn'){
                    return 'out-vn';
                }else{
                    $shipping->update([
                        'status' => Helper::changeStatusShipping($shipping->status),
                    ]);
                }
            }
            else{
                if($request->type == 'not-update-status'){
                    $total = $shipping->total_price;
                    $buy_code_old = $shipping->buy_code ? $shipping->buy_code : '';
                    $package_code_old = $shipping->package_code ? $shipping->package_code : '';
                    $weight = (float) $request->weight;
                    $price = (float) $request->price;
                    $exchange_rates = (float) $request->exchange_rates;
                    $cn_transport_fee = (float) $request->cn_transport_fee;
                    $total_price = $weight * $price + $cn_transport_fee * $exchange_rates;
                    $shipping->update([
                        'weight' => $request->weight ? $request->weight : '' ,
                        'price' => $request->price ? $request->price : '' ,
                        'exchange_rates' => $request->exchange_rates ? $request->exchange_rates : '' ,
                        'package_code' => $request->package_code ? $request->package_code : '' ,
                        'buy_code' => $request->buy_code ? $request->buy_code : '' ,
                        'cn_transport_fee' => $request->cn_transport_fee ? $request->cn_transport_fee : '' ,
                        'total_price' => $total_price,
                        'customer_id' => $request->customer_id,
                        'size' => $request->size,
                        'admin_note' => $request->admin_note
                    ]);
                    if($request->buy_code != ''){
                        $purchase_cn_to_vn_fee = 0;
                        $help_buy_orders = $this->model->where(['buy_code' => $request->buy_code])->get();
                        if($help_buy_orders){
                            foreach ($help_buy_orders as $index => $order) {
                                $purchase_cn_to_vn_fee += $order->total_price;
                            }
                            $order = Order::where(['order_number' => $request->buy_code])->first();
                            if($order){
                                $final_total_price = $order->final_total_price + $purchase_cn_to_vn_fee;
                                $order->update([
                                    'purchase_cn_to_vn_fee' => $purchase_cn_to_vn_fee,
                                    'final_total_price' => $final_total_price
                                ]);
                            }
                            if($request->buy_code != $buy_code_old){
                                $order = Order::where(['order_number' => $buy_code_old])->first();
                                if($order){
                                    $order->update([
                                        'purchase_cn_to_vn_fee' => $order->purchase_cn_to_vn_fee - $total,
                                        'final_total_price' => $order->final_total_price - $total
                                    ]);
                                }
                            }
                        }else{
                            $order = Order::where(['order_number' => $buy_code_old])->first();
                            if($order){
                                $order->update([
                                    'purchase_cn_to_vn_fee' => $order->purchase_cn_to_vn_fee - $total,
                                    'final_total_price' => $order->final_total_price - $total
                                ]);
                            }
                        }
                    }else{
                        $order = Order::where(['order_number' => $buy_code_old])->first();
                        if($order){
                            $order->update([
                                'purchase_cn_to_vn_fee' => $order->purchase_cn_to_vn_fee - $total,
                                'final_total_price' => $order->final_total_price - $total
                            ]);
                        }
                    }
                    if($request->package_code != ''){
                        $via_border_transport_fee = 0;
                        $trans_orders = $this->model->where(['package_code' => $request->package_code])->get();
                        if($trans_orders){
                            foreach ($trans_orders as $index => $order) {
                                $via_border_transport_fee += $order->total_price;
                            }
                            $order = Order::where(['order_number' => $request->package_code])->first();
                            if($order){
                                $final_total_price = $order->final_total_price + $via_border_transport_fee;
                                $order->update([
                                    'via_border_transport_fee' => $via_border_transport_fee,
                                    'final_total_price' => $final_total_price
                                ]);
                            }
                            if($request->package_code != $package_code_old){
                                $order = Order::where(['order_number' => $package_code_old])->first();
                                if($order){
                                    $order->update([
                                        'via_border_transport_fee' => $order->via_border_transport_fee - $total,
                                        'final_total_price' => $order->final_total_price - $total
                                    ]);
                                }
                            }
                        }else{
                            $order = Order::where(['order_number' => $package_code_old])->first();
                            if($order){
                                $order->update([
                                    'via_border_transport_fee' => $order->via_border_transport_fee - $total,
                                    'final_total_price' => $order->final_total_price - $total
                                ]);
                            }
                        }
                    }else{
                        $order = Order::where(['order_number' => $package_code_old])->first();
                        if($order){
                            $order->update([
                                'via_border_transport_fee' => $order->via_border_transport_fee - $total,
                                'final_total_price' => $order->final_total_price - $total
                            ]);
                        }
                    }
                }else{
                    return 'fail';
                }
           }
        }
        return 'success';
    }

    /**
     * Get all with filters
     */
    public function getAllWithFilter( $order_status, $customer_id, $from, $to){
        if($order_status != ''){
            return $this->model
            ->where(function($query) use ($customer_id)  {
                if($customer_id) {
                    $query->where('customer_id', $customer_id);
                }
             })
            ->where('status', $order_status)->whereBetween('updated_at', [$from, $to])->orderBy('created_at', 'DESC')->paginate(config('data.default_paginate'));
        }
        else{
            return $this->model
            ->where(function($query) use ($customer_id)  {
                if($customer_id) {
                    $query->where('customer_id', $customer_id);
                }
            })
            ->whereBetween('updated_at', [$from, $to])->orderBy('created_at', 'DESC')->paginate(config('data.default_paginate'));
        }
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function getFilterShipping($columns = ['*'])
    {
        return $this->model
            ->filter(new ShippingFilter())
            ->get($columns);
    }

    public function findByShipCode($ship_code){
        return $this->model->where(['ship_code' => $ship_code])->get();
    }

    public function deleteShipCode($id){
        $this->model->destroy($id);
    }
}