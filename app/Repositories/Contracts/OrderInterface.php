<?php

namespace App\Repositories\Contracts;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Interface OrderTempInterface
 *
 * @package App\Repositories\Contracts
 */
interface OrderInterface extends RepositoryInterface
{
    /**
     * @return mixed
     */
    public function findUnSentOrder() :Collection;

    /**
     * @return LengthAwarePaginator
     */
    public function getOrderWithItem() :LengthAwarePaginator;

    /**
     * @param Request $request
     * @return mixed
     */
    public function createOrder(Request $request);

    /**
     * @param array $columns
     * @return mixed
     */
    public function getFilterOrder($columns = ['*']);

    /**
     * @param         $id
     * @param Request $request
     * @return mixed
     */
    public function sendOrder($id, Request $request);

    /**
     * @return mixed
     */
    public function findTransportOrders();

    /**
     * @param $data
     * @return mixed
     */
    public function createTransportOrder($data);

    /**
     * @param $order
     * @return mixed
     */
    public function updateFinalTotalPrice($order);

    /**
     * @return LengthAwarePaginator
     */
    public function getTransportOrderWithItem() :LengthAwarePaginator;

    /**
     * @param array $columns
     * @return mixed
     */
    public function getFilterTransportOrder($columns = ['*']);

    /**
     * @return mixed
     */
    public function calculateDebt();

    /**
     * @param $data
     * @return mixed
     */
    public function calculateOrderRevenue($data);

    /**
     * @param $data
     * @param $supporters
     * @return mixed
     */
    public function exportRevenue($data, $supporters);

    /**
     * @param array $data
     * @return mixed
     */
    public function calculateDebtByConditions($data);

    /**
     * @param array $customerId
     * @return mixed
     */
    public function paymentDebt($customerId);

    /**
     * @param integer $id
     * @return mixed
     */
    public function getLinkQty($id);

    /**
     * @param $where
     * @return mixed
     */
    public function findByCustomer($where);
}
