<?php

namespace App\Repositories\Contracts;

/**
 * Interface BlogInterface
 *
 * @package App\Repositories\Contracts
 */
interface TransactionHistoryInterface extends RepositoryInterface
{
    /**
     * @param array $where
     * @return mixed
     */
    public function findByCustomer(array $where);

    /**
     * @param id
     * @return mixed
     */
    public function findByCustomerId($id);
}
