<?php

namespace App\Repositories\Contracts;

interface SlideRepositoryInterface extends RepositoryInterface
{
    /**
     * @return mixed
     */
    public function getSlideTaobao();

    /**
     * @return mixed
     */
    public function getSlide1688();

    /**
     * @return mixed
     */
    public function getSlideTmall();
}
