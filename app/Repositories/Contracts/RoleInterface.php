<?php

namespace App\Repositories\Contracts;

interface RoleInterface extends RepositoryInterface
{
    /**
     * Update permission of role
     * @param       $id
     * @param array $permissions
     * @return mixed
     */
    public function updatePermission($id, $permissions = []);
}