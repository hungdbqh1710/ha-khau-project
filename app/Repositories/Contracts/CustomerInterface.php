<?php

namespace App\Repositories\Contracts;

/**
 * Interface CustomerInterface
 *
 * @package App\Repositories\Contracts
 */
interface CustomerInterface extends RepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function updateCustomerAndProfile($data);

    /**
     * @param $data
     * @return mixed
     */
    public function updatePassword($data);

    /**
     * @param string $phoneNumber
     * @return mixed
     */
    public function findByPhoneNumber($phoneNumber);

    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail($email);

    /**
     * @return mixed
     */
    public function getOrderSuccess();

    /**
     * @return mixed
     */
    public function getByStatus($status);
    
}
