<?php

namespace App\Repositories\Contracts;

interface NotificationRepositoryInterface extends RepositoryInterface
{
    /**
     * Update read_at column
     * @return mixed
     */
    public function markAsRead($id);

    /**
     * Get all notification of customer
     * @return mixed
     */
    public function getFilterCustomerNotification();
}
