<?php

namespace App\Repositories\Contracts;

/**
 * Interface BlogInterface
 *
 * @package App\Repositories\Contracts
 */
interface BlogInterface extends RepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function storeBlog($data);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateBlog($id, $data);
}
