<?php

namespace App\Repositories\Contracts;

/**
 * Interface ExchangeRateInterface
 *
 * @package App\Repositories\Contracts
 */
interface ExchangeRateInterface extends RepositoryInterface
{
    /** Get active exchange rate
     * @return mixed
     */
    public function findActiveExchangeRate();
}
