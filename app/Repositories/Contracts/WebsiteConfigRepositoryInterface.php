<?php

namespace App\Repositories\Contracts;

interface WebsiteConfigRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $typePayment
     * @return mixed
     */
    public function getPaymentInfo($typePayment);
}
