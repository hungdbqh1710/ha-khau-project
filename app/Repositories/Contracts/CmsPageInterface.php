<?php

namespace App\Repositories\Contracts;

/**
 * Interface CmsPageInterface
 *
 * @package App\Repositories\Contracts
 */
interface CmsPageInterface extends RepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function storeCmsPage($data);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateCmsPage($id, $data);

    /**
     * @param array $attribute
     * @return mixed
     */
    public function firstOrFail(array $attribute);
}
