<?php

namespace App\Repositories\Contracts;

/**
 * Interface DeliveryAddressInterface
 *
 * @package App\Repositories\Contracts
 */
interface DeliveryAddressInterface extends RepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function createDeliveryAddress($data);

    /**
     * @param $data
     * @return mixed
     */
    public function updateDeliveryAddress($data);

    /**
     * @param $id
     * @return mixed
     */
    public function updateAddressDefault($id);

    /** Get default delivery address
     * @return mixed
     */
    public function findDefaultDeliveryAddress();
}
