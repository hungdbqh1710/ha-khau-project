<?php

namespace App\Repositories\Contracts;

/**
 * Interface OrderTempInterface
 *
 * @package App\Repositories\Contracts
 */
interface OrderTempInterface extends RepositoryInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function createProduct(array $data);
}
