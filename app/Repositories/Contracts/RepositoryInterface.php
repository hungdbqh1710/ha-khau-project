<?php

namespace App\Repositories\Contracts;

use Illuminate\Support\Collection;

interface RepositoryInterface
{
    /**
     * @return string
     */
    public function getModelClass(): string;
    /**
     * @param int|string $id
     */
    public function getOneById($id);

    /**
     * @param int[]|string[] $ids
     * @return Collection
     */
    public function getByIds(array $ids): Collection;

    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * @param       $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, array $attributes);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param null  $limit
     * @param array $columns
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*']);

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function findByField($field, $value, $columns = ['*']);

    /**
     * @param $where
     * @return mixed
     */
    public function findWhere($where);
}
