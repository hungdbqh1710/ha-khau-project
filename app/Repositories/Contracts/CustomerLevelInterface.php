<?php

namespace App\Repositories\Contracts;

interface CustomerLevelInterface extends RepositoryInterface
{
    /**
     * @param $customer
     * @return mixed
     */
    public function getMinimumDepositByCustomer($customer);
}