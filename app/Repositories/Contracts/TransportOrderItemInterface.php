<?php

namespace App\Repositories\Contracts;

interface TransportOrderItemInterface extends RepositoryInterface
{
    /**
     * @param $data
     * @param integer $orderId
     * @return mixed
     */
    public function createItems($data, $orderId);

    /**
     * @param $id
     * @return mixed
     */
    public function getLinkQty($id);
}