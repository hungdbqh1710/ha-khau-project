const ORDER_PROCESSING_ENDPOINT = 'https://hakhau.localx/orders';
const DOMAIN_PATTERN_ITEM_TAOBAO = 'item.taobao.com';
const DOMAIN_PATTERN_WORLD_TAOBAO = 'world.taobao.com';
const DOMAIN_PATTERN_ITEM_TMALL = 'tmall';
const DOMAIN_PATTERN_ITEM_1688 = '1688';