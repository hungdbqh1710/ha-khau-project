var url = window.location.href;
var domain = window.location.host;
var detail = '';
var r = true;
getProductInfo(r);

function getProductInfo(r) {
    if (domain.match(DOMAIN_PATTERN_ITEM_TAOBAO)) {
        orderFromItemTaobao();
    } else if (domain.match(DOMAIN_PATTERN_WORLD_TAOBAO)) {
        orderFromWorldTaobao();
    } else if (domain.match(DOMAIN_PATTERN_ITEM_TMALL)) {
        orderFromTmall();
    } else if (domain.match(DOMAIN_PATTERN_ITEM_1688)) {
        orderFrom1688();
    } else {
        window.open(ORDER_PROCESSING_ENDPOINT);
    }

    function orderFromItemTaobao() {
        var flag = false;
        $("script").each(function () {
            var script = $(this).text();
            if (script.indexOf('KISSY.merge') > -1) {
                string = script;
                flag = true;
            }
            if (script.indexOf('shopName') > -1) {
                string2 = script;
            }
            if (script.indexOf('skuMap') > -1) {
                string3 = script;
            }
        });
        if (flag) {
            var start = string.indexOf('g_config,') + 9;
            var end = string.indexOf(');');
            var g_config = string.substring(start, end);
            var find = '\'';
            var re = new RegExp(find, 'g');
            g_config = g_config.replace(re, '\"');
            var array_rep = [
                'domain',
                'sibApi',
                'skuInfo',
                'skuMap',
                'locale',
                'site',
                'hngarea',
                'baseUrl',
                'stdPrices',
                'range',
                'price',
                'thumb',
                'video',
                'apiItem',
                'apiItemInfo',
                'cartUrl',
                'toCartUrl',
                'segment_config',
                'idsMod',
                'currency',
                'targetCurrency',
                'baseCurrency',
                'fractionDigits',
                'symbol',
                'format',
                'url',
                'sns',
                'itemId',
                'spurl',
                'support',
                'csGroupUrl',
                'insurance',
                'insuranceUrl',
                'counter',
                'itemId',
                'collectCountUrl',
                'reviewCountUrl',
                'buyInfo',
                'mobile',
                'priceUrl',
                'fav',
                'title',
                'shopTitle',
                'logoUrl',
                'creditLevel',
                'thumb',
                'itemRecommendUrl',
                'shopRecommendUrl',
                'sidebarCoupon',
                'url',
                'itemTitle',
                'itemMainPic',
                'nick',
                'sellerId',
                'userGuidInfo',
                'url',
                'quickExp',
                'promotags',
                'preSale',
                'bigPromo',
                'coupon',
                'applyShopUrl',
                'itemId',
                'recommend',
                'greetingUrl',
                'crossCatUrl',
                'crossSameCatUrl',
                'cartUrl',
                'buyTogetherUrl',
                'idata',
                'item',
                'tka',
                'activity',
                'success',
                'start',
                'pre',
                'end',
                'specPrice',
                'ju',
                'stepPre',
                'exchangerateUrl',
                'overseaDeliveryUrl',
                'deliveryFee',
                'overseaNewDeliveryUrl',
                'newDeliveryFee',
                'priceCutUrl',
                'DyBase',
                'iurl',
                'promoCombo',
                'mjsAndMealUrl',
                'mealPageDataUrl',
                'mjsPageDataUrl',
                'descUrl',
                'descUrlSSL',
                'videoUrl',
                'asyncUrl',
                'min',
                'max',
            ];
            $.each(array_rep, function (i, val) {
                g_config = g_config.replace(val + ':', '\"' + val + '\":');
            });
            g_config = g_config.replace('priceUrl', '\"priceUrl\"');
            var detail = $.parseJSON(g_config);

            image = [];
            $('#J_ThumbNav').find('li').each(function () {
                link = $(this).find('img').attr('src');
                image.push(link);
            })

        } else {
            eval(string2);
            var detail = g_config;
            if (typeof string3 !== "undefined") {
                var start = string3.indexOf('skuMap     : ') + 13;
                var end = string3.indexOf('}}') + 3;
                var skumap = string3.substring(start, end);

                var f = '\'';
                var r = new RegExp(f, 'g');
                skumap = skumap.replace(r, '\"');

                var array_rep = [
                    'domain',
                    'sibApi',
                    'skuInfo',
                    'skuMap',
                    'locale',
                    'site',
                    'hngarea',
                    'baseUrl',
                    'stdPrices',
                    'range',
                    'price',
                    'thumb',
                    'video',
                    'apiItem',
                    'apiItemInfo',
                    'cartUrl',
                    'toCartUrl',
                    'segment_config',
                    'idsMod',
                    'currency',
                    'targetCurrency',
                    'baseCurrency',
                    'fractionDigits',
                    'symbol',
                    'format',
                    'url',
                    'sns',
                    'itemId',
                    'spurl',
                    'support',
                    'csGroupUrl',
                    'insurance',
                    'insuranceUrl',
                    'counter',
                    'itemId',
                    'collectCountUrl',
                    'reviewCountUrl',
                    'buyInfo',
                    'mobile',
                    'priceUrl',
                    'fav',
                    'title',
                    'shopTitle',
                    'logoUrl',
                    'creditLevel',
                    'thumb',
                    'itemRecommendUrl',
                    'shopRecommendUrl',
                    'sidebarCoupon',
                    'url',
                    'itemTitle',
                    'itemMainPic',
                    'nick',
                    'sellerId',
                    'userGuidInfo',
                    'url',
                    'quickExp',
                    'promotags',
                    'preSale',
                    'bigPromo',
                    'coupon',
                    'applyShopUrl',
                    'itemId',
                    'recommend',
                    'greetingUrl',
                    'crossCatUrl',
                    'crossSameCatUrl',
                    'cartUrl',
                    'buyTogetherUrl',
                    'idata',
                    'item',
                    'tka',
                    'activity',
                    'success',
                    'start',
                    'pre',
                    'end',
                    'specPrice',
                    'ju',
                    'stepPre',
                    'exchangerateUrl',
                    'overseaDeliveryUrl',
                    'deliveryFee',
                    'overseaNewDeliveryUrl',
                    'newDeliveryFee',
                    'priceCutUrl',
                    'DyBase',
                    'iurl',
                    'promoCombo',
                    'mjsAndMealUrl',
                    'mealPageDataUrl',
                    'mjsPageDataUrl',
                    'descUrl',
                    'descUrlSSL',
                    'videoUrl',
                    'asyncUrl',
                    'min',
                    'max',
                ];
                $.each(array_rep, function (i, val) {
                    skumap = skumap.replace(val + ':', '\"' + val + '\":');
                });
                skumap = skumap.replace('priceUrl', '\"priceUrl\"');
                var skumap = $.parseJSON(skumap);

                detail.skuInfo = {skuMap: skumap};
            }

            image = [];
            $('#J_UlThumb').find('li').each(function () {
                link = $(this).find('img').attr('src');
                image.push(link);
            })
        }

        var item = [];
        $('#J_isku').find('dl.tb-prop').each(function (key, value) {
            name = $(this).find('dt').text();
            item[key] = [];
            $(this).find('li').each(function () {
                if ($(this).attr('data-value') !== '') {
                    item[key].push([$(this).attr('data-value')]);
                }
            })
        })

        properties_name = [];
        $('#J_isku').find('dl.tb-prop').each(function (key, value) {
            name = $(this).find('dt').text();
            $(this).find('li').each(function () {
                properties_name.push([$(this).attr('data-value'), name + ':' + $(this).find('span').text()]);
            })
        })

        prop_image = [];
        $('#J_isku').find('dl.tb-prop').each(function (key, value) {
            name = $(this).find('dt').text();
            $(this).find('li').each(function () {
                prop_image.push([$(this).attr('data-value'), $(this).find('a').attr('style')]);
            })
        })

        var selected_prop_tb = [];
        $('.tb-skin li.tb-selected').each(function () {
            selected_prop_tb.push($(this).attr('data-value'));
        });

        var optionTb = $('.tb-skin .tb-prop ul').length;
        var optionSelectedTb = $('.tb-skin .tb-prop ul .tb-selected').length;

        if (optionTb > optionSelectedTb) {
            alert('Bạn chưa chọn đầy đủ thuộc tính sản phẩm');
            return;
        }

        var propertySelectedTB = []; // Thuộc tính của sp đã chọn
        $.each(properties_name, function (key, value) {
            if (selected_prop_tb.includes(value[0])) {
                propertySelectedTB.push(value[1]);
            }
        });

        var qty = $('#J_IptAmount').val();
        var selected_image = $('#J_ImgBooth').attr('src');

        var str_price = $('#J_StrPrice em.tb-rmb-num').text()
        var promo_price = $('#J_PromoPriceNum').text();
        var price = '';
        if (typeof str_price === 'undefined') {
            price = promo_price;
        } else if (typeof str_price !== 'undefined' && promo_price !== '') {
            price = promo_price;
        } else {
            price = str_price;
        }

        var shop_name = '';
        shop_name = getShopNameTaobao();

        data = detail;
        data.item = item;
        data.properties_name = properties_name;
        data.image = image;
        data.prop_image = prop_image;
        data.selected = {
            qty: qty,
            product_image: selected_image,
            price: price,
            product_name: detail.idata.item.title,
            product_link: window.location.href,
            shop_name: shop_name,
            property: propertySelectedTB
        }

        if (data.descUrlSSL === undefined) {
            var url_desc = 'https:' + data.descUrl;
        } else {
            var url_desc = 'https:' + data.descUrlSSL;
        }

        function setDesc(desc) {
            data.nh_desc = desc;
        }

        sendDataToHaKhau(data);

        function getPromoTaobao() {
            if (data.sibApi === undefined) {
                var url = 'https:' + data.sibUrl;
            } else {
                var url = 'https:' + data.sibApi;
            }
            jQuery.ajax({
                url: url,
                dataType: 'html',
                beforeSend: function () {
                },
                success: function (data) {
                    sendTaobao(data);
                }
            });
        }

        function sendTaobao(dataPromo) {
            data.promo = JSON.stringify(dataPromo);
            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: ORDER_PROCESSING_ENDPOINT + '/parse-product/taobao-china',
                data: {
                    data: JSON.stringify(data)
                },
                beforeSend: function () {
                },
                success: function (data) {
                    if (r) {
                        window.open(data.url);
                    } else {
                        window.open(ORDER_PROCESSING_ENDPOINT + '/tai-khoan/danh-sach-don-hang?urlCrawl=' + encodeURIComponent(url));
                    }
                },
                complete: function () {
                    $('.ha-khau-loading').hide();
                }
            });
        }
    }

    function orderFromWorldTaobao() {
        var flag = false;
        $("script").each(function () {
            var script = $(this).text();
            if (script.indexOf('KISSY.merge') > -1) {
                string = script;
                flag = true;
            }
            if (script.indexOf('shopName') > -1) {
                string2 = script;
            }
        });
        if (flag) {
            var start = string.indexOf('g_config,') + 9;
            var end = string.indexOf(');');
            var g_config = string.substring(start, end);
            var find = '\'';
            var re = new RegExp(find, 'g');
            g_config = g_config.replace(re, '\"');
            var array_rep = [
                'domain',
                'sibApi',
                'skuInfo',
                'skuMap',
                'locale',
                'site',
                'hngarea',
                'baseUrl',
                'stdPrices',
                'range',
                'price',
                'thumb',
                'video',
                'apiItem',
                'apiItemInfo',
                'cartUrl',
                'toCartUrl',
                'segment_config',
                'idsMod',
                'currency',
                'targetCurrency',
                'baseCurrency',
                'fractionDigits',
                'symbol',
                'format',
                'url',
                'sns',
                'itemId',
                'spurl',
                'support',
                'csGroupUrl',
                'insurance',
                'insuranceUrl',
                'counter',
                'itemId',
                'collectCountUrl',
                'reviewCountUrl',
                'buyInfo',
                'mobile',
                'priceUrl',
                'fav',
                'title',
                'shopTitle',
                'logoUrl',
                'creditLevel',
                'thumb',
                'itemRecommendUrl',
                'shopRecommendUrl',
                'sidebarCoupon',
                'url',
                'itemTitle',
                'itemMainPic',
                'nick',
                'sellerId',
                'userGuidInfo',
                'url',
                'quickExp',
                'promotags',
                'preSale',
                'bigPromo',
                'coupon',
                'applyShopUrl',
                'itemId',
                'recommend',
                'greetingUrl',
                'crossCatUrl',
                'crossSameCatUrl',
                'cartUrl',
                'buyTogetherUrl',
                'idata',
                'item',
                'tka',
                'activity',
                'success',
                'start',
                'pre',
                'end',
                'specPrice',
                'ju',
                'stepPre',
                'exchangerateUrl',
                'overseaDeliveryUrl',
                'deliveryFee',
                'overseaNewDeliveryUrl',
                'newDeliveryFee',
                'priceCutUrl',
                'DyBase',
                'iurl',
                'promoCombo',
                'mjsAndMealUrl',
                'mealPageDataUrl',
                'mjsPageDataUrl',
                'descUrl',
                'descUrlSSL',
                'videoUrl',
                'asyncUrl',
                'min',
                'max',
                'rateSum'
            ];
            $.each(array_rep, function (i, val) {
                g_config = g_config.replace(val + ':', '\"' + val + '\":');
            });
            g_config = g_config.replace('priceUrl', '\"priceUrl\"');
            var detail = $.parseJSON(g_config);

            image = [];
            $('#J_ThumbNav').find('li').each(function () {
                link = $(this).find('img').attr('src');
                image.push(link);
            })

        } else {
            eval(string2);
            var detail = g_config;

            image = [];
            $('#J_UlThumb').find('li').each(function () {
                link = $(this).find('img').attr('src');
                image.push(link);
            })
        }

        var item = [];
        $('#J_SKU').find('dl').each(function (key, value) {
            name = $(this).find('dt').text();
            item[key] = [];
            $(this).find('li').each(function () {
                item[key].push([$(this).attr('data-pv')]);
            })
        })

        properties_name = [];
        $('#J_SKU').find('dl').each(function (key, value) {
            name = $(this).find('dt').text();
            $(this).find('li').each(function () {
                properties_name.push([$(this).attr('data-pv'), name + ':' + $(this).find('span').text()]);
            })
        })

        prop_image = [];
        $('#J_SKU').find('dl').each(function (key, value) {
            name = $(this).find('dt').text();
            $(this).find('li').each(function () {
                prop_image.push([$(this).attr('data-pv'), $(this).find('a').attr('style')]);
            })
        })

        data = detail;
        data.item = item;
        data.properties_name = properties_name;
        data.image = image;
        data.prop_image = prop_image;

        if (data.descUrlSSL === undefined) {
            var url_desc = 'https:' + data.descUrl;
        } else {
            var url_desc = 'https:' + data.descUrlSSL;
        }

        function setDesc(desc) {
            data.nh_desc = desc;
        }

        jQuery.ajax({
            url: url_desc,
            beforeSend: function () {
                $('.ha-khau-loading').show();
            },
            success: function (data) {
                setDesc(data);
                getPromoTaobao();
            }
        });

        function getPromoTaobao() {
            if (data.sibApi === undefined) {
                var url = 'https:' + data.sibUrl;
            } else {
                var url = 'https:' + data.sibApi;
            }
            jQuery.ajax({
                url: url,
                dataType: 'html',
                beforeSend: function () {
                },
                success: function (data) {
                    sendTaobao(data);
                }
            });
        }

        function sendTaobao(dataPromo) {
            data.promo = dataPromo;
            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: ORDER_PROCESSING_ENDPOINT + '/parse-product/taobao',
                data: {
                    data: JSON.stringify(data)
                },
                beforeSend: function () {
                },
                success: function (data) {
                    if (r) {
                        window.open(data.url);
                    } else {
                        window.open(ORDER_PROCESSING_ENDPOINT + '/tai-khoan/danh-sach-don-hang?urlCrawl=' + encodeURIComponent(url));
                    }
                },
                complete: function () {
                    $('.ha-khau-loading').hide();
                }
            });
        }
    }

    function orderFromTmall() {
        $("script").each(function () {
            var script = $(this).text();
            if (script.indexOf('TShop.Setup') > -1) {
                detail = script;
            }
            if (script.indexOf("url='//mdskip.taobao.com/core/initItemDetail") > -1) {
                string_promo = script;
            }
        });
        var properties_name = [];
        $('.tb-skin .tb-sku').find('dl.tb-prop').each(function (key, value) {
            name = $(this).find('dt').text();
            $(this).find('li').each(function () {
                properties_name.push([$(this).attr('data-value'), name + ':' + $(this).find('span').text()]);
            })
        })

        var image = [];
        $('#J_UlThumb').find('li').each(function () {
            link = $(this).find('img').attr('src');
            image.push(link);
        });


        var prop_image = [];
        $('.tb-skin .tb-sku').find('dl').each(function (key, value) {
            name = $(this).find('dt').text();
            $(this).find('li').each(function () {
                prop_image.push([$(this).attr('data-value'), $(this).find('a').attr('style')]);
            })
        })

        var shop_name = $('.sea-icon-tmall').parent().text();
        if (shop_name == '') shop_name = $('.shopLink').text();
        if (shop_name == '') shop_name = $('.slogo-shopname strong').text();
        if (shop_name == '') shop_name = $('.seller_nickname').attr('value');

        var selected_prop = [];
        $('.tb-wrap li.tb-selected').each(function () {
            selected_prop.push($(this).attr('data-value'));
        });

        var numberOption = $('.tb-sku .tm-sale-prop ul').length;
        var numberOptionSelected = $('.tb-sku .tm-sale-prop ul .tb-selected').length;

        if (numberOption > numberOptionSelected) {
            alert('Bạn chưa chọn đầy đủ thuộc tính sản phẩm');
            return;
        }

        var propertySelected = []; // Thuộc tính của sp đã chọn
        $.each(properties_name, function (key, value) {
            if (selected_prop.includes(value[0])) {
                propertySelected.push(value[1]);
            }
        });
        var imageSelectedTmall = $('#J_ImgBooth').attr('src'); // Ảnh sp đã chọn
        var qtySelectedTmall = $('.mui-amount-input').val(); // Số lượng sp đã chọn
        var nameSelectedTmall = $('#J_DetailMeta').find('.tb-detail-hd h1').text(); // Tên sp đã chọn
        var priceSelectedTmall = 0; // Giá sp đã chọn

        if ($('#J_PromoPrice').find('.tm-price').length > 0) {
            priceSelectedTmall = $('#J_PromoPrice').find('.tm-price').text();
        } else if ($('#J_PromoPrice').find('.tb-wrTuan-num').length > 0) {
            priceSelectedTmall = $('#J_PromoPrice').find('.tb-wrTuan-num').text();
        } else {
            priceSelectedTmall = $('#J_StrPriceModBox').find('.tm-price').text();
        }
        priceSelectedTmall = priceSelectedTmall.replace('¥', '');

        var data = {};
        data.detail = detail;
        data.properties_name = properties_name;
        data.image = image;
        data.prop_image = prop_image;
        data.selected_prop = selected_prop;
        data.selected = {
            qty: qtySelectedTmall,
            product_image: imageSelectedTmall,
            price: priceSelectedTmall,
            product_name: nameSelectedTmall,
            product_link: window.location.href,
            property: propertySelected,
            shop_name: shop_name
        };

        sendDataToHaKhau(data);

        var start_dec = detail.indexOf('httpsDescUrl') + 15;
        var end_dec = detail.indexOf('"},"apiAddCart');
        var url_tmall_desc = 'https:' + detail.substring(start_dec, end_dec);

        function setTMallDesc(descTmall) {
            data.desc = descTmall;
        }

        getPromo();

        function getPromo() {
            var start = detail.indexOf('"initApi":"') + 11;
            var end = detail.indexOf('","initCspuExtraApi');
            var url_promo = 'https:' + detail.substring(start, end);

            function setPromo(promo) {
                data.promo = JSON.stringify(promo);
            }

            jQuery.ajax({
                url: url_promo,
                beforeSend: function () {

                },
                success: function (data) {
                    setPromo(data);
                    sendServerTmall();
                }
            });
        }

        function sendServerTmall(dataPromo) {
            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: ORDER_PROCESSING_ENDPOINT + '/parse-product/tmall',
                data: {
                    data: JSON.stringify(data)
                },
                beforeSend: function () {

                },
                success: function (data) {
                    if (r) {
                        window.open(data.url);
                    } else {
                        window.open(ORDER_PROCESSING_ENDPOINT + '/tai-khoan/danh-sach-don-hang?urlCrawl=' + encodeURIComponent(url));
                    }
                },
                complete: function () {
                    $('.ha-khau-loading').hide();
                }
            });
        }
    }

    function orderFrom1688() {
        var data = {};
        var shop_name = '';
        shop_name = getShopName1688();
        if (checkHasProperty()) {
            var price = $('p.price span.value').text();
            if (price == 0) {
                alert('Bạn chưa chọn đủ thuộc tính và số lượng');
                return;
            }
            var details = [];
            var image = $('a[trace="largepic"] img').attr('src');
            $('table.table-sku').find('tr').each(function (key, row) {
                $(this).each(function (key, tr) {
                    var row_name = ($(this).data('sku-config')).skuName;
                    var row_price = $(this).find('td.price').find('em.value').text();
                    var row_amount = ($(this).find('td.amount').find('.unit-detail-amount-control').find('input').val());
                    if (row_amount > 0) {
                        details.push({name: row_name, amount: row_amount, price: row_price});
                    }
                });
            });

            var propertyLeading = null;
            var listLeading = $('.list-leading');
            if (listLeading.length !== 0) {
                var property_key = $('.obj-leading').find('.obj-header').find('span.obj-title').text();
                var selectedLeading = listLeading.find('a.image.selected');
                if (selectedLeading.length !== 0) {
                    propertyLeading = property_key + ': ' + selectedLeading.attr('title');
                } else {
                    propertyLeading = property_key + ': ' + listLeading.find('.no-image.selected').attr('title');
                }
            }

            var link = window.location.href;
            var item_id = link.split('.html')[0];
            item_id = item_id.split('offer/')[1];

            var price_range = getPriceRange();

            var product_name = $('#mod-detail-title h1.d-title').text();
            var propertyType = $('.obj-sku').find('.obj-header').find('span.obj-title').text();

            data.selected = details.map(function (item) {
                var property = [];
                property.push(propertyType + ': ' + item.name);
                if (propertyLeading !== null) {
                    property.push(propertyLeading);
                }
                return {
                    qty: item.amount,
                    product_name: product_name,
                    product_image: image,
                    price_range: price_range,
                    product_link: window.location.href,
                    price: item.price,
                    item_id: item_id,
                    property: property,
                    site: 1688,
                    shop_name: shop_name
                };
            });
        } else {
            price_range = getPriceRange();
            link = window.location.href;
            item_id = link.split('.html')[0];
            item_id = item_id.split('offer/')[1];
            var product_name = $('#mod-detail-title h1.d-title').text();
            price = $('.price-now').text();
            var qty = $('.amount-input').val();
            var image = $('a[trace="largepic"] img').attr('src');

            if (price == '') {
                price = $('#mod-detail-price .price .value').first().text();
            }
            if (qty != 0) {
                data.selected = {
                    qty: qty,
                    product_name: product_name,
                    product_image: image,
                    product_link: window.location.href,
                    price_range: price_range,
                    price: price,
                    item_id: item_id,
                    property: [],
                    shop_name: shop_name
                }
            } else {
                alert('Bạn chưa chọn số lượng');
                return;
            }
        }

        sendDataToHaKhau(data);

        function checkHasProperty() {
            if ($('list-leading').length === 0 && $('.obj-sku').length === 0) {
                return false;
            }
            return true;
        }
    }

    function sendDataToHaKhau(data) {
        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: ORDER_PROCESSING_ENDPOINT + '/temps',
            data: {
                data: data
            },
            beforeSend: function () {
                $('.ha-khau-loading').show();
            },
            success: function (data) {
                window.open(data.url);
            },
            error: function () {
                alert('Có lỗi xảy ra, vui lòng thử lại sau!');
            },
            complete: function () {
                $('.ha-khau-loading').hide();
            }
        });
    }

    function getPriceRange() {
        var price_range_product = [];

        //Lay du lieu tren trang
        var params = getParamsOnPage();
        try {
            var priceRange = params.iDetailData.sku.priceRange;
            if (priceRange != undefined) {
                for (var k = 0; k < priceRange.length; k++) {
                    var item = {};
                    item.begin = priceRange[k][0];
                    item.end = k + 1 == priceRange.length ? "" : parseInt(priceRange[k + 1][0]) - 1;
                    item.price = priceRange[k][1];
                    price_range_product.push(item);
                }
            }
        } catch (e) {
            console.error(e.message);
        }

        if (price_range_product.length) {
            return price_range_product;
        }

        //Lay du lieu tren dom HTML
        try {
            var $dom = document.querySelectorAll('tr.price > td');
            for (var i = 0; i < $dom.length; i++) {
                var range = $dom[i].getAttribute('data-range');
                if (range) {
                    price_range_product.push(JSON.parse(range));
                }
            }
        } catch (e) {
            console.error(e.message);
        }

        return price_range_product;
    };

    function getParamsOnPage() {
        if (this.params_on_page) {
            return this.params_on_page;
        }
        var self = this;
        try {
            var scripts = document.querySelectorAll("script");
            for (var i = 0; i < scripts.length; i++) {
                var html = scripts[i].textContent;
                var res = html.search("iDetailConfig");
                if (res != -1) {
                    eval(html);
                    self.params_on_page = {
                        iDetailConfig: iDetailConfig,
                        iDetailData: iDetailData
                    };
                    break;
                }
            }
        } catch (e) {
            console.warn(e.message);
        }
        return this.params_on_page;
    };

    function getShopName1688() {
        var shop_name = '';
        try {
            var dom = document.getElementsByName("sellerId");
            if (dom.length) {
                shop_name = dom[0].value;
            }

            if (!shop_name) {
                dom = document.getElementsByClassName('contact-div');
                if (dom.length) {
                    shop_name = dom[0].getElementsByTagName('a')[0].innerHTML;
                }
            }

            if (!shop_name) {
                dom = document.querySelectorAll("meta[property='og:product:nick']")[0].getAttribute("content");
                dom = dom.split(';');
                dom = dom[0];
                dom = dom.split('=');
                shop_name = dom[1];
            }
            return shop_name;
        } catch (e) {
            return '';
        }
    };

    function getShopNameTaobao() {
        try {
            var shop_name = '';
            if (document.getElementsByClassName('tb-seller-name').length > 0) {
                shop_name = document.getElementsByClassName('tb-seller-name')[0].textContent;

                if (shop_name == '' || shop_name == null) {

                    var shop_card = document.getElementsByClassName('shop-card');
                    var data_nick = shop_card.length > 0 ? shop_card[0].getElementsByClassName('ww-light') : '';
                    shop_name = (data_nick.length > 0 ? data_nick[0].getAttribute('data-nick') : '');
                    if (shop_name == '') {
                        /* Find base info*/
                        if (document.getElementsByClassName('base-info').length > 0) {
                            for (var i = 0; i < document.getElementsByClassName('base-info').length; i++) {
                                if (document.getElementsByClassName('base-info')[i].getElementsByClassName('seller').length > 0) {
                                    if (document.getElementsByClassName('base-info')[i].getElementsByClassName('seller')[0].getElementsByClassName('J_WangWang').length > 0) {
                                        shop_name = document.getElementsByClassName('base-info')[i].getElementsByClassName('seller')[0].getElementsByClassName('J_WangWang')[0].getAttribute('data-nick');
                                        break;
                                    }
                                    if (document.getElementsByClassName('base-info')[i].getElementsByClassName('seller')[0].getElementsByClassName('ww-light').length > 0) {
                                        shop_name = document.getElementsByClassName('base-info')[i].getElementsByClassName('seller')[0].getElementsByClassName('ww-light')[0].getAttribute('data-nick');
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if ($('#J_tab_shopDetail').length > 0) {
                shop_name = $('#J_tab_shopDetail').find('span').first().data('nick');
            }
            shop_name = shop_name.trim();

            if (!shop_name) {
                shop_name = document.querySelectorAll(".tb-shop-name")[0].getElementsByTagName("h3")[0].getElementsByTagName("a")[0].getAttribute("title");
            }

            return shop_name;
        } catch (ex) {
            return "";
        }
    };
}
