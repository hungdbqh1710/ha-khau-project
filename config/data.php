<?php

return [
    'record'                 => 10,
    'number_decimals'        => 3,
    'order_status'           => [
        1 => 'Chưa gửi',
        2 => 'Đang xử lý',
        3 => 'Đã xác nhận',
        4 => 'Đã báo giá',
        5 => 'Đã đặt hàng',
        6 => 'Đã về kho Lào Cai',
        7 => 'Đang chuyển hàng',
        8 => 'Thành công',
        9 => 'Đã hủy'
    ],
    'limit_content'          => 60,
    'limit_title'            => 40,
    'default_paginate'       => 10,
    'mime_image'             => ['png', 'jpg', 'bmp', 'tiff', 'jpeg', 'PNG', 'JPG', 'BMP', 'TIFF', 'JPEG'],
    'is_active'              => [
        0 => 'Không hoạt động',
        1 => 'Hoạt động'
    ],
    'limit_content_in_admin' => 85,
    'list_sites'             => [
        0 => [
            'site_name'  => 'Tao Bao',
            'url_search' => 'https://s.taobao.com/search?_input_charset=utf-8&q='
        ],
        1 => [
            'site_name'  => '1688',
            'url_search' => 'https://s.1688.com/selloffer/offer_search.htm?keywords='
        ],
        2 => [
            'site_name'  => 'Tmall',
            'url_search' => 'https://list.tmall.com/search_product.htm?q='
        ]
    ],
    'transport_order_item_status'  => [
        1 => 'Chưa về kho',
        2 => 'Đã về kho',
        3 => 'Đang chuyển hàng',
        4 => 'Thành công',
        5 => 'Đã hủy',
    ],
    'ha_khau_address'        => '190 Nhạc Sơn - Kim Tân - TP Lào Cai',
    'prefix_order_number'    => '#',
    'min_deposit_default'    => 70,
    'string_limit_link_qty'  => 30,
    'filter_date_type'       => [
        1 => 'Theo khoảng thời gian',
        2 => 'Theo tháng'
    ],
    'slide_limit_title' => 25,
    'purchase_order_status'           => [
        2 => 'Đang xử lý',
        4 => 'Đã báo giá',
        5 => 'Đã đặt hàng',
        6 => 'Đã về kho Lào Cai',
        8 => 'Thành công',
        9 => 'Đã hủy'
    ],
    'transport_order_status'  => [
        2 => 'Đã xác nhận',
        3 => 'Đã về kho TQ',
        4 => 'Đã về kho LC',
        7 => 'Đang giao hàng VN',
        8 => 'Thành công',
        9 => 'Đã hủy'
    ],
    'purchase_order_items_status' => [
        1 => 'Đã nhận hàng',
        2 => 'Trả hàng'
    ],
    'receive_type'  => [
        0 => 'Nhận hàng tại Lào Cai',
        1 => 'Nhận hàng tại nhà'
    ],
    'order_type' => [
        1 => 'Mua hộ',
        2 => 'Vận chuyển'
    ],
    'shipping_type' => [
        'in-cn' => 'Đã về kho TQ',
        'out-cn' => 'Đã xuất kho TQ',
        'in-vn' => 'Đã về kho VN',
        'out-vn' => 'Đã xuất kho VN'
    ]
];
