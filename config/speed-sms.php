<?php

return [
    'url-get-verify-code'  => 'https://api.speedsms.vn/index.php/pin/create',
    'url-verify-code'      => 'https://api.speedsms.vn/index.php/pin/verify',
    'api-access-token'     => env('VERIFY_CODE_API_ACCESS_TOKEN'),
    'password'             => env('VERIFY_CODE_PASSWORD'),
    'app-id'               => env('VERIFY_CODE_APP_ID'),
    'pin-time-to-live'     => env('VERIFY_CODE_PIN_TIME_TO_LIVE'),
    'content'              => 'Ma xac nhan dang ky la: {pin_code}',
];